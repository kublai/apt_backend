Apptitude Backend System
===============================

This system uses PHP as development language and Yii2 for the framework, The advanced template has been customized to fit this project.

MySQL is used as main database system. (for security reasons a copy with real information is not available inside the repo)

This software has been developed by Bytebox Spain for Fundación Capital


DIRECTORY STRUCTURE

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
tests                    contains various tests for the advanced application
    codeception/         contains tests developed with Codeception PHP Testing Framework
```

Installation
=============
This system is working with bower, composer and Yii2. All of them versions of 2015. In order to build a dev environment, clone the repo, scp composer.lock to the local machine and execute
```
composer install 
```

