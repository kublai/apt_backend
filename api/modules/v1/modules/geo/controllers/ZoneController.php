<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\modules\v1\modules\geo\controllers;

use api\rest\ActiveController;

/**
 * Zone Controller API
 *

 */
class ZoneController extends ActiveController {

    public $modelClass = 'api\models\geo\Zone';

}
