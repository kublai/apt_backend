<?php

namespace api\modules\v1\modules\geo;

class Module extends \yii\base\Module {

    public $controllerNamespace = 'api\modules\v1\modules\geo\controllers';

    public function init() {
        parent::init();

        // custom initialization code goes here
    }

}
