<?php

namespace api\modules\v1\modules\lms;

class Module extends \yii\base\Module {

    public $controllerNamespace = 'api\modules\v1\modules\lms\controllers';

    public function init() {
        parent::init();

        // custom initialization code goes here
    }

}
