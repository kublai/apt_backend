<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\modules\v1\modules\lms\controllers;

use api\rest\ActiveController;

/**
 * Campaign Controller API
 *

 */
class CampaignController extends ActiveController {

    public $modelClass = 'api\models\lms\Campaign';

}
