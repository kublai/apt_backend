<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\modules\v1\actions\me;

use Yii;

/**
 * 

 */
class SynchronizeGetAction extends Action {

    /**
     * @inheritdoc
     */
    public function run() {
        parent::run();
        $campaign = $this->getCurrentCampaign();
        if ($campaign === null) {
            Yii::$app->response->setStatusCode(204);
            return;
        }

        return ['data' => $this->user->getCampaignData($campaign)];
    }

}
