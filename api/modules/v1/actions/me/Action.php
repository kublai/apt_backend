<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\modules\v1\actions\me;

use Yii;
use yii\base\Action as BaseAction;
use yii\web\ForbiddenHttpException;
use common\models\user\Role;
use api\models\lms\Campaign;

/**
 * 

 */
class Action extends BaseAction {

    /**
     *
     * @var \api\models\user\User 
     */
    protected $user;

    /**
     * Checks whether the user can execute the action and executes it if 
     * affirmative.
     */
    public function run() {
        $this->loadUser();
        $this->checkAccess();
    }

    /**
     * Loads the current authorized user into the property.
     * 
     * @throws ForbiddenHttpException
     */
    public function loadUser() {
        $this->user = Yii::$app->user->identity;
        if ($this->user === null) {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * Checks if the current user has the role needed to execute the action.
     * 
     * @throws ForbiddenHttpException
     */
    public function checkAccess() {
        if ($this->user->role >= Role::STUDENT) {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * Returns the current campaign in which the user is instructor.
     * 
     * @return Campaign
     */
    public function getCurrentCampaign() {
        return Campaign::findCurrent()
                        ->innerJoin('lms_instructor', [
                            'and',
                            'lms_instructor.campaign_id = lms_campaign.id',
                            ['lms_instructor.user_id' => $this->user->id]
                        ])
                        ->one();
    }

}
