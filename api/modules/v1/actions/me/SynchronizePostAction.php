<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\modules\v1\actions\me;

use Yii;
use api\modules\v1\models\Synchronizer;
use yii\web\BadRequestHttpException;
use yii\helpers\Json;

/**
 * 

 */
class SynchronizePostAction extends Action {

    /**
     * @inheritdoc
     */
    public function run() {
        parent::run();
        $campaign = $this->getCurrentCampaign();
        if ($campaign === null) {
            Yii::$app->response->setStatusCode(204);
            return;
        }
        if (($package = Json::decode(Yii::$app->request->post('package', null))) === null) {
            throw new BadRequestHttpException('Missing param "package"');
        }
        if (isset($package['data']) === false) {
            throw new BadRequestHttpException('Missing index "data"');
        }
        $sync = new Synchronizer([
            'user' => $this->user,
            'campaign' => $campaign,
            'data' => $package['data']
        ]);
        return [
            'result' => $sync->run()
        ];
    }

}
