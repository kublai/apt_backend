<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\modules\v1\actions\me;

/**
 * 

 */
class IndexAction extends Action {

    /**
     * @inheritdoc
     */
    public function run() {
        parent::run();
        return $this->user;
    }

}
