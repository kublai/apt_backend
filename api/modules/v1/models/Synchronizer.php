<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\modules\v1\models;

use Yii;
use custom\base\Model;
use common\models\user\User;
use yii\base\InvalidConfigException,
    yii\base\InvalidParamException;
use common\models\user\Role;
use common\models\lms\Campaign,
    common\models\lms\StudentSession,
    common\models\lms\Student,
    common\models\lms\Module;
use custom\db\exceptions\SaveException;
use yii\base\Exception;
use common\models\lms\InstructorSynclog;
use api\modules\v1\exceptions\NewStudentException;

/**
 * Class to handle the synchronization process with the device.
 * 

 */
class Synchronizer extends Model {

    /**
     *
     * @var array
     */
    public $data;

    /**
     *
     * @var Campaign
     */
    protected $campaign;

    /**
     *
     * @var User
     */
    protected $user;

    /**
     *
     * @var InstructorSynclog
     */
    protected $log;

    /**
     *
     * @var array
     */
    protected static $dataMapping = [
        'password_hash',
        'username',
        'name',
        'last_name',
        'phone',
        'email'
    ];

    /**
     * 
     * @throws InvalidConfigException
     */
    public function init() {
        parent::init();

        if ($this->user === null) {
            throw new InvalidConfigException('User must be provided');
        }

        if ($this->campaign === null) {
            throw new InvalidConfigException('Campaign must be provided');
        }
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['data', 'safe']
        ];
    }

    /**
     * 
     * @return boolean
     */
    public function run() {
        $this->createLog();
        $trans = Yii::$app->db->beginTransaction();
        try {
            $this->log->status = InstructorSynclog::STATUS_SUCCESS;
            $this->process();
            $trans->commit();
            return true;
        } catch (\Exception $ex) {
            $trans->rollback();
            $this->log->addLine($ex->getMessage());
            $this->log->log = $ex->getTraceAsString();
            $this->log->status = InstructorSynclog::STATUS_FAILED;
            return false;
        } finally {
            $this->log->save();
        }
    }

    /**
     * Creates the synchonization log object.
     */
    protected function createLog() {
        $this->log = new InstructorSynclog([
            'instructor_id' => $this->user->id
            , 'campaign_id' => $this->campaign->id
            , 'created_at' => time()
            , 'status' => InstructorSynclog::STATUS_STARTED
        ]);
        $this->log->save();
    }

    /**
     * 
     * @param User $user
     * @throws InvalidParamException
     */
    public function setUser(User $user) {
        if ($user->role >= Role::STUDENT) {
            throw new InvalidParamException('Invalid role to synchronize');
        }
        $this->user = $user;
    }

    /**
     * 
     * @return User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * 
     * @param Campaign $campaign
     */
    public function setCampaign(Campaign $campaign) {
        $this->campaign = $campaign;
    }

    /**
     * 
     * @return Campaign
     */
    public function getCampaign() {
        return $this->campaign;
    }

    /**
     * 
     * @param User $user
     * @param array $data
     */
    protected function syncUserData(User $user, array &$data) {
        foreach (static::$dataMapping as $attribute) {
            if (isset($data[$attribute])) {
                $user->$attribute = $data[$attribute];
            }
        }
    }

    /**
     * 
     * @throws SaveException
     */
    protected function process() {
        $this->syncUserData($this->user, $this->data);
        if ($this->user->save() === false) {
            throw new SaveException($this->user);
        }
        if (isset($this->data['students'])) {
            foreach ($this->data['students'] as $student) {
                $this->processStudent($student);
            }
        }
    }

    /**
     * Process students.
     * 
     * @param array $data
     * @throws Exception
     */
    protected function processStudent(array $data) {
        try {
            $this->doProcessStudent($data);
        } catch (InvalidParamException $ex) {
            $this->log($ex->getMessage(), InstructorSynclog::STATUS_WARNING);
        } catch (NewStudentException $ex) {
            $this->log($ex->getMessage(), InstructorSynclog::STATUS_WARNING);
        }
    }

    /**
     * Logs a message into the synclog.
     * 
     * @param string $message
     * @param int $status
     */
    protected function log($message, $status = InstructorSynclog::STATUS_WARNING) {
        $this->log->status = $status;
        $this->log->addLine($message);
    }

    /**
     * 
     * @param array $data
     * @throws InvalidConfigException
     * @throws InvalidParamException
     * @throws NewStudentException
     * @throws Exception
     */
    protected function doProcessStudent(array $data) {
        if (isset($data['slug']) === false) {
            throw new InvalidConfigException('Unable to find a user because its slug param is missing.');
        }
        $user = User::find()->where(['slug' => $data['slug']])->one();
        if ($user === null) {
            throw new InvalidParamException("User with the slug [{$data['slug']}] does not exist.");
        }

        try {
            $this->syncUserData($user, $data);
            if ($user->save() === false) {
                throw new SaveException($user);
            }
            $this->createStudent($user, isset($data['new']) && $data['new']);
            if (isset($data['sessions'])) {
                foreach ($data['sessions'] as $session) {
                    $this->processSession($user, $session);
                }
            }
        } catch (NewStudentException $e) {
            throw $e;
        } catch (\Exception $ex) {
            throw new Exception("Synchronization fail for student with id [{$user->id}]. {$ex->getMessage()}");
        }
    }

    /**
     * 
     * @param User $user
     * @throws SaveException
     * @throws NewStudentException
     */
    protected function createStudent(User $user, $allowNew = true) {
        if (Student::find()->where([
                    'user_id' => $user->id,
                    'campaign_id' => $this->campaign->id
                ])->exists() === false) {
            if ($allowNew) {
                $student = new Student([
                    'user_id' => $user->id,
                    'campaign_id' => $this->campaign->id,
                    'instructor_id' => $this->user->id
                ]);
                if ($student->save() === false) {
                    throw new SaveException($student);
                }
            } else {
                throw new NewStudentException("The student with id [{$user->id}] has been deleted from the campaign and therefore can not be synchronized");
            }
        }
    }

    /**
     * 
     * @param User $user
     * @param array $data
     * @throws InvalidConfigException
     * @throws SaveException
     */
    protected function processSession(User $user, array $data) {
        if (isset($data['id']) === false) {
            throw new InvalidConfigException("Session data must have an id param.");
        }
        if (StudentSession::find()->where(['id' => $data['id']])->exists()) {
            return;
        }

        $session = new StudentSession();
        $session->attributes = $data;
        $session->module_id = Module::findOne(['code' => $data['module_code']])->id;
        $session->campaign_id = $this->campaign->id;
        $session->student_id = $user->id;
        if ($session->save() === false) {
            throw new SaveException($session);
        }
    }

}
