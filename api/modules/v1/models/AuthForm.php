<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\modules\v1\models;

use Yii;
use custom\base\Model;
use common\models\user\User;
use common\models\user\Auth;
use common\models\user\Role;

class AuthForm extends Model {

    public $username;
    public $password;
    public $client_id;
    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // username and password are both required
            [['username', 'password', 'client_id'], 'required'],
            // password is validated by validatePassword()
            ['client_id', 'validateClientId'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the client id.
     * 
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateClientId($attribute, $params) {
         if ( !in_array($this->$attribute,Yii::$app->params['client_ids']) ){
                $this->addError($attribute, 'Invalid client id');
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params) {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login() {
        if ($this->validate()) {
            return Auth::authorize($this->getUser());
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser() {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

}
