<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\modules\v1\controllers;

use api\rest\Controller;

/**
 * User Controller API
 *

 */
class MeController extends Controller {

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'index' => [
                'class' => 'api\modules\v1\actions\me\IndexAction'
            ],
            'campaign' => [
                'class' => 'api\modules\v1\actions\me\CampaignAction'
            ],
            'synchronize-new-students' => [
                'class' => 'api\modules\v1\actions\me\SynchronizeGetAction'
            ],
            'synchronize-get' => [
                'class' => 'api\modules\v1\actions\me\SynchronizeGetAction'
            ],
            'synchronize-post' => [
                'class' => 'api\modules\v1\actions\me\SynchronizePostAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    protected function verbs() {
        return [
            'campaign' => ['GET', 'HEAD'],
            'synchronize-get' => ['GET', 'HEAD'],
            'syncrhonize-post' => ['POST']
        ];
    }

}
