<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\modules\v1\controllers;

use api\rest\ActiveController;

/**
 * User Controller API
 *

 */
class UserController extends ActiveController {

    public $modelClass = 'api\models\user\User';

}
