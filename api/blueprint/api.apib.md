FORMAT: 1A
HOST: https://apigrad15.bytebox.es/

# Aptitude

API for the Aptitude application.

# Aptitude API Root [/]

This resource does not have any attributes. Instead it offers the initial API affordances in the form of the links in the JSON body.

It is recommend to follow the “url” link values or Link headers to get to resources instead of constructing your own URLs to keep your client decoupled from implementation details.

## Retrieve the Entry Point [GET]

+ Response 200 (application/json)

        {
            "questions_url": "/questions"
        }

## Group v1

Resources related to version 1 of the API.

## Authorization [/v1/auth]

The end point to perform the authorization.

### Perform the authorization [POST]

It takes the following post params to perform the user authorization to the API.

- username (string) - The username.
- password (string) - The password of the user.
- client_id (string) - An allowed client id to access the API.

+ Response 200 (application/json)

                {
                    "access_token": "YOUR_ACCESS_TOKEN",
                    "expires_at": 1458151040
                }

+ Response 401 (application/json)

                {
                    "name": "Unauthorized",
                    "message": "Authentication error ocurred.",
                    "code": 0,
                    "status": 401,
                    "type": "yii\\web\\UnauthorizedHttpException"
                }

## Group me

Resources related to the information that the authorized can handle.

## My data [/v1/me]

Authorized user information.

### Get my data [GET]

Gets the current authorized user data. A header with the authorized access 
token must be included in the request.

+ Request 
    
    + Headers

            Authorization: Bearer YOUR_ACCESS_TOKEN

+ Response 200 (application/json)
    
            {
                "slug": "6ec29cb756775af41bcde0f0caa33e53",
                "username": "blackbox",
                "password_hash": "16f1816066f231bbe39be9495ece0b3719e166d6",
                "name": "Administrador",
                "last_name": "Pruebas",
                "phone": "",
                "email": "",
                "role": 1
            }

## My Campaign [/v1/me/campaign]

Campaign data where the authorized user is assigned as instructor.

### Get campaign information [GET]

Gets the current campaign information where the authorized user is asigned, 
including the modules ordered as they where sorted in the backend application.

A header with the authorized access token must be included in the request.

+ Request 
    
    + Headers

            Authorization: Bearer YOUR_ACCESS_TOKEN

+ Response 200 (application/json)
    
            {
                "name": "Sembrando Oportunidades",
                "begin_at": 1420113600,
                "end_at": 1483228740,
                "modules": [
                    {
                        "id": 1,
                        "code": "00",
                        "name": "Introducción a la capacitación y como usar la tableta",
                        "description": null
                    },
                    {
                        "id": 2,
                        "code": "01",
                        "name": "Soy un ser único, especial y capaz",
                        "description": null
                    }
            }

## Synchronization [/v1/me/synchronize]

A synchronization data object has the following data structure:

            {
                "data": {
                    "slug": "2222eeee2222eeee2222eeee2222eeee",
                    "username": "blackbox",
                    "password_hash": "11111fffff11111fffff11111fffff11111fffff",
                    "name": "Administrador",
                    "last_name": "Pruebas",
                    "phone": "",
                    "email": "",
                    "role": 1,
                    "students": [
                        {
                            "slug": "2e2e2e2e2e2e2e2e2e2e2e2e2e2e2e2e",
                            "username": "studenttest",
                            "name": "Student",
                            "last_name": "Test",
                            "role": 16,
                            "sessions": [
                                {
                                    "id": "3333dddd3333dddd3333dddd3333dddd",
                                    "module_code": "00",
                                    "begin_at": 1454528417,
                                    "end_at": 1454529134,
                                    "latitude": 40.561134338379,
                                    "longitude": -4.0157070159912,
                                    "completed": 1,
                                    "utility": 9,
                                    "difficulty": 7
                                },
                                {
                                    "id": "dddd3333dddd3333dddd3333dddd3333",
                                    "module_code": "01",
                                    "begin_at": 1454529255,
                                    "end_at": 1454529306,
                                    "latitude": 40.56111907959,
                                    "longitude": -4.0157432556152,
                                    "completed": 0,
                                    "utility": 0,
                                    "difficulty": 0
                                }
                            ]
                        }
                }
            }

Where the first block inside data is the current authorized user information, the 
"students" param is an array of student json objects. The student object has a 
"sessions" param that is the sessions done by the student.

### Retrieve the data from the server [GET]

Gets the synchronization data from the server. This action should be called to 
get all the synchronization information stored in the server for the authorized 
user.

A header with the authorized access token must be included in the request.

+ Request 
    
    + Headers

            Authorization: Bearer YOUR_ACCESS_TOKEN

+ Response 200 (application/json)
    
            {
                "data": {
                    "slug": "2222eeee2222eeee2222eeee2222eeee",
                    "username": "blackbox",
                    "password_hash": "11111fffff11111fffff11111fffff11111fffff",
                    "name": "Administrador",
                    "last_name": "Pruebas",
                    "phone": "",
                    "email": "",
                    "role": 1,
                    "students": [
                        {
                            "slug": "2e2e2e2e2e2e2e2e2e2e2e2e2e2e2e2e",
                            "username": "studenttest",
                            "name": "Student",
                            "last_name": "Test",
                            "role": 16,
                            "sessions": [
                                {
                                    "id": "3333dddd3333dddd3333dddd3333dddd",
                                    "module_code": "00",
                                    "begin_at": 1454528417,
                                    "end_at": 1454529134,
                                    "latitude": 40.561134338379,
                                    "longitude": -4.0157070159912,
                                    "completed": 1,
                                    "utility": 9,
                                    "difficulty": 7
                                },
                                {
                                    "id": "dddd3333dddd3333dddd3333dddd3333",
                                    "module_code": "01",
                                    "begin_at": 1454529255,
                                    "end_at": 1454529306,
                                    "latitude": 40.56111907959,
                                    "longitude": -4.0157432556152,
                                    "completed": 0,
                                    "utility": 0,
                                    "difficulty": 0
                                }
                            ]
                        }
                    ]
                }
            }

### Push data to the server [POST]

Push a synchronization data object to the server. The data pushed to the server 
that existed before will be replaced with the new data. 

The expected POST parameters are:

+ package: (string|required) - A json synchronization data object.

+ Request 
    
    + Headers

            Authorization: Bearer YOUR_ACCESS_TOKEN
  
+ Response 200 (application/json)

                {
                    "result": true
                }