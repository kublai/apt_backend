<?php

namespace api\controllers;

use yii\rest\Controller;
use api\models\Migration;
use Yii;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    protected function verbs() {
        return [
            'index' => ['GET', 'HEAD'],
            'upgrade' => ['GET', 'HEAD']
        ];
    }

    public function actionIndex() {
        return;
    }

    public function actionUpgrade() {
        $model = new Migration();
        $model->load(Yii::$app->request->get(), '');
        return $model->get();
    }

}
