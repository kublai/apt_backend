<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\models\lms;

use common\models\lms\Module as BaseModule;

/**
 * 

 */
class Module extends BaseModule {

    /**
     * @inheritdoc
     */
    public function fields() {
        return [
            'id' => 'id',
            'code' => 'code',
            'name' => 'name',
            'description' => 'description'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getCampaigns() {
        $query = parent::getCampaigns();
        //$query->modelClass = Campaign::className();
        return $query;
    }
    


}
