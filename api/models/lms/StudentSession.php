<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\models\lms;

use common\models\lms\StudentSession as BaseStudentSession;

class StudentSession extends BaseStudentSession {

    public function fields() {
        $moduleCode = function($model) {
            return $model->module->code;
        };
        return [
            'id' => 'id',
            'module_code' => $moduleCode,
            'begin_at' => 'begin_at',
            'end_at' => 'end_at',
            'latitude' => 'latitude',
            'longitude' => 'longitude',
            'completed' => 'completed',
            'utility' => 'utility',
            'difficulty' => 'difficulty'
        ];
    }

}
