<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\models\lms;

use common\models\lms\Campaign as BaseCampaign;
use yii\rest\Serializer;

/**
 * 

 */
class Campaign extends BaseCampaign {

    public function fields() {
        return [
            'name' => 'name',
            'begin_at' => 'begin_at',
            'end_at' => 'end_at',
            'modules' => 'modules',
            'active_modules' => 'activeModules'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getModules() {
        $query = parent::getModules();
        $query->modelClass = Module::className();
        return $query;
    }

    /**
     * Modules tagged as active in lms_campaign_module
     * author: Kublai Gómez
     * date: 28/08/2020
     * 
     * @return yii\db\ActiveQuery
     */
    public function getActiveModules(){
        $query = parent::getActiveModules();
        $query->modelClass = Module::className();
        return $query;
    }
}
