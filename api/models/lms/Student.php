<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\models\lms;

use common\models\lms\Student as BaseStudent;

class Student extends BaseStudent {

    /**
     * @inheritdoc
     */
    public function fields() {
        return [
            'slug' => function($model) {
                return $model->user->slug;
            },
            'username' => function($model) {
                return $model->user->username;
            },
            'name' => function($model) {
                return $model->user->name;
            },
            'last_name' => function($model) {
                return $model->user->last_name;
            },
            'role' => function($model) {
                return $model->user->role;
            },
            'sessions' => 'sessions'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getSessions() {
        $q = parent::getSessions();
        $q->modelClass = StudentSession::className();
        return $q;
    }

}
