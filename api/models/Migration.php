<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\models;

use Yii;
use custom\base\Model;
use yii\base\InvalidParamException;

/**
 * 

 */
class Migration extends Model {

    const MIGRATIONS_PATH = '@api/migrations';

    /**
     *
     * @var string
     */
    public $last;

    /**
     * @inheritdoc 
     */
    public function rules() {
        return [
            ['last', 'string']
        ];
    }

    /**
     * Return an array of migrations for the application.
     * 
     * @return array
     * @throws InvalidParamException
     */
    public function get() {
        if ($this->validate() === false) {
            throw new InvalidParamException();
        }

        $dir = Yii::getAlias(self::MIGRATIONS_PATH);
        $init = empty($this->last) ? true : false;
        $migrations = [];
        $handle = opendir($dir);
        while (($file = readdir($handle)) !== false) {
            if (preg_match('/^[0-9]{8}_[0-9]{6}/', $file)) {
                if ($init === false) {
                    if ($this->getFileName($file) === $this->last) {
                        $init = true;
                    }
                } else {
                    $migrations[] = require ($dir . DIRECTORY_SEPARATOR . $file);
                }
            }
        }
        closedir($handle);
        return $init ? [
            'found' => true,
            'migrations' => $migrations
                ] : [
            'found' => false,
            'migrations' => []
        ];
    }

    /**
     * Returns the file name without the extension.
     * 
     * @param string $name
     * @return string
     */
    protected function getFileName($name) {
        return str_replace('.php', '', $name);
    }

}
