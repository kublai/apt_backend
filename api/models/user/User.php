<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\models\user;

use common\models\user\User as BaseUser;
use common\models\user\Auth;
use common\models\geo\Location;
use api\models\lms\Student,
    common\models\lms\Campaign;

/**
 * 

 */
class User extends BaseUser {

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        $auth = Auth::find()->where(['and', ['access_token' => $token], ['>=', 'expires_at', time()]])->one();
        if ($auth === null) {
            return null;
        }
        $q = $auth->getUser();
        $q->modelClass = static::className();
        return $q->one();
    }

    /**
     * @inheritdoc
     */
    public function fields() {
        return [
            'slug' => 'slug',
            'username' => 'username',
            'password_hash' => 'password_hash',
            'name' => 'name',
            'last_name' => 'last_name',
            'phone' => 'phone',
            'email' => 'email',
            'role' => 'role'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getLocation() {
        $query = parent::getLocation();
        $query->modelClass = Location::className();
        return $query;
    }

    /**
     * 
     * @param Campaign $campaign
     * @return array
     */
    public function getCampaignData(Campaign $campaign) {
        return [
            'slug' => $this->slug,
            'username' => $this->username,
            'password_hash' => $this->password_hash,
            'name' => $this->name,
            'last_name' => $this->last_name,
            'phone' => $this->phone,
            'email' => $this->email,
            'role' => $this->role,
            'students' => $this->getCampaignStudents($campaign)->all()
        ];
    }

    /**
     * @inheritdoc
     */
    public function getStudents() {
        $q = parent::getStudents();
        $q->modelClass = Student::className();
        return $q;
    }

}
