<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\models\geo;

use common\models\geo\Place as BasePlace;

/**
 * 

 */
class Place extends BasePlace {

    /**
     * @inheritdoc
     */
    public function fields() {
        return [
            'id' => 'id',
            'name' => 'name',
            'region_id' => 'region_id'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getLocations() {
        $q = parent::getLocations();
        $q->modelClass = Location::className();
        return $q;
    }

    /**
     * @inheritdoc
     */
    public function getRegion() {
        $q = parent::getRegion();
        $q->modelClass = Region::className();
        return $q;
    }

}
