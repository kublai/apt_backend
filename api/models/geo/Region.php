<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\models\geo;

use common\models\geo\Region as BaseRegion;

/**
 * 

 */
class Region extends BaseRegion {

        /**
     * @inheritdoc
     */
    public function fields() {
        return [
            'id' => 'id',
            'name' => 'name',
            'zone_id' => 'zone_id'
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function getPlaces() {
        $q = parent::getPlaces();
        $q->modelClass = Place::className();
        return $q;
    }

    /**
     * @inheritdoc
     */
    public function getLocations() {
        $q = parent::getLocations();
        $q->modelClass = Location::className();
        return $q;
    }

    /**
     * @inheritdoc
     */
    public function getZone() {
        $q = parent::getZone();
        $q->modelClass = Zone::className();
        return $q;
    }

}
