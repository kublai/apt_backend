<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\models\geo;

use common\models\geo\Location as BaseLocation;
use api\models\user\User;

/**
 * 

 */
class Location extends BaseLocation {

    /**
     * @inheritdoc
     */
    public function getUser() {
        $query = parent::getUser();
        $query->modelClass = User::className();
        return $query;
    }

}
