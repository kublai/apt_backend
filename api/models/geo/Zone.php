<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\models\geo;

use common\models\geo\Zone as BaseZone;

/**
 * 

 */
class Zone extends BaseZone {

        /**
     * @inheritdoc
     */
    public function fields() {
        return [
            'id' => 'id',
            'name' => 'name',
            'country_id' => 'country_id'
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function getRegions() {
        $q = parent::getRegions();
        $q->modelClass = Region::className();
        return $q;
    }

    /**
     * @inheritdoc
     */
    public function getLocations() {
        $q = parent::getLocations();
        $q->modelClass = Location::className();
        return $q;
    }

    /**
     * @inheritdoc
     */
    public function getCountry() {
        $q = parent::getCountry();
        $q->modelClass = Country::className();
        return $q;
    }

}
