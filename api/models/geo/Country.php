<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\models\geo;

use common\models\geo\Country as BaseCountry;

/**
 * 

 */
class Country extends BaseCountry {

    /**
     * @inheritdoc
     */
    public function fields() {
        return [
            'id' => 'id',
            'name' => 'name'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getZones() {
        $q = parent::getZones();
        $q->modelClass = Zone::className();
        return $q;
    }

    /**
     * @inheritdoc
     */
    public function getLocations() {
        $q = parent::getLocations();
        $q->modelClass = Location::className();
        return $q;
    }

}
