<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
return [
    'code' => '20150915_175000_database_creation',
    'type' => 'SQL',
    'data' => 'CREATE TABLE "migration" ('
    . '`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,'
    . '`code` TEXT NOT NULL UNIQUE,'
    . '`type` TEXT NOT NULL,'
    . '`data` TEXT);'
    . 'CREATE TABLE "user" ('
    . '`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,'
    . '`slug` TEXT NOT NULL UNIQUE,'
    . '`username` TEXT NOT NULL,'
    . '`password_hash` TEXT,'
    . '`role` INTEGER,'
    . '`name` TEXT,'
    . '`last_name` TEXT,'
    . '`email` TEXT,'
    . '`phone` TEXT,'
    . '`location_id` INTEGER);'
    . 'CREATE TABLE "lms_module" ('
    . '`id` INTEGER NOT NULL UNIQUE,'
    . '`order` INTEGER NOT NULL UNIQUE,'
    . '`code` TEXT NOT NULL UNIQUE,'
    . '`name` TEXT NOT NULL,'
    . '`description` TEXT,'
    . '`active` INTEGER NOT NULL DEFAULT 0,'
    . 'PRIMARY KEY(id));'
    . 'CREATE TABLE "lms_student_session" ('
    . '`id` TEXT NOT NULL,'
    . '`student_id` INTEGER NOT NULL,'
    . '`module_id` INTEGER NOT NULL,'
    . '`latitude` REAL,'
    . '`longitude` REAL,'
    . '`begin_at` INTEGER NOT NULL,'
    . '`end_at` INTEGER,'
    . '`utility` INTEGER,'
    . '`difficulty` INTEGER,'
    . '`completed` INTEGER NOT NULL DEFAULT 0,'
    . 'PRIMARY KEY(id));'
];
