<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace api\rest;

use yii\rest\Controller as BaseController;
use yii\helpers\ArrayHelper;
use yii\filters\auth\HttpBearerAuth,
    yii\filters\auth\QueryParamAuth,
    yii\filters\auth\CompositeAuth;

/**
 * 

 */
class Controller extends BaseController {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return ArrayHelper::merge(parent::behaviors(), [
                    'authenticator' => [
                        'class' => CompositeAuth::className(),
                        'authMethods' => [
                            HttpBearerAuth::className(),
                            QueryParamAuth::className()
                        ],
                    ]
        ]);
    }

}
