<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
return [
    '' => 'site/index',
    'upgrade' => 'site/upgrade',
    [
        'class' => 'yii\rest\UrlRule',
        'controller' => ['v1/auth'],
        'pluralize' => false
    ],
    [
        'class' => 'yii\rest\UrlRule',
        'controller' => [
            'v1/user',
            'v1/geo/country',
            'v1/geo/zone',
            'v1/geo/region',
            'v1/geo/place',
            'v1/lms/campaign',
            'v1/lms/module'
        ]
    ],
    [
        'class' => 'yii\rest\UrlRule',
        'controller' => ['v1/me'],
        'pluralize' => false,
        'patterns' => [
            'GET ' => 'index',
            'POST synchronize' => 'synchronize-post',
            'GET,HEAD synchronize' => 'synchronize-get',
            'GET,HEAD campaign' => 'campaign',
        ],
    ],
];
