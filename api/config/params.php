<?php

return [
    'adminEmail' => 'info@bytebox.es',
    'access_token_maxlifetime' => 3600,
    'client_ids' => [ 
        '81f9201$_801f2cf7b*cd2b3a10d$_0f853f78-9', // generic ID, created first for Paraguay (2015)
        'lksdkk3$ksd*foui390812396874$23857-72cd' // ID for PxMF Colombia (17-08-2016) 
        ]
];
