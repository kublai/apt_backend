<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace console\controllers;

use api\models\lms\Campaign;
use custom\console\Controller;
use Exception;
use phpDocumentor\Reflection\Types\False_;
use Yii;
use console\models\user\PasswordChange;
use common\models\user\User,
    common\models\user\Role,
    common\models\lms\Instructor,
    common\models\user\UserStatus;
use yii\helpers\Console;

/**
 * Controller for commands related with the user model.
 * 

 */
class UserController extends Controller {

    /**
     * Command to change the password of the user with the provided id.
     * 
     * @param integer $id
     */
    public function actionChangePass($id) {
        $model = new PasswordChange();
        if ($model->loadUser($id) === false) {
            return 'User with id[' . $id . '] doesn\'t exist';
        }
        $this->prompPasswords($model);
        if ($model->save() === false) {
            echo Yii::t('user_creation', 'A problem ocurred while changing the user password');
        } else {
            echo Yii::t('user_creation', 'User password changed successfully');
        }
        echo PHP_EOL;
    }

    /**
     * Command to change the password of all the users in the system. Useful for
     * the development environment.
     */
    public function actionChangeAllPasswords() {
        $model = new PasswordChange();
        $this->prompPasswords($model);

        if ($model->saveMassive()) {
            echo Yii::t('user_creation', 'Users passwords changed successfully');
        } else {
            echo Yii::t('user_creation', 'A problem ocurred while changing passwords');
        }
        echo PHP_EOL;
    }

    /**
     * Prompts the user for passwords.
     * 
     * @param PasswordChange $model
     */
    protected function prompPasswords(PasswordChange $model) {
        while (true) {
            $model->password = $this->prompt(Yii::t('user_creation', 'Enter a password: '), [
                'required' => true,
            ]);
            $model->password_check = $this->prompt(Yii::t('user_creation', 'Retype the password: '), [
                'required' => true,
            ]);

            if ($model->validate(['password']) === false) {
                echo $model->getFirstError('password') . PHP_EOL;
                continue;
            }
            if ($model->validate(['password_check']) === false) {
                echo $model->getFirstError('password_check') . PHP_EOL;
                continue;
            }
            break;
        }
    }

    /**
     * Creates a default user with admin access
     */
    public function actionCreateAdmin() {
        $user = new User();
        $user->username = 'blackbox';
        $user->role = Role::SUPER_ADMIN;
        $user->setPassword('a12345');
        $user->generateAuthKey();

        if ($user->save() === false) {
            throw new SaveException($user);
        }
    }
    

    /**
     * Change the state of a list of users to active or inactive for an Instructor in a Campaign $filepath $campaignId $activeStatus(true|false)
     * @param $filePath file with the list of usernames to process
     * @param $campaignId CampaignId od the users in the list
     * @param $activeStatus true/false for active field
     */
    public function actionSetUserState($filePath, $campaignId, $activeStatus){
        //set the format of active status from string to boolean
        $activeStatus = $activeStatus=="true"?true:false;

        $integrator = new UserStatus(['path' => $filePath, 'campaignId'=>$campaignId, 'state'=>$activeStatus]);
        if ($integrator->process() === false) {
            foreach ($integrator->getErrors() as $errors) {
                foreach ($errors as $error) {
                    $this->output($error, Console::FG_RED);
                }
            }
            $this->output(Yii::t('common/user/status','Process end with errors. No changes has been done.'), Console::FG_PURPLE);
        } else {
            $this->output(Yii::t('common/user/status', 'The file has been processed successfully'), Console::FG_GREEN);
        }
    }
}

