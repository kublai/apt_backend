<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace console\controllers;

use Yii;
use custom\console\Controller;
use common\models\lms\InstructorIntegration,
    common\models\lms\MultiInstructorIntegration,
    common\models\lms\Campaign,
    common\models\lms\Instructor,
    \common\models\lms\DeleteStudentsCampaign,
    \common\models\lms\ChangeStudentFacilitator;

use common\models\geo\CountryIntegration;
use yii\helpers\Console;
use yii\base\Exception;

/**
 * Controller for commands related with the lms models.
 * 

 */
class LmsController extends Controller {

    /**
     * Clears all the progress of the campaign with the provided id.
     * 
     * @param int $id
     */
    public function actionClearCampaign($id) {
        $campaign = Campaign::findOne($id);
        if ($campaign === null) {
            $this->output("Campaign with id [$id] Not Found", Console::FG_RED);
        } else {
            if (strtolower($this->prompt('All of the campaign student progress will be deleted. Are you sure? [y/n]:', [
                                'required' => true
                                , 'pattern' => '/y|n/i'
                    ])) === 'y') {
                $campaign->clearProgress();
                $this->output("Progress cleared for campaign [$id]", Console::FG_BLUE);
            }
        }
    }

    /**
     * Integrates a csv with students data for the instructor.
     * 
     * @param string $filePath
     * @param int $campaignId
     * @param int $instructorId
     * @throws Exception
     */
    public function actionIntegrateInstructorCsv($filePath, $campaignId, $instructorId) {
        $instructor = Instructor::findOne(['campaign_id' => $campaignId, 'user_id' => $instructorId]);
        if ($instructor == null) {
            throw new Exception('The instructor does not exist');
        }
        $integrator = new InstructorIntegration([
            'path' => $filePath
            , 'instructor' => $instructor
        ]);
        if ($integrator->process() === false) {
            foreach ($integrator->getErrors() as $errors) {
                foreach ($errors as $error) {
                    $this->output($error, Console::FG_RED);
                }
            }
            $this->output(Yii::t('common/lms/integration','Process end with errors. No changes has been done.'), Console::FG_PURPLE);
        } else {
            $this->output(Yii::t('common/lms/integration', 'The file has been integrated successfully'), Console::FG_GREEN);
        }
    }

    /**
     * Deactivate a list of users by user
     * @param $filepath
     */
    public function actionDeactiveStudents($filepath){

    }
    /**
     * Integrate data for multiple instructors all of them in one operator 
     * (instructors should exist in the system)
     * File format:
     * idInstructor,name,lastname,gender,userid,phone,country,zone,region,place,address,long,lat
     * 
     * @param string $filePath data filepath.
     * @param int $IdCampaign Campaign ID to insert data.
     * @param int $IdOperator of the operator, shoud already be defined inside the campaign.
     */
    public function actionMultiInstructorIntegrate($filePath, $idCampaign, $idOperator){
        $integrator = new MultiInstructorIntegration([
            'filePath' => $filePath,
            'idCampaign' => $idCampaign,
            'idOperator' => $idOperator        
        ]);
        if ( $integrator->process() === false){
            foreach ($integrator->getErrors() as $errors) {
                foreach ($errors as $error) {
                    $this->output($error, Console::FG_RED);
                }
            }
            $this->output(Yii::t('common/lms/integration','Process end with errors. No changes has been done.'), Console::FG_PURPLE);
        }else{
            $this->output(Yii::t('common/lms/integration', 'The file has been integrated successfully'), Console::FG_GREEN);
        }
    }
    
    /**
     * Integrates CSV file for geo information
     * @param type $filePath
     */
    public function actionIntegrateCountryCsv($filePath){
        $integrateCountry = new CountryIntegration(['filePath' => $filePath]);
        $rowsProcessed = $integrateCountry->process(); 
        if ($rowsProcessed == 0 ){
            foreach ($integrateCountry->getErrors() as $errors){
                foreach($errors as $error){
                    $this->output($error, Console::FG_RED);
                }
            } 
        }else{
            $this->output("OK: file has been processed. ", Console::FG_GREEN);
        }
        
    }
    
    /**
     * Delete students. Removes data from sessions, campaigns and from user tables. file format: username, name, lastname
     * 
     * @param type $filePath
     * @param type $campaignId
     */
    public function actionDeleteStudentsCsv($filePath, $campaignId){
        $studentsMaint = new DeleteStudentsCampaign([
            'filePath' => $filePath,
            'campaignId' => intval($campaignId)
        ]);
        $nroReg = $studentsMaint->process();
        if ($studentsMaint->hasErrors()){           
            foreach($studentsMaint->getErrors() as $errors){
                foreach($errors as $error){
                     $this->output($error,  Console::FG_RED);   
                }
            }
        }else{
            $this->output("OK: " . $nroReg ." Students eliminated.", Console::FG_GREEN);
        }
    }
    
    /**
     * Change Facilitators for students file format: student_username, current_facilitator_id, new_facilitator_id
     * @param string $filePath
     * @param int $campaignId
     */
    
    public function actionChangeStudentFacilitator($filePath, $campaignId){
        $changeFacilitator = new ChangeStudentFacilitator([
                'filePath' => $filePath,
                'campaignId' => intval($campaignId)
        ]);
        $nroReg = $changeFacilitator->process();
        if ($changeFacilitator->hasErrors()){           
            foreach($changeFacilitator->getErrors() as $errors){
                foreach($errors as $error){
                     $this->output($error,  Console::FG_RED);   
                }
            }
        }else{
            $this->output("OK: " . $nroReg ." Students changed facilitator.", Console::FG_GREEN);
        }
    }

}
