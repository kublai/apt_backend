<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace console\models\user;

use common\models\user\PasswordChange as BasePasswordChange;

/**
 * Model used to change a concrete user password
 * 

 */
class PasswordChange extends BasePasswordChange {
    
}
