<?php

use yii\db\Migration;
use yii\db\Schema;

class m160223_153542_user_gender extends Migration {

    public function up() {
        $this->addColumn('user', 'gender', Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 1 COMMENT "1 for Male and 2 for Female" AFTER phone');
    }

    public function down() {
        $this->dropColumn('user', 'gender');
    }

}
