<?php

use yii\db\Migration;

class m160225_103017_student_module_integrity_repair extends Migration {

    public function up() {
        $this->execute(<<<SQL
DELETE lsm FROM lms_student_module lsm
LEFT JOIN lms_student_session lss ON 
    lss.campaign_id = lsm.campaign_id
    AND lss.module_id = lsm.module_id
    AND lss.student_id = lsm.student_id
WHERE lss.id IS NULL;
SQL
        );
    }

    public function down() {
        return true;
    }

}
