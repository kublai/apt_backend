<?php

use yii\db\Schema;
use yii\db\Migration;
use custom\db\exceptions\SaveException;
use common\models\lms\Module;

class m150909_153203_code_for_modules extends Migration {

    public function up() {
        $this->delete('lms_campaign_module');
        $this->delete('lms_module');
        $this->addColumn('lms_module', 'code', Schema::TYPE_STRING . ' NOT NULL AFTER id');
        $this->createIndex('UNIQUE_Code', 'lms_module', 'code', true);
        $names = [
            '00' => 'Introducción a la capacitación y como usar la tableta',
            '01' => 'Soy un ser único, especial y capaz',
            '02' => '¿Para qué tener mi negocio?',
            '03' => '¿A quién vendo mi producto o servicio?',
            '04' => '¿Cómo calculo mis costos y mis ganancias?'
        ];
        $trans = Yii::$app->db->beginTransaction();
        try {
            foreach ($names as $code => $name) {
                $module = new Module([
                    'code' => $code,
                    'name' => $name
                ]);
                if ($module->save() === false) {
                    throw new SaveException($module);
                }
            }
            $trans->commit();
        } catch (Exception $ex) {
            $trans->rollBack();
            throw $ex;
        }
    }

    public function down() {
        $this->dropColumn('lms_module', 'code');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
