<?php

use yii\db\Schema;
use yii\db\Migration;

class m150613_131054_cambio_hash_a_sha1 extends Migration {

    public function up() {
        $this->alterColumn('user', 'password_hash', 'char(40) not null');

        $pass = sha1('A12345');
        $this->update('user', ['password_hash' => $pass]);
    }

    public function down() {
        $this->alterColumn('user', 'password_hash', Schema::TYPE_STRING);
        return true;
    }

}
