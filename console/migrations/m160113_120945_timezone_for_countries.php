<?php

use yii\db\Schema;
use yii\db\Migration;

class m160113_120945_timezone_for_countries extends Migration {

    public function up() {
        $this->addColumn('geo_country', 'timezone', Schema::TYPE_STRING . ' DEFAULT "UTC" COMMENT "PHP timezone identifier" AFTER name');
        $this->update('geo_country', ['timezone' => 'America/Asuncion'], ['name' => 'Paraguay']);
        $this->update('geo_country', ['timezone' => 'America/Bogota'], ['name' => 'Colombia']);
    }

    public function down() {
        $this->dropColumn('geo_country', 'timezone');
    }

}
