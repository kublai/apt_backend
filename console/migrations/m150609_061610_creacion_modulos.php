<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\lms\Module;

class m150609_061610_creacion_modulos extends Migration {

    public function up() {
        $names = [
            'Ingreso a la aplicación',
            'Introducción a la capacitación y como usar la tableta',
            'Soy un ser único, especial y capaz',
            '¿Para qué tener mi negocio?',
            '¿A quién vendo mi producto o servicio?',
            '¿Cómo calculo mis costos y mis ganancias?'
        ];
        foreach ($names as $name) {
            $this->insert('lms_module', [
                'name' => $name
            ]);
        }
        return true;
    }

    public function down() {
        $this->truncateTable('lms_module');
        return true;
    }

}
