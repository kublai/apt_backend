<?php

use yii\db\Schema;
use yii\db\Migration;

class m150614_004315_tabla_cache_db extends Migration {

    public function up() {
        $this->createTable('yii_cache', [
            'id' => 'char(128) NOT NULL PRIMARY KEY',
            'expire' => Schema::TYPE_INTEGER . ' NOT NULL',
            'data' => Schema::TYPE_BINARY
        ]);
        $this->createIndex('INDEX_Expire', 'yii_cache', 'expire');
    }

    public function down() {
        $this->dropTable('yii_cache');
        return true;
    }

}
