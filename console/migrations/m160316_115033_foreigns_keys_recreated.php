<?php

use yii\db\Migration;

class m160316_115033_foreigns_keys_recreated extends Migration {

    public function up() {
        $this->dropForeignKey('ID_LmsStudent_CampaignId', 'lms_student');
        $this->dropIndex('ID_LmsStudent_CampaignId', 'lms_student');
        $this->dropForeignKey('FK_LmsStudent_InstructorId', 'lms_student');
        $this->dropIndex('FK_LmsStudent_InstructorId', 'lms_student');
        $this->addForeignKey('FK_LmsStudent_InstructorId_CampaignId', 'lms_student', ['instructor_id', 'campaign_id'], 'lms_instructor', ['user_id', 'campaign_id'], 'CASCADE', 'CASCADE');

        $this->dropForeignKey('FK_LmsInstructor_CampaignId', 'lms_instructor');
        $this->dropForeignKey('FK_LmsInstructor_OperatorId', 'lms_instructor');
        $this->dropIndex('FK_LmsInstructor_OperatorId', 'lms_instructor');
        $this->addForeignKey('FK_LmsInstructor_OperatorId_CampaignId', 'lms_instructor', ['operator_id', 'campaign_id'], 'lms_operator', ['operator_id', 'campaign_id'], 'CASCADE', 'CASCADE');

        $this->dropForeignKey('FK_LmsStudentModule_CampaignId_ModuleId', 'lms_student_module');
    }

    public function down() {
        $this->dropForeignKey('FK_LmsStudent_InstructorId_CampaignId', 'lms_student');
        $this->dropIndex('FK_LmsStudent_InstructorId_CampaignId', 'lms_student');
        $this->addForeignKey('FK_LmsStudent_InstructorId', 'lms_student', 'instructor_id', 'lms_instructor', 'user_id', 'CASCADE', 'CASCADE');
        $this->createIndex('ID_LmsStudent_CampaignId', 'lms_student', 'campaign_id');
        $this->addForeignKey('ID_LmsStudent_CampaignId', 'lms_student', 'campaign_id', 'lms_campaign', 'id', 'CASCADE', 'CASCADE');
        
        $this->dropForeignKey('FK_LmsInstructor_OperatorId_CampaignId', 'lms_instructor');
        $this->dropIndex('FK_LmsInstructor_OperatorId_CampaignId', 'lms_instructor');
        $this->addForeignKey('FK_LmsInstructor_OperatorId', 'lms_instructor', 'operator_id', 'lms_operator', 'operator_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_LmsInstructor_CampaignId', 'lms_instructor', 'campaign_id', 'lms_campaign', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('FK_LmsStudentModule_CampaignId_ModuleId', 'lms_student_module', ['campaign_id', 'module_id'], 'lms_campaign_module', ['campaign_id', 'module_id'], 'CASCADE', 'CASCADE');
    }

}
