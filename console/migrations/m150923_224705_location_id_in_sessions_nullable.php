<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\lms\StudentSession;
use common\models\geo\Location;

class m150923_224705_location_id_in_sessions_nullable extends Migration {

    public function up() {
        $this->alterColumn(StudentSession::tableName(), 'location_id', Schema::TYPE_INTEGER);
    }

    public function down() {
        $this->dropForeignKey('FK_LmsStudentSession_LocationId', StudentSession::tableName());
        $this->alterColumn(StudentSession::tableName(), 'location_id', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->addForeignKey('FK_LmsStudentSession_LocationId', StudentSession::tableName(), 'location_id', Location::tableName(), 'id', 'RESTRICT', 'CASCADE');
    }

}
