<?php

use yii\db\Migration;

class m170211_090452_fix_synclog_fkeys extends Migration
{
    public function up()
    {
        $this->dropForeignKey('FK_LmsInstructorSynclog_InstructorId', 'lms_instructor_synclog');
        $this->dropIndex('FK_LmsInstructorSynclog_InstructorId', 'lms_instructor_synclog');
        
        $this->dropForeignKey('FK_LmsInstructorSynclog_CampaignId', 'lms_instructor_synclog');
        $this->dropIndex('FK_LmsInstructorSynclog_CampaignId', 'lms_instructor_synclog');
        
        $this->createIndex('FK_LmsInstructorSynclog_CampaingId_Instructor_id', 'lms_instructor_synclog', 'campaign_id,instructor_id');
        $this->addForeignKey('FK_LmsInstructorSynclog_InstructorId', 'lms_instructor_synclog', ['campaign_id','instructor_id'],'lms_instructor',['campaign_id','user_id'],'CASCADE','CASCADE');
    }

    public function down()
    {
        echo "m170211_090452_fix_synclog_fkeys cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
