<?php

use yii\db\Migration;

class m160116_174446_add_modulo_9 extends Migration
{
    public function up() {
        $names = [
            '09' => 'Como manejar los riesgos de mi negocio',
            '10' => 'Comunico lo que pienso',
            '11' => 'Si llegamos a acuerdos vivimos mejor',
            '12' => 'Unidos somos más fuertes',
            '13' => 'Las dificultades son oportunidades',
            '14' => 'Fortaleciendo mi negocio',
            '15' => 'Tengo un negocio de calidad',
            '16' => 'Con ganas podemos lograr nuestros sueños'
        ];
        
        $trans = Yii::$app->db->beginTransaction();
        try {
            foreach ($names as $code => $name) {
            echo $code . ":" . $name;
            $this->insert('lms_module', [
                'code' => $code,
                'name' => $name
            ]);
        }
        $trans->commit();
        return true;
        
        } catch (Exception $ex) {
            $trans->rollBack();
            throw $ex;
        }
    }

    public function down()
    {
        echo "m160116_174446_add_modulo_9 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
