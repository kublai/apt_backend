<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\lms\Module;

class m151113_131211_modulos_5_8 extends Migration {

    public function up() {
        $names = [
            '05' => '¡La responsabilidad de salir adelante está en mis manos!',
            '06' => 'Ahorro y buen vivir',
            '07' => 'Las deudas sanas',
            '08' => 'Las cuentas claras'
        ];
        $trans = Yii::$app->db->beginTransaction();
        try {
            foreach ($names as $code => $name) {
                $module = new Module([
                    'code' => $code,
                    'name' => $name
                ]);
                if ($module->save() === false) {
                    throw new SaveException($module);
                }
            }
            $trans->commit();
        } catch (Exception $ex) {
            $trans->rollBack();
            throw $ex;
        }
    }

    public function down() {
        echo "m151113_131211_modulos_5_8 cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
