<?php

use yii\db\Schema;
use yii\db\Migration;

class m150924_021946_role_normalization extends Migration {

    public function up() {
        $this->update('user', ['role' => 2], ['role' => 10]);
        $this->update('user', ['role' => 8], ['role' => 20]);
        $this->update('user', ['role' => 16], ['role' => 30]);
    }

    public function down() {
        $this->update('user', ['role' => 10], ['role' => 2]);
        $this->update('user', ['role' => 20], ['role' => 8]);
        $this->update('user', ['role' => 30], ['role' => 16]);
    }

}
