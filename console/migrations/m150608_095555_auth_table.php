<?php

use yii\db\Schema;
use yii\db\Migration;

class m150608_095555_auth_table extends Migration {

    public function up() {
        $this->createTable('usr_auth', [
            'id' => Schema::TYPE_INTEGER . ' not null',
            'access_token' => 'varbinary(255) not null',
            'expires_at' => Schema::TYPE_INTEGER,
            'created_at' => Schema::TYPE_INTEGER,
            'created_by' => Schema::TYPE_INTEGER,
            'created_by' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
            'updated_by' => Schema::TYPE_INTEGER
        ]);
        $this->addPrimaryKey('PRIMARY KEY', 'usr_auth', 'id');
        $this->addForeignKey('ID_UsrAuth_Id', 'usr_auth', 'id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('INDEX_UsrAuth_AccessToken', 'usr_auth', 'access_token');
    }

    public function down() {
        $this->dropTable('usr_auth');
    }

}
