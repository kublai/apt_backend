<?php

use yii\db\Schema;
use yii\db\Migration;

class m160215_115736_sincronize_log_table extends Migration {

    public function up() {
        $this->createTable('lms_instructor_synclog', [
            'id' => Schema::TYPE_PK
            , 'instructor_id' => Schema::TYPE_INTEGER . ' NOT NULL'
            , 'campaign_id' => Schema::TYPE_INTEGER . ' NOT NULL'
            , 'created_at' => Schema::TYPE_INTEGER . ' NOT NULL'
            , 'status' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 1'
            , 'info' => Schema::TYPE_TEXT
            , 'log' => Schema::TYPE_BINARY
        ]);
        $this->addForeignKey('FK_LmsInstructorSynclog_InstructorId', 'lms_instructor_synclog', 'instructor_id', 'lms_instructor', 'user_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_LmsInstructorSynclog_CampaignId', 'lms_instructor_synclog', 'campaign_id', 'lms_instructor', 'campaign_id', 'CASCADE', 'CASCADE');
    }

    public function down() {
        $this->dropTable('lms_instructor_synclog');
    }

}
