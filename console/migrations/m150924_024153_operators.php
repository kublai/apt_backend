<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\user\User,
    common\models\user\Operator;
use common\models\lms\Campaign;
use custom\db\exceptions\SaveException;

class m150924_024153_operators extends Migration {

    const CAMPAIGN_TABLE_NAME = 'lms_operator';

    public function up() {
        $this->addColumn('user', 'operator_id', Schema::TYPE_INTEGER . ' AFTER `role`');
        $this->addForeignKey('FK_User_OperatorId', 'user', 'operator_id', 'user', 'id', 'SET NULL', 'CASCADE');

        $this->createTable(self::CAMPAIGN_TABLE_NAME, [
            'operator_id' => Schema::TYPE_INTEGER,
            'campaign_id' => Schema::TYPE_INTEGER
        ]);
        $this->addPrimaryKey('PK_LmsOperator_OperatorId_CampaignId', self::CAMPAIGN_TABLE_NAME, ['operator_id', 'campaign_id']);
        $this->addForeignKey('FK_LmsOperator_OperatorId', self::CAMPAIGN_TABLE_NAME, 'operator_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_LmsOperator_CampaignId', self::CAMPAIGN_TABLE_NAME, 'campaign_id', 'lms_campaign', 'id', 'CASCADE', 'CASCADE');

        $this->addColumn('lms_instructor', 'operator_id', Schema::TYPE_INTEGER);

        $operator = $this->createTestOperator();
        if ($operator) {
            $users = User::find()->where(['or', ['role' => 8], ['role' => 16]])->all();
            if ($users) {
                foreach ($users as $user) {
                    $user->link('operator', $operator);
                }
            }
            $campaigns = Campaign::find()->all();
            if ($campaigns) {
                foreach ($campaigns as $campaign) {
                    $campaign->link('operators', $operator);
                }
                $this->update('lms_instructor', ['operator_id' => $operator->id]);
            }
        }

        $this->alterColumn('lms_instructor', 'operator_id', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->addForeignKey('FK_LmsInstructor_OperatorId', 'lms_instructor', 'operator_id', self::CAMPAIGN_TABLE_NAME, 'operator_id', 'CASCADE', 'CASCADE');
    }

    protected function createTestOperator() {
        $operator = new Operator([
            'username' => 'operator1',
            'status' => Operator::STATUS_ACTIVE,
            'name' => 'Operador',
            'last_name' => 'Primero',
            'email' => 'operador.primero@blackbox.es'
        ]);
        $operator->setPassword('A12345');
        $operator->generateAuthKey();
        if ($operator->save() === false) {
            throw new SaveException($operator);
        }
        return $operator;
    }

    public function down() {
        $this->dropForeignKey('FK_User_OperatorId', 'user');
        $this->dropColumn('user', 'operator_id');

        $this->dropForeignKey('FK_LmsInstructor_OperatorId', 'lms_instructor');
        $this->dropColumn('lms_instructor', 'operator_id');

        $this->dropForeignKey('FK_LmsOperator_OperatorId', self::CAMPAIGN_TABLE_NAME);
        $this->dropForeignKey('FK_LmsOperator_CampaignId', self::CAMPAIGN_TABLE_NAME);
        $this->dropTable(self::CAMPAIGN_TABLE_NAME);
    }

}
