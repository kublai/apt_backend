<?php

use yii\db\Schema;
use yii\db\Migration;

class m150609_004914_nuevos_campos_modelo_lms extends Migration {

    public function up() {
        $this->addColumn('lms_campaign_module', 'order', Schema::TYPE_INTEGER . ' not null DEFAULT 1');
        $this->addColumn('lms_module', 'description', Schema::TYPE_TEXT . ' after name');
        $this->dropColumn('lms_module', 'order');
    }

    public function down() {
        $this->dropColumn('lms_campaign_module', 'order');
        $this->dropColumn('lms_module', 'description');
        $this->addColumn('lms_module', 'order', Schema::TYPE_INTEGER . ' not null DEFAULT 1');
    }

}
