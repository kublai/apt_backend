<?php

use yii\db\Migration;

class m160104_122831_fk_cascade_reorganized extends Migration {

    public function up() {
        $this->dropForeignKey('FK_LmsCampaignModule_CampaignId', 'lms_campaign_module');
        $this->addForeignKey('FK_LmsCampaignModule_CampaignId', 'lms_campaign_module', 'campaign_id', 'lms_campaign', 'id', 'CASCADE', 'CASCADE');
        $this->dropForeignKey('FK_LmsCampaignModule_ModuleId', 'lms_campaign_module');
        $this->addForeignKey('FK_LmsCampaignModule_ModuleId', 'lms_campaign_module', 'module_id', 'lms_module', 'id', 'CASCADE', 'CASCADE');

        $this->dropForeignKey('FK_LmsInstructor_CampaignId', 'lms_instructor');
        $this->addForeignKey('FK_LmsInstructor_CampaignId', 'lms_instructor', 'campaign_id', 'lms_campaign', 'id', 'CASCADE', 'CASCADE');
        $this->dropForeignKey('FK_LmsInstructor_UserId', 'lms_instructor');
        $this->addForeignKey('FK_LmsInstructor_UserId', 'lms_instructor', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

        $this->dropForeignKey('FK_LmsStudent_InstructorId', 'lms_student');
        $this->addForeignKey('FK_LmsStudent_InstructorId', 'lms_student', 'instructor_id', 'lms_instructor', 'user_id', 'CASCADE', 'CASCADE');
        $this->dropForeignKey('ID_LmsStudent_CampaignId', 'lms_student');
        $this->addForeignKey('ID_LmsStudent_CampaignId', 'lms_student', 'campaign_id', 'lms_campaign', 'id', 'CASCADE', 'CASCADE');
        $this->dropForeignKey('ID_LmsStudent_UserId', 'lms_student');
        $this->addForeignKey('ID_LmsStudent_UserId', 'lms_student', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

        $this->dropForeignKey('FK_LmsStudentModule_ModuleId', 'lms_student_module');
        $this->addForeignKey('FK_LmsStudentModule_CampaignId_ModuleId', 'lms_student_module', ['campaign_id', 'module_id'], 'lms_campaign_module', ['campaign_id', 'module_id'], 'CASCADE', 'CASCADE');
    }

    public function down() {
        $this->dropForeignKey('FK_LmsCampaignModule_CampaignId', 'lms_campaign_module');
        $this->addForeignKey('FK_LmsCampaignModule_CampaignId', 'lms_campaign_module', 'campaign_id', 'lms_campaign', 'id', 'RESTRICT', 'CASCADE');
        $this->dropForeignKey('FK_LmsCampaignModule_ModuleId', 'lms_campaign_module');
        $this->addForeignKey('FK_LmsCampaignModule_ModuleId', 'lms_campaign_module', 'module_id', 'lms_module', 'id', 'RESTRICT', 'CASCADE');

        $this->dropForeignKey('FK_LmsInstructor_CampaignId', 'lms_instructor');
        $this->addForeignKey('FK_LmsInstructor_CampaignId', 'lms_instructor', 'campaign_id', 'lms_campaign', 'id', 'RESTRICT', 'CASCADE');
        $this->dropForeignKey('FK_LmsInstructor_UserId', 'lms_instructor');
        $this->addForeignKey('FK_LmsInstructor_UserId', 'lms_instructor', 'user_id', 'user', 'id', 'RESTRICT', 'CASCADE');

        $this->dropForeignKey('FK_LmsStudent_InstructorId', 'lms_student');
        $this->addForeignKey('FK_LmsStudent_InstructorId', 'lms_student', 'instructor_id', 'lms_instructor', 'user_id', 'RESTRICT', 'CASCADE');
        $this->dropForeignKey('ID_LmsStudent_CampaignId', 'lms_student');
        $this->addForeignKey('ID_LmsStudent_CampaignId', 'lms_student', 'campaign_id', 'lms_campaign', 'id', 'RESTRICT', 'CASCADE');
        $this->dropForeignKey('ID_LmsStudent_UserId', 'lms_student');
        $this->addForeignKey('ID_LmsStudent_UserId', 'lms_student', 'user_id', 'user', 'id', 'RESTRICT', 'CASCADE');

        $this->dropForeignKey('FK_LmsStudentModule_CampaignId_ModuleId', 'lms_student_module');
        $this->addForeignKey('FK_LmsStudentModule_ModuleId', 'lms_student_module', 'module_id', 'lms_campaign_module', 'module_id', 'CASCADE', 'CASCADE');
    }

}
