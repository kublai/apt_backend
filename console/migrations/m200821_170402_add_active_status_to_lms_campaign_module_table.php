<?php

use yii\db\Migration;

/**
 * Class m200821_170402_add_active_status_to_lms_campaign_module_table
 */
class m200821_170402_add_active_status_to_lms_campaign_module_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('lms_campaign_module', 'active', $this->Integer()->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('lms_campaign_module', 'active');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200821_170402_add_active_status_to_lms_campaign_module_table cannot be reverted.\n";

        return false;
    }
    */
}
