<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\user\User;

class m150923_144608_username_field_moved_to_binary extends Migration {

    public function up() {
        $this->alterColumn(User::tableName(), 'username', Schema::TYPE_STRING . ' COLLATE utf8_bin');
    }

    public function down() {
        $this->alterColumn(User::tableName(), 'username', Schema::TYPE_STRING);
    }
}
