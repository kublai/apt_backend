<?php

use yii\db\Schema;
use yii\db\Migration;

class m160713_112807_campaign_stats_fields extends Migration
{
    public function up()
    {
            $this->addColumn('lms_campaign', 'min_sync_week', Schema::TYPE_INTEGER . ' after end_at' );
            $this->addColumn('lms_campaign', 'min_sync_month', Schema::TYPE_INTEGER . ' after min_sync_week ');
            $this->addColumn('lms_campaign','max_km_between_classes', Schema::TYPE_INTEGER . ' after min_sync_month ');
            $this->addColumn('lms_campaign','max_absence_instructor_days', Schema::TYPE_INTEGER. ' after max_km_between_classes');
            $this->addColumn('lms_campaign','alarm_begin_at', Schema::TYPE_INTEGER . ' after max_absence_instructor_days');
             $this->addColumn('lms_campaign','alarm_end_at', Schema::TYPE_INTEGER . ' after alarm_begin_at');
    }

    public function down()
    {
        echo "m160713_112807_campaign_stats_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
