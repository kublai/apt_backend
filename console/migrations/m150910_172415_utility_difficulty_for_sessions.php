<?php

use yii\db\Schema;
use yii\db\Migration;

class m150910_172415_utility_difficulty_for_sessions extends Migration {

    const TABLE = 'lms_student_session';
    const FIELD_A = 'utility';
    const FIELD_B = 'difficulty';

    public function up() {
        $this->addColumn(self::TABLE, self::FIELD_A, Schema::TYPE_INTEGER . ' AFTER completed');
        $this->addColumn(self::TABLE, self::FIELD_B, Schema::TYPE_INTEGER . ' AFTER utility');
    }

    public function down() {
        $this->dropColumn(self::TABLE, self::FIELD_A);
        $this->dropColumn(self::TABLE, self::FIELD_B);
    }

}
