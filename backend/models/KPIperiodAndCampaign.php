<?php

/**
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\models;

use custom\base\Model;
use Yii;
use common\models\lms\Campaign;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use common\models\user\Operator;
use common\models\user\User,
    common\models\user\Role;

/**
 * Description of KPIperiodAndCampaign
 *
 * @author kublai
 */
class KPIperiodAndCampaign extends Model {

    /**
     * @var int Campaign id
     */
    public $idCampaign;

    /**
     *
     * @var int
     */
    public $operatorId;

    /**
     * @var string Begin Date
     */
    public $beginDate;

    /**
     * @var string End Date
     */
    public $endDate;

    /**
     * @var int Module Id
     */
    public $idModule;

    /**
     *
     * @var Campaign
     */
    protected $campaign;

    /**
     *
     * @var Operator
     */
    protected $operator;

    /**
     * 
     * @var type int
     */
    public $instructorId;

    /**
     * 
     * @var int
     */
    public $min_sync_week;

    /**
     *
     * @var type int
     */
    public $min_sync_month;

    /**
     *
     * @var type int
     */
    public $max_km_between_classes;

    /**
     *
     * @var type int
     */
    public $max_absence_instructor_days;

    /**
     *
     * @var type int
     */
    public $alarm_begin_at;

    /**
     *
     * @var type int
     */
    public $alarm_end_at;

    /**
     * @var type int
     */
    public $tabSelected;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['idCampaign', 'beginDate', 'endDate'], 'required'],
            [['beginDate', 'endDate', 'idModule', 'idCampaign', 'alarm_begin_at', 'alarm_end_at', 'min_sync_week', 'min_sync_month', 'max_km_between_classes', '$max_absence_instructor_days'], 'safe'],
            [['beginDate', 'endDate'], 'date', 'format' => 'yyyy-M-d'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        $transFile = 'backend/index';
        return[
            'idCampaign' => Yii::t($transFile, 'Campaign'),
            'beginDate' => Yii::t($transFile, 'From'),
            'endDate' => Yii::t($transFile, 'To')
        ];
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null) {
        if (parent::load($data, $formName)) {
            $this->loadCampaign();
            return true;
        } else {
            return false;
        }
    }

    public function saveCampaign() {
        if ($this->idCampaign != null && $this->campaign->validate()) {
            $this->campaign->save();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Loads the campaign object.
     */
    public function loadCampaign() {
        if ($this->idCampaign !== null) {
            $this->campaign = Campaign::findOne($this->idCampaign);
        }
    }

    /**
     * Loads the operator object.
     */
    public function loadOperator() {
        if ($this->operatorId !== null) {
            $this->operator = Operator::findOne($this->operatorId);
        }
    }

    /**
     * Returns the campaign object.
     * 
     * @return Campaign
     */
    public function getCampaign() {
        return $this->campaign;
    }

    /**
     * Returns the operator object.
     * 
     * @return Operator
     */
    public function getOperator() {
        return $this->operator;
    }

    /**
     * Modules by Operator.
     * 
     * @param type $this
     */
    public function getBarGraphData() {
        if ($this->campaign === null) {
            return;
        }
        $colorPalette = static::getColorPalette();
        $colors = count($colorPalette);
        //Generating labels for each module
        $labels_code = $this->getModuleLabels(true);
        $studentsDetails = [];
        $query = (new Query())
                ->from('lms_operator')
                ->select([
                    'lms_instructor.operator_id'
                    , 'lms_student_module.module_id'
                    , 'lms_campaign_module.order'
                    , 'COUNT(lms_student_module.student_id) total'
                ])
                ->innerJoin('lms_instructor', 'lms_instructor.operator_id = lms_operator.operator_id AND lms_instructor.campaign_id = lms_operator.campaign_id')
                ->innerJoin('lms_student', 'lms_student.instructor_id = lms_instructor.user_id AND lms_student.campaign_id = lms_instructor.campaign_id')
                ->innerJoin('lms_student_module', 'lms_student_module.student_id = lms_student.user_id AND lms_student_module.campaign_id = lms_student.campaign_id')
                ->innerJoin('lms_campaign_module', 'lms_campaign_module.module_id = lms_student_module.module_id AND lms_campaign_module.campaign_id = lms_student_module.campaign_id')
                ->where(['lms_student_module.completed' => 1, 'lms_student_module.campaign_id' => $this->campaign->id])
                ->groupBy(['lms_instructor.operator_id', 'lms_student_module.module_id']);
        $operators = [];
        if ($this->operator !== null) {
            $query->andWhere(['lms_operator.operator_id' => $this->operator->id]);
            $operators = [$this->operator];
        } else {
            $operators = $this->campaign->operators;
        }
        foreach ($query->all() as $row) {
            if (isset($studentsDetails[$row['operator_id']]) === false) {
                $studentsDetails[$row['operator_id']] = [];
            }
            $studentsDetails[$row['operator_id']][] = ['module_order' => $row['order'], 'total' => $row['total']];
        }

        $color_counter = 0;
        $moduleDefaultData = array_fill(0, $this->campaign->getModules()->count(), 0);
        $dataset = [];
        foreach ($operators as $operator) {
            $aux = $moduleDefaultData;
            $studentsCount = (new Query())
                    ->from('lms_operator')
                    ->innerJoin('lms_instructor', 'lms_instructor.operator_id = lms_operator.operator_id AND lms_instructor.campaign_id = lms_operator.campaign_id')
                    ->innerJoin('lms_student', 'lms_student.instructor_id = lms_instructor.user_id AND lms_student.campaign_id = lms_instructor.campaign_id')
                    ->where(['lms_operator.campaign_id' => $this->campaign->id, 'lms_operator.operator_id' => $operator->id])
                    ->count();
            if (isset($studentsDetails[$operator->id])) {
                foreach ($studentsDetails[$operator->id] as $data) {
                    $aux[$data['module_order'] - 1] = round($studentsCount <= 0 ? 0 : $data['total'] / $studentsCount * 100, 2);
                }
            }
            $operator_data = [
                'data' => $aux
                , 'label' => $operator->name . " " . $operator->last_name
                , 'fillColor' => $colorPalette[$color_counter]
                , 'pointStrokeColor' => '#fff'
            ];
            $color_counter++;
            if ($color_counter >= $colors) {
                $color_counter = 0;
            }
            $dataset[] = $operator_data;
        } 
        return [
            'labels' => $labels_code,
            'datasets' => $dataset
        ];
    }

    /**
     * Generate data for RADAR GRAPH
     * 
     * @return array
     */
    public function getRadarGraphData() {
        if ($this->campaign === null) {
            return;
        }
        $colorPalette = static::getColorPalette();
        $colors = count($colorPalette);

        $labels_code = $this->getModuleLabels();
        $dataset = [];
        $studentsDetails = [];
        $query = (new Query())
                ->from('lms_student_session')
                ->select([
                    'lms_instructor.operator_id'
                    , 'lms_student_session.module_id'
                    , 'lms_campaign_module.order'
                    , 'FLOOR(AVG(lms_student_session.end_at - lms_student_session.begin_at) / 60)  total'
                ])
                ->innerJoin('lms_student', 'lms_student.user_id = lms_student_session.student_id AND lms_student.campaign_id = lms_student_session.campaign_id')
                ->innerJoin('lms_instructor', 'lms_instructor.user_id = lms_student.instructor_id AND lms_instructor.campaign_id = lms_student.campaign_id')
                ->innerJoin('lms_campaign_module', 'lms_campaign_module.module_id = lms_student_session.module_id AND lms_campaign_module.campaign_id = lms_student_session.campaign_id')
                ->where(['lms_student_session.completed' => 1, 'lms_student_session.campaign_id' => $this->campaign->id])
                ->groupBy(['lms_instructor.operator_id', 'lms_student_session.module_id'])
                ->orderBy(['lms_instructor.operator_id' => SORT_ASC]);
        $operators = [];
        if ($this->operator !== null) {
            $query->andWhere(['lms_instructor.operator_id' => $this->operator->id]);
            $operators = [$this->operator];
        } else {
            $operators = $this->campaign->operators;
        }
        foreach ($query->all() as $row) {
            if (isset($studentsDetails[$row['operator_id']]) === false) {
                $studentsDetails[$row['operator_id']] = [];
            }
            $studentsDetails[$row['operator_id']][] = ['module_order' => $row['order'], 'total' => $row['total']];
        }

        $color_counter = 0;
        $moduleDefaultData = array_fill(0, $this->campaign->getModules()->count(), 0);
        foreach ($operators as $operator) {
            $aux = $moduleDefaultData;
            if (isset($studentsDetails[$operator->id])) {
                foreach ($studentsDetails[$operator->id] as $data) {
                    $aux[$data['module_order'] - 1] = $data['total'];
                }
            }
            $operator_data = [
                'data' => $aux
                , 'label' => $operator->name . " " . $operator->last_name
                , 'fillColor' => $colorPalette[$color_counter]
                , 'strokeColor' => $colorPalette[$color_counter]
                , 'pointColor' => $colorPalette[$color_counter]
                , 'pointStrokeColor' => '#fff'
                , 'pointHighlightFill' => '#fff'
                , 'pointHighlightStroke' => $colorPalette[$color_counter]
            ];
            $color_counter++;
            if ($color_counter >= $colors) {
                $color_counter = 0;
            }
            $dataset[] = $operator_data;
        }
        return [
            'labels' => $labels_code,
            'datasets' => $dataset
        ];
    }

    /**
     * Generate data for POLAR AREA GRAPH
     * 
     * @return array
     */
    public function getPolarGraphData() {
        if ($this->campaign === null) {
            return;
        }
        $colorPalette = static::getColorPalette();
        $colors = count($colorPalette);

        $query = (new Query())
                ->from('lms_operator')
                ->select([
                    'lms_instructor.operator_id'
                    , 'COUNT(DISTINCT(lms_student_module.module_id)) total'
                ])
                ->innerJoin('lms_instructor', 'lms_instructor.operator_id = lms_operator.operator_id AND lms_instructor.campaign_id = lms_operator.campaign_id')
                ->innerJoin('lms_student', 'lms_student.instructor_id = lms_instructor.user_id AND lms_student.campaign_id = lms_instructor.campaign_id')
                ->innerJoin('lms_student_module', 'lms_student_module.student_id = lms_student.user_id AND lms_student_module.campaign_id = lms_student.campaign_id')
                ->where(['lms_student_module.completed' => 1, 'lms_student_module.campaign_id' => $this->campaign->id])
                ->groupBy(['lms_instructor.operator_id']);
        $operators = [];
        if ($this->operator !== null) {
            $query->andWhere(['lms_operator.operator_id' => $this->operator->id]);
            $operators = [$this->operator];
        } else {
            $operators = $this->campaign->operators;
        }
        $operators_progress = ArrayHelper::map($query->all(), 'operator_id', 'total');
        //Generating datasets
        $dataset = [];
        $color_counter = 0;
        foreach ($operators as $operator) {
            $counter = isset($operators_progress[$operator->id]) ? $operators_progress[$operator->id] : 0;
            $operator_data = [
                'value' => $counter
                , 'color' => $colorPalette[$color_counter]
                , 'highlight' => $colorPalette[$color_counter]
                , 'label' => $operator->name . ' ' . $operator->last_name
            ];
            $color_counter++;
            if ($color_counter >= $colors) {
                $color_counter = 0;
            }
            $dataset[] = $operator_data;
        }
        return [
            'dataset' => $dataset
        ];
    }

    /**
     * 
     * @return array
     */
    public function getOperatorGraphData() {

        if ($this->operator === null) {
            return;
        }
        $operator = Operator::findOne($this->operatorId);
        if ($operator === null) {
            return;
        }
        if ($this->idModule != NULL) {
            $module = \common\models\lms\Module::findOne($this->idModule);
            if ($module === null) {
                return;
            }
            $whereCondition = [
                'lms_instructor.operator_id' => $operator->id
                , 'lms_instructor.campaign_id' => $this->campaign->id
                , 'lms_campaign_module.module_id' => $this->idModule
            ];
        } else {
            $whereCondition = [
                'lms_instructor.operator_id' => $operator->id
                , 'lms_instructor.campaign_id' => $this->campaign->id
            ];
        }
        if ($this->instructorId != null) {
            $whereCondition = array_merge($whereCondition, ['instructor_id' => $this->instructorId]);
            $labels_code = $this->getModuleLabels(true);
        } else {
            $labels_code = $this->getModuleLabels(false);
        }

        $instructorsData = $this->getDataQuery($whereCondition);
        $instructors = $operator->instructors;
        if ($this->idModule === NULL) {
            $moduleDefaultData = array_fill(0, $this->campaign->getModules()->count(), 0);
        } else {
            $moduleDefaultData = array_fill(0, 1, 0);
        }
        $i = $color_counter = 0;
        $colorPalette = static::getColorPalette();
        $colors = count($colorPalette);
        $auxMaxScale = 0;
        foreach ($instructors as $instructor) {
            $aux = $moduleDefaultData;
            while (isset($instructorsData[$i]) && $instructorsData[$i]['instructor_id'] == $instructor->id) {
                if ($this->idModule === NULL) {
                    if (!empty($instructorsData[$i]['order'])) {
                        $aux[(int) $instructorsData[$i]['order'] - 1] = (int) $instructorsData[$i]['total'];
                    }
                } else {
                    $aux[0] = (int) $instructorsData[$i]['total'];
                }
                ++$i;
            }
            $instructorData = [
                'data' => $aux
                , 'label' => $instructor->name . " " . $instructor->last_name
                , 'fillColor' => $colorPalette[$color_counter]
                , 'pointStrokeColor' => '#fff'
            ];

            if (max($aux) > $auxMaxScale) {
                $auxMaxScale = max($aux);
            }
            $color_counter ++;
            if ($color_counter >= $colors) {
                $color_counter = 0;
            }
            $dataset[] = $instructorData;
        }
        $scaleSteps = round($auxMaxScale / 10) + 1;
        $scaleStepsWidth = 10;
        $scaleStartValue = 0;
        return [
            'labels' => $labels_code,
            'datasets' => $dataset,
            'scaleSteps' => $scaleSteps,
            'scaleStepsWidth' => $scaleStepsWidth,
            'scaleStartValue' => $scaleStartValue
        ];
    }

    /**
     * 
     * @return array
     */
    public function getOperatorGraphDataV2() {
        if ($this->operator === null) {
            return;
        }
        $operator = Operator::findOne($this->operatorId);
        if ($operator === null) {
            return;
        }

        $module = \common\models\lms\Module::findOne($this->idModule);
        $whereCondition = [
            'lms_instructor.operator_id' => $operator->id,
            'lms_instructor.campaign_id' => $this->campaign->id,
             'lms_campaign_module.module_id' => $this->idModule
        ];
        $labels_code = $this->getInstructorsNamesLabels(); //ordererd names of instructors a-z
        $instructorsData = $this->getDataQuery($whereCondition);
        $i =  0;
        $colorPalette = static::getColorPalette();
        $aux = array_fill(0,sizeof($labels_code),0);       
        foreach($labels_code as $instructorName){
            foreach ($instructorsData as $data ){             
                if($data['username']==$instructorName){
                    $aux[$i]=intval($data['total']);
                }
            }
            $i++;
        }
        $dataset[] = [
        'data' =>  $aux
        , 'label' => $module->name
        , 'fillColor' => $colorPalette[0]
        , 'pointStrokeColor' => '#fff'
        ];
        $auxMaxScale = max($aux);
        $scaleSteps = round($auxMaxScale / 10) + 1;
        $scaleStepsWidth = 10;
        $scaleStartValue = 0;
        return [
            'labels' => $labels_code,
            'datasets' => $dataset,
            'scaleSteps' => $scaleSteps,
            'scaleStepsWidth' => $scaleStepsWidth,
            'scaleStartValue' => $scaleStartValue
        ];
    }

    
        
    public function getInstructorsNamesLabels(){
        $labels=[];
        $instructors = $this->campaign->getInstructors()->all();
        foreach ($instructors as $instructor){ 
            $labels [] = $instructor->username;
        }
         asort($labels); 
         foreach ($labels as $key=>$value){
             $orderedLabels[]=$value;
         }
         return $orderedLabels;
        
    }
    
    
    
    /**
     * Returns data for instructors advance by module (used by operator profile to check instructor statistics
     * @return array
     */
    public function getInstructorGraphData() {
        $operator = Operator::findOne($this->operatorId);
        if ($operator === null) {
            return;
        }
        $whereCondition = [
            'lms_instructor.operator_id' => $operator->id,
            'lms_instructor.campaign_id' => $this->campaign->id,
            'instructor_id' => $this->instructorId
        ];
        $labels_code = $this->getModuleLabels(true);
        $instructor = User::findOne($this->instructorId); // Using User model to get Instructor's name and lastname          
        $instructorsData = $this->getDataQuery($whereCondition);
        $i = $color_counter = 0;
        $colorPalette = static::getColorPalette();
        $colors = count($colorPalette);
        $auxMaxScale = 0;
        $moduleDefaultData = array_fill(0, $this->campaign->getModules()->count(), 0);
        $aux = $moduleDefaultData;
        while (isset($instructorsData[$i]) && $instructorsData[$i]['instructor_id'] != null) {
            $aux[$i] = (int) $instructorsData[$i]['total'];
            ++$i;
        }

        $instructorData = [
            'data' => $aux
            , 'label' => $instructor->name . " " . $instructor->last_name
            , 'fillColor' => $colorPalette[$color_counter]
            , 'pointStrokeColor' => '#fff'
        ];

        if (max($aux) > $auxMaxScale) {
            $auxMaxScale = max($aux);
        }
        $dataset[] = $instructorData;
        $scaleSteps = round($auxMaxScale / 10) + 1;
        $scaleStepsWidth = 10;
        $scaleStartValue = 0;

        return [
            'labels' => $labels_code,
            'datasets' => $dataset,
            'scaleSteps' => $scaleSteps,
            'scaleStepsWidth' => $scaleStepsWidth,
            'scaleStartValue' => $scaleStartValue
        ];
    }

    /**
     * Auxiliar function to build the query to recover data 
     * @param type $whereCondition
     * @return type
     */
    protected function getDataQuery($whereCondition) {
        return (new Query())
                        ->from('lms_instructor')
                        ->select([
                            'user.username',
                            'lms_instructor.user_id instructor_id',
                            'lms_campaign_module.order',
                            'SUM(lms_student_module.completed) total'
                        ])
                        ->leftJoin('lms_student', 'lms_student.instructor_id = lms_instructor.user_id AND lms_student.campaign_id = lms_instructor.campaign_id')
                        ->leftjoin('user','lms_student.instructor_id = user.id')
                        ->leftJoin('lms_student_module', 'lms_student_module.student_id = lms_student.user_id AND lms_student_module.campaign_id = lms_student.campaign_id')
                        ->leftJoin('lms_campaign_module', 'lms_campaign_module.module_id = lms_student_module.module_id AND lms_campaign_module.campaign_id = lms_student_module.campaign_id')
                       
                        ->where($whereCondition)
                        ->andWhere( 'lms_campaign_module.order is not null')
                        ->groupBy([ 'user.username','instructor_id', 'lms_student_module.module_id'])
                        ->orderBy([ 'user.username' => SORT_ASC, 'lms_campaign_module.order' => SORT_ASC])
                        ->all();
    }

    /**
     * Auxiliar function to build labels using modules code field.
     * 
     * @return string
     */
    public function getModuleLabels($globalScope = true) {

        $labels_code = [];

        if ($globalScope) {
            $modules = $this->campaign->getModules()->orderBy('order')->all();
            foreach ($modules as $module) {
                $labels_code [] = Yii::t('backend/index', 'Module') . ' ' . $module['code'];
            }
            return $labels_code;
        }

        if ($this->idModule === NULL) {
            $modules = $this->campaign->getModules()->orderBy('order')->all();
            foreach ($modules as $module) {
                $labels_code [] = Yii::t('backend/index', 'Module') . ' ' . $module['code'];
            }
        } else {
            $modules = $this->campaign->getModules()->where(['lms_module.id' => $this->idModule])->all();
            if (sizeof($modules) > 0) {
                $labels_code[] = Yii::t('backend/index', 'Module') . ' ' . $modules[0]['code'] . ' - ' . $modules[0]['name'];
            }
        }
        return $labels_code;
    }
    


    /**
     * 
     * @return string[]
     */
    public static function getColorPalette() {
        return ['#57A64A', '#46BFBD', '#FDB45C', '#5DA5DA', '#D87575', '#60BD68', '#152268', '#4D4D4D', '#FAA43A', '#E92D32', '#DECF3F', '#FFC652', '#83AE84'];
    }

    /**
     * Return total number of students in this Campaign
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getTotalStudents() {
        $q = User::find()->innerJoin('lms_student ls', 'ls.user_id = user.id')->andWhere(['ls.campaign_id' => $this->campaign->id]);
        if ($this->operator !== null) {
            $q->innerJoin('lms_instructor li', 'li.user_id = ls.instructor_id AND li.campaign_id = ls.campaign_id')
                    ->andWhere(['li.operator_id' => $this->operator->id]);
        }
        return $q;
    }

    /**
     * Return total number of male students in this Campaign
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getTotalMales() {
        return $this->getTotalStudents()->andWhere(['gender' => User::GENDER_MALE]);
    }

    /**
     * Return total number of female students in this Campaign
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getTotalFemales() {
        return $this->getTotalStudents()->andWhere(['gender' => User::GENDER_FEMALE]);
    }

    /**
     * Return total number of instructors in this Campaign
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getTotalInstructors() {
        $q = User::find()->innerJoin('lms_instructor li', 'li.user_id = user.id')->andWhere(['campaign_id' => $this->campaign->id]);
        if ($this->operator !== null) {
            $q->andWhere(['li.operator_id' => $this->operator->id]);
        }
        return $q;
    }

    /**
     * 
     * @return \common\models\lms\CampaignQuery
     */
    public function getVisibleCampaigns() {
        $q = Campaign::find();
        if (Role::isOperator(Yii::$app->user)) {
            $q->innerJoin('lms_operator lo', 'lo.campaign_id = lms_campaign.id')
                    ->andWhere(['lo.operator_id' => Yii::$app->user->id]);
        }
        return $q;
    }

}
