<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\models\user;

use Yii;
use common\models\user\LoginForm as BaseLoginForm;
use common\models\user\Role;
use kublai\captcha\CaptchaValidator;

/**
 * Backend login behavior.
 * 

 */
class LoginForm extends BaseLoginForm {

    protected static $rolesAllowed = [
        Role::SUPER_ADMIN,
        Role::ADMIN,
        Role::OPERATOR
    ];

    /**
     *
     * @var string
     */
    public $captcha;

    /**
     * @inheritdoc
     */
    public function rules() {
        return array_merge(parent::rules(), [
            ['captcha', CaptchaValidator::className()]
        ]);
    }

    /**
     * @inheritdoc
     * 
     * Allows only Admin users to log in.
     */
    public function validate($attributeNames = null, $clearErrors = true) {
        if (parent::validate($attributeNames, $clearErrors)) {
            if (!in_array($this->getUser()->role, static::$rolesAllowed)) {
                $this->addError('role', Yii::t('backend/role', 'Your user doesn\'t have a valid role to enter the management area'));
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

}
