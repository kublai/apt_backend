<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\models;

use Yii;
use common\models\user\User;
use yii\base\Model;

/**
 * Password reset request form.
 * 

 */
class PasswordResetRequestForm extends Model {

    public $email;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => User::className(),
                'filter' => ['not', ['status' => User::STATUS_INACTIVE]],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail() {
        /* @var $user User */
        $user = User::find()->where(['and',
                    ['not', ['status' => User::STATUS_INACTIVE]],
                    ['email' => $this->email],
        ])->one();

        if ($user) {
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }

            if ($user->save()) {
                return Yii::$app->mailer->compose(['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user])
                                ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name])
                                ->setTo($this->email)
                                ->setSubject(Yii::t('backend/user/reset_pass', 'Password reset for {platformName}', [
                                    'platformName' => Yii::$app->name
                                ]))
                                ->send();
            }
        }

        return false;
    }

}
