<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\models;

use custom\base\Model;
use Yii;
use common\models\lms\Campaign;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use common\models\user\Operator;
use common\models\user\User;
use common\models\user\Role;
use common\models\lms\Instructor;
use yii\data\ArrayDataProvider;
/**
 * Description of KPIperiodAndCampaign
 *
 * @author kublai
 */
class Alarm extends Model {
    
    /**
     *
     * @var type integer
     */
    public $idCampaign;
    
   
     /*
     * @var type object
     */
    protected  $campaign;


    /**
     * 
     * @return type array
     */
    public function rules() {
        return [
            [['idCampaign'], 'required'],
            [['campaign'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        $transFile = 'backend/index';
        return[
            'idCampaign' => Yii::t($transFile, 'Campaign'),
            'beginDate' => Yii::t($transFile, 'From'),
            'endDate' => Yii::t($transFile, 'To')
        ];
    }
    
    /**
     * 
     * @return type int
     */
    public function getCampaign(){
        return $this->campaign;
    }

    /**
     * 
     */
    public function loadCampaign (){
        if ($this->idCampaign !== null){
            $this->campaign = Campaign::findOne(['id' => $this->idCampaign]);
        }else{
            throw  new \Exception('Alarm object need\'s an idCampaign to load Campaign Data' );
        }
    }
   
    /**
     * Calulate the numer os sessions per week between 2 dates
     * @return array
     * @throws \Exception
     */
    public function sessionsPerWeeks(){
       $c = $this->campaign;
         if ($this->campaign->alarm_begin_at === null || $this->campaign->alarm_end_at === null){
           return;
        }  
        $weeks= $this->calcWeeksArray();
        $instructors = $c->getInstructors()->all();
        $dateStart = $c->alarm_begin_at;
        $dataGlobal = $this->initDataArray($instructors);
        foreach ($weeks as $key =>$week){        
            $sessions = $c->getInstructorsTotalSessionsBetweenDates($dateStart,$dateStart+$week*24*60*60);
            $dateStart = $dateStart+ $week*24*60*60; //+1 seg ?
            foreach($dataGlobal as $row){                      
                $sessionsNumber = $this->extractTotalSessions($row['instructor_id'],$sessions);
                $dataGlobal[$row['instructor_id']][$key] =$sessionsNumber;
            }
        }
        return $dataGlobal;
    }
    
    /**
     * Calculate the number of syncs between two dates in weeks
     * @return array
     * @throws \Exception
     */
    public function syncsPerWeeks(){
        $c = $this->campaign;
        if ($this->campaign->alarm_begin_at === null || $this->campaign->alarm_end_at === null){
           return ;
        }  
        $weeks= $this->calcWeeksArray();
        $instructors = $c->getInstructors()->all();
        $dateStart = $c->alarm_begin_at;
        $dataGlobal = $this->initDataArray($instructors);
        foreach ($weeks as $key =>$week){      
            $sessions = $c->getInstructorsTotalSyncsBetweenDates($dateStart,$dateStart+$week*24*60*60);      
            $dateStart = $dateStart+ $week*24*60*60; //+1 seg ?
            foreach($dataGlobal as $row){      
                $sessionsNumber = $this->extractTotalSyncs($row['instructor_id'],$sessions);
                $dataGlobal[$row['instructor_id']][$key] =$sessionsNumber;
            }
        }
         return $dataGlobal;
    }
    
    /**
     * Calculate the number of syncs between two dates in months
     * @return array
     * @throws \Exception
     */
    public function syncsPerMonths(){
        $c = $this->campaign;
        if ($this->campaign->alarm_begin_at === null || $this->campaign->alarm_end_at === null){
           return;
        }  
        $months= $this->calcMonthsArray();
        $instructors = $c->getInstructors()->all();
        $dateStart = $c->alarm_begin_at;
        $dataGlobal = $this->initDataArray($instructors);
        foreach ($months as $key =>$monthDays){        
            $sessions = $c->getInstructorsTotalSyncsBetweenDates($dateStart,$dateStart+$monthDays*24*60*60);      
            $dateStart = $dateStart+ $monthDays*24*60*60; //+1 seg ?
            foreach($dataGlobal as $row){      
                $sessionsNumber = $this->extractTotalSyncs($row['instructor_id'],$sessions);
                $dataGlobal[$row['instructor_id']][$key] =$sessionsNumber;
            }
        }
         return $dataGlobal;
    }
    
   /**
    * Calculate distance between sessions for each instructor
    * @return array
    */
    public function distanceBetweenSessions(){
        $c = $this->campaign;
        $dataGlobal=[];
        if ($this->campaign->alarm_begin_at === null || $this->campaign->alarm_end_at === null){
           return;
        }  
        $instructors = $c->getInstructors()->all();
        foreach ($instructors as $instructor){
            $locationHistory = $c->getInstructorSessionsLocationHistory($c->alarm_begin_at , $c->alarm_end_at,$instructor->id );  
            
            for ($i=1; $i<sizeof($locationHistory) && sizeof($locationHistory)>1; $i++){
                $session_a = $locationHistory[$i-1];
                $session_b = $locationHistory[$i];
                
                 $p1 = new Point($session_a['latitude'], $session_a['longitude']);
                 $p2 = new Point($session_b['latitude'], $session_b['longitude']);
                 $distance =  round(abs($this->geoDistance($p1, $p2)),1);
                 if ($distance >0 && $session_a['user_id'] == $session_b['user_id']){
                    $data['instructor_id'] = $instructor->id;
                    $data['name'] = $instructor->name . " " . $instructor->last_name;
                    $data['user'] = $session_a['name'] . " " . $session_a['last_name'] ;
                    $data['date_a']=  $this->convertFromUnixTimestamp($session_a['begin_at'])->format('d-m-Y');
                    $data['lat_session_a']=  $session_a['latitude'];
                    $data['lon_session_a']=  $session_a['longitude'];
                    $data['date_b']=  $this->convertFromUnixTimestamp($session_b['begin_at'])->format('d-m-Y');
                    $data['lat_session_b']=  $session_b['latitude'];
                    $data['lon_session_b']=  $session_b['longitude'];
                    $data['distance_km']=  $distance;
                    $dataGlobal[$instructor->id . " " . $i]=$data;
                 }
            }         
         }
         return $dataGlobal;
    }
    
    public function daysBetweenSessions(){
        $c = $this->campaign;
        $dataGlobal=[];
        if ($this->campaign->alarm_begin_at === null || $this->campaign->alarm_end_at === null){
           return;
        }  
        $instructors = $c->getInstructors()->all();
        foreach ($instructors as $instructor){
            $daysHistory = $c->getInstructorDaysHistory($c->alarm_begin_at , $c->alarm_end_at,$instructor->id );  
            
             for ($i=1; $i<sizeof($daysHistory) && sizeof($daysHistory)>1; $i++){

                $session_a = $daysHistory[$i-1];
                $session_b = $daysHistory[$i];
                $days = intval(($session_b['begin_at']-$session_a['begin_at'])/(24*60*60));
                //$days = intval($this->convertFromUnixTimestamp($session_a['begin_at'])->diff($this->convertFromUnixTimestamp($session_b['begin_at']))->format("%d"));
                if ($days>$c->max_absence_instructor_days){
                    $data['instructor_id'] = $instructor->id;
                    $data['Instructor'] = $instructor->name . " " . $instructor->last_name;
                    $data['student'] =$session_a['name'] . " " . $session_a['last_name'] ;
                    $data['module_a'] =$session_a['module_code'];
                    $data['module_b'] =$session_b['module_code'];
                    $data['date_a']=  $this->convertFromUnixTimestamp($session_a['begin_at'])->format('d-m-Y');
                    $data['date_b']=  $this->convertFromUnixTimestamp($session_b['begin_at'])->format('d-m-Y');
                    $data['days']=  $days;
                    $dataGlobal[$instructor->id . " " . $i]=$data;
                }
             }
        }
        return $dataGlobal;
    }

    private function initDataArray($instructors){
        $data= [];
        foreach( $instructors as $instructor){
            $data_init['instructor_id'] = $instructor->id;
            $data_init['name'] = $instructor->name . " " . $instructor->last_name;
            $data[$instructor->id] = $data_init;
        }       
        return $data;
    }
 
    /**
     * Calculate the number of weeks between 2 dates
     * Monday to sunday = 1 week
     * @return type array
     */
    private function calcWeeksArray(){
        $c = $this->campaign;
        $dayOfWeek= date('w',$c->alarm_begin_at);   
        $daysUntilNextSunday = ($dayOfWeek == 0) ? 0 : (7 -$dayOfWeek); //number of days until next monday    
        $k=1;
        $totalDays=$daysUntilNextSunday;
        $weeks['S-' . $k] = $daysUntilNextSunday; 
        while ( $c->alarm_end_at > ($c->alarm_begin_at + 24*60*60*($totalDays+7)) ){
            $weeks['S-' . ++$k]  = 7;
            $totalDays+=7;
        }
        $daysLastWeek = ($c->alarm_end_at - ($c->alarm_begin_at + 24*60*60*$totalDays) ) / (24*60*60);
        if ($daysLastWeek>0){
            $weeks['S-' . ++$k] = $daysLastWeek;
        }
        return $weeks;
    }
    
    private function calcMonthsArray(){
        $c = $this->campaign;
        $daysInFirstMonth=date('t',$c->alarm_begin_at); //number of days in startdate month
        $currentDayOfStartDate=date('j',$c->alarm_begin_at); // day number in startdate month
        $k=1;
        $months['M '. $k] =$daysInFirstMonth - $currentDayOfStartDate + 1; //days in the first month
        $totalDays =  $months['M '.$k];
        while ($c->alarm_end_at > ($c->alarm_begin_at + $totalDays * 24 * 60 * 60) ){           
            $months['M '. ($k+1)]=date('t', $c->alarm_begin_at + $totalDays * 24 * 60 * 60);
            $totalDays +=  $months['M '. ($k+1)];
            $k++;
        }
         $months['M '.$k] = date('j',$c->alarm_end_at);         //adjust number of days in last month
         return $months;
    }
    
    
    private function extractTotalSessions($instructor_id, $sessions){
        foreach($sessions as $session){
            if (intval($session['instructor_id']) == intval($instructor_id)){
                return intval($session['total_sessions']);
            }
        }
        return "";
    }
    
    private function extractTotalSyncs($instructor_id,$syncs){
         foreach($syncs as $sync){
            if (intval($sync['instructor_id']) == intval($instructor_id)){
                return intval($sync['total']);
            }
        }
        return "";
    }
    
    private function convertFromUnixTimestamp($date){
        $d = new \DateTime();
        $d->setTimestamp($date);
        return $d; 
    }

    private function convertToUnixTimestamp($date, $format='d-m-Y'){      
        if ($date == ""){
            return null;
        }else{
            $d =  DateTime::createFromFormat($format,$date);
            $d->setTime(0,0,0);         
            return $d->format('U');
        }
    }
    
    public function buildDataProvider($model){
        $dataProv= new ArrayDataProvider([
                   'allModels' => $model,
                   'pagination' => [
                         'pageSize' => 30,
                     ],
                     'sort' => [
                         'attributes' => ['instructor_id', 'name'],
                     ], 
                ]);   
        return $dataProv;
    }
    
    /**
     * Distance between  2 geo positions
     * @param \backend\models\point $p1
     * @param \backend\models\point $p2
     * @return type
     */
    private function geoDistance(Point $p1, Point $p2){
         $r = 6371; // km
         $dLat = deg2rad($p2->lat - $p1->lat);
         $dLon = deg2rad($p2->lon - $p1->lon);
         $lat1=deg2rad($p1->lat);
         $lat2=deg2rad($p1->lat);
         $a =sin($dLat/2) * sin($dLat/2) +  sin($dLon/2) * sin($dLon/2) * cos($lat1) * cos($lat2); 
         $c = 2 * atan2(sqrt($a), sqrt(1-$a)); 
         return $r* $c;
    }
    
    
}

/**
 * Auxiliar class to work with coordinates
 */
class Point{
    public $lat, $lon;

    function __construct($lat,$lon){
        $this->lat = $lat;
        $this->lon = $lon;
    }
    
}
