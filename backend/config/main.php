<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log', 'captcha'],
    'modules' => [
        'geo' => [
            'class' => 'backend\modules\geo\Module',
        ],
        'user' => [
            'class' => 'backend\modules\user\Module',
        ],
        'lms' => [
            'class' => 'backend\modules\lms\Module',
        ],
        'captcha' => [
            'class' => 'kublai\captcha\Module',
            'cache' => [
                'class' => 'yii\caching\DbCache',
                'cacheTable' => 'yii_cache'
            ]
        ]
        , 'gridview' => 'kartik\grid\Module'
    ],
    'components' => [
        'user' => [
            'class' => 'custom\web\User',
            'identityClass' => 'common\models\user\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => require 'urls.php'
        ],
    ],
    'params' => $params,
];
