<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\controllers;

use custom\web\Controller as BaseController;
use custom\filters\AccessControl;
use yii\filters\VerbFilter;
use common\models\user\Role;

/**
 * Base controller class for all the backend controllers.
 * 

 */
class CrudController extends BaseController {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'userRoles' => [Role::ADMIN, Role::SUPER_ADMIN]
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ]
        ];
    }

}
