<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

/**
 * Description of newPHPClass
 *
 * @author kublai
 */
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use common\models\lms\Campaign;

class DependentDropdownController extends Controller {
    
    public function actionModules($id){
        if (!Yii::$app->getRequest()->isAjax){
            return;
        }
        
        $campaign = new Campaign();
        $campaign->id = $id;
        $rows =   $campaign->getModules()->all();
        $droptions="";
        if(count($rows)>0){
            foreach($rows as $row) {
                $droptions .= '<option value='.$row->id.'>'.$row->name.'</option>';
            }
        }
        else{
            $droptions .= "<option></option>";
        }

        return $droptions;
    }
    
    public function actionAlarmsCampaignParameters($id){
        $campaign =  Campaign::findOne($id);
        
        $alarmParams['min_sync_week'] = $campaign->min_sync_week;
        $alarmParams['min_sync_month'] = $campaign->min_sync_month;
        $alarmParams['max_km_between_classes'] = $campaign->max_km_between_classes;
        $alarmParams['max_absence_instructor_days'] = $campaign->max_absence_instructor_days;
        if ($campaign->alarm_begin_at !== NULL){
            $dateBegin = new \DateTime();
            $dateBegin->setTimestamp($campaign->alarm_begin_at);      
            $alarmParams['alarm_begin_at'] = $dateBegin->format('d-m-Y');
        }else{
             $alarmParams['alarm_begin_at'] = "";
        }
        if($campaign->alarm_end_at !== NULL){
            $dateEnd = new \DateTime();
            $dateEnd->setTimestamp($campaign->alarm_end_at);   
            $alarmParams['alarm_end_at'] = $dateEnd->format('d-m-Y');   
        }else{
            $alarmParams['alarm_end_at'] = "";
        }
        
        return  json_encode($alarmParams) ;
    }
}
