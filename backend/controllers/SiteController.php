<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\controllers;

use Yii;
use custom\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use backend\models\user\LoginForm;
use backend\models\PasswordResetRequestForm,
    backend\models\ResetPasswordForm,
    backend\models\KPIperiodAndCampaign;
use common\models\user\Role;
use yii\web\ForbiddenHttpException;
use common\models\user\User;
use DateTime;
use backend\modules\lms\models\Campaign;
use backend\models\Alarm;
use yii\data\ArrayDataProvider;
/**
 * Site controller
 * 

 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error', 'reset-password'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login', 'request-password-reset'],
                        'allow' => true,
                        'roles' => ['?']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        $session = Yii::$app->session;
        //model for KPIS;
        $model = new KPIperiodAndCampaign();
        if (Role::isOperator(Yii::$app->user)) {
            $model->operatorId = Yii::$app->user->id;
            $model->loadOperator();
        }
        //init KPI variables
        //if it's not post or if it's post but not jpax post
        if ($model->load(Yii::$app->request->post()) === false && !Yii::$app->request->isPjax) {
            $model->tabSelected=1; //KPI's tab is selected by default
            if( !is_null($model->getVisibleCampaigns()->one())){
                $model->idCampaign = $model->getVisibleCampaigns()->one()->id;
                $model->loadCampaign();
                 date_default_timezone_set($model->campaign->getTimeZone() );
                $model->idModule = $model->getCampaign()->getModules()->one();
            }else{
                return $this->render('error',['name' => 'Error', 'message' => 'No hay datos para visualizar, el usuario no tiene campañas asignadas']);
            }
        } else { 
            if(Yii::$app->request->isPjax){
                $session->open();
                $postData = unserialize($session->get('postData'));
                $postDataAlarms = unserialize($session->get('postDataAlarms'));
                $session->close();
                $model->idCampaign = $postData['idCampaign'];
            }else{
                $postData = Yii::$app->getRequest()->post('KPIperiodAndCampaign');
                $postDataAlarms = Yii::$app->getRequest()->post('Campaign'); //campaign alarm parameters
                $session->open();
                $session->set('postData', serialize($postData));
                $session->set('postDataAlarms', serialize($postDataAlarms));
                $session->close();
                $model->idCampaign = Yii::$app->getRequest()->post('KPIperiodAndCampaign')['idCampaign'];
            }
            $model->tabSelected =  $postData['tabSelected'];       
            $model->loadCampaign();       
             date_default_timezone_set($model->campaign->getTimeZone() );
            $model->idModule = (array_key_exists('idModule', $postData) ? $postData['idModule'] : NULL);
            
            //load alarm parameters into the model and save them into campaign model
            if ($postDataAlarms!=null) {
                $model->campaign->min_sync_week = (array_key_exists('min_sync_week', $postDataAlarms) ? $postDataAlarms['min_sync_week'] : NULL);
                $model->campaign->min_sync_month = (array_key_exists('min_sync_month', $postDataAlarms) ? $postDataAlarms['min_sync_month'] : NULL);
                $model->campaign->max_km_between_classes = (array_key_exists('max_km_between_classes', $postDataAlarms) ? $postDataAlarms['max_km_between_classes'] : NULL);
                $model->campaign->max_absence_instructor_days = (array_key_exists('max_absence_instructor_days', $postDataAlarms) ? $postDataAlarms['max_absence_instructor_days'] : NULL);
                $model->campaign->alarm_begin_at = (array_key_exists('min_sync_week', $postDataAlarms) ? $postDataAlarms['alarm_begin_at'] : NULL);
                $model->campaign->alarm_end_at = (array_key_exists('min_sync_week', $postDataAlarms) ? $postDataAlarms['alarm_end_at'] : NULL);
                $model->campaign->alarm_begin_at = $this->ConvertToUnixTimestamp($model->campaign->alarm_begin_at ); 
                $model->campaign->alarm_end_at = $this->ConvertToUnixTimestamp($model->campaign->alarm_end_at );        
            }             
            $model->saveCampaign();
         }   
         
        //init Alarm Variables
        $alarmModel = new Alarm();
        $alarmModel->idCampaign = $model->idCampaign; 
        $alarmModel->loadCampaign(); 
        $sessionPerWeeksProvider = $alarmModel->buildDataProvider($alarmModel->sessionsPerWeeks());   
        $syncsPerWeeksProvider  =  $alarmModel->buildDataProvider($alarmModel->syncsPerWeeks());       
        $syncsPerMonthsProvider =  $alarmModel->buildDataProvider($alarmModel->syncsPerMonths());
        $distanceBetweenSessions =  $alarmModel->buildDataProvider($alarmModel->distanceBetweenSessions());
        $daysBetweenSessions =  $alarmModel->buildDataProvider($alarmModel->daysBetweenSessions());
        $toDate = new DateTime();
        $toDate->setTimestamp($model->campaign->end_at);
        $fromDate = new DateTime();
        $fromDate->setTimestamp($model->campaign->begin_at);

        return $this->render('index', ['model' => $model, 
            'sessionPerWeeksProvider'=>$sessionPerWeeksProvider,
            'syncsPerWeeksProvider'=>$syncsPerWeeksProvider,
            'syncsPerMonthsProvider' => $syncsPerMonthsProvider,
            'distanceBetweenSessions'=> $distanceBetweenSessions,
            'daysBetweenSessions'=> $daysBetweenSessions]);
    }
    
    private function ConvertToUnixTimestamp($date, $format='d-m-Y'){       
        if ($date == ""){
            return null;
        }else{
            $d =  DateTime::createFromFormat($format,$date);
            $d->setTime(0,0,0);     
            return $d->format('U');
        }
    }

    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionRequestPasswordReset() {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('backend/user/reset_pass', 'Check your email for further instructions. Don\'t forget to check the spam folder if the email doesn\'t appear in your inbox.'));
                return $this->goBack(['/site/login']);
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('backend/user/reset_pass', 'Sorry, we are unable to reset password for email provided.'));
            }
        }

        return $this->render('requestPasswordResetToken', [
                    'model' => $model,
        ]);
    }

    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            $this->logUserIn($model->getUser());
            Yii::$app->getSession()->setFlash('success', Yii::t('backend/user/reset_pass', 'New password was saved.'));
            return $this->goBack(['/site/index']);
        }

        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

    protected function logUserIn(User $user) {
        $wUser = Yii::$app->user;
        if ($user->role >= Role::INSTRUCTOR) {
            throw new ForbiddenHttpException();
        }
        $wUser->setRole($user->role);
        $wUser->login($user);
    }

}
