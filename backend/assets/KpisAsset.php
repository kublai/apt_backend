<?php
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Kublai Góemez <kublai@bytebox.es>
 * @since 2.0
 */
class KpisAsset extends AssetBundle {

    public $sourcePath = '@backend/assets/';
    public $js = [
        'js/kpis.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii2mod\alert\AlertAsset'
    ];

}
