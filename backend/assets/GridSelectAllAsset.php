<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * 

 */
class GridSelectAllAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/';
    public $js = [
        'js/grid-select-all.js'
    ];
    public $depends = [
        'yii\grid\GridViewAsset',
    ];
}
