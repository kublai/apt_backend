/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

var grad = grad || {};
(function ($, document, yii) {
    $(".advanced-search .collapsible").click(function (ev) {
        var $this = $(this).find(".glyphicon");
        $this.toggleClass("glyphicon-menu-down glyphicon-menu-right");
    });

    yii.confirm = function (message, ok, cancel) {
        swal({
            title: message,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si",
            cancelButtonText: "Cancelar",
            closeOnConfirm: true,
            closeOnCancel: true
        },
                function (isConfirm) {
                    if (isConfirm) {
                        !ok || ok();
                    } else {
                        !cancel || cancel();
                    }
                });
    };
})($, document, yii);