/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

(function () {
    var $grid = $('.full-selection');
    var $selectionAllCheck = $grid.find('input[name="selection_all"]');
    var $selectionResume = $('.selection-resume');
    var $selectionAll = $('.selection-all');

    var $bAddSelected = $('.btn.btn-add-selected');
    var $bSelectAll = $('a.select-all');
    var $bUnselectAll = $('a.unselect-all');
    var all = 0;
    var total = parseInt($selectionResume.data('count'));

    var _eventSelectAll = function (ev) {
        ev.preventDefault();
        all = 1;
        $selectionResume.addClass('hidden');
        $selectionAll.removeClass('hidden');
        $bSelectAll.off('click');
        $bUnselectAll.on('click', _eventUnselectAll);
    };

    var _eventAddSelected = function (ev) {
        ev.preventDefault();

        $.post(window.location.href, {
            'all': all,
            'user_ids': $grid.yiiGridView('getSelectedRows')
        }).done(function (data) {
            if (data.result === true) {
                window.location.reload();
            }
        });
    };

    var _eventUnselectAll = function (ev) {
        ev.preventDefault();
        all = 0;
        $selectionAll.addClass('hidden');
        $bUnselectAll.off('click');
        checkFunction();
    };

    var checkFunction = function () {
        var selected, selectable;
        selectable = $grid.find('input[name="selection[]"]').length;
        selected = $grid.yiiGridView('getSelectedRows').length;
        if (selected === selectable && $selectionResume.hasClass('hidden') && selectable !== total) {
            $selectionResume.removeClass('hidden');
            $bSelectAll.on('click', _eventSelectAll);
        } else if (selected !== selectable) {
            all = 0;
            if (!$selectionResume.hasClass('hidden')) {
                $selectionResume.addClass('hidden');
                $bSelectAll.off('click');
            }
            if (!$selectionAll.hasClass('hidden')) {
                $selectionAll.addClass('hidden');
                $bUnselectAll.off('click');
            }
        }
        if (selected > 0 && $bAddSelected.hasClass('disabled')) {
            $bAddSelected.removeClass('disabled');
            $bAddSelected.on('click', _eventAddSelected);
        } else if (selected <= 0 && !$bAddSelected.hasClass('disabled')) {
            $bAddSelected.addClass('disabled');
            $bAddSelected.off('click');
        }
    };

    $selectionAllCheck.on("change", checkFunction);
    $grid.find('input[name="selection[]"]').on("change", checkFunction);
})();