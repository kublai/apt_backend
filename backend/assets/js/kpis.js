/**
 
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

jQuery(document).ready(function () {
    if ($('#mainContainer').data('tabSelected') === 1) {
        $('#alarmsDataContainer').hide();
        $('#kpisDataContainer').show();

        $('.nav-tabs').find('a[href="#sectionKpis"]').trigger('click');

    } else {
        $('#alarmsDataContainer').show();
        $('#kpisDataContainer').hide();
        $('.nav-tabs').find('a[href="#sectionAlarms"]').trigger('click');
    }

    $('#loading').hide();

    $(document).on('pjax:send', function () {
        $('#loading').show();
    });
    $(document).on('pjax:complete', function () {
        $('#loading').hide();
    });
    
    
});

