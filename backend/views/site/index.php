<?php

use custom\helpers\Html;
use backend\assets\KpisAsset;

/* @var $this yii\web\View */

$this->title = 'Backend Graduación - Fundación Capital';
KpisAsset::register($this);
?>
<div id='mainContainer' class="site-index" data-tab-selected="<?= $model->tabSelected?>">
    <ul class="nav nav-tabs" id="mainTab">
        <li class="active">
            <a data-toggle="tab" href="#sectionKpis"><?= Yii::t('backend/index', 'KPI\'s') ?></a>
        </li>
        <li>
            <a data-toggle="tab" href="#sectionAlarms"><?= Yii::t('backend/index', 'Alarms') ?></a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="sectionKpis" class="tab-pane fade in active">
            <?=
            $this->render('index/_kpis', ['model' => $model]);
            ?>    
        </div>
        <div id="sectionAlarms" class="tab-pane fade ">
            <?=
            $this->render('index/_alarms',['model' => $model, 
                'sessionPerWeeksProvider'=>$sessionPerWeeksProvider,
                'syncsPerWeeksProvider'=>$syncsPerWeeksProvider,
                'syncsPerMonthsProvider'=>$syncsPerMonthsProvider,
                'distanceBetweenSessions'=> $distanceBetweenSessions,
                'daysBetweenSessions'=> $daysBetweenSessions]);
            ?> 
        </div>
    </div>
</div>
