<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use dosamigos\chartjs\ChartJs;
use common\models\user\Role;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\jui\DatePicker;
use yii\grid\GridView;

/* @var $model backend\models\KPIperiodAndCampaign */
/* @var $this yii\web\View */

$campaign = $model->getCampaign();
?>
<div id="loading"><span class="spinner glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> </div>
<?php
$form = ActiveForm::Begin(['id' => 'alarm-filter-form']);
?>
<div class="row">
    <div class='col-sm-12'>
        <h3><?= Yii::t('backend/index/alarms', 'Alarms') ?></h3>
    </div>
    <?php
    $items = ArrayHelper::map($model->getCampaign()->getModules()->all(), 'id', 'name');
    $modulesLbl = Yii::t('backend/index/kpis', 'Select module');
    echo Html::hiddenInput('KPIperiodAndCampaign[tabSelected]', 2); //hidden field to select alarms tab
    ?>
    <?=
            $form->field($model, 'idCampaign', ['options' => ['class' => 'col-sm-6']])
            ->dropDownList(
                    ArrayHelper::map($model->getVisibleCampaigns()->all(), 'id', 'name'), [
                'onchange' => '
                    $.get( "' . Url::toRoute('dependent-dropdown/alarms-campaign-parameters') . '", { id: $(this).val() } )
                        .done(function( data_string ) {
                            data_object = JSON.parse(data_string);
                            $( "#' . Html::getInputId($model->campaign, 'min_sync_week') . '" ).val( data_object.min_sync_week );
                            $( "#' . Html::getInputId($model->campaign, 'min_sync_month') . '" ).val( data_object.min_sync_month );
                            $( "#' . Html::getInputId($model->campaign, 'max_km_between_classes') . '" ).val( data_object.max_km_between_classes );
                            $( "#' . Html::getInputId($model->campaign, 'max_absence_instructor_days') . '" ).val( data_object.max_absence_instructor_days );
                            $( "#' . Html::getInputId($model->campaign, 'alarm_begin_at') . '" ).val( data_object.alarm_begin_at );
                            $( "#' . Html::getInputId($model->campaign, 'alarm_end_at') . '" ).val( data_object.alarm_end_at );
                        }
                    );
                '
    ]);
    ?>
    <?=
    $form->field($model->campaign, 'alarm_begin_at', ['options' => ['class' => 'col-sm-3']])->widget(DatePicker::classname(), [
        'options' => ['class' => 'form-control'],
        'dateFormat' => 'dd-MM-yyyy'
    ])->label(Yii::t('backend/index/alarms', 'Analize from'));
    ?>
    <?=
    $form->field($model->campaign, 'alarm_end_at', ['options' => ['class' => 'col-sm-3']])->widget(DatePicker::classname(), [
        'options' => ['class' => 'form-control'],
        'dateFormat' => 'dd-MM-yyyy'
    ])->label(Yii::t('backend/index/alarms', 'Analize to'));
    ?>
</div>
<div class="row">
    <div class='col-sm-12'>
        <h4><?= Yii::t('backend/index/alarms', 'Campaign parameters') ?></h4>
    </div>
    <div class='col-sm-12'>
        <div class="row">
            <div class="col-sm-3"><?= $form->field($model->campaign, 'min_sync_week')->label(Yii::t('backend/index/alarms', 'Min syncs per week')); ?></div>
            <div class="col-sm-3"><?= $form->field($model->campaign, 'min_sync_month')->label(Yii::t('backend/index/alarms', 'Min syncs per month')); ?></div>
            <div class="col-sm-3"><?= $form->field($model->campaign, 'max_km_between_classes')->label(Yii::t('backend/index/alarms', 'Max Km between 2 sessions')); ?></div>
            <div class="col-sm-3"><?= $form->field($model->campaign, 'max_absence_instructor_days')->label(Yii::t('backend/index/alarms', 'Max absence instructor days between 2 sessions')); ?></div>
        </div>
        <div class="row">
            <div class='col-sm-2'>
                <label class='control-label'>&nbsp;</label>
                <?= Html::submitButton(Yii::t('backend/index/alarms', 'Apply'), ['class' => 'form-control btn btn-primary ']); ?>
            </div>
        </div>
        <?php
        ActiveForm::end();
        ?>
    </div>
</div>
<div class="row">
    <div class='col-sm-12'>
        <h3><?= Yii::t('backend/index/alarms', 'Sessions per week and instructor') ?></h3>
        <?php
        Pjax::begin(['id' => 'alarms-data-container', 'timeout' => false, 'enablePushState' => false, 'clientOptions' => ['method' => 'POST']]);
        echo GridView::widget([
            'dataProvider' => $sessionPerWeeksProvider,
        ]);
        Pjax::end();
        ?>
        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
        <small><?= Yii::t('backend/index/alarms', 'This report show  the number of sessions per week, each column correspond to one week.') ?></small>
    </div>
</div>
<div class="row">
    <div class='col-sm-12'>
        <h3><?= Yii::t('backend/index/alarms', 'Syncs per week and instructor') ?></h3>
        <?php
        $minSyncsWeek = $model->campaign->min_sync_week;
        if ($syncsPerWeeksProvider->allModels != NULL){
            $auxWeeks = array_slice($syncsPerWeeksProvider->allModels,0,1);
        }else{
            $auxWeeks = NULL;
        }
        if (sizeof($auxWeeks)>0){
            $auxWeeks =  $auxWeeks[0];
            foreach ($auxWeeks as $auxKey => $value){
                $columnsCustomWeeks[] = [
                            'attribute' => $auxKey,
                            'contentOptions' =>  function($model, $key, $index, $column) use ($minSyncsWeek, $auxKey){
                                if ($model[$auxKey] == "") {
                                    return [];
                                }
                                if( intval($model[$auxKey]) < intval($minSyncsWeek) && $auxKey!="instructor_id" && $auxKey!="name"  ){
                                    return ['class' => 'danger'];
                                }else{
                                    return [];
                                }
                            }
                        ];
            }           
        }else{
            $columnsCustomWeeks=[];
        }
        
        Pjax::begin(['id' => 'syncs-week-data-container', 'timeout' => false, 'enablePushState' => false, 'clientOptions' => ['method' => 'POST']]);
        echo GridView::widget([
            'dataProvider' => $syncsPerWeeksProvider,
             'columns' => $columnsCustomWeeks
        ]);
        Pjax::end();
        ?>

        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
        <small><?= Yii::t('backend/index/alarms', 'Number of synchronizations per week, if the number calculated is less than the minimum then the cell has a red background') ?></small>
    </div>
</div>
<div class="row">
    <div class='col-sm-12'>
        <h3><?= Yii::t('backend/index/alarms', 'Syncs per month and instructor') ?></h3>
        <?php
        $minSyncs = $model->campaign->min_sync_month;
        if ($syncsPerWeeksProvider->allModels != NULL){
            $aux = array_slice($syncsPerMonthsProvider->allModels,0,1);
        }else{
            $aux = NULL;
        }
        if (sizeof($aux)>0){
            $aux= $aux[0];
            foreach ($aux as $auxKey => $value){
                $columnsCustomMonths[] = [
                                'attribute' => $auxKey,
                                'contentOptions' =>  function($model, $key, $index, $column) use ($minSyncs, $auxKey){
                                    if ($model[$auxKey] == "") {
                                        return [];
                                    }
                                    if( intval($model[$auxKey]) < intval($minSyncs) && $auxKey!="instructor_id" && $auxKey!="name"  ){
                                        return ['class' => 'danger'];
                                    }else{
                                        return [];
                                    }
                                }
                            ];
            }
        }else{
            $columnsCustomMonths = [];
        }
        Pjax::begin(['id' => 'syncs-month-data-container', 'timeout' => false, 'enablePushState' => false, 'clientOptions' => ['method' => 'POST']]);
        echo GridView::widget([
            'dataProvider' => $syncsPerMonthsProvider,   
            'columns' => $columnsCustomMonths
        ]);
        Pjax::end();
        ?>
        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
        <small><?= Yii::t('backend/index/alarms',  'Number of syncs per month. the first and last month could not be completed, is depends of the  analized period') ?></small>
    </div>
</div>
<div class="row">
    <div class='col-sm-12'>
        <h3><?= Yii::t('backend/index/alarms', 'Distance between sessions') ?></h3>
        <?php
        $maxDistance = $model->campaign->max_km_between_classes;
        Pjax::begin(['id' => 'distance-sessions-data-container', 'timeout' => false, 'enablePushState' => false, 'clientOptions' => ['method' => 'POST']]);
        echo GridView::widget([
            'dataProvider' => $distanceBetweenSessions,
             'rowOptions'=>function($model) use ($maxDistance){
                    if($model['distance_km'] > intval($maxDistance) ){
                        return ['class' => 'danger'];
                    }
            }  
        ]);
        Pjax::end();
        ?>
        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
        <small><?= Yii::t('backend/index/alarms','This report shows the distance between to consecutive sessions') ?></small>
    </div>
</div>
<div class="row">
    <div class='col-sm-12'>
        <h3><?= Yii::t('backend/index/alarms', 'Days between sessions') ?></h3>
        <?php
        $maxdays = $model->campaign->max_absence_instructor_days;
        Pjax::begin(['id' => 'days-sessions-data-container', 'timeout' => false, 'enablePushState' => false, 'clientOptions' => ['method' => 'POST']]);
        echo GridView::widget([
            'dataProvider' => $daysBetweenSessions,
            'rowOptions'=>function($model) use ($maxdays){
                    if($model['days'] > intval($maxdays) ){
                        return ['class' => 'danger'];
                    }
            }  
        ]);
        Pjax::end();
        ?>
        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
        <small><?= Yii::t('backend/index/alarms', 'number of days between 2 consecutive sessions') ?></small>
    </div>
</div>