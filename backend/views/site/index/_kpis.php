<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use dosamigos\chartjs\ChartJs;
use common\models\user\Role;
use yii\helpers\Url;

/* @var $model backend\models\KPIperiodAndCampaign */
/* @var $this yii\web\View */



$campaign = $model->getCampaign();
$graf1 = $model->getBarGraphData();
$graf2 = $model->getRadarGraphData();
$graf3 = $model->getPolarGraphData();
$nInstructors = $model->getTotalInstructors()->count();
$nStudents = $model->getTotalStudents()->count();
$nMaleStudents = $model->getTotalMales()->count();
$nFemaleStudents = $model->getTotalFemales()->count();
$idModule = $model->idModule;
?>
<div class="row">
    <div class='col-sm-12'>
        <h3><?= Yii::t('backend/index', 'Filter') ?></h3>
    </div>
    <?php
    $items = ArrayHelper::map($model->getCampaign()->getModules()->all(), 'id', 'name');
    $modulesLbl = Yii::t('backend/index/kpis', 'Select module');
    $form = ActiveForm::Begin(['id' => 'kpi-filter-form']);
    echo Html::hiddenInput('KPIperiodAndCampaign[tabSelected]', 1);
    ?>
    <?=
            $form->field($model, 'idCampaign', ['options' => ['class' => 'col-sm-4']])
            ->dropDownList(
                    ArrayHelper::map($model->getVisibleCampaigns()->all(), 'id', 'name'), [
                'onchange' => '
                        $.get( "' . Url::toRoute('dependent-dropdown/modules') . '", { id: $(this).val() } )
                            .done(function( data ) {
                                $( "#' . Html::getInputId($model, 'idModule') . '" ).html( data );
                            }
                        );
                    '
    ]);
    ?>
    <?php
   if (Role::isOperator(Yii::$app->user)) {
        echo $form->field($model, 'idModule', ['options' => ['class' => 'col-sm-4']])
                ->dropDownList($items)->label($modulesLbl);
    }
    ?>
    <div class='col-sm-2'>
        <label class='control-label'>&nbsp;</label>
        <?= Html::submitButton(Yii::t('backend/index', 'Apply'), ['class' => 'form-control btn btn-primary ']); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<div id="kpisDataContainer">
    <div class="row">
        <div class='col-sm-12'>
            <h3><?= Yii::t('backend/index/kpis', 'Campaign general information') ?></h3>
            <div class="row">
                <div class="col-sm-12"><?= Yii::t('backend/index/kpis', 'Name') ?>: <?= $campaign->name ?></div>
                <div class="col-sm-6"><?= Yii::t('backend/index/kpis', 'From') ?>: <?= gmdate("d-m-Y", $campaign->begin_at); ?></div>
                <div class="col-sm-6"><?= Yii::t('backend/index/kpis', 'To') ?>:  <?= gmdate("d-m-Y", $campaign->end_at); ?></div>
                <div class="col-sm-6"><?= Yii::t('backend/index/kpis', 'Total Instructors') ?>:  <?= $nInstructors ?></div>
                <div class="col-sm-6"><?= Yii::t('backend/index/kpis', 'Total Students') ?>:  <?= $nStudents ?></div>
                <div class="col-sm-6"><?= Yii::t('backend/index/kpis', 'Number of Men') ?>:  <?= $nMaleStudents ?></div>
                <div class="col-sm-6"><?= Yii::t('backend/index/kpis', 'Number of Women') ?>:  <?= $nFemaleStudents ?></div>
            </div>
        </div>   
    </div>


    <?php
    if (Role::isOperator(Yii::$app->user)) :
        if ($model->idModule === null) {
            $modulesIds = ArrayHelper::getColumn($model->getCampaign()->getModules()->all(), 'id');
            if (sizeof($modulesIds) > 0) {
                $model->idModule = $modulesIds[0];
            }
        }
        $oprGraf = $model->getOperatorGraphDataV2();
        ?>
        <div class="row">
            <div class="col-sm-12" >
                <h3><?= Yii::t('backend/index/kpis', 'Global progress by instructor') ?></h3>
                <div id="oprGraf1-legend" class="legend"></div>
                <canvas id="oprGraf1" style="width:100%; max-height: 500px"></canvas>
            </div>
            <?php
            ChartJs::widget([
                'type' => 'Bar',
                'options' => [
                    'id' => 'oprGraf1'
                ],
                'clientOptions' => [
                    'scaleOverride' => true,
                    'scaleSteps' => $oprGraf['scaleSteps'],
                    'scaleStepWidth' => $oprGraf['scaleStepsWidth'],
                    'scaleStartValue' => $oprGraf['scaleStartValue'],
                    'legendTemplate' => "<ul class=\"opr-<%=name.toLowerCase()%>-legend legend\"><% for (var j=0; j<datasets.length; j++){%><li class=\"col-md-3\"><span style=\"background-color:<%=datasets[j].fillColor%>;padding-left: 20px;\"></span>&nbsp;<%if(datasets[j].label){%><%=datasets[j].label%><%}%></li><%}%></ul>"
                ],
                'data' => [
                    'labels' => $oprGraf['labels'],
                    'datasets' => $oprGraf['datasets']
                ]
            ]);
            ?>
        </div>
        <div class="col-sm-12" >
            <small><strong><?= Yii::t('backend/index/kpis', 'X axis: ') ?></strong>
                <?= Yii::t('backend/index/kpis', 'Code modules that are in the project') ?></small>
        </div>
        <div class="col-sm-12" >
            <small><strong><?= Yii::t('backend/index/kpis', 'Y axis: ') ?></strong>
                <?= Yii::t('backend/index/kpis', 'Number of users asigned to each manager.') ?>
            </small>
        </div>
    <?php endif; ?>


    <div class="row">
        <div class="col-sm-12" >
            <h3><?= Yii::t('backend/index/kpis', 'Training Modules') ?></h3>
            <div id="graf1-legend" class="legend"></div>

            <canvas id="graf1" style="width:100%; max-height: 400px"></canvas>
        </div>
        <?php
        ChartJs::widget([
            'type' => 'Bar',
            'options' => [
                'id' => 'graf1'
            ],
            'clientOptions' => [
                'legendTemplate' => "<ul class=\"<%=name.toLowerCase()%>-legend legend\"><% for (var i=0; i<datasets.length; i++){%><li class=\"col-md-3\"><span style=\"background-color:<%=datasets[i].fillColor%>;padding-left: 20px;\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
            ],
            'data' => [
                'labels' => $graf1['labels'],
                'datasets' => $graf1['datasets']
            ]
        ]);
        ?>
        <div class="col-sm-12" >
            <small><strong><?= Yii::t('backend/index/kpis', 'X axis: ') ?></strong>
                <?= Yii::t('backend/index/kpis', 'Code modules that are in the project') ?></small>
        </div>
        <div class="col-sm-12" >
            <small><strong><?= Yii::t('backend/index/kpis', 'Y axis: ') ?></strong>
                <?= Yii::t('backend/index/kpis', 'Percentage of users that completed a module') ?>
            </small>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12" >
            <h3><?= Yii::t('backend/index/kpis', 'User times by module') ?></h3>
            <div id="graf2-legend" class="legend"></div>
            <canvas id="graf2" style="width:100%; max-height: 500px"></canvas>
        </div> 
        <?php
        ChartJs::widget([
            'type' => 'Radar',
            'options' => [
                'id' => 'graf2'
            ],
            'clientOptions' => [
                'legendTemplate' => "<ul class=\"<%=name.toLowerCase()%>-legend legend\"><% for (var i=0; i<datasets.length; i++){%><li class=\"col-md-3\"><span style=\"background-color:<%=datasets[i].fillColor%>;padding-left: 20px;\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
            ],
            'data' => [
                'labels' => $graf2['labels'],
                'datasets' => $graf2['datasets']
            ]
        ]);
        ?>  
        <div class="col-sm-12" >
            <small><strong><?= Yii::t('backend/index/kpis', 'Radar: ') ?></strong>
                <?= Yii::t('backend/index/kpis', 'Average time (in minutes) that users take in each module') ?></small>
        </div>

    </div>
</div>