<?php
/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

use dosamigos\chartjs\ChartJs;
?>


<div class="jumbotron">
    <h1>
<?= Yii::t('backend/index', 'Graduation Project') ?>
    </h1>

    <p class="lead">
<?= Yii::t('backend/index', 'Management area') ?>
    </p>
</div>