<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kublai\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <?= $form->errorSummary($model)?>
            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'rememberMe')->checkbox() ?>
            <?=
            $form->field($model, 'captcha', [
                'template' => "{input}\n{hint}\n{error}"
            ])->widget(Captcha::className())
            ?>
            <div style="color:#999;margin:1em 0">
                <?=
                Yii::t('backend/user/reset_pass', 'If you forgot your password you can reset it {link}', [
                    'link' => Html::a(Yii::t('backend/actions', 'here'), ['site/request-password-reset'])
                ])
                ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
