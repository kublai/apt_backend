<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use Yii;

/* @var $this custom\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

$this->title = Yii::t('backend/user/reset_pass', 'Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-8">
        <div class="site-request-password-reset">
            <h2 class="border-bottom-base">
                <?= Html::encode($this->title) ?>
            </h2>
            <p>
                <?= Yii::t('backend/user/reset_pass', 'Please fill out your email. A link to reset password will be sent there.') ?>
            </p>
            <div class="row padding-top-30">
                <div class="col-md-6">
                    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                    <?= $form->field($model, 'email') ?>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('backend/actions', 'Send'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
