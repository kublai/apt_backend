<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use common\models\user\Role;

$transFile = 'backend/menu';

if (Yii::$app->user->isGuest === false && Role::check(Role::SUPER_ADMIN, Yii::$app->user->getRole())) {
    $usersMenu = [
        'label' => Yii::t($transFile, 'Users'),
        'items' => [
            ['label' => Yii::t($transFile, 'Users'), 'url' => ['/user/user/index']],
            ['label' => Yii::t($transFile, 'Operators'), 'url' => ['/user/operator/index']],
            ['label' => Yii::t($transFile, 'Admins'), 'url' => ['/user/admin/index']]
        ]
    ];
} else {
    $usersMenu = [
        'label' => Yii::t($transFile, 'Users'),
        'url' => ['/user/user/index']
    ];
}

$menuItems = [$usersMenu];
$subMenuLms = [[
'label' => Yii::t($transFile, 'Campaigns'),
 'url' => ['/lms/campaign/index']
        ]];
if (Yii::$app->user->isGuest === false && 
        (Role::check(Role::ADMIN, Yii::$app->user->getRole()) || 
         Role::check(Role::SUPER_ADMIN, Yii::$app->user->getRole()) ||
         Role::check(Role::OPERATOR, Yii::$app->user->getRole())
        )) {
    $menuItems[] = [
        'label' => Yii::t($transFile, 'Geolocation'),
        'url' => ['/geo/country/index']
    ];
    $subMenuLms[] = ['label' => Yii::t($transFile, 'Modules'), 'url' => ['/lms/module/index']];
}
$menuItems[] = [
    'label' => Yii::t($transFile, 'Lms'),
    'items' => $subMenuLms
];
if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
} else {
    $menuItems[] = [
        'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
        'url' => ['/site/logout'],
        'linkOptions' => ['data-method' => 'post']
    ];
}
return $menuItems;
