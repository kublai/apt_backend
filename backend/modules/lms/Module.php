<?php

namespace backend\modules\lms;

class Module extends \yii\base\Module {

    public $controllerNamespace = 'backend\modules\lms\controllers';

    public function init() {
        parent::init();

        $this->setAliases([
            '@lms' => '@backend/modules/lms',
            '@models' => '@lms/models',
            '@views' => '@lms/views',
        ]);
    }

}
