<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\widgets;

use Yii;
use custom\mapbox\MapboxAsset;
use yii\helpers\Json;

class SessionsMap extends BaseSessionsMap {

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        MapboxAsset::register($this->getView());
    }

    /**
     * @inheritdoc 
     */
    public function getJs() {
        $container = $this->jsOptions['container'];
        $markers = Json::encode($this->markers);
        $aToken = Yii::$app->params['MAPBOX_ACCESS_TOKEN'];
        $this->view->registerJs(<<<SCRIPT
L.mapbox.accessToken = '{$aToken}';
var markersLayer = L.mapbox.featureLayer({$markers});
var map = L.mapbox.map({$container}); 
markersLayer.eachLayer(function(layer) {
    layer.bindPopup(layer.feature.properties.content);
});
markersLayer.on('mouseover', function(e) {
    e.layer.openPopup();
});
markersLayer.on('mouseout', function(e) {
    e.layer.closePopup();
});
L.mapbox.geocoder("mapbox.places")
    .query("{$this->country}", function(err, data) {
    if (data.lbounds) {
        map.fitBounds(data.lbounds);
    } else if (data.latlng) {
        map.setView([data.latlng[0], data.latlng[1]], 13);
    }
    markersLayer.addTo(map);
    map.fitBounds(markersLayer.getBounds());
    map.addLayer(L.mapbox.styleLayer('mapbox://styles/mapbox/streets-v11'));
});
SCRIPT
        );
    }

}
