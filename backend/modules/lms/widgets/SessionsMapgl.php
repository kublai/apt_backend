<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\widgets;

use Yii;
use common\models\lms\StudentSession;
use custom\mapbox\MapboxGlAsset;
use yii\helpers\Json;

class SessionsMapgl extends BaseSessionsMap {

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        MapboxGlAsset::register($this->getView());
    }

    /**
     * @inheritdoc 
     */
    public function getFeature(StudentSession $s) {
        $coords = [$s->longitude, $s->latitude];
        $this->bounds[] = $coords;
        return [
            'type' => 'Feature'
            , 'properties' => [
                'content' => $this->getPopupContent($s)
                , 'marker-symbol' => 'college'
                , 'marker-color' => '#ff0000'
                , 'marker-size' => 'large'
            ]
            , 'geometry' => [
                'type' => 'Point'
                , 'coordinates' => $coords
            ]
        ];
    }

    /**
     * @inheritdoc 
     */
    public function getJs() {
        $opt = Json::encode($this->jsOptions);
        $markers = Json::encode($this->markers);
        $bounds = Json::encode($this->bounds);
        $aToken = Yii::$app->params['MAPBOX_ACCESS_TOKEN'];
        return <<<SCRIPT
mapboxgl.accessToken = '{$aToken}';
var map = new mapboxgl.Map({$opt});
map.on('style.load', function () {
    map.addSource("markers", {
        "type": "geojson",
        "data": {$markers}
    });

    map.addLayer({
        "id": "sessions",
        "interactive": true,
        "type": "symbol",
        "source": "markers",
        "layout": {
            "icon-image": "{marker-symbol}",
            "icon-allow-overlap": true
        }
    });
        
    var popup = new mapboxgl.Popup();
    map.on('click', function (e) {
        map.featuresAt(e.point, {
            radius: 7.5,
            includeGeometry: true,
            layer: 'markers'
        }, function (err, features) {
            if (err || !features.length) {
                popup.remove();
                return;
            }
            var feature = features[0];
            popup.setLngLat(feature.geometry.coordinates)
                .setHTML(feature.properties.content)
                .addTo(map);
        });
    });
        
    map.fitBounds({$bounds});
});
SCRIPT;
    }

}
