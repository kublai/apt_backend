<?php
/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\widgets;

use Yii;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use common\models\lms\StudentSession;
use custom\helpers\Html;
use Faker\Provider\Color;

abstract class BaseSessionsMap extends Widget {

    /**
     *
     * @var array 
     */
    public $options = [];

    /**
     * @var array 
     */
    public $jsOptions = [];

    /**
     *
     * @var string
     */
    public $country;
    
    /**
     *
     * @var StudentSession[]
     */
    protected $sessions;

    /**
     *
     * @var array
     */
    protected $bounds;

    /**
     *
     * @var array 
     */
    protected $markers = [
        'type' => 'FeatureCollection'
        , 'features' => []
    ];
    protected $studentsColor;
    protected $usedColors;

    /**
     * @inheritdoc
     */
    public function init() {
        if ($this->sessions === null) {
            throw new InvalidConfigException('Sessions property must be provided on initialization');
        }
        if (isset($this->options['id']) === false) {
            $this->options['id'] = 'map0';
        }
        $this->jsOptions['container'] = $this->options['id'];
    }

    /**
     * 
     * @param array $s
     */
    public function setSessions(array $s) {
        $this->sessions = $s;
    }

    /**
     * 
     * @return array
     */
    public function getSessions() {
        return $this->sessions;
    }

    /**
     * @inheritdoc
     */
    public function run() {
        echo Html::tag('div', '', $this->options);
        foreach ($this->sessions as $session) {
            if (isset($session->latitude) && isset($session->longitude)) {
                $this->markers['features'][] = $this->getFeature($session);
            }
        }
        $this->registerJs();
    }

    public function generateColor() {
        $color = Color::hexColor();
        if (isset($this->usedColors[$color])) {
            return $this->generateColor();
        }
        return $color;
    }

    /**
     * 
     * @param StudentSession $s
     * @return array
     */
    public function getFeature(StudentSession $s) {
        $coords = [$s->longitude, $s->latitude];
        $this->bounds[] = $coords;
        if (isset($this->studentsColor[$s->student_id]) === false) {
            $this->studentsColor[$s->student_id] = $this->generateColor();
        }
        return [
            'type' => 'Feature'
            , 'properties' => [
                'content' => $this->getPopupContent($s)
                , 'marker-symbol' => 'college'
                , 'marker-color' => $this->studentsColor[$s->student_id]
                , 'marker-size' => 'large'
            ]
            , 'geometry' => [
                'type' => 'Point'
                , 'coordinates' => $coords
            ]
        ];
    }

    /**
     * 
     * @param StudentSession $s
     */
    protected function getPopupContent(StudentSession $s) {
        ob_start();
        ob_implicit_flush(false);
        ?>
        <div class="map-session-popup-content">
            <h4><?= $s->student->user->fullName ?></h4>
            <div>
                <?= Yii::$app->formatter->asDatetime($s->begin_at) ?> 
                - 
                <?= Yii::$app->formatter->asDatetime($s->end_at) ?>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }

    /**
     * @inheritdoc 
     */
    public function registerJs() {
        $this->view->registerJs($this->getJs());
    }

    /**
     * @return string
     */
    abstract public function getJs();
}
