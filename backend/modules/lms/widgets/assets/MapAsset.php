<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\widgets\assets;

use yii\web\AssetBundle;

/**
 * MapAsset for the map widgets.
 * 
 * @authorjl <jl@bytebox.es>
 */
class MapAsset extends AssetBundle {

    /**
     * @inheritdoc
     */
    public $sourcePath = '@backend/modules/lms/widgets/assets/mapAsset';

    /**
     * @inheritdoc
     */
    public $js = [
        'map-asset.js'
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'custom\mapbox\MapboxAsset'
    ];

}
