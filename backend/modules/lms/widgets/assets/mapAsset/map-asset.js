/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
var grad = grad || {};
grad.statsMap = (function (L) {
    var options;
    return {
        "init": function (_options) {
            options = $.extend({}, {
                "token": "",
                "geoJson": {},
                "mapId": "",
                "circles": []
            }, _options);
            return this;
        }
        , "render": function () {
            var i, max, markersLayer, map;
            L.mapbox.accessToken = options.token;
            markersLayer = L.mapbox.featureLayer(options.geoJson);
            map = L.mapbox.map(options.mapId, 'mapbox.streets', {
                minZoom: 6
            });
            markersLayer.eachLayer(function (layer) {
                layer.bindPopup(layer.feature.properties.content);
            });
            markersLayer.on('mouseover', function (e) {
                e.layer.openPopup();
            });
            markersLayer.on('mouseout', function (e) {
                e.layer.closePopup();
            });
            if (options.circles.length) {
                for (i = 0, max = options.circles.length; i < max; i += 1) {
                    L.circle(
                            L.latLng(options.circles[i].coords[0], options.circles[i].coords[1]),
                            options.circles[i].options.radius,
                            options.circles[i].options
                            ).addTo(markersLayer);
                }
            }
            L.mapbox.geocoder("mapbox.places")
                    .query("{$this->country}", function (err, data) {
                        if (data.lbounds) {
                            map.fitBounds(data.lbounds);
                        } else if (data.latlng) {
                            map.setView([data.latlng[0], data.latlng[1]], 13);
                        }
                        markersLayer.addTo(map);
                        map.fitBounds(markersLayer.getBounds());
                    });
        }
    };
}(L));