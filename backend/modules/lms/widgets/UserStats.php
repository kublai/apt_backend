<?php
/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\widgets;

use Yii;
use yii\base\Widget;
use backend\modules\lms\models\StatsViewInterface;
use yii\base\InvalidConfigException;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use dosamigos\chartjs\ChartJs;
use yii\helpers\ArrayHelper;
use backend\modules\lms\widgets\SessionsMap;
use custom\helpers\Time;
use backend\modules\lms\controllers\CampaignController;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
use backend\models\KPIperiodAndCampaign;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

class UserStats extends Widget {

    /**
     *
     * @var bool
     */
    public $loadMap = true;

    /**
     *
     * @var string
     */
    public $url = [];

    /**
     *
     * @var StatsViewInterface
     */
    protected $model;

    /**
     *
     * @var string 
     */
    protected $views;

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        if ($this->model === null) {
            throw new InvalidConfigException('Instructor view model must be provided on initialization');
        }
        $this->ensureView();
    }

    /**
     * Ensures the active view based on the tab parameter.
     */
    protected function ensureView() {
        $this->views = [
            CampaignController::VIEW_TAB_STATS_STUDENT => ''
            , CampaignController::VIEW_TAB_STATS_MODULE => ''
            , CampaignController::VIEW_TAB_STATS_MAIN => ''
        ];
        //$t = count($this->views);
        for ($i = 0, $t = count($this->views); $i < $t; ++$i) {
            if (isset($this->views[Yii::$app->request->get('tab')])) {
                $this->views[Yii::$app->request->get('tab')] = 'active';
                return;
            }
        }
        $this->views[CampaignController::VIEW_TAB_STATS_MAIN] = 'active';
    }

    /**
     * 
     * @param StatsViewInterface $i
     */
    public function setModel(StatsViewInterface $i) {
        $this->model = $i;
    }

    /**
     * 
     * @return StatsViewInterface
     */
    public function getModel() {
        return $this->model;
    }

    /**
     * @inheritdoc
     */
    public function run() {
        ?>
        <div class="mv-10">
            <?php
            $this->drawGraph();
            ?>
        </div>
        <div class="stats padding-top-30">
            <ul class="nav nav-tabs nav-justified mb-20" id="campaign-view-tab">
                <li role="presentation" class="<?= $this->views[CampaignController::VIEW_TAB_STATS_MAIN] ?>" >
                    <a href="#main" aria-controls="home" role="tab" data-toggle="tab">
                        <?= Yii::t('backend/lms/campaign', 'Location of the sessions') ?>
                    </a>
                </li>               
                <li role="presentation" class="<?= $this->views[CampaignController::VIEW_TAB_STATS_MODULE] ?>" >
                    <a href="#module" aria-controls="home" role="tab" data-toggle="tab">
                        <?= Yii::t('backend/lms/campaign', 'Stats by Module') ?>
                    </a>
                </li>
                <li role="presentation" class="<?= $this->views[CampaignController::VIEW_TAB_STATS_STUDENT] ?>" >
                    <a href="#student" aria-controls="home" role="tab" data-toggle="tab">
                        <?= Yii::t('backend/lms/campaign', 'Stats by Student') ?>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane <?= $this->views[CampaignController::VIEW_TAB_STATS_MAIN] ?> clearfix" id="main">
                    <?php
                    echo DetailView::widget([
                        'model' => $this->model,
                        'attributes' => [
                            'studentsWithSessions',
                            'studentsWithoutSessions'
                        ],
                    ]);
                    ?>
                    <div class="">
                        <?= $this->renderMap() ?>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane <?= $this->views[CampaignController::VIEW_TAB_STATS_MODULE] ?> clearfix" id="module">

                    <?php
                    $urlMod = $this->url;
                    $urlMod['tab'] = CampaignController::VIEW_TAB_STATS_MODULE;
                    $modDataProvider = new ActiveDataProvider([
                        'query' => $this->model->moduleStatQuery()
                        , 'pagination' => [
                            'pageParam' => 'modStat_page'
                            , 'pageSizeParam' => 'modStat_per-page'
                        ]
                    ]);
                    $modGridColumns = [
                        ['attribute' => 'code', 'label' => Yii::t('common/lms/module', 'Module Code')]
                        , ['attribute' => 'name', 'label' => Yii::t('common/lms/module', 'Module Name')]
                        , ['attribute' => 'completed', 'label' => Yii::t('common/lms/campaign', 'Sessions number')]
                        , ['attribute' => 'experienced_time', 'value' => function ($model) {
                                return Time::getTimestring($model['experienced_time']);
                            }, 'label' => Yii::t('common/lms/campaign', 'Experienced Time')]
                    ];
                    ?>
                    <div class="clearfix">
                        <div class="pull-right">
                            <label>
                                <?= Yii::t('backend/actions', 'Export Data') ?>:
                            </label> 
                            <?=
                            ExportMenu::widget([
                                'dataProvider' => $modDataProvider
                                , 'columns' => $modGridColumns
                                , 'target' => ExportMenu::TARGET_SELF
                                , 'clearBuffers' => true
                                , 'exportConfig' => [
                                    ExportMenu::FORMAT_TEXT => false,
                                    ExportMenu::FORMAT_PDF => false
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                    <?php
                    Pjax::begin(['options' => ['id' => 'user-stats-module']]);
                    echo GridView::widget([
                        'options' => [
                            'class' => 'grid-view full-selection'
                        ],
                        'dataProvider' => $modDataProvider
                        , 'filterUrl' => $urlMod
                        , 'columns' => $modGridColumns
                        , 'view' => $this->getView()
                    ]);
                    Pjax::end();
                    ?>
                </div>
                <div role="tabpanel" class="tab-pane <?= $this->views[CampaignController::VIEW_TAB_STATS_STUDENT] ?> clearfix" id="student">
                    <?php
                    $urlSt = $this->url;
                    $urlSt['tab'] = CampaignController::VIEW_TAB_STATS_STUDENT;
                    $stdDataProvider = new ActiveDataProvider([
                        'query' => $this->model->studentStatQuery()
                        , 'pagination' => [
                            'pageParam' => 'stdStat_page'
                            , 'pageSizeParam' => 'stdStat_per-page'
                        ]
                    ]);
                    $stdGridColumns = [
                        ['attribute' => 'user_name', 'label' => Yii::t('common/lms/user', 'Student')],
                        ['attribute' => 'code', 'label' => Yii::t('common/lms/module', 'Module Code')],
                        ['attribute' => 'name', 'label' => Yii::t('common/lms/module', 'Module Name')],
                        ['attribute' => 'completed', 'label' => Yii::t('common/lms/campaign', 'Completed')],
                        ['attribute' => 'experienced_time', 'value' => function ($model) {
                                return Time::getTimestring($model['experienced_time']);
                            }, 
                            'label' => Yii::t('common/lms/campaign', 'Experienced Time')],
                        ['attribute'=> 'session_date', 'value' => function($model){
                                $aux = new \DateTime($model['session_date']);
                                return $aux->format("d/m/Y H:i:s");
                            },'label' => Yii::t('common/lms/campaign', 'Session date') ]
                    ];
                    ?>
                    <div class="clearfix">
                        <div class="pull-right">
                            <label>
                                <?= Yii::t('backend/actions', 'Export Data') ?>:
                            </label> 
                            <?=
                            ExportMenu::widget([
                                'dataProvider' => $stdDataProvider
                                , 'columns' => $stdGridColumns
                                , 'target' => ExportMenu::TARGET_SELF
                                , 'clearBuffers' => true
                                , 'exportConfig' => [
                                    ExportMenu::FORMAT_TEXT => false,
                                    ExportMenu::FORMAT_PDF => false
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                    <?php
                    Pjax::begin([
                        'options' => ['id' => 'user-stats-student']
                        , 'timeout' => 3000
                    ]);
                    echo GridView::widget([
                        'options' => [
                            'class' => 'grid-view full-selection'
                        ]
                        , 'dataProvider' => $stdDataProvider
                        , 'filterUrl' => $urlSt
                        , 'columns' => $stdGridColumns
                        , 'view' => $this->getView()
                    ]);
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
        <?php
        //$this->drawGraph();
    }

    /**
     * Renders the map component of the stats.
     * 
     * @return string
     */
    protected function renderMap() {
        return SessionsMap::widget([
                    'sessions' => $this->model->getStudentSessions()->onlyLocalized()->all()
                    , 'country' => $this->model->getCampaign()->location->country->name
                    , 'options' => [
                        'class' => 'clearfix'
                        , 'style' => 'height:400px;'
                    ]
                    , 'jsOptions' => [
                        'container' => 'map'
                        , 'style' => 'mapbox://styles/bytebox/ciky6rbjl00ljbrlya04bb4xa'
                        , 'zoom' => 9
                    ]
                        ]
        );
    }

    /**
     * Draws the instructor statictics graph.
     */
    protected function drawGraph() {
        $kpi = new KPIperiodAndCampaign();
        $type= 0; //no type defined by default, 1 -> Operator graph, 2 -> Instructor graph
        
         
        $kpi->idCampaign = Yii::$app->getRequest()->getQueryParam("campaignId");
        $kpi->loadCampaign();
        $kpi->operatorId = Yii::$app->getRequest()->getQueryParam("operatorId");
        $kpi->instructorId = Yii::$app->getRequest()->getQueryParam("instructorId");
        
        if ($kpi->operatorId != null){
            $kpi->loadOperator(); 
            $type = 1; //getOperatorGraphData
        }
        
        if ($kpi->instructorId !=null){
              $operator = \common\models\lms\Instructor::findOne(['user_id'=>$kpi->instructorId, 'campaign_id'=>$kpi->idCampaign ]);
              if ($operator!=null){
                  $kpi->operatorId = $operator->operator_id;
                   $kpi->loadOperator(); 
                   $type = 2; //getInstructorGraphData
              }
        }
        
        //Getting the module ID
        //control to check if there is a valid periodAnCampaign value
        if(Yii::$app->getRequest()->post('KPIperiodAndCampaign') != null){
            $kpi->idModule = Yii::$app->getRequest()->post('KPIperiodAndCampaign')['idModule'];
            if ($kpi->idModule === null) {
                $modulesIds = ArrayHelper::getColumn($kpi->getCampaign()->getModules()->all(), 'id');
                if (sizeof($modulesIds)>0){
                    $kpi->idModule = $modulesIds[0];
                }
            }
        }else{
            $modulesIds = ArrayHelper::getColumn($kpi->getCampaign()->getModules()->all(), 'id');
                if (sizeof($modulesIds)>0){
                    $kpi->idModule = $modulesIds[0];
                }   
        }
        

        switch ($type){
            case 1:
                 $graphData = $kpi->getOperatorGraphDataV2(); 
                 break;
            case 2:
                 $graphData = $kpi->getInstructorGraphData();
                break;
            default:
                return;
        }
        
        
        ?>
        <div class="row">
            <div class="col-sm-12" >
                <h3><?= Yii::t('backend/index/kpis', 'Global progress by instructor') ?></h3>
                <div class="row">
                    <?php if ($type ==1): ?>
                    <div class="col-sm-6">
                        <?php
                        $items = ArrayHelper::map($kpi->getCampaign()->getModules()->all(), 'id', 'name');
                        $modulesLbl = Yii::t('backend/index/kpis', 'Select module');
                        $form = ActiveForm::begin(['id' => 'module-form']);
                        ?>
                        <?= $form->field($kpi, 'idModule')->dropDownList($items)->label($modulesLbl) ?>
                     </div>
                </div>
                <div class="row">
                     <div class="col-sm-2">
                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('backend/index', 'Apply'), ['class' => 'form-control btn btn-primary ']) ?>
                        </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                </div>
                <?php endif; ?>
                <div id="oprGraf1-legend" class="legend"></div>

                <canvas id="oprGraf1" style="width:100%; max-height: 500px"></canvas>
            </div>
            <?php
            ChartJs::widget([
                'type' => 'Bar',
                'options' => [
                    'id' => 'oprGraf1'
                ],
                'clientOptions' => [
                    'scaleOverride' => true,
                    'scaleSteps' => $graphData['scaleSteps'],
                    'scaleStepWidth' => $graphData['scaleStepsWidth'],
                    'scaleStartValue' => $graphData['scaleStartValue'],
                    'legendTemplate' => "<ul class=\"opr-<%=name.toLowerCase()%>-legend legend\"><% for (var j=0; j<datasets.length; j++){%><li class=\"col-md-3\"><span style=\"background-color:<%=datasets[j].fillColor%>;padding-left: 20px;\"></span>&nbsp;<%if(datasets[j].label){%><%=datasets[j].label%><%}%></li><%}%></ul>"
                ],
                'data' => [
                    'labels' => $graphData['labels'],
                    'datasets' => $graphData['datasets']
                ]
            ]);
            ?>

        </div>
        <div class="row">
            <div class="col-sm-12" >
                <small><strong><?= Yii::t('backend/index/kpis', 'X axis: ') ?></strong>
                    <?= Yii::t('backend/index/kpis', 'Code modules that are in the project') ?></small>
            </div>
            <div class="col-sm-12" >
                <small><strong><?= Yii::t('backend/index/kpis', 'Y axis: ') ?></strong>
                    <?= Yii::t('backend/index/kpis', 'Number of users asigned to each manager.') ?>
                </small>
            </div>
            <div class="col-sm-12" >
                <small><strong><?= Yii::t('backend/index/kpis', 'Note: ') ?></strong>
                    <?= Yii::t('backend/index/kpis', 'This graph show th actual number of asigned users to this instructor. Users without sessions are not showed here. If a user has completed a module more than once counts as one time only') ?>
                </small>
            </div>
        </div>
        <?php
    }

}
