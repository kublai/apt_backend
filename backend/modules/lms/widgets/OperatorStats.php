<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\widgets;

use yii\base\Widget;
use backend\modules\lms\widgets\OperatorMap;
use backend\modules\lms\models\OperatorView;

/**
 * 
 */
class OperatorStats extends UserStats {

    /**
     *
     * @var OperatorView
     */
    protected $model;

    /**
     * @inheritdoc
     */
    protected function renderMap() {
        return OperatorMap::widget([
                    'data' => $this->model->getMapStats()->all()
                    , 'country' => $this->model->getCampaign()->location->country->name
                    , 'options' => [
                        'class' => 'clearfix'
                        , 'style' => 'height:600px;'
                    ]
                    , 'jsOptions' => [
                        'container' => 'map'
                        , 'style' => 'mapbox://styles/bytebox/ciky6rbjl00ljbrlya04bb4xa'
                        , 'zoom' => 9
                    ]
                        ]
        );
    }

}
