<?php
/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\widgets;

use Yii;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use common\models\lms\StudentSession;
use custom\helpers\Html;
use Faker\Provider\Color;
use yii\helpers\Json;
use common\models\user\User;
use backend\modules\lms\widgets\assets\MapAsset;

class OperatorMap extends Widget {

    const EARTH_RADIUS = 6371000;

    /**
     *
     * @var array 
     */
    public $options = [];

    /**
     * @var array 
     */
    public $jsOptions = [];

    /**
     *
     * @var string
     */
    public $country;

    /**
     *
     * @var array
     */
    public $data;

    /**
     *
     * @var bool
     */
    public $render = true;
    
    /**
     *
     * @var array
     */
    protected $bounds;

    /**
     *
     * @var array 
     */
    protected $markers = [
        'type' => 'FeatureCollection'
        , 'features' => []
    ];
    protected $circles = [];
    protected $color;
    protected $usedColors;

    /**
     * @inheritdoc
     */
    public function init() {
        if ($this->data === null) {
            throw new InvalidConfigException('Sessions property must be provided on initialization');
        }
        if (isset($this->options['id']) === false) {
            $this->options['id'] = 'map0';
        }
        $this->jsOptions['container'] = $this->options['id'];
        MapAsset::register($this->getView());
    }

    /**
     * @inheritdoc
     */
    public function run() {
        echo Html::tag('div', '', $this->options);
        $this->loadData();
        $this->initJs();
        if ($this->render === true) {
            $this->registerJs();
        }
    }

    public function loadData() {
        foreach ($this->data as $session) {
            if (isset($session['latitude']) && isset($session['longitude'])) {
                $this->markers['features'][] = $this->getFeature($session);
                $this->circles[] = $this->getCircle($session);
            }
        }
    }

    public function generateColor() {
        $color = Color::hexColor();
        if (isset($this->usedColors[$color])) {
            return $this->generateColor();
        }
        return $color;
    }

    public function getCircle($s) {
        $coords = [(float) $s['latitude'], (float) $s['longitude']];
        $stDevLat = (float) $s['stDevLat'];
        $stDevLon = (float) $s['stDevLon'];
        if ($stDevLat > $stDevLon) {
            $to = $coords;
            $to[0] += $stDevLat;
        } else {
            $to = $coords;
            $to[1] += $stDevLon;
        }
        $r = static::getDistanceBetweenPoints([$coords[1], $coords[0]], [$to[1], $to[0]]);
        if ($r < 10) {
            $r = 10;
        }
        return [
            'coords' => $coords
            , 'options' => [
                'radius' => $r
                , 'color' => $this->color[$s['user_id']]
            ]
        ];
    }

    /**
     * 
     * @param StudentSession $s
     * @return array
     */
    public function getFeature($s) {
        $coords = [(float) $s['longitude'], (float) $s['latitude']];
        $this->bounds[] = $coords;
        if (isset($this->color[$s['user_id']]) === false) {
            $this->color[$s['user_id']] = $this->generateColor();
        }
        return [
            'type' => 'Feature'
            , 'properties' => [
                'content' => $this->getPopupContent($s)
                , 'marker-symbol' => 'college'
                , 'marker-color' => $this->color[$s['user_id']]
                , 'marker-size' => 'large'
            ]
            , 'geometry' => [
                'type' => 'Point'
                , 'coordinates' => $coords
            ]
        ];
    }

    /**
     * 
     * @param User $u
     */
    protected function getPopupContent($s) {
        $u = User::findOne($s['user_id']);
        ob_start();
        ob_implicit_flush(false);
        ?>
        <div class="map-session-popup-content">
            <h4><?= $u->fullName ?></h4>
            <div>
                <strong><?= Yii::t('common/lms/campaign', 'Sessions number') ?>: </strong>
                <?= $s['n'] ?>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }

    public function initJs() {
        $options = Json::encode([
                    'token' => Yii::$app->params['MAPBOX_ACCESS_TOKEN']
                    , 'geoJson' => $this->markers
                    , 'mapId' => $this->jsOptions['container']
                    , 'circles' => $this->circles
        ]);
        $this->view->registerJs(<<<SCRIPT
grad.statsMap.init({$options});
SCRIPT
        );
    }

    /**
     * @inheritdoc 
     */
    public function registerJs() {
        $this->view->registerJs(<<<SCRIPT
grad.statsMap.render();
SCRIPT
        );
    }

    /**
     * Returns the distance between two points in meters.
     * 
     * @param array $a
     * @param array $b
     * @return float
     */
    public static function getDistanceBetweenPoints(array $a, array $b) {
        $latFrom = deg2rad($b[0]);
        $lonFrom = deg2rad($b[1]);
        $latTo = deg2rad($a[0]);
        $lonTo = deg2rad($a[1]);
        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;
        return 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2))) * self::EARTH_RADIUS;
    }

}
