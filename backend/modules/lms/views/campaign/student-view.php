<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use yii\grid\GridView;
use custom\helpers\Html;
use yii\grid\SerialColumn;
use backend\modules\lms\controllers\CampaignController;
use yii\grid\ActionColumn;
use backend\modules\lms\widgets\SessionsMap;

/* @var $this yii\web\View */
/* @var $campaign common\models\lms\Campaign */
/* @var $student common\models\lms\Student|null */
/* @var $searchModel backend\modules\lms\models\SearchStudentSession */
/* @var $isOneStudentView bool Defines whether the view is for a particular student or is for all the campaign students */

$isOneStudentView = $student !== null;
$this->title = Yii::t('common/lms/campaign', 'Campaign {name}', [
            'name' => $campaign->name
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/lms/campaign', 'Campaigns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $campaign->name, 'url' => ['view', 'id' => $campaign->id]];

if ($isOneStudentView) {
    $this->title .= ' - ' . $student->user->getFullName();
    $this->params['breadcrumbs'][] = Html::encode($student->user->getFullName());
} else {
    $this->params['breadcrumbs'][] = Yii::t('backend/actions', '{modelClass} List', [
                'modelClass' => Yii::t('common/lms/campaign', 'Sessions')
    ]);
}

$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$query = clone $dataProvider->query;
$returnUrl = Yii::$app->request->get('returnUrl') ? Yii::$app->request->get('returnUrl') : (Yii::$app->request->referrer !== null ? Yii::$app->request->referrer . '&tab=' . CampaignController::VIEW_TAB_STUDENTS : ['view', 'id' => $campaign->id, 'tab' => CampaignController::VIEW_TAB_STUDENTS]);
?>
<div class="campaign-index">
    <div class="clearfix">
        <?=
        Html::a(
                Html::tag('i', '', ['class' => 'glyphicon glyphicon-arrow-left']) . ' ' . Yii::t('backend/actions', 'Return')
                , $returnUrl
                , ['class' => 'btn btn-warning pull-right']
        )
        ?>
        <?php if ($isOneStudentView) : ?>
            <?=
            Html::a(
                    Html::tag('i', '', ['class' => 'glyphicon glyphicon-trash']) . ' ' . Yii::t('backend/lms/campaign', 'Delete all sessions')
                    , ['delete-student-sessions', 'campaignId' => $campaign->id, 'studentId' => $student->user_id, 'returnUrl' => $returnUrl]
                    , [
                'class' => 'btn btn-danger pull-right mh-20'
                , 'data' => [
                    'confirm' => Yii::t('backend/lms/campaign', 'Are you sure you want to delete all the sessions of this student?')
                    , 'method' => 'post'
                ]
                    ]
            )
            ?>
        <?php endif; ?>
    </div>
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('search/_studentsession', ['model' => $searchModel]); ?>
   
    <?php if ($isOneStudentView) : ?>
        <div class="pv-30">
            <h3><?= Yii::t('backend/lms/campaign', 'Location of the sessions') ?></h3>
            <?=
            SessionsMap::widget([
                'sessions' => $query->onlyLocalized()->all()
                , 'country' => $campaign->location->country->name
                , 'options' => [
                    'class' => 'clearfix'
                    , 'style' => 'height:400px;'
                ]
                , 'jsOptions' => [
                    'container' => 'map'
                    , 'style' => 'mapbox://styles/bytebox/ciky6rbjl00ljbrlya04bb4xa'
                    , 'zoom' => 6
                ]
                    ]
            )
            ?>
        </div>
    <?php endif; ?>
    
    <?php
    
    $deleteButton = function ($url, $model) use ($returnUrl) {
        $options = [
            'title' => Yii::t('yii', 'Delete'),
            'aria-label' => Yii::t('yii', 'Delete'),
            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'data-method' => 'post',
            'data-pjax' => '0',
        ];
        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete-student-session', 'id' => $model->id, 'returnUrl' => $returnUrl], $options);
    };
    $cols = [
        'module.code'
        , 'module.name'
        , 'begin_at:datetime'
        , 'end_at:datetime'
        , 'completed:boolean'
        , 'utility'
        , 'difficulty'
        , 'location.latitude'
        , 'location.longitude'
        , 'updated_at:datetime'
    ];
    if ($isOneStudentView === false) {
        array_pop($cols);
        $cols = array_merge([
            'student.instructor.fullName'
            , 'student.user.fullName'
                ], $cols);
    }
    $cols[] = [
        'class' => ActionColumn::className()
        , 'template' => '{delete} '
        , 'buttons' => [
            'delete' => $deleteButton
        ]
    ];
    //var_dump($dataProviderNoGeoInfo->query->all());die;
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $cols,
    ]);
    ?>
</div>