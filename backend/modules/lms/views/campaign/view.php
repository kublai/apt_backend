<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use yii\web\View;
use custom\helpers\Html;
use common\models\user\Role;
use backend\modules\lms\assets\CampaignAsset;
use backend\modules\lms\models\Campaign,
    backend\modules\lms\models\OperatorView,
    backend\modules\lms\models\SearchStudent,
    backend\modules\lms\models\SearchOperator,
    backend\modules\lms\models\SearchInstructor;
use backend\modules\lms\controllers\CampaignController;

/* @var $this View */
/* @var $model Campaign */
/* @var $searchStudent SearchStudent */
/* @var $searchOperator SearchOperator */
/* @var $searchInstructor SearchInstructor */
/* @var $operatorView OperatorView|null */

CampaignAsset::register($this);
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/lms/campaign', 'Campaigns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$vIndex = $vModules = $vOperators = $vInstructors = $vStudents = '';
switch (Yii::$app->request->get('tab')) {
    case CampaignController::VIEW_TAB_MODULES:
        $vModules = 'active';
        break;
    case CampaignController::VIEW_TAB_OPERATORS:
        $vOperators = 'active';
        break;
    case CampaignController::VIEW_TAB_INSTRUCTORS:
        $vInstructors = 'active';
        break;
    case CampaignController::VIEW_TAB_STUDENTS:
        $vStudents = 'active';
        break;
    default:
        $vIndex = 'active';
        break;
}
?>
<div class="campaign-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <ul class="nav nav-tabs nav-justified" id="campaign-view-tab">
        <li role="presentation" class="<?= $vIndex ?>">
            <a href="#info" aria-controls="home" role="tab" data-toggle="tab"><?= Yii::t('backend/actions', 'Info') ?></a>
        </li>
        <li role="presentation" class="<?= $vModules ?>"><a href="#modules" aria-controls="home" role="tab" data-toggle="tab"><?= Yii::t('common/lms/module', 'Modules') ?></a></li>
        <?php if (Role::isOperator(Yii::$app->user) === false) : ?>
            <li role="presentation" class="<?= $vOperators ?>"><a href="#operators" aria-controls="home" role="tab" data-toggle="tab"><?= Yii::t('common/user/user', 'Operators') ?></a></li>
        <?php else: ?>
            <li role="presentation"><a href="#myStats" aria-controls="home" role="tab" data-toggle="tab"><?= Yii::t('common/user/user', 'Instructors Activity') ?></a></li>
        <?php endif; ?>
        <li role="presentation" class="<?= $vInstructors ?>"><a href="#instructors" aria-controls="home" role="tab" data-toggle="tab"><?= Yii::t('common/user/user', 'Instructors') ?></a></li>
        <li role="presentation" class="<?= $vStudents ?>"><a href="#students" aria-controls="home" role="tab" data-toggle="tab"><?= Yii::t('common/user/user', 'Students') ?></a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane <?= $vIndex ?> clearfix" id="info">
            <?=
            $this->render('tabs/_info', [
                'model' => $model
            ])
            ?>
        </div>
        <div role="tabpanel" class="tab-pane clearfix <?= $vModules ?>" id="modules">
            <?=
            $this->render('tabs/_modules', [
                'model' => $model
            ])
            ?>
        </div>
        <?php if (Role::isOperator(Yii::$app->user) === false) : ?>
            <div role="tabpanel" class="tab-pane clearfix <?= $vOperators ?>" id="operators">
                <?=
                $this->render('tabs/_operators', [
                    'model' => $model
                    , 'searchModel' => $searchOperator
                ])
                ?>
            </div>
        <?php else: ?>
            <div role="tabpanel" class="tab-pane" id="myStats">
                <?=
                $this->render('tabs/_operator', [
                    'model' => $model
                    , 'viewModel' => $operatorView
                ])
                ?>
            </div>
        <?php endif; ?>
        <div role="tabpanel" class="tab-pane clearfix <?= $vInstructors ?>" id="instructors">
            <?=
            $this->render('tabs/_instructors', [
                'model' => $model
                , 'searchModel' => $searchInstructor
            ])
            ?>
        </div>
        <div role="tabpanel" class="tab-pane clearfix <?= $vStudents ?>" id="students">
            <?=
            $this->render('tabs/_students', [
                'model' => $model
                , 'searchModel' => $searchStudent
            ])
            ?>
        </div>
    </div>
</div>