<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use yii\web\View;
use custom\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use common\models\lms\Student;
use backend\modules\lms\models\Campaign;
use backend\modules\lms\models\SearchStudent;
use backend\modules\lms\controllers\CampaignController;

/* @var $this View */
/* @var $model Campaign */
/* @var $searchModel SearchStudent */

$operatorFullName = function($sModel) use ($model) {
    return Student::findOne(['user_id' => $sModel->id, 'campaign_id' => $model->id])->operator->fullName;
};
$instructorFullName = function($sModel) use ($model) {
    return Student::findOne(['user_id' => $sModel->id, 'campaign_id' => $model->id])->instructor->fullName;
};
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$columns = [
    'id'
    , [
        'label' => Yii::t('common/user/user', 'Operator'),
        'value' => $operatorFullName,
        'attribute' => 'operatorName'
    ]
    , [
        'label' => Yii::t('common/user/user', 'Instructor'),
        'value' => $instructorFullName,
        'attribute' => 'instructorName'
    ]
    , [
        'label' => Yii::t('common/geo/region', 'Region'),
        'value' => 'location.region.name',
        'attribute' => 'regionName'
    ]
    , 'username'
    , 'name'
    , 'last_name'
];
?>
<div class="col-xs-12 buttons">
    <div class="clearfix pv-20">
        <div class="pull-right">
            <label>
                <?= Yii::t('backend/actions', 'Export Data') ?>:
            </label> 
            <?=
            ExportMenu::widget([
                'dataProvider' => $dataProvider
                , 'columns' => $columns
                , 'target' => ExportMenu::TARGET_SELF
                , 'clearBuffers' => true
                , 'exportConfig' => [
                    ExportMenu::FORMAT_TEXT => false,
                    ExportMenu::FORMAT_PDF => false
                ]
            ]);
            ?>
        </div>
        <div class="pull-right mh-20">
            <?=
            Html::a(Yii::t('backend/actions', '{modelClass} List', [
                        'modelClass' => Yii::t('common/lms/campaign', 'Sessions')
                    ]), [
                'student-view',
                'campaignId' => $model->id
                    ], [
                'class' => 'btn btn-primary pull-right',
            ])
            ?>
        </div>
    </div>
</div>
<div class="col-xs-12 content">
    <?php
    $deleteButton = function($url, $iModel) use ($model) {
        $options = array_merge([
            'title' => Yii::t('backend/actions', 'Delete'),
            'aria-label' => Yii::t('backend/actions', 'Delete'),
            'data' => [
                'confirm' => Yii::t('backend/actions', 'Are you sure you want to delete this item?')
            ]
        ]);
        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['/lms/campaign/delete-student', 'campaignId' => $model->id, 'studentId' => $iModel->id], $options);
    };
    $changeInsButton = function ($url, $sModel) use ($model) {
        $options = array_merge([
            'title' => Yii::t('backend/actions', 'Change {modelClass}', [
                'modelClass' => Yii::t('common/lms/user', 'Instructor')
            ]),
            'aria-label' => Yii::t('backend/actions', 'Change {modelClass}', [
                'modelClass' => Yii::t('common/lms/user', 'Instructor')
            ])
            , 'data-pjax' => '0'
        ]);
        return Html::a('<span class="glyphicon glyphicon-user"></span>', ['/lms/campaign/student-instructor', 'campaignId' => $model->id, 'studentId' => $sModel->id], $options);
    };
    $listButton = function ($url, $sModel) use ($model) {
        $options = array_merge([
            'title' => Yii::t('backend/actions', '{modelClass} List', [
                'modelClass' => Yii::t('common/lms/campaign', 'Sessions')
            ]),
            'aria-label' => Yii::t('backend/actions', '{modelClass} List', [
                'modelClass' => Yii::t('common/lms/campaign', 'Sessions')
            ])
            , 'data-pjax' => '0'
        ]);
        return Html::a('<span class="glyphicon glyphicon-th-list"></span>', ['/lms/campaign/student-view', 'campaignId' => $model->id, 'studentId' => $sModel->id], $options);
    };
    $columns[] = [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{list} {changeIns} {delete}',
        'buttons' => [
            'delete' => $deleteButton,
            'changeIns' => $changeInsButton,
            'list' => $listButton
        ]
    ];
    echo GridView::widget([
        'dataProvider' => $dataProvider
        , 'filterModel' => $searchModel
        , 'filterUrl' => ['view', 'id' => $model->id, 'tab' => CampaignController::VIEW_TAB_STUDENTS]
        , 'columns' => $columns
    ]);
    ?>
</div>