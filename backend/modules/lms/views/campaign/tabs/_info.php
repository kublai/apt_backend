<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use yii\web\View;
use custom\helpers\Html;
use yii\widgets\DetailView;
use common\models\user\Role,
    common\models\user\User;
use backend\modules\lms\models\Campaign;

/* @var $this View */
/* @var $model Campaign */
?>
<?php if (Role::isOperator(Yii::$app->user) === false) : ?>
    <p class="clearfix">
        <?=
        Html::a(Yii::t('backend/actions', 'Update'), [
            'update', 'id' => $model->id
                ], [
            'class' => 'btn btn-primary pull-right'
        ])
        ?>
    </p>
<?php endif; ?>
<div class="col-xs-12 content">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id'
            , 'location.country.name'
            , 'name'
            , 'begin_at:datetime'
            , 'end_at:datetime'
            , 'created_at:datetime'
            , [
                'attribute' => 'created_by'
                , 'value' => ($user = User::findOne($model->created_by)) ? $user->username : null
            ]
            , 'updated_at:datetime'
            , [
                'attribute' => 'updated_by'
                , 'value' => ($user = User::findOne($model->updated_by)) ? $user->username : null
            ]
        ],
    ])
    ?>
</div>