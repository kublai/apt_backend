<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use yii\web\View;
use custom\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use backend\modules\lms\models\Campaign;
use backend\modules\lms\models\SearchOperator;
use backend\modules\lms\controllers\CampaignController;

/* @var $this View */
/* @var $model Campaign */
/* @var $searchModel SearchOperator */
$columns = [
    'username',
    'name',
    'last_name',
];
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
?>
<div class="col-xs-12 buttons">
    <div class="clearfix pv-20">
        <div class="pull-right">
            <label>
                <?= Yii::t('backend/actions', 'Export Data') ?>:
            </label> 
            <?=
            ExportMenu::widget([
                'dataProvider' => $dataProvider
                , 'columns' => $columns
                , 'target' => ExportMenu::TARGET_SELF
                , 'clearBuffers' => true
                , 'exportConfig' => [
                    ExportMenu::FORMAT_TEXT => false,
                    ExportMenu::FORMAT_PDF => false
                ]
            ]);
            ?>
        </div>
        <div class="pull-right mh-20">
            <?=
            Html::a(Yii::t('backend/actions', 'Add {modelClass}', [
                        'modelClass' => Yii::t('common/user/user', 'Operators')
                    ]), [
                'operator-list',
                'campaignId' => $model->id
                    ], [
                'class' => 'btn btn-primary pull-right',
            ])
            ?>
        </div>
    </div>
</div>
<div class="col-xs-12 content">
    <?php
    $listButton = function ($url, $oModel) use ($model) {
        $options = [
            'title' => Yii::t('backend/actions', 'Add {modelClass}', [
                'modelClass' => Yii::t('common/user/user', 'Instructors')
            ]),
            'aria-label' => Yii::t('backend/actions', 'Add {modelClass}', [
                'modelClass' => Yii::t('common/user/user', 'Instructors')
            ])
            , 'data-pjax' => '0'
        ];
        return Html::a('<span class="glyphicon glyphicon-plus"></span>', ['/lms/campaign/instructor-list', 'campaignId' => $model->id, 'operatorId' => $oModel->id], $options);
    };
    $statsButton = function($url, $oModel) use ($model) {
        $options = array_merge([
            'title' => Yii::t('backend/lms/campaign', 'Stats'),
            'aria-label' => Yii::t('backend/lms/campaign', 'Stats')
        ]);
        return Html::a('<span class="glyphicon glyphicon-stats"></span>', ['operator-view', 'campaignId' => $model->id, 'operatorId' => $oModel->id], $options);
    };
    $deleteButton = function($url, $oModel) use ($model) {
        $options = array_merge([
            'title' => Yii::t('backend/actions', 'Delete'),
            'aria-label' => Yii::t('backend/actions', 'Delete'),
            'data' => [
                'confirm' => Yii::t('backend/actions', 'Are you sure you want to delete this item?')
            ]
        ]);
        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['/lms/campaign/delete-operator', 'campaignId' => $model->id, 'operatorId' => $oModel->id], $options);
    };
    $columns[] = [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{list} {stats} {delete}',
        'buttons' => [
            'list' => $listButton,
            'stats' => $statsButton,
            'delete' => $deleteButton
        ]
    ];
    echo GridView::widget([
        'dataProvider' => $dataProvider
        , 'filterModel' => $searchModel
        , 'filterUrl' => ['view', 'id' => $model->id, 'tab' => CampaignController::VIEW_TAB_OPERATORS]
        , 'columns' => $columns
    ]);
    ?>
</div>