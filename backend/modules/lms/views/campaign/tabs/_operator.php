<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use yii\web\View;
use backend\modules\lms\models\Campaign;
use backend\modules\lms\models\OperatorView;
use backend\modules\lms\widgets\OperatorMap;

/* @var $this View */
/* @var $model Campaign */
/* @var $viewModel OperatorView */
?>
<div class="col-xs-12 content clearfix">
    <?php
    echo OperatorMap::widget([
        'data' => $viewModel->getMapStats()->all()
        , 'country' => $model->location->country->name
        , 'options' => [
            'class' => 'clearfix'
            , 'style' => 'height:500px;'
        ]
        , 'render' => false
            ]
    );
    $this->registerJs(<<<SCRIPT
$('a[href="#myStats"][data-toggle="tab"]').on('shown.bs.tab', function (e) {
    grad.statsMap.render();
});
SCRIPT
    );
    ?>
</div>