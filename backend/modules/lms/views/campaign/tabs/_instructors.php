<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use yii\web\View;
use custom\helpers\Html;
use kartik\grid\GridView;
use common\models\user\Role,
    common\models\user\User;
use kartik\export\ExportMenu;
use backend\modules\lms\models\Campaign;
use backend\modules\lms\models\SearchInstructor;
use backend\modules\lms\controllers\CampaignController;

/* @var $this View */
/* @var $model Campaign */
/* @var $searchModel SearchInstructor */

$operatorFullName = function($iModel) use ($model) {
    return User::find()
                    ->innerJoin('lms_operator lo', 'lo.operator_id = user.id')
                    ->innerJoin('lms_instructor li', 'li.operator_id = lo.operator_id AND li.campaign_id = lo.campaign_id')
                    ->andWhere(['lo.campaign_id' => $model->id, 'li.user_id' => $iModel->id])
                    ->one()
            ->fullName;
};
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$columns = [
    [
        'label' => Yii::t('common/user/user', 'Operator'),
        'value' => $operatorFullName,
        'attribute' => 'operatorName'
    ]
    , 'username'
    , 'name'
    , 'last_name'
];
?>
<div class="col-xs-12 buttons">
    <div class="clearfix pv-20">
        <div class="pull-right">
            <label>
                <?= Yii::t('backend/actions', 'Export Data') ?>:
            </label> 
            <?=
            ExportMenu::widget([
                'dataProvider' => $dataProvider
                , 'columns' => $columns
                , 'target' => ExportMenu::TARGET_SELF
                , 'clearBuffers' => true
                , 'exportConfig' => [
                    ExportMenu::FORMAT_TEXT => false,
                    ExportMenu::FORMAT_PDF => false
                ]
            ]);
            ?>
        </div>
        <?php if (Role::isOperator(Yii::$app->user)) : ?>
            <div class="pull-right mh-20">
                <?=
                Html::a(Yii::t('backend/actions', 'Add {modelClass}', [
                            'modelClass' => Yii::t('common/user/user', 'Instructors')
                        ]), [
                    '/lms/campaign/instructor-list',
                    'campaignId' => $model->id,
                    'operatorId' => Yii::$app->user->id
                        ], [
                    'class' => 'btn btn-primary pull-right',
                ])
                ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="col-xs-12 content">
    <?php
    $listButton = function ($url, $iModel) use ($model) {
        $options = array_merge([
            'title' => Yii::t('backend/actions', 'Add {modelClass}', [
                'modelClass' => Yii::t('common/lms/user', 'Students')
            ]),
            'aria-label' => Yii::t('backend/actions', 'Add {modelClass}', [
                'modelClass' => Yii::t('common/lms/user', 'Students')
            ])
            , 'data-pjax' => '0'
        ]);
        return Html::a('<span class="glyphicon glyphicon-plus"></span>', ['/lms/campaign/student-list', 'campaignId' => $model->id, 'operatorId' => $iModel->operator_id, 'instructorId' => $iModel->id], $options);
    };
    $statsButton = function($url, $iModel) use ($model) {
        $options = array_merge([
            'title' => Yii::t('backend/lms/campaign', 'Stats'),
            'aria-label' => Yii::t('backend/lms/campaign', 'Stats')
        ]);
        return Html::a('<span class="glyphicon glyphicon-stats"></span>', ['instructor-view', 'campaignId' => $model->id, 'instructorId' => $iModel->id], $options);
    };
    $csvButton = function ($url, $iModel) use ($model) {
        $options = array_merge([
            'title' => Yii::t('backend/actions', 'Integrate CSV'),
            'aria-label' => Yii::t('backend/actions', 'Integrate CSV')
            , 'data-pjax' => '0'
        ]);
        return Html::a('<span class="glyphicon glyphicon-file"></span>', ['/lms/campaign/integrate-instructor-csv', 'campaignId' => $model->id, 'instructorId' => $iModel->id], $options);
    };
    $logButton = function ($url, $iModel) use ($model) {
        $options = array_merge([
            'title' => Yii::t('backend/lms/campaign', 'Synchronization Log'),
            'aria-label' => Yii::t('backend/lms/campaign', 'Synchronization Log')
            , 'data-pjax' => '0'
        ]);
        return Html::a('<span class="glyphicon glyphicon-sort"></span>', ['/lms/campaign/instructor-synclog', 'campaignId' => $model->id, 'instructorId' => $iModel->id], $options);
    };
    $deleteButton = function($url, $iModel) use ($model) {
        $options = array_merge([
            'title' => Yii::t('backend/actions', 'Delete'),
            'aria-label' => Yii::t('backend/actions', 'Delete'),
            'data' => [
                'confirm' => Yii::t('backend/actions', 'Are you sure you want to delete this item?')
            ]
        ]);
        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['/lms/campaign/delete-instructor', 'campaignId' => $model->id, 'instructorId' => $iModel->id], $options);
    };
    $columns[] = [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{list} {stats} {csv} {log} {delete}',
        'buttons' => [
            'list' => $listButton
            , 'stats' => $statsButton
            , 'csv' => $csvButton
            , 'log' => $logButton
            , 'delete' => $deleteButton
        ]
    ];
    echo GridView::widget([
        'dataProvider' => $dataProvider
        , 'filterModel' => $searchModel
        , 'filterUrl' => ['view', 'id' => $model->id, 'tab' => CampaignController::VIEW_TAB_INSTRUCTORS]
        , 'columns' => $columns
    ]);
    ?>
</div>