<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use yii\web\View;
use custom\helpers\Html;
use common\models\user\Role;
use backend\modules\lms\models\Campaign;

/* @var $this View */
/* @var $model Campaign */

$modules = $model->modules;
$activeModules = $model->activeModules;

?>
<div class="col-xs-12 buttons">
    <?php if (Role::isOperator(Yii::$app->user) === false) : ?>
        <p class="clearfix">
            <?=
            Html::a(Yii::t('backend/actions', 'Edit {modelClass}', [
                        'modelClass' => Yii::t('common/lms/module', 'Modules')
                    ]), [
                '/lms/campaign/module-list',
                'id' => $model->id
                    ], [
                'class' => 'btn btn-primary pull-right',
            ])
            ?>
        </p>
    <?php endif; ?>
</div>

<div class="col-xs-12 content">
    <div class="row" style="padding-top:10px; padding-bottom:10px; border-bottom:1px solid #337ab7;">
        <div class="col-xs-12 col-sm-10"><?= Yii::t('backend/actions','Module') ?></div>
        <div class="col-xs-12 col-sm-2" style="text-align:center;"><?= Yii::t('backend/actions','Active / Inactive') ?></div>
    </div>
    <?php for ($i = 0, $l = count($modules); $i < $l; ++$i) : ?>
        <div class="row" style="padding-top:10px; padding-bottom:10px; border-bottom:1px solid #eee;">
            <div class="col-xs-12 col-sm-10"><?= $i ?>. <?= $modules[$i]->name ?></div>
            <div class="col-xs-12 col-sm-2" style="text-align:center;">
            <?php 
                $chk="";
                for ($j = 0; $j < count($activeModules); ++$j){
                    if ($activeModules[$j]->id == $modules[$i]->id) $chk="checked='checked'";
                }
            ?>
            <input class="toogle-module" type="checkbox" <?= $chk ?> data-moduleid='<?= $modules[$i]->id ?>' data-campaignid='<?=Yii::$app->request->queryParams['id']  ?>' >  </div>
        </div>
    <?php endfor; ?>
</div>