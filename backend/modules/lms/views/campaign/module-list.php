<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use custom\helpers\Html;
use backend\modules\lms\assets\CampaignAsset;
use kartik\sortable\Sortable;
use yii\helpers\Url;
use backend\modules\lms\controllers\CampaignController;

/* @var $this yii\web\View */
/* @var $model backend\modules\lms\models\Campaign */

CampaignAsset::register($this);
$this->title = Yii::t('backend/actions', 'Edit {modelClass}', [
            'modelClass' => Yii::t('common/lms/module', 'Modules')
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/lms/campaign', 'Campaigns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campaign-module-list row">
    <div class="clearfix">
        <?= Html::a(Html::tag('i', '', ['class' => 'glyphicon glyphicon-arrow-left']) . ' ' . Yii::t('backend/actions', 'Return'), ['view', 'id' => $model->id, 'tab' => CampaignController::VIEW_TAB_MODULES], ['class' => 'btn btn-warning pull-right']) ?>
    </div>
    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Yii::t('backend/lms/campaign', 'Drag the modules you want to include or exlude from one side to the other. You can also establish the order you want that the modules appear in the campaign.') ?></p>
    <?php if ($model->isActive()) : ?>
        <p class="alert alert-warning">
            <strong>
                <?= Yii::t('backend/lms/campaign', 'Active campaign') ?>:
            </strong>
            <?= Yii::t('backend/lms/campaign', 'You wouldn\'t be allowed to exclude module from the campaign.') ?>
        </p>
    <?php endif; ?>
    <div class="module-list-panels row">
        <div class="col-sm-6">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong><?= Yii::t('common/lms/campaign', 'Campaign modules') ?></strong></div>
                <div class="panel-body">
                    <?php
                    $cModules = [];
                    foreach ($model->modules as $module) {
                        $cModules[] = ['content' => $this->render('item/_module', ['model' => $module])];
                    }
                    echo Sortable::widget([
                        'itemOptions' => ['class' => 'row'],
                        'items' => $cModules,
                        'connected' => true,
                        'options' => [
                            'class' => 'campaign-modules',
                            'data-url' => Url::to(['module-move', 'id' => $model->id])
                        ]
                    ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-info">
                <div class="panel-heading"><strong><?= Yii::t('common/lms/campaign', 'Available modules') ?></strong></div>
                <div class="panel-body">
                    <?php
                    $aModules = [];
                    foreach ($model->excludedModules as $module) {
                        $aModules[] = ['content' => $this->render('item/_module', ['model' => $module])];
                    }
                    echo Sortable::widget([
                        'itemOptions' => ['class' => 'row'],
                        'connected' => true,
                        'items' => $aModules,
                        'options' => [
                            'class' => 'available-modules',
                            'data-url' => Url::to(['module-move', 'id' => $model->id])
                        ]
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
