<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\geo\widgets\LocationFormWidget;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\lms\models\Campaign */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="campaign-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <?=
    LocationFormWidget::widget([
        'form' => $form, 'model' => $model,
        'localized' => $model->getCountryPropertyName(),
        'options' => ['options' =>['class' => 'col-sm-3']]
    ])
    ?>

    <?= $form->field($model, 'name', ['options' => ['class' => 'col-sm-3']])->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'begin_at', ['options' => ['class' => 'col-sm-3']])->widget(DatePicker::classname(), [
        'options' => ['class' => 'form-control'],
        'dateFormat' => 'yyyy-MM-dd'
    ]);
    ?>
    <?=
    $form->field($model, 'end_at', ['options' => ['class' => 'col-sm-3']])->widget(DatePicker::classname(), [
        'options' => ['class' => 'form-control'],
        'dateFormat' => 'yyyy-MM-dd'
    ]);
    ?>
    </div>
    <div class="row ">
        <div class="col-sm-3">
<?= Html::submitButton($model->isNewRecord ? Yii::t('backend/actions', 'Create') : Yii::t('backend/actions', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div>
<?php ActiveForm::end(); ?>

</div>
