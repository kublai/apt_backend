<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use custom\helpers\Html;
use backend\modules\lms\controllers\CampaignController;
use backend\modules\lms\widgets\UserStats;

/* @var $this yii\web\View */
/* @var $model backend\modules\lms\models\InstructorView */

$this->title = Yii::t('backend/lms/campaign', '{username} Stats', [
            'username' => $model->user->getFullName()
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/lms/campaign', 'Campaigns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->campaign->name, 'url' => ['view', 'id' => $model->campaign->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campaign-instructor-list">
    <div class="clearfix">
        <?=
        Html::a(
                Html::tag('i', '', ['class' => 'glyphicon glyphicon-arrow-left']) . ' ' . Yii::t('backend/actions', 'Return')
                , (Yii::$app->request->referrer !== null ? Yii::$app->request->referrer . '&tab=' . CampaignController::VIEW_TAB_INSTRUCTORS : ['view', 'id' => $model->campaign->id, 'tab' => CampaignController::VIEW_TAB_INSTRUCTORS])
                , ['class' => 'btn btn-warning pull-right']
        )
        ?>
    </div>
    <h1 class="mb-20">
        <?= Html::encode($this->title) ?>
    </h1>
    <?=
    UserStats::widget([
        'model' => $model
        , 'view' => $this
        , 'url' => ['instructor-view', 'campaignId' => $model->campaign->id, 'instructorId' => $model->instructor->user_id]
    ])
    ?>
</div>
