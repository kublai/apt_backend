<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use custom\helpers\Html;
use backend\modules\lms\controllers\CampaignController;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\lms\models\StudentForm */

$this->title = Yii::t('backend/lms/campaign', 'Change {username} instructor', [
            'username' => $model->student->user->getFullName()
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/lms/campaign', 'Campaigns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->campaign->name, 'url' => ['view', 'id' => $model->campaign->id]];
$this->params['breadcrumbs'][] = $this->title;
$returnUrl = Yii::$app->request->get('returnUrl') ? Yii::$app->request->get('returnUrl') : (Yii::$app->request->referrer !== null ? Yii::$app->request->referrer . '&tab=' . CampaignController::VIEW_TAB_STUDENTS : ['view', 'id' => $campaign->id, 'tab' => CampaignController::VIEW_TAB_STUDENTS]);
?>
<div class="campaign-instructor-list">
    <div class="clearfix">
        <?=
        Html::a(
                Html::tag('i', '', ['class' => 'glyphicon glyphicon-arrow-left']) . ' ' . Yii::t('backend/actions', 'Return')
                , $returnUrl
                , ['class' => 'btn btn-warning pull-right']
        )
        ?>
    </div>
    <h1>
        <?= Html::encode($this->title) ?>
    </h1>
    <div class="p-40">
        <div class="well bg-info p-40">
            <?php
            $form = ActiveForm::begin([
                        'action' => Url::to(['student-instructor', 'campaignId' => $model->campaign->id, 'studentId' => $model->student->user_id, 'returnUrl' => $returnUrl])
            ]);
            echo $form->field($model, 'instructorId')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($model->getInstructors()->all(), 'id', 'fullName')
                , 'language' => 'es'
            ]);
            echo Html::submitButton(Yii::t('backend/actions', 'Save'), ['class' => 'btn btn-primary']);
            ActiveForm::end();
            ?>
            <div class="mv-20">
                <div class="content">
                    <p>
                        <?= Yii::t('common/lms/change-instructor', 'This feature allows you to change the instructor of a student and move all of his session data already synchronized, so before using it you have to consider that...') ?>
                    </p>
                    <ul>
                        <li><?= Yii::t('common/lms/change-instructor', 'A student could only be assigned to instructors of his same operator.') ?></li>
                        <li><?= Yii::t('common/lms/change-instructor', 'The statistics of the two instructors will be affected.') ?></li>
                        <li><?= Yii::t('common/lms/change-instructor', 'All of the session data of the student not yet synchronized from the tablet of the original instructor will be lost.') ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>