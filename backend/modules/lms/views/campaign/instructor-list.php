<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use custom\helpers\Html;
use backend\modules\lms\assets\CampaignAsset;
use backend\widgets\GridView;
use backend\modules\lms\controllers\CampaignController;

/* @var $this yii\web\View */
/* @var $model backend\modules\lms\models\Campaign */
/* @var $dataProvider yii\data\ActiveDataProvider */

CampaignAsset::register($this);

$this->title = Yii::t('backend/lms/campaign', 'Add {modelClass} to {username}', [
            'modelClass' => Yii::t('common/lms/user', 'Instructors'),
            'username' => $oModel->getFullName()
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/lms/campaign', 'Campaigns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campaign-instructor-list">
    <div class="clearfix">
        <?=
        Html::a(
                Html::tag('i', '', ['class' => 'glyphicon glyphicon-arrow-left']) . ' ' . Yii::t('backend/actions', 'Return')
                , (Yii::$app->request->referrer !== null ? Yii::$app->request->referrer . '&tab=' . CampaignController::VIEW_TAB_OPERATORS : ['view', 'id' => $campaign->id, 'tab' => CampaignController::VIEW_TAB_OPERATORS])
                , ['class' => 'btn btn-warning pull-right']
        )
        ?>
    </div>
    <h1>
        <?= Html::encode($this->title) ?>
    </h1>
    <?= $this->render('search/_instructor', ['model' => $searchModel]); ?>
    <?=
    GridView::widget([
        'options' => [
            'class' => 'grid-view full-selection'
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            'name',
            'last_name',
            ['class' => 'yii\grid\CheckboxColumn'],
        ],
        'view' => $this
    ]);
    ?>
</div>
