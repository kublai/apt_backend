<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use custom\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\lms\models\Module */
?>
<div id="<?= $model->id ?>" class="module-edit col-xs-12">
    <div class="clearfix">
        <?= Html::encode($model->name) ?>
    </div>
</div>