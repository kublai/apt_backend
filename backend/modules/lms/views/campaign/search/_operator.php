<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\geo\widgets\LocationFormWidget;

/* @var $this yii\web\View */
/* @var $model backend\modules\lms\models\SearchOperator */
?>
<div class="panel panel-default advanced-search">
    <div class="panel-heading">
        <a class="clearfix collapsible" data-toggle="collapse" href="#search-instructor" aria-expanded="false" aria-controls="search-instructor">
            <div class="pull-right">
                <?= Yii::t('backend/actions', 'Advanced search') ?> 
                <span class="glyphicon glyphicon-menu-right" id="collapse-icon"></span>
            </div>
        </a>
    </div>
    <div class="campaign-instructor-search collapse panel-body" id="search-instructor">
        <?php
        $form = ActiveForm::begin([
                    'method' => 'get',
        ]);
        ?>
        <?= $form->field($model, 'id') ?>
        <?= $form->field($model, 'username') ?>
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'last_name') ?>
        <?=
        LocationFormWidget::widget([
            'form' => $form,
            'model' => $model,
            'localized' => $model->getPlacePropertyName()
        ])
        ?> 
        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend/actions', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('backend/actions', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
