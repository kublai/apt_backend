<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\lms\models\SearchStudentSession */
?>
<div class="panel panel-default advanced-search">
    <div class="panel-heading">
        <a class="clearfix collapsible" data-toggle="collapse" href="#search-session" aria-expanded="false" aria-controls="search-student">
            <div class="pull-right">
                <?= Yii::t('backend/actions', 'Advanced search') ?> 
                <span class="glyphicon glyphicon-menu-right" id="collapse-icon"></span>
            </div>
        </a>
    </div>
    <div class="campaign-instructor-search collapse panel-body" id="search-session">
        <?php
        $form = ActiveForm::begin([
                    'method' => 'get'
                    , 'action' => Url::to(['/lms/campaign/student-view', 'campaignId' => Yii::$app->request->get('campaignId'), 'studentId' => Yii::$app->request->get('studentId')])
                    , 'layout' => 'horizontal'
        ]);
        ?>
        <?= $form->field($model, 'completed')->radioList([null => Yii::t('general', 'All'), '0' => Yii::t('general', 'No'), '1' => Yii::t('general', 'Yes')]) ?>
        <?=
        $form->field($model, 'begin_at_from')->widget(DatePicker::classname(), [
            'options' => ['class' => 'form-control'],
            'dateFormat' => 'yyyy-MM-dd'
        ]);
        ?>
        <?=
        $form->field($model, 'begin_at_to')->widget(DatePicker::classname(), [
            'options' => ['class' => 'form-control'],
            'dateFormat' => 'yyyy-MM-dd'
        ]);
        ?>
        <?=
        $form->field($model, 'end_at_to')->widget(DatePicker::classname(), [
            'options' => ['class' => 'form-control'],
            'dateFormat' => 'yyyy-MM-dd'
        ]);
        ?>
        <?=
        $form->field($model, 'end_at_from')->widget(DatePicker::classname(), [
            'options' => ['class' => 'form-control'],
            'dateFormat' => 'yyyy-MM-dd'
        ]);
        ?>
        <div class="form-group">
            <div class="col-sm-3">

            </div>
            <div class="col-sm-6">
                <?= Html::submitButton(Yii::t('backend/actions', 'Search'), ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton(Yii::t('backend/actions', 'Reset'), ['class' => 'btn btn-default']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
