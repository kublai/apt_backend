<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\lms\models\Campaign */

$this->title = Yii::t('backend/actions', 'Create {modelClass}', ['modelClass' => Yii::t('common/lms/campaign', 'Campaign')]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/lms/Campaign', 'Campaign'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campaign-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
