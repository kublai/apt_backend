<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use custom\helpers\Html;
use backend\modules\lms\controllers\CampaignController;
use backend\modules\lms\widgets\OperatorStats;

/* @var $this yii\web\View */
/* @var $model backend\modules\lms\models\OperatorView */

$this->title = Yii::t('backend/lms/campaign', '{username} Stats', [
            'username' => $model->operator->getFullName()
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/lms/campaign', 'Campaigns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->campaign->name, 'url' => ['view', 'id' => $model->campaign->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campaign-instructor-list">
    <div class="clearfix">
        <?=
        Html::a(
                Html::tag('i', '', ['class' => 'glyphicon glyphicon-arrow-left']) . ' ' . Yii::t('backend/actions', 'Return')
                , (Yii::$app->request->referrer !== null ? Yii::$app->request->referrer . '&tab=' . CampaignController::VIEW_TAB_OPERATORS : ['view', 'id' => $model->campaign->id, 'tab' => CampaignController::VIEW_TAB_OPERATORS])
                , ['class' => 'btn btn-warning pull-right']
        )
        ?>
    </div>
    <h1>
        <?= Html::encode($this->title) ?>
    </h1>
    <?= OperatorStats::widget([
        'model' => $model
        , 'view' => $this
        , 'url' => ['operator-view', 'campaignId' => $model->campaign->id, 'operatorId' => $model->operator->id]
    ]) ?>
</div>
