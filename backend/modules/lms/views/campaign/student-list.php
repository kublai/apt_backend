<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use custom\helpers\Html;
use backend\modules\lms\assets\CampaignAsset;
use backend\widgets\GridView;
use backend\modules\lms\controllers\CampaignController;
use common\models\user\User;

/* @var $this yii\web\View */
/* @var $model backend\modules\lms\models\Campaign */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $iModel common\models\user\User */

CampaignAsset::register($this);

$this->title = User::find()
                ->innerJoin('lms_operator lo', 'lo.operator_id = user.id')
                ->innerJoin('lms_instructor li', 'li.operator_id = lo.operator_id AND li.campaign_id = lo.campaign_id')
                ->andWhere(['lo.campaign_id' => $model->id, 'li.user_id' => $iModel->id])
                ->one()
        ->fullName . ' - ' . Yii::t('backend/lms/campaign', 'Add {modelClass} to {username}', [
            'modelClass' => Yii::t('common/lms/user', 'Students')
            , 'username' => $iModel->getFullName()
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/lms/campaign', 'Campaigns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
$returnUrl = Yii::$app->request->referrer !== null ? Yii::$app->request->referrer . '&tab=' . CampaignController::VIEW_TAB_INSTRUCTORS : ['view', 'id' => $campaign->id, 'tab' => CampaignController::VIEW_TAB_INSTRUCTORS];
?>
<div class="campaign-instructor-list">
    <div class="clearfix">
        <?=
        Html::a(
                Html::tag('i', '', ['class' => 'glyphicon glyphicon-arrow-left']) . ' ' . Yii::t('backend/actions', 'Return')
                , $returnUrl
                , ['class' => 'btn btn-warning pull-right']
        )
        ?>
        <?=
        Html::a(
                Html::tag('i', '', ['class' => 'glyphicon glyphicon-file']) . ' ' . Yii::t('backend/actions', 'Integrate CSV')
                , ['integrate-instructor-csv', 'campaignId' => $model->id, 'instructorId' => $iModel->id, 'returnUrl' => $returnUrl]
                , ['class' => 'btn btn-primary pull-right mh-20']
        )
        ?>
    </div>
    <h1>
        <?= Html::encode($this->title) ?>
    </h1>
    <?= $this->render('search/_student', ['model' => $searchModel]); ?>
    <?=
    GridView::widget([
        'options' => [
            'class' => 'grid-view full-selection'
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            'name',
            'last_name',
            ['class' => 'yii\grid\CheckboxColumn'],
        ],
        'view' => $this
    ]);
    ?>
</div>
