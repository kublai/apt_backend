<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\user\Role;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\lms\models\SearchCampaign */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common/lms/campaign', 'Campaigns');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campaign-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    $template = '{view}';
    ?>
    <p>
        <?php
        if (Role::isOperator(Yii::$app->user) === false) :
            $template .= ' {delete}';
            ?>
            <?=
            Html::a(Yii::t('backend/actions', 'Create {modelClass}', [
                        'modelClass' => Yii::t('common/lms/campaign', 'Campaign')
                    ]), ['create'], ['class' => 'btn btn-success'])
            ?>
        <?php endif; ?>
        <?= Html::a(Yii::t('backend/lms/campaign', 'Export Report'), ['lms-report'], ['class' => 'btn btn-primary']) ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'location.country.name',
            'name',
            'begin_at:datetime',
            'end_at:datetime',
            'created_at:datetime',
            // 'created_by',
            'updated_at:datetime',
            // 'updated_by',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $template
            ],
        ],
    ]);
    ?>

</div>
