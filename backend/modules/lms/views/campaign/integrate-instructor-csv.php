<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use custom\helpers\Html;
use yii\helpers\Url;
use backend\modules\lms\controllers\CampaignController;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $campaign backend\modules\lms\models\Campaign */
/* @var $model backend\modules\lms\models\InstructorFileIntegration */

$this->title = $model->instructor->operator->fullName . ' - ' . Yii::t('backend/lms/campaign', 'Integrate {modelClass} to {username}', [
            'modelClass' => Yii::t('common/lms/user', 'Students')
            , 'username' => $model->instructor->user->fullName
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/lms/campaign', 'Campaigns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $campaign->name, 'url' => ['view', 'id' => $campaign->id]];
$this->params['breadcrumbs'][] = $this->title;
$returnUrl = Yii::$app->request->get('returnUrl') ? Yii::$app->request->get('returnUrl') : (Yii::$app->request->referrer !== null ? Yii::$app->request->referrer . '&tab=' . CampaignController::VIEW_TAB_INSTRUCTORS : ['view', 'id' => $campaign->id, 'tab' => CampaignController::VIEW_TAB_INSTRUCTORS]);
?>
<div class="campaign-instructor-list">
    <div class="clearfix">
        <?=
        Html::a(
                Html::tag('i', '', ['class' => 'glyphicon glyphicon-arrow-left']) . ' ' . Yii::t('backend/actions', 'Return')
                , $returnUrl
                , ['class' => 'btn btn-warning pull-right']
        )
        ?>
    </div>
    <h1>
        <?= Html::encode($this->title) ?>
    </h1>
    <div class="p-40">
        <div class="well bg-info p-40">
            <?php if ($model->hasErrors()) : ?>
                <ul>
                    <?php foreach ($model->getErrors() as $errors) : ?>
                        <?php foreach ($errors as $error) : ?>
                            <li class="text-danger"><?= $error ?></li>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
            <?php
            $form = ActiveForm::begin([
                        'action' => Url::to(['integrate-instructor-csv', 'campaignId' => $campaign->id, 'instructorId' => $model->instructor->user_id, 'returnUrl' => $returnUrl])
                        , 'options' => [
                            'enctype' => 'multipart/form-data'
                        ]
                        , 'layout' => 'inline'
            ]);
            echo $form->field($model, 'file')->fileInput(['class' => 'form-control']);
            echo Html::submitButton(Yii::t('backend/actions', 'Process'), ['class' => 'btn btn-primary ph-40 mh-40']);
            ActiveForm::end();
            ?>
            <div class="mv-20">
                <h2><?= Yii::t('common/lms/integration', 'Instructions to integrate a CSV') ?></h2>
                <div class="content">
                    <ul>
                        <li>
                            <?=
                            Yii::t('common/lms/integration', 'The CSV should have the default format in which most spreadsheets export, that is, fields delimited by \',\' (comma), text fields optionally encapsulated in \'"\' (double quote) and the escape character \'\\\' (backslash).')
                            ?>
                        </li>
                        <li>
                            <?= Yii::t('common/lms/integration', 'CSV columns must have the following information, except optional columns that should be left blank if they have any data.') ?>
                            <ol type="A">
                                <li>
                                    <?= Yii::t('common/lms/integration', 'Name of the user') ?>
                                </li>
                                <li>
                                    <?= Yii::t('common/lms/integration', 'Last name of the user') ?>
                                </li>
                                <li>
                                    <?= Yii::t('common/lms/integration', 'Gender [1 for Male and 2 for Female]') ?>
                                </li>
                                <li>
                                    <?= Yii::t('common/lms/integration', 'Identity number') ?>
                                </li>
                                <li>
                                    <?= Yii::t('common/lms/integration', 'Phone (optional)') ?>
                                </li>
                                <li>
                                    <?= Yii::t('common/lms/integration', 'Country name (Exactly as it appears in the management area)') ?>
                                </li>
                                <li>
                                    <?= Yii::t('common/lms/integration', 'Zone name (Exactly as it appears in the management area)') ?>
                                </li>
                                <li>
                                    <?= Yii::t('common/lms/integration', 'Region name (Exactly as it appears in the management area)') ?>
                                </li>
                                <li>
                                    <?= Yii::t('common/lms/integration', 'Place name (Exactly as it appears in the management area)') ?>
                                </li>
                                <li>
                                    <?= Yii::t('common/lms/integration', 'Address (optional)') ?>
                                </li>
                                <li>
                                    <?= Yii::t('common/lms/integration', 'Latitude (optional)') ?>
                                </li>
                                <li>
                                    <?= Yii::t('common/lms/integration', 'Longitude (optional)') ?> 
                                </li>
                            </ol>
                        </li>
                        <li>
                            <?= Yii::t('common/lms/integration', 'The integration is done atomically, so if an error occurs during it, no records will be processed. In this case, you must correct any errors and re-integrate the CSV.') ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>