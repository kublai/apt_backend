<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
use custom\helpers\Html;
use backend\modules\lms\controllers\CampaignController;
use common\models\lms\InstructorSynclog;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model common\models\lms\Instructor */

$this->title = Yii::t('backend/lms/campaign', '{username} Synchronization Log', [
            'username' => $model->user->getFullName()
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/lms/campaign', 'Campaigns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->campaign->name, 'url' => ['view', 'id' => $model->campaign->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campaign-instructor-list">
    <div class="clearfix">
        <?=
        Html::a(
                Html::tag('i', '', ['class' => 'glyphicon glyphicon-arrow-left']) . ' ' . Yii::t('backend/actions', 'Return')
                , (Yii::$app->request->referrer !== null ? Yii::$app->request->referrer . '&tab=' . CampaignController::VIEW_TAB_INSTRUCTORS : ['view', 'id' => $model->campaign->id, 'tab' => CampaignController::VIEW_TAB_INSTRUCTORS])
                , ['class' => 'btn btn-warning pull-right']
        )
        ?>
    </div>
    <h1 class="mv-30">
        <?= Html::encode($this->title) ?>
    </h1>
    <?=
    GridView::widget([
        'options' => [
            'class' => 'grid-view full-selection'
        ]
        , 'dataProvider' => new ActiveDataProvider([
            'query' => InstructorSynclog::find()->andWhere(['instructor_id' => $model->user_id, 'campaign_id' => $model->campaign_id])
            , 'pagination' => [
                'pageSize' => 20,
            ]
            , 'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC
                ]
            ]
                ])
        , 'columns' => [
            'id'
            , 'created_at:datetime'
            , 'statusName'
            , 'info:html'
        ]
        , 'view' => $this
    ]);
    ?>
</div>
