<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\lms\models\Module */

$this->title = Yii::t('backend/actions', 'Update {modelClass}: ', [
            'modelClass' => Yii::t('common/lms/module', 'Module'),
        ]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/lms/module', 'Modules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend/actions', 'Update');
?>
<div class="module-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
