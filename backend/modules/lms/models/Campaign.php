<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\models;

use common\models\lms\Campaign as BaseCampaign;
use Yii;

/**
 * 
 * @property Module[] $excludedModules The modules not included in this campaign.
 * 

 */
class Campaign extends BaseCampaign {

    /**
     * Return the list of modules that are inactive in a campaign (data from lms_campaign_module)
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getExcludedModules() {
        $q = Module::find()
                ->leftJoin('lms_campaign_module', [
                    'and',
                    'module_id = id',
                    ['campaign_id' => $this->id]
                ])
                ->where(['campaign_id' => null]);
        $q->multiple = true;
        return $q;
    }

    /**
     * Convert begin_at and end_at to unix timestamp format before saving
     * 
     * @param type $insert
     * @return boolean
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->begin_at = intval(Yii::$app->formatter->asTimestamp($this->begin_at . ' 00:00:00'));
            $this->end_at = intval(Yii::$app->formatter->asTimestamp($this->end_at . ' 23:59:59'));
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        $msgfile = 'backend/lms/campaign';
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t($msgfile, 'ID'),
            'location_id' => Yii::t($msgfile, 'Location ID'),
            'name' => Yii::t($msgfile, 'Name'),
            'begin_at' => Yii::t($msgfile, 'Begin At'),
            'end_at' => Yii::t($msgfile, 'End At'),
            'created_at' => Yii::t($msgfile, 'Created At'),
            'created_by' => Yii::t($msgfile, 'Created By'),
            'updated_at' => Yii::t($msgfile, 'Updated At'),
            'updated_by' => Yii::t($msgfile, 'Updated By'),
        ]);
    }

}
