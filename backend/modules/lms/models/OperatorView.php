<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\models;

use Yii;
use custom\base\Model;
use yii\base\InvalidConfigException;
use common\models\lms\Student,
    common\models\lms\Campaign,
    common\models\lms\StudentSession;
use common\models\user\Operator;
use yii\db\Query;

/**
 * @property Operator $operator The operator itself
 * 

 */
class OperatorView extends Model implements StatsViewInterface {

    /**
     *
     * @var Operator
     */
    protected $operator;

    /**
     *
     * @var Campaign
     */
    protected $campaign;

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init() {
        if ($this->operator === null) {
            throw new InvalidConfigException('Instructor must be provided');
        }
    }

    /**
     * 
     * @param Operator $o
     */
    public function setOperator(Operator $o) {
        $this->operator = $o;
    }

    /**
     * 
     * @return Operator
     */
    public function getOperator() {
        return $this->operator;
    }

    /**
     * 
     * @param Campaign $o
     */
    public function setCampaign(Campaign $o) {
        $this->campaign = $o;
    }

    /**
     * @inheritdoc
     */
    public function getCampaign() {
        return $this->campaign;
    }

    /**
     * @inheritdoc
     */
    public function getMapStats() {
        return (new Query())
                        ->select([
                            'li.user_id'
                            , 'aux.avgLatitude as latitude'
                            , 'aux.avgLongitude as longitude'
                            , 'SQRT(SUM(POW(gl.latitude - aux.avgLatitude, 2)) / aux.n) as stDevLat'
                            , 'SQRT(SUM(POW(gl.longitude - aux.avgLongitude, 2)) / aux.n) as stDevLon'
                            , 'aux.n'
                        ])
                        ->from('lms_instructor li')
                        ->innerJoin('lms_student ls', 'ls.instructor_id = li.user_id AND ls.campaign_id = li.campaign_id')
                        ->innerJoin('lms_student_session lss', 'lss.student_id = ls.user_id AND lss.campaign_id = ls.campaign_id')
                        ->innerJoin('geo_location gl', 'gl.id = lss.location_id')
                        ->andWhere([
                            'li.operator_id' => $this->operator->id
                            , 'li.campaign_id' => $this->campaign->id
                        ])
                        ->andWhere(['not', ['gl.latitude' => null, 'gl.longitude' => null]])
                        ->innerJoin([
                            'aux' => (new Query())
                            ->select([
                                'li.user_id'
                                , 'AVG(gl.latitude) avgLatitude'
                                , 'AVG(gl.longitude) avgLongitude'
                                , 'COUNT(gl.id) n'
                            ])
                            ->from('lms_instructor li')
                            ->innerJoin('lms_student ls', 'ls.instructor_id = li.user_id AND ls.campaign_id = li.campaign_id')
                            ->innerJoin('lms_student_session lss', 'lss.student_id = ls.user_id AND lss.campaign_id = ls.campaign_id')
                            ->innerJoin('geo_location gl', 'gl.id = lss.location_id')
                            ->andWhere([
                                'li.operator_id' => $this->operator->id
                                , 'li.campaign_id' => $this->campaign->id
                            ])
                            ->andWhere(['not', ['gl.latitude' => null, 'gl.longitude' => null]])
                            ->groupBy(['li.user_id'])
                                ], 'aux.user_id = li.user_id')
                        ->groupBy(['li.user_id']);
    }

    /**
     * @inheritdoc
     */
    public function getStudentsWithSessions() {
        return Student::find()
                        ->innerJoin('lms_instructor li', 'li.user_id = lms_student.instructor_id AND li.campaign_id = lms_student.campaign_id')
                        ->innerJoin('lms_student_module lsm', 'lsm.student_id = lms_student.user_id AND lsm.campaign_id = lms_student.campaign_id')
                        ->andWhere([
                            'li.operator_id' => $this->operator->id
                            , 'lms_student.campaign_id' => $this->campaign->id
                        ])
                        ->groupBy(['lsm.student_id'])->count();
    }

    /**
     * @inheritdoc
     */
    public function getStudentsWithoutSessions() {
        return Student::find()
                        ->innerJoin('lms_instructor li', 'li.user_id = lms_student.instructor_id AND li.campaign_id = lms_student.campaign_id')
                        ->leftJoin('lms_student_module lsm', 'lsm.student_id = lms_student.user_id AND lsm.campaign_id = lms_student.campaign_id')
                        ->andWhere([
                            'li.operator_id' => $this->operator->id
                            , 'lms_student.campaign_id' => $this->campaign->id
                        ])
                        ->andWhere(['lsm.student_id' => null])->count();
    }

    /**
     * @inheritdoc
     */
    public function moduleStatQuery() {
        return (new Query())
                        ->select([
                            'lm.id', 'lm.code', 'lm.name', 'SUM(lsm.completed) completed', 'SUM( end_at - begin_at ) as  experienced_time'
                        ])
                        ->from('lms_module lm')
                        ->innerJoin('lms_campaign_module lcm', 'lcm.module_id = lm.id')
                        ->leftJoin('lms_student_module lsm', 'lsm.module_id = lcm.module_id AND lsm.campaign_id = lcm.campaign_id')
                        ->leftJoin('lms_student ls', 'ls.user_id = lsm.student_id AND ls.campaign_id = lsm.campaign_id')
                        ->leftJoin('lms_instructor li', 'li.user_id = ls.instructor_id AND li.campaign_id = ls.campaign_id')
                        ->leftJoin('lms_student_session lss', 'lss.student_id = lsm.student_id AND lss.module_id = lsm.module_id AND lss.campaign_id = lsm.campaign_id')
                        ->andWhere([
                            'li.operator_id' => $this->operator->id
                            , 'lcm.campaign_id' => $this->campaign->id
                        ])
                        ->groupBy('lm.id')
                        ->orderBy(['lcm.order' => SORT_ASC]);
    }

    /**
     * @inheritdoc
     */

    public function studentStatQuery() {
        //for the students we will show the completed status as is to facilitate the analisys of the data whe the file is downloaded
        return (new Query())
                        ->select(['CONCAT(us.name, " ", us.last_name) user_name',
                            'lm.id', 'lm.code', 'lm.name', 
                            'lss.completed completed',
                            '( end_at - begin_at ) as  experienced_time',
                            'FROM_UNIXTIME(lss.begin_at) as session_date' 
                        ])
                        ->from('lms_module lm')
                        ->innerJoin('lms_campaign_module lcm', 'lcm.module_id = lm.id')
                        ->innerJoin('lms_student_module lsm', 'lsm.module_id = lcm.module_id AND lsm.campaign_id = lcm.campaign_id')
                        ->innerJoin('lms_student ls', 'ls.user_id = lsm.student_id AND ls.campaign_id = lsm.campaign_id')
                        ->innerJoin('lms_instructor li', 'li.user_id = ls.instructor_id AND li.campaign_id = ls.campaign_id')
                        ->innerJoin('user us', 'us.id = ls.user_id')
                        ->innerJoin('lms_student_session lss', 'lss.student_id = lsm.student_id AND lss.module_id = lsm.module_id AND lss.campaign_id = lsm.campaign_id')
                        ->andWhere([
                            'li.operator_id' => $this->operator->id,
                            'ls.campaign_id' => $this->campaign->id,
                        ])
                        ->groupBy(['ls.user_id', 'lm.id'])
                        ->orderBy(['user_name' => SORT_ASC, 'lcm.order' =>SORT_ASC, 'session_date' => SORT_ASC]);
    }

    //DEPRECATED FUNCTION
    // this function is not used (Oct-10-2020)
    // because we are showing the session_date now for all the students, 
    // I made this change first in the facilitator stats section and now
    // this code it's updating the Opertator Stats section
    public function studentStatQueryOld() {
        return (new Query())
                        ->select([
                            'CONCAT(us.name, " ", us.last_name) user_name', 
                            'lm.code', 'lm.name', 
                            'SUM(lss.completed) completed', 
                            'SUM( end_at - begin_at ) as  experienced_time'
                        ])
                        ->from('lms_module lm')
                        ->innerJoin('lms_campaign_module lcm', 'lcm.module_id = lm.id')
                        ->innerJoin('lms_student_module lsm', 'lsm.module_id = lcm.module_id AND lsm.campaign_id = lcm.campaign_id')
                        ->innerJoin('lms_student ls', 'ls.user_id = lsm.student_id AND ls.campaign_id = lsm.campaign_id')
                        ->innerJoin('lms_instructor li', 'li.user_id = ls.instructor_id AND li.campaign_id = ls.campaign_id')
                        ->innerJoin('user us', 'us.id = ls.user_id')
                        ->innerJoin('lms_student_session lss', 'lss.student_id = lsm.student_id AND lss.module_id = lsm.module_id AND lss.campaign_id = lsm.campaign_id')
                        ->andWhere([
                            'li.operator_id' => $this->operator->id
                            , 'ls.campaign_id' => $this->campaign->id
                        ])
                        ->groupBy(['ls.user_id', 'lm.id'])
                        ->orderBy(['lcm.order' => SORT_ASC]);
    }

    /**
     * @inheritdoc
     */
    public function getStudentSessions() {
        $q = StudentSession::find()
                ->innerJoin('lms_student ls', 'ls.user_id = lms_student_session.student_id AND ls.campaign_id = lms_student_session.campaign_id')
                ->innerJoin('lms_instructor li', 'li.user_id = ls.instructor_id AND li.campaign_id = ls.campaign_id')
                ->andWhere([
            'li.operator_id' => $this->operator->id
            , 'ls.campaign_id' => $this->campaign->id
        ]);
        $q->multiple = true;
        return $q;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'studentsWithSessions' => Yii::t('common/lms/campaign', 'Students with sessions')
            , 'studentsWithoutSessions' => Yii::t('common/lms/campaign', 'Students without sessions')
        ];
    }

}
