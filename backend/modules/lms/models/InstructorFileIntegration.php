<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\models;

use Yii;
use yii\base\Model,
    yii\base\InvalidConfigException;
use common\models\lms\Instructor,
    common\models\lms\InstructorIntegration;
use yii\web\UploadedFile;

/**
 * 

 */
class InstructorFileIntegration extends Model {

    /**
     *
     * @var UploadedFile
     */
    public $file;

    /**
     *
     * @var Instructor
     */
    protected $instructor;

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        if ($this->instructor === null) {
            throw new InvalidConfigException('Instructor must be provided on initialization');
        }
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['file', 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null) {
        $this->file = UploadedFile::getInstance($this, 'file');
        return $this->file !== null;
    }

    /**
     * 
     * @param Instructor $i
     */
    public function setInstructor(Instructor $i) {
        $this->instructor = $i;
    }

    /**
     * 
     * @return Instructor
     */
    public function getInstructor() {
        return $this->instructor;
    }

    /**
     * 
     * @return boolean
     */
    public function save() {
        if ($this->validate() === false) {
            return false;
        }

        $integrator = new InstructorIntegration([
            'instructor' => $this->instructor
            , 'path' => $this->file->tempName
        ]);
        if ($integrator->process() === false) {
            $this->addErrors($integrator->errors);
            return false;
        } else {
            return true;
        }
    }

}
