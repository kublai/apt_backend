<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\lms\StudentSession as BaseStudentSession;
use yii\db\ActiveQuery;
use Yii;
use common\models\user\User;

/**
 * SearchStudentSession represents the model behind the search form about 
 * `common\models\lms\StudentSession`.
 */
class SearchStudentSession extends BaseStudentSession {

    /**
     *
     * @var string
     */
    public $begin_at_from;

    /**
     *
     * @var string
     */
    public $begin_at_to;

    /**
     *
     * @var string
     */
    public $end_at_from;

    /**
     *
     * @var string
     */
    public $end_at_to;

    /**
     *
     * @var User 
     */
    protected $operator;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'module_id', 'completed', 'utility', 'difficulty'], 'integer'],
            [['begin_at_from', 'begin_at_to', 'end_at_from', 'end_at_to'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * 
     * @param User $o
     */
    public function setOperator(User $o) {
        $this->operator = $o;
    }

    /**
     * 
     * @return User
     */
    public function getOperator() {
        return $this->operator;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = $this->getQuery();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $this->addFilters($query);
        return $dataProvider;
    }

    /**
     * Gets the base query for the search
     * 
     * @return ActiveQuery
     */
    public function getQuery() {
        $query = static::find()->where([
                    'lms_student_session.campaign_id' => $this->campaign_id
                ])
                ->innerJoin('lms_module', 'lms_module.id = lms_student_session.module_id')
                ->innerJoin('lms_student', 'lms_student.user_id = lms_student_session.student_id AND lms_student.campaign_id = lms_student_session.campaign_id')
                ->innerJoin('lms_instructor', 'lms_instructor.user_id = lms_student.instructor_id AND lms_instructor.campaign_id = lms_student.campaign_id');
        if ($this->operator !== null) {
            $query->andWhere(['lms_instructor.operator_id' => $this->operator->id]);
        }
        return $query;
    }

    /**
     * Add filters to the query depending on the loaded properties.
     * 
     * @param ActiveQuery $query
     */
    public function addFilters(ActiveQuery $query) {
        if (!empty($this->student_id)) {
            $query->andFilterWhere(['lms_student_session.student_id' => $this->student_id]);
        }
        if (!empty($this->module_id)) {
            $query->andFilterWhere(['lms_student_session.module_id' => $this->module_id]);
        }
        if (!empty($this->utility)) {
            $query->andFilterWhere(['<=', 'lms_student_session.utility', $this->utility]);
        }
        if (!empty($this->difficulty)) {
            $query->andFilterWhere(['<=', 'lms_student_session.difficulty', $this->difficulty]);
        }
        if (!empty($this->completed)) {
            $query->andFilterWhere(['lms_student_session.completed' => $this->completed]);
        }
        if (!empty($this->begin_at_from)) {
            $query->andFilterWhere(['>=', 'lms_student_session.begin_at', Yii::$app->formatter->asTimestamp($this->begin_at_from)]);
        }
        if (!empty($this->begin_at_to)) {
            $query->andFilterWhere(['<', 'lms_student_session.begin_at', Yii::$app->formatter->asTimestamp($this->begin_at_to)]);
        }
        if (!empty($this->end_at_from)) {
            $query->andFilterWhere(['>', 'lms_student_session.end_at', Yii::$app->formatter->asTimestamp($this->end_at_from)]);
        }
        if (!empty($this->end_at_to)) {
            $query->andFilterWhere(['<=', 'lms_student_session.end_at', Yii::$app->formatter->asTimestamp($this->end_at_to)]);
        }
    }

    public function behaviors() {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), [
            'begin_at_from' => Yii::t('common/lms/user', 'Begins After'),
            'begin_at_to' => Yii::t('common/lms/user', 'Begins Before'),
            'end_at_to' => Yii::t('common/lms/user', 'Ends After'),
            'end_at_from' => Yii::t('common/lms/user', 'Ends Before'),
            'module.code' => Yii::t('common/lms/module', 'Code'),
            'module.name' => Yii::t('common/lms/module', 'Module'),
            'location.region.name' => Yii::t('common/geo/region', 'Region'),
            'student.instructor.fullName' => Yii::t('common/lms/user', 'Instructor'),
            'student.user.fullName' => Yii::t('common/lms/user', 'Student')
        ]);
    }

}
