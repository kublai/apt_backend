<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\models;

use common\models\lms\Campaign,
    common\models\lms\StudentSessionQuery;
use yii\db\Query,
    yii\db\ActiveQuery;

/**
 * Interface to use the StatsWidget
 */
interface StatsViewInterface {

    /**
     * 
     * @return Campaign
     */
    public function getCampaign();

    /**
     * 
     * @return StudentSessionQuery
     */
    public function getStudentSessions();

    /**
     * 
     * @return ActiveQuery
     */
    public function getStudentsWithSessions();

    /**
     * 
     * @return ActiveQuery
     */
    public function getStudentsWithoutSessions();

    /**
     * 
     * @return Query
     */
    public function moduleStatQuery();

    /**
     * 
     * @return Query
     */
    public function studentStatQuery();
}
