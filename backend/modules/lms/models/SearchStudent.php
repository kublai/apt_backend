<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\user\models\User;
use common\models\user\Role;
use yii\db\ActiveQuery;
use common\models\user\Operator;
use backend\modules\lms\controllers\CampaignController;

/**
 * SearchUser represents the model behind the search form about `backend\modules\user\models\User`.
 */
class SearchStudent extends User {

    /**
     *
     * @var integer
     */
    public $campaign_id;

    /**
     *
     * @var boolean 
     */
    public $inCampaign = true;

    /**
     *
     * @var string
     */
    public $operatorName;

    /**
     *
     * @var string
     */
    public $instructorName;

    /**
     *
     * @var string
     */
    public $regionName;

    /**
     *
     * @var Operator 
     */
    protected $operator;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id'], 'integer'],
            [['username', 'name', 'last_name', 'operatorName', 'instructorName', 'regionName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = $this->getQuery();
        $dataProvider = new ActiveDataProvider([
            'query' => $query
            , 'pagination' => [
                'pageSize' => 20
                , 'pageParam' => 'st_page'
                , 'pageSizeParam' => 'st_per-page'
                , 'params' => array_merge(Yii::$app->request->get(), [
                    'id' => $this->campaign_id
                    , 'tab' => CampaignController::VIEW_TAB_STUDENTS
                ])
            ]
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $this->addFilters($query);
        return $dataProvider;
    }

    /**
     * Gets the base query for the search
     * 
     * @return ActiveQuery
     */
    public function getQuery() {
        $query = User::find()
                ->leftJoin('lms_student ls', [
                    'and'
                    , 'ls.user_id = user.id'
                    , ['ls.campaign_id' => $this->campaign_id]
                ])
                ->leftJoin('user ins', 'ins.id = ls.instructor_id')
                ->leftJoin('lms_instructor li', [
                    'and'
                    , 'li.user_id = ls.instructor_id'
                    , 'li.campaign_id = ls.campaign_id'
                ])
                ->leftJoin('user opt', 'opt.id = li.operator_id')
                ->leftJoin('geo_location', 'geo_location.id = user.location_id')
                ->leftJoin('geo_region', 'geo_region.id = geo_location.region_id');
        if ($this->inCampaign === true) {
            $query->andWhere(['ls.campaign_id' => $this->campaign_id]);
        } else {
            $query->andWhere(['ls.campaign_id' => null]);
        }
        $query->andWhere(['user.role' => Role::STUDENT]);
        if ($this->operator !== null) {
            $query->andWhere(['user.operator_id' => $this->operator->id]);
        }
        return $query;
    }

    /**
     * Add filters to the query depending on the loaded properties.
     * 
     * @param ActiveQuery $query
     */
    public function addFilters(ActiveQuery $query) {
        if (!empty($this->id)) {
            $query->andFilterWhere(['user.id' => $this->id]);
        }
        if (!empty($this->operatorName)) {
            $query->andFilterWhere([
                'or'
                , ['like', 'opt.name', '%' . $this->operatorName . '%', false]
                , ['like', 'opt.last_name', '%' . $this->operatorName . '%', false]
                , ['like', 'CONCAT(opt.name, " ", opt.last_name)', '%' . $this->operatorName . '%', false]
            ]);
        }
        if (!empty($this->instructorName)) {
            $query->andFilterWhere([
                'or'
                , ['like', 'ins.name', '%' . $this->instructorName . '%', false]
                , ['like', 'ins.last_name', '%' . $this->instructorName . '%', false]
                , ['like', 'CONCAT(ins.name, " ", ins.last_name)', '%' . $this->instructorName . '%', false]
            ]);
        }
        if (!empty($this->regionName)) {
            $query->andFilterWhere(['like', 'geo_region.name', '%' . $this->regionName . '%', false]);
        }
        if (!empty($this->username)) {
            $query->andFilterWhere(['like', 'user.username', $this->username . '%', false]);
        }
        if (!empty($this->name)) {
            $query->andFilterWhere(['like', 'user.name', '%' . $this->name . '%', false]);
        }
        if (!empty($this->last_name)) {
            $query->andFilterWhere(['like', 'user.last_name', '%' . $this->last_name . '%', false]);
        }
        if (!empty($this->place_id)) {
            $query->andFilterWhere(['place_id' => $this->place_id]);
        } elseif (!empty($this->region_id)) {
            $query->andFilterWhere(['region_id' => $this->region_id]);
        } elseif (!empty($this->zone_id)) {
            $query->andFilterWhere(['zone_id' => $this->zone_id]);
        } elseif (!empty($this->country_id)) {
            $query->andFilterWhere(['country_id' => $this->country_id]);
        }
    }

    /**
     * 
     * @param Operator $user
     */
    public function setOperator(Operator $user = null) {
        $this->operator = $user;
    }

    /**
     * 
     * @return Operator
     */
    public function getOperator() {
        return $this->operator;
    }

    public function behaviors() {
        return [];
    }

}
