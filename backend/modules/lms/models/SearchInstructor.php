<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\user\models\User;
use common\models\user\Role;
use yii\db\ActiveQuery;
use common\models\user\Operator;
use backend\modules\lms\controllers\CampaignController;

/**
 * SearchUser represents the model behind the search form about `backend\modules\user\models\User`.
 */
class SearchInstructor extends User {

    /**
     *
     * @var integer
     */
    public $campaign_id;

    /**
     *
     * @var boolean 
     */
    public $inCampaign = true;

    /**
     *
     * @var string
     */
    public $operatorName;

    /**
     *
     * @var Operator 
     */
    protected $operator;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'country_id', 'zone_id', 'region_id', 'place_id'], 'integer'],
            [['username', 'name', 'last_name', 'operator', 'operatorName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = $this->getQuery();
        $dataProvider = new ActiveDataProvider([
            'query' => $query
            , 'pagination' => [
                'pageParam' => 'in_page'
                , 'pageSizeParam' => 'in_per-page'
                , 'params' => array_merge(Yii::$app->request->get(), [
                    'id' => $this->campaign_id
                    , 'tab' => CampaignController::VIEW_TAB_INSTRUCTORS
                ])
            ]
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $this->addFilters($query);
        return $dataProvider;
    }

    /**
     * Gets the base query for the search
     * 
     * @return ActiveQuery
     */
    public function getQuery() {
        $query = User::find()
                        ->leftJoin('lms_instructor', [
                            'and',
                            'lms_instructor.user_id = user.id',
                            ['campaign_id' => $this->campaign_id]
                        ])->leftJoin('user opt', [
            'and',
            'opt.id = lms_instructor.operator_id'
        ]);
        if ($this->inCampaign === true) {
            $query->andWhere(['campaign_id' => $this->campaign_id]);
        } else {
            $query->andWhere(['campaign_id' => null]);
        }
        $query->andWhere(['user.role' => Role::INSTRUCTOR]);
        if ($this->operator !== null) {
            $query->andWhere(['user.operator_id' => $this->operator->id]);
        }
        $query->joinWith('location');
        return $query;
    }

    /**
     * Add filters to the query depending on the loaded properties.
     * 
     * @param ActiveQuery $query
     */
    public function addFilters(ActiveQuery $query) {
        if (!empty($this->id)) {
            $query->andFilterWhere(['user.id' => $this->id]);
        }

        if (!empty($this->username)) {
            $query->andFilterWhere(['like', 'user.username', $this->username . '%', false]);
        }
        if (!empty($this->name)) {
            $query->andFilterWhere(['like', 'user.name', '%' . $this->name . '%', false]);
        }
        if (!empty($this->last_name)) {
            $query->andFilterWhere(['like', 'user.last_name', '%' . $this->last_name . '%', false]);
        }
        if (!empty($this->operatorName)) {
            $query->andFilterWhere([
                'or'
                , ['like', 'opt.name', '%' . $this->operatorName . '%', false]
                , ['like', 'opt.last_name', '%' . $this->operatorName . '%', false]
                , ['like', 'CONCAT(opt.name, " ", opt.last_name)', '%' . $this->operatorName . '%', false]
            ]);
        }
        if (!empty($this->place_id)) {
            $query->andFilterWhere(['place_id' => $this->place_id]);
        } elseif (!empty($this->region_id)) {
            $query->andFilterWhere(['region_id' => $this->region_id]);
        } elseif (!empty($this->zone_id)) {
            $query->andFilterWhere(['zone_id' => $this->zone_id]);
        } elseif (!empty($this->country_id)) {
            $query->andFilterWhere(['country_id' => $this->country_id]);
        }
    }

    /**
     * 
     * @param Operator $user
     */
    public function setOperator(Operator $user = null) {
        $this->operator = $user;
    }

    /**
     * 
     * @return Operator
     */
    public function getOperator() {
        return $this->operator;
    }

    public function behaviors() {
        return [];
    }

}
