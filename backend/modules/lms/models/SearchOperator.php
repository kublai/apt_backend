<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\data\ActiveDataProvider;
use backend\modules\user\models\Operator;
use backend\modules\lms\controllers\CampaignController;

/**
 * SearchOperator represents the model behind the search form about `backend\modules\user\models\User`.
 */
class SearchOperator extends Operator {

    /**
     *
     * @var integer
     */
    public $campaign_id;

    /**
     *
     * @var boolean 
     */
    public $inCampaign = true;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'country_id', 'zone_id', 'region_id', 'place_id'], 'integer'],
            [['username', 'name', 'last_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = $this->getQuery();
        $dataProvider = new ActiveDataProvider([
            'query' => $query
            , 'pagination' => [
                'pageParam' => 'op_page'
                , 'pageSizeParam' => 'op_per-page'
                , 'params' => array_merge(Yii::$app->request->get(), [
                    'id' => $this->campaign_id
                    , 'tab' => CampaignController::VIEW_TAB_STUDENTS
                ])
            ]
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $this->addFilters($query);
        return $dataProvider;
    }

    /**
     * Gets the base query for the search
     * 
     * @return ActiveQuery
     */
    public function getQuery() {
        $query = Operator::find()
                ->leftJoin('lms_operator', [
            'and',
            'lms_operator.operator_id = operator.id',
            ['lms_operator.campaign_id' => $this->campaign_id]
        ]);
        if ($this->inCampaign === true) {
            $query->andWhere(['lms_operator.campaign_id' => $this->campaign_id]);
        } else {
            $query->andWhere(['lms_operator.campaign_id' => null]);
        }
        $query->joinWith('location');
        return $query;
    }

    /**
     * Add filters to the query depending on the loaded properties.
     * 
     * @param ActiveQuery $query
     */
    public function addFilters(ActiveQuery $query) {
        if (!empty($this->id)) {
            $query->andFilterWhere(['operator.id' => $this->id]);
        }

        if (!empty($this->username)) {
            $query->andFilterWhere(['like', 'operator.username', $this->username . '%', false]);
        }
        if (!empty($this->name)) {
            $query->andFilterWhere(['like', 'operator.name', '%' . $this->name . '%', false]);
        }
        if (!empty($this->last_name)) {
            $query->andFilterWhere(['like', 'operator.last_name', '%' . $this->name . '%', false]);
        }
        if (!empty($this->place_id)) {
            $query->andFilterWhere(['place_id' => $this->place_id]);
        } elseif (!empty($this->region_id)) {
            $query->andFilterWhere(['region_id' => $this->region_id]);
        } elseif (!empty($this->zone_id)) {
            $query->andFilterWhere(['zone_id' => $this->zone_id]);
        } elseif (!empty($this->country_id)) {
            $query->andFilterWhere(['country_id' => $this->country_id]);
        }
    }

    public function behaviors() {
        return [];
    }

}
