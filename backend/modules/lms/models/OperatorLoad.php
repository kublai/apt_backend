<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\models;

use Yii;
use custom\base\Model;
use yii\base\InvalidConfigException;
use Exception;

/**
 * 

 */
class OperatorLoad extends Model {

    /**
     *
     * @var Campaign
     */
    protected $campaign;

    /**
     *
     * @var boolean 
     */
    public $all = 0;

    /**
     *
     * @var array
     */
    public $user_ids = [];

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init() {
        if ($this->campaign === null) {
            throw new InvalidConfigException('Campaign must be provided');
        }
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['campaign', 'all', 'user_ids'], 'safe'],
            ['all', 'boolean']
        ];
    }

    /**
     * 
     * @param Campaign $campaign
     */
    public function setCampaign(Campaign $campaign) {
        $this->campaign = $campaign;
    }

    /**
     * 
     * @return Campaign
     */
    public function getCampaign() {
        return $this->campaign;
    }

    /**
     * Processes the model filtering the instructors and including them in the 
     * campaign.
     * 
     * @return boolean
     */
    public function process() {
        if ($this->validate() === false) {
            return false;
        }

        $searchModel = new SearchOperator([
            'campaign_id' => $this->campaign->id,
            'inCampaign' => false
        ]);
        $query = $searchModel->getQuery();
        if ($this->all) {
            $searchModel->load(Yii::$app->request->queryParams);
            $searchModel->addFilters($query);
            $instructors = $query->all();
        } else {
            $instructors = $query->andWhere(['operator.id' => $this->user_ids])->all();
        }
        return $this->processOperators($instructors);
    }

    /**
     * Includes the provided users as operators in the campaign.
     * 
     * @param array $users
     * @return boolean
     */
    protected function processOperators(array $users) {
        $trans = Yii::$app->db->beginTransaction();
        try {
            foreach ($users as $user) {
                $this->campaign->link('operators', $user);
            }
            $trans->commit();
            return true;
        } catch (Exception $ex) {
            $trans->rollback();
            return false;
        }
    }

}
