<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\lms\models\Campaign;
use common\models\user\Role;

/**
 * SearchCampaign represents the model behind the search form about `backend\modules\lms\models\Campaign`.
 */
class SearchCampaign extends Campaign {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'location_id', 'begin_at', 'end_at', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Campaign::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'location_id' => $this->location_id,
            'begin_at' => $this->begin_at,
            'end_at' => $this->end_at,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        if (Role::check(Role::OPERATOR, Yii::$app->user->role)) {
            $query->joinWith('operators');
            $query->andWhere(['operator.id' => Yii::$app->user->id]);
        }
        return $dataProvider;
    }

}
