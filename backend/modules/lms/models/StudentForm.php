<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\models;

use Yii;
use custom\base\Model;
use yii\base\InvalidConfigException;
use common\models\lms\Student,
    common\models\lms\Campaign,
    common\models\lms\Instructor;
use common\models\user\User;

/**
 * 

 */
class StudentForm extends Model {

    /**
     *
     * @var Student
     */
    protected $student;

    /**
     *
     * @var Campaign
     */
    protected $campaign;

    /**
     *
     * @var int
     */
    public $instructorId;

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init() {
        if ($this->student === null) {
            throw new InvalidConfigException('Student must be provided');
        }
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['instructorId', 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'instructorId' => Yii::t('backend/actions', 'Current')
        ];
    }

    /**
     * 
     * @param Student $s
     */
    public function setStudent(Student $s) {
        $this->student = $s;
        $this->instructorId = $s->instructor_id;
        $this->campaign = $s->campaign;
    }

    /**
     * 
     * @return Student
     */
    public function getStudent() {
        return $this->student;
    }

    /**
     * 
     * @return Campaign
     */
    public function getCampaign() {
        return $this->campaign;
    }

    /**
     * Processes the data and changes the instructor of the student.
     * 
     * @return boolean
     */
    public function process() {
        if ($this->validate() === false) {
            return false;
        }
        $instructor = Instructor::findOne([
                    'campaign_id' => $this->student->campaign_id
                    , 'user_id' => $this->instructorId
                    , 'operator_id' => $this->student->user->operator_id
        ]);
        if ($instructor === null) {
            $this->addError('instructorId', Yii::t('backend/lms/campaign', 'Invalid instructor selected'));
            return false;
        }
        $this->student->instructor_id = $instructor->user_id;
        if ($this->student->save() === false) {
            $this->addError('instructorId', Yii::t('backend/lms/campaign', 'An error ocurred on changing the student instructor'));
            return false;
        }
        return true;
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getInstructors() {
        return User::find()
                        ->innerJoin('lms_instructor', 'lms_instructor.user_id = user.id')
                        ->andWhere(['lms_instructor.campaign_id' => $this->student->campaign_id, 'lms_instructor.operator_id' => $this->student->user->operator_id])
                        ->orderBy(['user.name' => SORT_ASC, 'user.last_name' => SORT_ASC]);
    }

}
