<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\models;

use custom\base\Model;
use common\models\lms\CampaignModule;
use Yii;

/**
 * 

 */
class ModuleMoveForm extends Model {

    /**
     *
     * @var int
     */
    public $campaign_id;

    /**
     *
     * @var int
     */
    public $module_id;

    /**
     *
     * @var int
     */
    public $order;

    /**
     * 
     * @return array
     */
    public function rules() {
        return [
            [['campaign_id', 'module_id', 'order'], 'required'],
            [['campaign_id', 'module_id', 'order'], 'integer']
        ];
    }

    /**
     * 
     * @return boolean
     */
    public function process() {
        if ($this->validate() === false) {
            return false;
        }

        $pk = [
            'campaign_id' => $this->campaign_id,
            'module_id' => $this->module_id
        ];
        $cModule = CampaignModule::findOne($pk);
        if ($cModule === null) {
            $cModule = new CampaignModule($pk);
        }
        if ($this->order == 0) {
            if ($cModule->campaign->isActive()) {
                $this->addError('general', Yii::t('common/lms/campaign', 'Campaign has begun and therefore cannot be modified.'));
            } else if ($cModule->delete() === false) {
                $this->addError('general', Yii::t('common/lms/campaign', 'An error has ocurred when trying to exclude the module from the campaign.'));
            }
        } else {
            $cModule->order = $this->order;
            if ($cModule->save() === false) {
                $this->addError('general', Yii::st('common/lms/campaign', 'An error has ocurred when trying to move the module.'));
            }
        }
        return $this->createResult();
    }

    /**
     * 
     * @return array
     */
    protected function createResult() {
        return [
            'result' => $this->hasErrors(),
            'errors' => $this->getErrors()
        ];
    }

}
