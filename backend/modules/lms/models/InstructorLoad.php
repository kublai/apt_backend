<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\models;

use Yii;
use custom\base\Model;
use yii\base\InvalidConfigException;
use Exception;
use common\models\user\Operator;

/**
 * 

 */
class InstructorLoad extends Model {

    /**
     *
     * @var Campaign
     */
    protected $campaign;

    /**
     *
     * @var Operator
     */
    protected $operator;

    /**
     *
     * @var boolean 
     */
    public $all = 0;

    /**
     *
     * @var array
     */
    public $user_ids = [];

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init() {
        if ($this->campaign === null) {
            throw new InvalidConfigException('Campaign must be provided');
        }
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['campaign', 'all', 'user_ids'], 'safe'],
            ['all', 'boolean']
        ];
    }

    /**
     * 
     * @param Campaign $campaign
     */
    public function setCampaign(Campaign $campaign) {
        $this->campaign = $campaign;
    }

    /**
     * 
     * @return Campaign
     */
    public function getCampaign() {
        return $this->campaign;
    }

    /**
     * 
     * @param Operator $user
     */
    public function setOperator(Operator $user) {
        $this->operator = $user;
    }

    /**
     * 
     * @return Operator
     */
    public function getOperator() {
        return $this->operator;
    }

    /**
     * Processes the model filtering the instructors and including them in the 
     * campaign.
     * 
     * @return boolean
     */
    public function process() {
        if ($this->validate() === false) {
            return false;
        }

        $searchModel = new SearchInstructor([
            'campaign_id' => $this->campaign->id,
            'inCampaign' => false
        ]);
        $query = $searchModel->getQuery();
        if ($this->all) {
            $searchModel->load(Yii::$app->request->queryParams);
            $searchModel->addFilters($query);
            $instructors = $query->all();
        } else {
            $instructors = $query->andWhere(['user.id' => $this->user_ids])->all();
        }
        return $this->processInstructors($instructors);
    }

    /**
     * Includes the provided users as instructors in the campaign.
     * 
     * @param array $users
     * @return boolean
     */
    protected function processInstructors(array $users) {
        $trans = Yii::$app->db->beginTransaction();
        try {
            foreach ($users as $user) {
                $this->campaign->link('instructors', $user, [
                    'operator_id' => $this->operator->id
                ]);
            }
            $trans->commit();
            return true;
        } catch (Exception $ex) {
            $trans->rollback();
            return false;
        }
    }

}
