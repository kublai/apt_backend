<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\models;

use Yii;
use custom\base\Model;
use yii\base\InvalidConfigException;
use common\models\lms\Instructor,
    common\models\lms\Student,
    common\models\lms\Module;
use yii\db\Query;

/**
 * @property Instructor $instructor The instructor itself
 * @property \common\models\lms\Campaign $campaign The instructor campaign
 * @property \common\models\user\User $user The instructor user
 * 

 */
class InstructorView extends Model implements StatsViewInterface {

    /**
     *
     * @var Instructor
     */
    protected $instructor;

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init() {
        if ($this->instructor === null) {
            throw new InvalidConfigException('Instructor must be provided');
        }
    }

    /**
     * 
     * @param Instructor $i
     */
    public function setInstructor(Instructor $i) {
        $this->instructor = $i;
    }

    /**
     * 
     * @return Instructor
     */
    public function getInstructor() {
        return $this->instructor;
    }

    /**
     * 
     * @return \common\models\user\User
     */
    public function getUser() {
        return $this->instructor->user;
    }

    /**
     * @inheritdoc
     */
    public function getCampaign() {
        return $this->instructor->campaign;
    }

    /**
     * @inheritdoc
     */
    public function getStudentsWithSessions() {
        return Student::find()
                        ->innerJoin('lms_student_module lsm', 'lsm.student_id = lms_student.user_id AND lsm.campaign_id = lms_student.campaign_id')
                        ->andWhere([
                            'lms_student.instructor_id' => $this->instructor->user_id
                            , 'lms_student.campaign_id' => $this->campaign->id
                        ])
                        ->groupBy(['lsm.student_id'])->count();
    }

    /**
     * @inheritdoc
     */
    public function getStudentsWithoutSessions() {
        return Student::find()
                        ->leftJoin('lms_student_module lsm', 'lsm.student_id = lms_student.user_id AND lsm.campaign_id = lms_student.campaign_id')
                        ->andWhere([
                            'lms_student.instructor_id' => $this->instructor->user_id
                            , 'lms_student.campaign_id' => $this->campaign->id
                        ])
                        ->andWhere(['lsm.student_id' => null])->count();
    }

    /**
     * @inheritdoc
     */
    public function moduleStatQuery() {
        // When we get count(lsm.completed) we are counting registers with a 0 and registers with a 1 in the completed column therefore
        // we are getting the total number of sessions (finished and not finished)
        return (new Query())
                        ->select(['CONCAT(us.name, " ", us.last_name) user_name',
                            'lm.id', 'lm.code', 'lm.name', 
                            'count(lsm.completed) completed', 
                            'SUM( end_at - begin_at ) as  experienced_time'
                        ])
                        ->from('lms_module lm')
                        ->innerJoin('lms_campaign_module lcm', 'lcm.module_id = lm.id')
                        ->innerJoin('lms_student_module lsm', 'lsm.module_id = lcm.module_id AND lsm.campaign_id = lcm.campaign_id')
                        ->innerJoin('lms_student ls', 'ls.user_id = lsm.student_id AND ls.campaign_id = lsm.campaign_id')
                        ->innerJoin('lms_student_session lss', 'lss.student_id = lsm.student_id AND lss.module_id = lsm.module_id AND lss.campaign_id = lsm.campaign_id')
                        ->innerJoin('user us', 'us.id = ls.user_id')
                        ->andWhere([
                            'ls.instructor_id' => $this->instructor->user_id
                            , 'lcm.campaign_id' => $this->instructor->campaign_id
                        ])
                        ->groupBy('lm.id','lm_codes')
                        ->orderBy(['lcm.order' => SORT_ASC]);
    }

    /**
     * @inheritdoc
     */
    public function studentStatQuery() {
        //for the students we will show the completed status as is to facilitate the analisys of the data whe the file is downloaded
        return (new Query())
                        ->select(['CONCAT(us.name, " ", us.last_name) user_name',
                            'lm.id', 'lm.code', 'lm.name', 
                            'lss.completed completed',
                            '( end_at - begin_at ) as  experienced_time',
                            'FROM_UNIXTIME(lss.begin_at) as session_date' 
                        ])
                        ->from('lms_module lm')
                        ->innerJoin('lms_campaign_module lcm', 'lcm.module_id = lm.id')
                        ->innerJoin('lms_student_module lsm', 'lsm.module_id = lcm.module_id AND lsm.campaign_id = lcm.campaign_id')
                        ->innerJoin('lms_student ls', 'ls.user_id = lsm.student_id AND ls.campaign_id = lsm.campaign_id')
                        ->innerJoin('lms_student_session lss', 'lss.student_id = lsm.student_id AND lss.module_id = lsm.module_id AND lss.campaign_id = lsm.campaign_id')
                        ->innerJoin('user us', 'us.id = ls.user_id')
                        ->andWhere([
                            'ls.instructor_id' => $this->instructor->user_id
                            , 'ls.campaign_id' => $this->instructor->campaign_id
                        ])
                        ->orderBy(['user_name' => SORT_ASC, 'lcm.order' =>SORT_ASC, 'session_date' => SORT_ASC]);
    }

    /**
     * @inheritdoc
     */
    public function getStudentSessions() {
        return $this->instructor->getStudentSessions();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'studentsWithSessions' => Yii::t('common/lms/campaign', 'Students with sessions')
            , 'studentsWithoutSessions' => Yii::t('common/lms/campaign', 'Students without sessions')
        ];
    }

}
