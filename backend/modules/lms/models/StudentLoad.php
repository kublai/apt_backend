<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\models;

use Yii;
use custom\base\Model;
use yii\base\InvalidConfigException;
use Exception;
use common\models\user\User,
    common\models\user\Operator;
use common\models\lms\Student;
use custom\db\exceptions\SaveException;

/**
 * 

 */
class StudentLoad extends Model {

    /**
     *
     * @var Campaign
     */
    protected $campaign;

    /**
     *
     * @var User
     */
    protected $instructor;

    /**
     *
     * @var Operator
     */
    protected $operator;
    
    /**
     *
     * @var boolean 
     */
    public $all = 0;

    /**
     *
     * @var array
     */
    public $user_ids = [];

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init() {
        if ($this->campaign === null) {
            throw new InvalidConfigException('Campaign must be provided');
        }
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['campaign', 'instructor', 'all', 'user_ids'], 'safe'],
            ['all', 'boolean']
        ];
    }

    /**
     * 
     * @param Campaign $campaign
     */
    public function setCampaign(Campaign $campaign) {
        $this->campaign = $campaign;
    }

    /**
     * 
     * @return Campaign
     */
    public function getCampaign() {
        return $this->campaign;
    }

    /**
     * 
     * @param User $user
     */
    public function setInstructor(User $user) {
        $this->instructor = $user;
    }

    /**
     * 
     * @return User
     */
    public function getInstructor() {
        return $this->instructor;
    }

    
    /**
     * 
     * @param Operator $user
     */
    public function setOperator(Operator $user) {
        $this->operator = $user;
    }

    /**
     * 
     * @return Operator
     */
    public function getOperator() {
        return $this->operator;
    }

    /**
     * Processes the model filtering the instructors and including them in the 
     * campaign.
     * 
     * @return boolean
     */
    public function process() {
        if ($this->validate() === false) {
            return false;
        }

        $searchModel = new SearchStudent([
            'campaign_id' => $this->campaign->id,
            'inCampaign' => false,
            'operator' => $this->operator
        ]);
        $query = $searchModel->getQuery();
        if ($this->all) {
            $searchModel->load(Yii::$app->request->queryParams);
            $searchModel->addFilters($query);
            $instructors = $query->all();
        } else {
            $instructors = $query->andWhere(['user.id' => $this->user_ids])->all();
        }
        return $this->processStudents($instructors);
    }

    /**
     * Includes the provided users as instructors in the campaign.
     * 
     * @param array $users
     * @return boolean
     */
    protected function processStudents(array $users) {
        $trans = Yii::$app->db->beginTransaction();
        try {
            foreach ($users as $user) {
                $st = new Student();
                $st->campaign_id = $this->campaign->id;
                $st->user_id = $user->id;
                $st->instructor_id = $this->instructor->id;
                if ($st->save() === false) {
                    throw new SaveException($st);
                }
            }
            $trans->commit();
            return true;
        } catch (Exception $ex) {
            $trans->rollback();
            return false;
        }
    }

}
