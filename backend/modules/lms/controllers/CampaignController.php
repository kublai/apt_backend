<?php

namespace backend\modules\lms\controllers;

use Yii;
use custom\filters\AccessControl;
use backend\modules\lms\models\Campaign;
use backend\modules\lms\models\SearchCampaign;
use backend\controllers\CrudController;
use yii\web\NotFoundHttpException,
    yii\web\ForbiddenHttpException;
use yii\web\Response;
use backend\modules\lms\models\ModuleMoveForm;
use backend\modules\lms\models\SearchInstructor,
    backend\modules\lms\models\SearchStudent,
    backend\modules\lms\models\SearchOperator,
    backend\modules\lms\models\InstructorLoad,
    backend\modules\lms\models\StudentLoad,
    backend\modules\lms\models\StudentForm,
    backend\modules\lms\models\OperatorLoad,
    backend\modules\lms\models\SearchStudentSession,
    backend\modules\lms\models\InstructorFileIntegration,
    backend\modules\lms\models\InstructorView,
    backend\modules\lms\models\OperatorView;
use common\models\user\User,
    common\models\user\Role;
use backend\modules\user\models\Operator;
use yii\helpers\ArrayHelper;
use common\models\lms\Student,
    common\models\lms\StudentSession,
    common\models\lms\Instructor,
    common\models\lms\StudentModule,
    common\models\lms\CampaignModule;
use yii\db\IntegrityException;
use common\models\lms\Report;

/**
 * CampaignController implements the CRUD actions for Campaign model.
 */
class CampaignController extends CrudController {

    const VIEW_TAB_MODULES = 'modules';
    const VIEW_TAB_OPERATORS = 'operators';
    const VIEW_TAB_INSTRUCTORS = 'instructors';
    const VIEW_TAB_STUDENTS = 'students';
    //Stats view tabs
    const VIEW_TAB_STATS_MAIN = 'main';
    const VIEW_TAB_STATS_MODULE = 'per-module';
    const VIEW_TAB_STATS_STUDENT = 'per-student';

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return ArrayHelper::merge(parent::behaviors(), [
                    'access' => [
                        'class' => AccessControl::className(),
                        'rules' => [
                            [
                                'actions' => ['module-list', 'module-move', 'operator-list', 'delete-operator', 'operator-view'],
                                'allow' => true,
                                'roles' => ['@'],
                                'userRoles' => [Role::ADMIN, Role::SUPER_ADMIN]
                            ],
                            [
                                'actions' => ['index', 'view', 'instructor-list', 'student-list', 'delete-instructor', 'delete-student', 'student-view', 'delete-student-session', 'delete-student-sessions', 'integrate-instructor-csv', 'instructor-view', 'student-instructor', 'instructor-synclog', 'lms-report','update-module-status'],
                                'allow' => true,
                                'roles' => ['@'],
                                'userRoles' => [Role::ADMIN, Role::SUPER_ADMIN, Role::OPERATOR]
                            ]
                        ],
                    ]
        ]);
    }

    /**
     * Lists all Campaign models.
     * 
     * @return \yii\web\Response
     */
    public function actionIndex() {
        $searchModel = new SearchCampaign();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Gets a csv report of the campaigns.
     */
    public function actionLmsReport() {
        $name = Yii::t('backend/lms/campaign', 'Report') . '_' . (new \DateTime())->format('YmdHis') . '.csv';
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $name);
        $output = fopen('php://output', 'w');
        $report = new Report([
            'operator' => Role::isOperator(Yii::$app->user) ? Yii::$app->user->identity : null
        ]);
        foreach ($report->fullLms(true) as $data) {
            fputcsv($output, $data);
        }
        fclose($output);
    }

    /**
     * Displays a single Campaign model.
     * 
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionView($id) {
        $model = $this->findModel($id);  //campaign 
        $searchOperator = new SearchOperator([
            'campaign_id' => $model->id,
            'inCampaign' => true
        ]);
        $operator = $operatorView = null;
        if (Role::isOperator(Yii::$app->user)) {
            $operator = $this->findOperator($model->id, Yii::$app->user->id);
            $operatorView = new OperatorView([
                'campaign' => $model
                , 'operator' => $operator
            ]);
        }
        $searchInstructor = new SearchInstructor([
            'campaign_id' => $model->id,
            'inCampaign' => true,
            'operator' => $operator
        ]);
        $searchStudent = new SearchStudent([
            'campaign_id' => $model->id,
            'inCampaign' => true,
            'operator' => $operator
        ]);
        return $this->render('view', [
                    'model' => $model,
                    'searchOperator' => $searchOperator,
                    'searchInstructor' => $searchInstructor,
                    'searchStudent' => $searchStudent,
                    'operatorView' => $operatorView
        ]);
    }

    /**
     * Creates a new Campaign model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * 
     * @return \yii\web\Response
     */
    public function actionCreate() {
        $model = new Campaign();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Campaign model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * 
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Campaign model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionDelete($id) {
        try {
            $this->findModel($id)->delete();
        } catch (IntegrityException $ex) {
            Yii::$app->session->setFlash('error', Yii::t('backend/lms/campaign', 'The campaign contains student progress and therefore cannot be deleted'));
        }
        return $this->redirect(['index']);
    }

    /**
     * 
     * @param interger $id
     * @return \yii\web\Response
     */
    public function actionModuleList($id) {
        $model = $this->findModel($id);

        return $this->render('module-list', ['model' => $model]);
    }

    /**
     * 
     * @param interger $id
     */
    public function actionModuleMove($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $moveModel = new ModuleMoveForm(['campaign_id' => $model->id]);
        $moveModel->load(Yii::$app->request->post(), '');
        return $moveModel->process();
    }

    /**
     * 
     * @param integer $campaignId
     * @return \yii\web\Response
     */
    public function actionOperatorList($campaignId) {
        if (Yii::$app->request->isAjax === true) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $this->processOperators($campaignId);
        } else {
            $model = $this->findModel($campaignId);
            $searchModel = new SearchOperator([
                'campaign_id' => $model->id,
                'inCampaign' => false
            ]);
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->render('operator-list', [
                        'model' => $model,
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider
            ]);
        }
    }

    /**
     * 
     * @param integer $campaignId
     * @return array
     */
    protected function processOperators($campaignId) {
        $model = $this->findModel($campaignId);
        $oLoad = new OperatorLoad(['campaign' => $model]);
        $return = [
            'result' => false
        ];
        if ($oLoad->load(Yii::$app->request->post(), '')) {
            $return['result'] = $oLoad->process();
            if ($return['result'] === false) {
                $return['errors'] = $oLoad->getErrors();
            }
        }
        return $return;
    }

    /**
     * Deletes an operator from a campaign.
     * 
     * @param int $campaignId
     * @param int $operatorId
     * @return \yii\web\Response
     */
    public function actionDeleteOperator($campaignId, $operatorId) {
        $campaign = $this->findModel($campaignId);
        $operator = $this->findOperator($campaignId, $operatorId);
        try {
            $campaign->unlink('operators', $operator, true);
        } catch (IntegrityException $ex) {
            Yii::$app->session->setFlash('error', Yii::t('common/lms/user', 'The operator you are trying to delete has students with session data and therefore can not be deleted'));
        }
        return $this->redirect(['view', 'tab' => self::VIEW_TAB_OPERATORS, 'id' => $campaign->id]);
    }

    /**
     * Renders the operator stats view.
     * 
     * @param integer $campaignId
     * @param integer $operatorId
     * @return \yii\web\Response
     */
    public function actionOperatorView($campaignId, $operatorId) {
        return $this->render('operator-view', [
                    'model' => new OperatorView([
                        'campaign' => $this->findModel($campaignId)
                        , 'operator' => $this->findOperator($campaignId, $operatorId)
                            ])
        ]);
    }

    /**
     * 
     * @param int $campaignId
     * @param int $operatorId
     * @return \yii\web\Response
     */
    public function actionInstructorList($campaignId, $operatorId) {
        if (Yii::$app->request->isAjax === true) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $this->processInstructors($campaignId, $operatorId);
        } else {
            $model = $this->findModel($campaignId);
            $operator = $this->findOperator($campaignId, $operatorId);
            $searchModel = new SearchInstructor([
                'campaign_id' => $model->id,
                'inCampaign' => false,
                'operator' => $operator
            ]);
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->render('instructor-list', [
                        'model' => $model,
                        'oModel' => $operator,
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider
            ]);
        }
    }

    /**
     * 
     * @param int $campaignId
     * @param int $operatorId
     * @return array
     */
    protected function processInstructors($campaignId, $operatorId) {
        $model = $this->findModel($campaignId);
        $operator = $this->findOperator($campaignId, $operatorId);
        $iLoad = new InstructorLoad(['campaign' => $model, 'operator' => $operator]);
        $return = [
            'result' => false
        ];
        if ($iLoad->load(Yii::$app->request->post(), '')) {
            $return['result'] = $iLoad->process();
            if ($return['result'] === false) {
                $return['errors'] = $iLoad->getErrors();
            }
        }
        return $return;
    }

    /**
     * Deletes an instructor from a campaign.
     * 
     * @param int $campaignId
     * @param int $instructorId
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDeleteInstructor($campaignId, $instructorId) {
        $campaign = $this->findModel($campaignId);
        $instructor = $this->findInstructor($campaignId, $instructorId);
        try {
            $campaign->unlink('instructors', $instructor, true);
        } catch (IntegrityException $ex) {
            Yii::$app->session->setFlash('error', Yii::t('common/lms/user', 'The instructor you are trying to delete has students with session data and therefore can not be deleted'));
        }
        return $this->redirect(['view', 'tab' => self::VIEW_TAB_INSTRUCTORS, 'id' => $campaign->id]);
    }

    /**
     * Integrates the instructor csv.
     * 
     * @param int $campaignId
     * @param int $instructorId
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionIntegrateInstructorCsv($campaignId, $instructorId) {
        $instructor = $this->findInstructorModel($campaignId, $instructorId);
        $model = new InstructorFileIntegration(['instructor' => $instructor]);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('common/lms/integration', 'The file has been integrated successfully'));
        }
        return $this->render('integrate-instructor-csv', [
                    'campaign' => $instructor->campaign
                    , 'model' => $model
        ]);
    }

    /**
     * Renders the instructor stats view.
     * 
     * @param integer $campaignId
     * @param integer $instructorId
     * @return \yii\web\Response
     */
    public function actionInstructorView($campaignId, $instructorId) {
        return $this->render('instructor-view', [
                    'model' => new InstructorView([
                        'instructor' => $this->findInstructorModel($campaignId, $instructorId)
                            ])
        ]);
    }

    /**
     * Renders the instructor sync log.
     * 
     * @param int $campaignId
     * @param int $instructorId
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionInstructorSynclog($campaignId, $instructorId) {
        $instructor = $this->findInstructorModel($campaignId, $instructorId);
        return $this->render('instructor-synclog', [
                    'model' => $instructor
        ]);
    }

    /**
     * 
     * @param integer $campaignId
     * @param integer $instructorId
     * @return \yii\web\Response
     */
    public function actionStudentList($campaignId, $operatorId, $instructorId) {
        if (Yii::$app->request->isAjax === true) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $this->processStudents($campaignId, $operatorId, $instructorId);
        } else {
            $model = $this->findModel($campaignId);
            $oModel = $this->findOperator($campaignId, $operatorId);
            $iModel = $this->findInstructor($campaignId, $instructorId);
            $searchModel = new SearchStudent([
                'campaign_id' => $model->id,
                'inCampaign' => false,
                'operator' => $oModel
            ]);
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->render('student-list', [
                        'model' => $model,
                        'iModel' => $iModel,
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider
            ]);
        }
    }

    /**
     * 
     * @param integer $campaignId
     * @param integer $instructorId
     * @return array
     */
    protected function processStudents($campaignId, $operatorId, $instructorId) {
        $model = $this->findModel($campaignId);
        $oModel = $this->findOperator($campaignId, $operatorId);
        $iModel = $this->findInstructor($campaignId, $instructorId);
        $sLoad = new StudentLoad(['campaign' => $model, 'operator' => $oModel, 'instructor' => $iModel]);
        $return = [
            'result' => false
        ];
        if ($sLoad->load(Yii::$app->request->post(), '')) {
            $return['result'] = $sLoad->process();
            if ($return['result'] === false) {
                $return['errors'] = $sLoad->getErrors();
            }
        }
        return $return;
    }

    /**
     * 
     * @param integer $campaignId
     * @param integer $studentId
     * @return \yii\web\Response
     */
    public function actionDeleteStudent($campaignId, $studentId) {
        $campaign = $this->findModel($campaignId);
        $student = $this->findStudent($campaignId, $studentId);
        if ($student->getSessions()->exists()) {
            Yii::$app->session->setFlash('error', Yii::t('common/lms/user', 'The student you are trying to delete has session data and therefore can not be deleted'));
        } else {
            $campaign->unlink('students', $student, true);
        }
        return $this->redirect(['view', 'tab' => self::VIEW_TAB_STUDENTS, 'id' => $campaign->id]);
    }

    /**
     * 
     * @param integer $campaignId
     * @param integer $studentId
     * @return \yii\web\Response
     */
    public function actionStudentView($campaignId, $studentId = null) {
        $campaign = $this->findModel($campaignId);
        $student = null;
        if ($studentId !== null) {
            $student = $this->findStudent($campaignId, $studentId);
        }
        $searchModel = new SearchStudentSession([
            'campaign_id' => $campaign->id,
            'student_id' => $studentId
        ]);
        if (Role::isOperator(Yii::$app->user)) {
            $searchModel->setOperator(Yii::$app->user->identity);
        }

        $searchModel->load(Yii::$app->request->queryParams);
        return $this->render('student-view', [
                    'searchModel' => $searchModel,
                    'campaign' => $campaign,
                    'student' => $student
        ]);
    }

    /**
     * Renders the student change instructor view.
     * 
     * @param integer $campaignId
     * @param integer $studentId
     * @return \yii\web\Response
     */
    public function actionStudentInstructor($campaignId, $studentId) {
        $model = new StudentForm(['student' => $this->findStudent($campaignId, $studentId)]);
        if ($model->load(Yii::$app->request->post()) && $model->process()) {
            Yii::$app->session->setFlash('success', Yii::t('backend/lms/campaign', 'The instructor of {username} has been changed', [
                        'username' => $model->student->user->fullName
            ]));
        }
        return $this->render('student-instructor', [
                    'model' => $model
        ]);
    }

    /**
     * 
     * @param integer $campaignId
     * @param integer $studentId
     * @return \yii\web\Response
     */
    public function actionDeleteStudentSessions($campaignId, $studentId) {
        $campaign = $this->findModel($campaignId);
        $student = $this->findStudent($campaignId, $studentId);
        try {
            StudentModule::deleteAll(['campaign_id' => $campaign->id, 'student_id' => $student->id]);
            Yii::$app->session->setFlash('success', Yii::t('common/lms/user', 'The sessions data of the student has been cleared'));
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $this->redirect(['student-view', 'campaignId' => $campaign->id, 'studentId' => $student->user_id, 'returnUrl' => Yii::$app->request->get('returnUrl')]);
    }

    /**
     * 
     * @param integer $campaignId
     * @param integer $studentId
     * @return \yii\web\Response
     */
    public function actionDeleteStudentSession($id) {
        $session = $this->findStudentSession($id);
        try {
            $session->delete();
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $this->redirect(['student-view', 'campaignId' => $session->campaign_id, 'studentId' => $session->student_id, 'returnUrl' => Yii::$app->request->get('returnUrl')]);
    }

     /**
      * Toogle the status of a module within a campaign from active -> disabled and from disabled -> active
      * depending on the current status
      * author: Kublai Gómez
      * date: 26-08-2020
      * @param integer $moduleId
      * @param integer $campaignId
      *  @return \yii\web\Response
      */
    public function actionUpdateModuleStatus(){
        if (Yii::$app->request->isAjax === true){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $params = \Yii::$app->getRequest()->getBodyParams();
            $module_id = intval($params['moduleId']);
            $campaign_id = intval($params['campaignId']);
            $change_status_to = $params['changeStatusTo'];
            $campaignModule = CampaignModule::find()->where(['module_id' => $module_id, 'campaign_id' => $campaign_id])->one();
            if ($change_status_to == "true"){
                $campaignModule->active = 1;
            }else{
                $campaignModule->active = 0;
            }
            $result = $campaignModule->save();

            return $result;
        }else{
            return; //if it's not an ajax request ignore
        }

    }

    /**
     * Finds the Campaign model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Campaign the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Campaign::findOne($id)) !== null) {
            if (Role::check(Role::OPERATOR, Yii::$app->user->role) && Campaign::isOperator(Yii::$app->user->identity) === false) {
                throw new ForbiddenHttpException('You are trying to access an unreacheable resource for you.');
            }
            date_default_timezone_set($model->location->country->timezone);
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * 
     * @param integer $cId
     * @param integer $oId
     * @return Operator
     * @throws NotFoundHttpException
     */
    protected function findOperator($cId, $oId) {
        if (($model = Operator::find()
                ->innerJoin('lms_operator', 'lms_operator.operator_id = operator.id')
                ->where(['lms_operator.campaign_id' => $cId, 'lms_operator.operator_id' => $oId])
                ->one()) !== null) {
            if (Role::isOperator(Yii::$app->user) && $model->id !== Yii::$app->user->id) {
                throw new ForbiddenHttpException('You are trying to access an unreacheable resource for you.');
            }
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * 
     * @param integer $cId
     * @param integer $iId
     * @return User
     * @throws NotFoundHttpException
     */
    protected function findInstructor($cId, $iId) {
        if (($model = User::find()
                ->innerJoin('lms_instructor', 'user_id = id')
                ->where(['campaign_id' => $cId, 'user_id' => $iId])
                ->one()) !== null) {
            $this->validateOperatorAction($model);
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * 
     * @param integer $cId
     * @param integer $iId
     * @return Instructor
     * @throws NotFoundHttpException
     */
    protected function findInstructorModel($cId, $iId) {
        if (($model = Instructor::findOne(['campaign_id' => $cId, 'user_id' => $iId])) !== null) {
            $this->validateOperatorAction($model->user);
            date_default_timezone_set($model->campaign->location->country->timezone);
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * 
     * @param integer $cId
     * @param integer $sId
     * @return Student
     * @throws NotFoundHttpException
     */
    protected function findStudent($cId, $sId) {
        if (($model = Student::findOne(['campaign_id' => $cId, 'user_id' => $sId])) !== null) {
            $this->validateOperatorAction($model->user);
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * 
     * @param string $id
     * @return StudentSession
     * @throws NotFoundHttpException
     */
    protected function findStudentSession($id) {
        if (($model = StudentSession::findOne(['id' => $id])) !== null) {
            $this->validateOperatorAction($model->student->user);
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * 
     * @param User $user
     * @throws ForbiddenHttpException
     */
    protected function validateOperatorAction(User $user) {
        if (Role::isOperator(Yii::$app->user) && $user->operator_id !== Yii::$app->user->id) {
            throw new ForbiddenHttpException('You are trying to access an unreacheable resource for you.');
        }
    }




}
