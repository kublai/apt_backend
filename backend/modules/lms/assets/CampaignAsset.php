<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\lms\assets;

use yii\web\AssetBundle;

class CampaignAsset extends AssetBundle {

    public $sourcePath = '@lms/assets/';
    public $css = [
        'css/campaign.css'
    ];
    public $js = [
        'js/campaign.js'
    ];
    public $depends = [
        'backend\assets\AppAsset'
    ];

}
