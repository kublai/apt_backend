/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Bytebox.es
 * @version     1.0
 */

(function () {
    (function () {
        var $sortable = $('.sortable.list');
        var url = $sortable.data('url');
        $sortable.on('sortupdate', function (e, ui) {
            var order, id;
            order = ui.item.index() + 1;
            if (ui.endparent.hasClass('available-modules')) {
                if (ui.startparent.hasClass('available-modules')) {
                    return true;
                }
                order = 0;
            }
            id = $(ui.item).find('.module-edit').attr('id');
            $.post(url, {
                'module_id': id,
                'order': order
            });
        });
    })();
    $('#campaign-view-tab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show'); 
    });

    /**
     * function used in the module tab of the campaigns views to update the state
     * of a module (active/inactive) inside a campaign
     * 
     * author: Kublai Gómez
     * date: 26-08-2020
     */
    $('.toogle-module').click(function(){
        let moduleId = this.dataset.moduleid;
        let campaignId = this.dataset.campaignid;
        let status = this.checked; //this is the value towards we will make the change
        $.post('update-module-status', {
            'moduleId': moduleId,
            'campaignId': campaignId,
            'changeStatusTo': status
        }).done(function (data) {
            if (data == false){
              swal("No ha sido posible actualizar el estado");
            }
        });     
    });

    
})();