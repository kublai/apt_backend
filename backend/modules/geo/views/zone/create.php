<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\geo\models\Zone */

$this->title = Yii::t('backend/actions', 'Create {modelClass}', [
            'modelClass' => Yii::t('common/geo/zone', 'Zone')
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/geo/country', 'Countries'), 'url' => ['/geo/country/index']];
$this->params['breadcrumbs'][] = ['label' => $model->country->name, 'url' => ['/geo/country/view', 'id' => $model->country_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/geo/zone', 'Zones'), 'url' => ['index', 'countryId' => $model->country_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zone-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
