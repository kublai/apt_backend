<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\geo\models\Zone */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/geo/country', 'Countries'), 'url' => ['/geo/country/index']];
$this->params['breadcrumbs'][] = ['label' => $model->country->name, 'url' => ['/geo/country/view', 'id' => $model->country_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/geo/zone', 'Zones'), 'url' => ['index', 'countryId' => $model->country_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zone-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend/actions', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a(Yii::t('backend/actions', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend/actions', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])
        ?>
        <?=
        Html::a(Yii::t('common/geo/region', 'Regions'), ['/geo/region/index', 'zoneId' => $model->id], [
            'class' => 'btn btn-info pull-right',
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'created_at:datetime',
            'created_by',
            'updated_at:datetime',
            'updated_by',
        ],
    ])
    ?>

</div>
