<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\geo\models\SearchZone */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common/geo/zone', 'Zones');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/geo/country', 'Countries'), 'url' => ['/geo/country/index']];
$this->params['breadcrumbs'][] = ['label' => $searchModel->country->name, 'url' => ['/geo/country/view', 'id' => $searchModel->country_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zone-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?=
        Html::a(Yii::t('backend/actions', 'Create {modelClass}', [
                    'modelClass' => Yii::t('common/geo/zone', 'Zone')
                ]), ['create', 'countryId' => $searchModel->country_id], ['class' => 'btn btn-success'])
        ?>
    </p>

    <?php
    $listButton = function ($url, $model) {
        $options = array_merge([
            'title' => Yii::t('common/geo/region', 'Regions'),
            'aria-label' => Yii::t('common/geo/region', 'Regions'),
            'data-pjax' => '0',
        ]);
        return Html::a('<span class="glyphicon glyphicon-th-list"></span>', ['/geo/region/index', 'zoneId' => $model->id], $options);
    };
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            'created_at:datetime',
            'created_by',
            'updated_at:datetime',
            'updated_by',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {list}',
                'buttons' => [
                    'list' => $listButton
                ]
            ],
        ],
    ]);
    ?>

</div>
