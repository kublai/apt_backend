<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\geo\models\Region */

$this->title = Yii::t('backend/actions', 'Create {modelClass}', [
            'modelClass' => Yii::t('common/geo/region', 'Region')
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/geo/country', 'Countries'), 'url' => ['/geo/country/index']];
$this->params['breadcrumbs'][] = ['label' => $model->zone->country->name, 'url' => ['/geo/country/view', 'id' => $model->zone->country_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/geo/zone', 'Zones'), 'url' => ['/geo/zone/index', 'countryId' => $model->zone->country_id]];
$this->params['breadcrumbs'][] = ['label' => $model->zone->name, 'url' => ['/geo/zone/view', 'id' => $model->zone_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/geo/region', 'Regions'), 'url' => ['index', 'zoneId' => $model->zone_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
