<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\geo\models\Place */

$this->title = Yii::t('backend/actions', 'Update {modelClass}: ', [
            'modelClass' => Yii::t('common/geo/place', 'Place'),
        ]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/geo/country', 'Countries'), 'url' => ['/geo/country/index']];
$this->params['breadcrumbs'][] = ['label' => $model->region->zone->country->name, 'url' => ['/geo/country/view', 'id' => $model->region->zone->country_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/geo/zone', 'Zones'), 'url' => ['/geo/zone/index', 'countryId' => $model->region->zone->country_id]];
$this->params['breadcrumbs'][] = ['label' => $model->region->zone->name, 'url' => ['/geo/zone/view', 'id' => $model->region->zone_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/geo/region', 'Regions'), 'url' => ['/geo/region/index', 'zoneId' => $model->region->zone_id]];
$this->params['breadcrumbs'][] = ['label' => $model->region->name, 'url' => ['/geo/region/view', 'id' => $model->region_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/geo/place', 'Places'), 'url' => ['index', 'regionId' => $model->region_id]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend/actions', 'Update');
?>
<div class="place-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
