<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\geo\models\SearchPlace */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common/geo/place', 'Places');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/geo/country', 'Countries'), 'url' => ['/geo/country/index']];
$this->params['breadcrumbs'][] = ['label' => $searchModel->region->zone->country->name, 'url' => ['/geo/country/view', 'id' => $searchModel->region->zone->country_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/geo/zone', 'Zones'), 'url' => ['/geo/zone/index', 'countryId' => $searchModel->region->zone->country_id]];
$this->params['breadcrumbs'][] = ['label' => $searchModel->region->zone->name, 'url' => ['/geo/zone/view', 'id' => $searchModel->region->zone_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/geo/region', 'Regions'), 'url' => ['/geo/region/index', 'zoneId' => $searchModel->region->zone_id]];
$this->params['breadcrumbs'][] = ['label' => $searchModel->region->name, 'url' => ['/geo/region/view', 'id' => $searchModel->region_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="place-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?=
        Html::a(Yii::t('backend/actions', 'Create {modelClass}', [
                    'modelClass' => Yii::t('common/geo/place', 'Place')
                ]), ['create', 'regionId' => $searchModel->region_id], [
            'class' => 'btn btn-success'
        ])
        ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            'created_at:datetime',
            'created_by',
            'updated_at:datetime',
            'updated_by',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

</div>
