<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\geo\models\Country */

$this->title = Yii::t('backend/actions', 'Create {modelClass}', ['modelClass' => Yii::t('common/geo/country', 'Country')]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/geo/country', 'Countries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="country-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
