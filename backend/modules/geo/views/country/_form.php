<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\geo\Country;

/* @var $this yii\web\View */
/* @var $model backend\modules\geo\models\Country */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="country-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'timezone')->dropDownList(Country::getTimezoneList()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend/actions', 'Create') : Yii::t('backend/actions', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
