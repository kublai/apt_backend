<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\geo\models\Country */

$this->title = Yii::t('backend/actions', 'Update {modelClass}: ', [
            'modelClass' => Yii::t('common/geo/country', 'Country')
        ]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/geo/country', 'Countries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend/actions', 'Update');
?>
<div class="country-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
