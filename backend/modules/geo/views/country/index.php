<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\geo\models\SearchCountry */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common/geo/country', 'Countries');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="country-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?=
        Html::a(Yii::t('backend/actions', 'Create {modelClass}', [
                    'modelClass' => Yii::t('common/geo/country', 'Country')
                ]), ['create'], ['class' => 'btn btn-success'])
        ?>
    </p>

    <?php
    $listButton = function ($url, $model) {
        $options = array_merge([
            'title' => Yii::t('common/geo/zone', 'Zones'),
            'aria-label' => Yii::t('common/geo/zone', 'Zones'),
            'data-pjax' => '0',
        ]);
        return Html::a('<span class="glyphicon glyphicon-th-list"></span>', ['/geo/zone/index', 'countryId' => $model->id], $options);
    };
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            'timezone',
            'created_at:datetime',
            'created_by',
            'updated_at:datetime',
            'updated_by',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {list}',
                'buttons' => [
                    'list' => $listButton
                ]
            ],
        ],
    ]);
    ?>

</div>
