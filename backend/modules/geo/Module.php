<?php

namespace backend\modules\geo;

class Module extends \yii\base\Module {

    public $controllerNamespace = 'backend\modules\geo\controllers';

    public function init() {
        parent::init();

        // custom initialization code goes here
    }

}
