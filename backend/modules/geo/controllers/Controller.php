<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\geo\controllers;

use backend\controllers\CrudController;
use yii\helpers\ArrayHelper;
use common\models\user\Role;

class Controller extends CrudController {

    public function behaviors() {
        return ArrayHelper::merge(parent::behaviors(), [
                    'access' => [
                        'rules' => [
                            [
                                'actions' => ['list','index'],
                                'allow' => true,
                                'roles' => ['@'],
                                'userRoles' => [Role::ADMIN, Role::SUPER_ADMIN,Role::OPERATOR]
                            ],
                        ],
                    ],
        ]);
    }

}
