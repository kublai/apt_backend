<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\geo\widgets;

use Yii;
use yii\base\Widget;
use common\models\geo\LocationInterface,
    common\models\geo\Country,
    common\models\geo\Zone,
    common\models\geo\Region,
    common\models\geo\Place;
use yii\helpers\ArrayHelper,
    yii\helpers\Url;
use kartik\depdrop\DepDrop;
use yii\widgets\ActiveForm;
use yii\base\InvalidConfigException;

class LocationFormWidget extends Widget {

    /**
     *
     * @var ActiveForm
     */
    protected $form;

    /**
     *
     * @var LocationInterface
     */
    protected $model;

    /**
     *
     * @var string
     */
    public $localized = 'address';
    
    /**
     *
     * @var array
     */
    public $options = [];

    public function init() {
        parent::init();

        if ($this->form === null) {
            throw InvalidConfigException('form property must be provided');
        }

        if ($this->model === null) {
            throw InvalidConfigException('model property must be provided');
        }
    }

    public function run() {
        
        echo $this->form->field($this->model, $this->model->getCountryPropertyName(), $this->options)->dropDownList(
                ArrayHelper::map(
                        Country::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'), [
            'id' => 'location_country_id',
            'prompt' => Yii::t('common/geo/country', 'Select country'),
                           
        ])->label(Yii::t('common/geo/country','Country'));
        if ($this->localized === $this->model->getCountryPropertyName()) {
            return;
        }
        echo $this->form->field($this->model, $this->model->getZonePropertyName(), $this->options)->widget(DepDrop::className(), [
            'options' => ['id' => 'location_zone_id', 'placeholder' => Yii::t('common/geo/zone', 'Select zone')],
            'data' => ArrayHelper::map(Zone::find()->where(['country_id' => $this->model->country_id])->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'),
            'pluginOptions' => [
                'url' => Url::to(['/geo/zone/list']),
                'depends' => ['location_country_id'],
            ],
        ])->label(Yii::t('common/geo/zone','Zone'));
        if ($this->localized === $this->model->getZonePropertyName()) {
            return;
        }
        echo $this->form->field($this->model, $this->model->getRegionPropertyName(), $this->options)->widget(DepDrop::className(), [
            'options' => ['id' => 'location_region_id', 'placeholder' => Yii::t('common/geo/region', 'Select region')],
            'data' => ArrayHelper::map(Region::find()->where(['zone_id' => $this->model->zone_id])->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'),
            'pluginOptions' => [
                'url' => Url::to(['/geo/region/list']),
                'depends' => ['location_zone_id']
            ]
        ])->label(Yii::t('common/geo/region','Region'));
        if ($this->localized === $this->model->getRegionPropertyName()) {
            return;
        }
        echo $this->form->field($this->model, $this->model->getPlacePropertyName(), $this->options)->widget(DepDrop::className(), [
            'options' => ['id' => 'location_place_id', 'placeholder' => Yii::t('common/geo/place', 'Select place')],
            'data' => ArrayHelper::map(Place::find()->where(['region_id' => $this->model->region_id])->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'),
            'pluginOptions' => [
                'url' => Url::to(['/geo/place/list']),
                'depends' => ['location_region_id']
            ]
        ])->label(Yii::t('common/geo/place','Place'));
        if ($this->localized === $this->model->getPlacePropertyName()) {
            return;
        }
        echo $this->form->field($this->model, 'address')->textInput()->label(Yii::t('common/geo/location','Address'));
    }

    /**
     * 
     * @param ActiveForm $activeForm
     */
    public function setForm(ActiveForm $activeForm) {
        $this->form = $activeForm;
    }

    /**
     * 
     * @param LocationInterface $model
     */
    public function setModel(LocationInterface $model) {
        $this->model = $model;
    }

}
