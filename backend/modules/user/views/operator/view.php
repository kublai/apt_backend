<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\user\User,
    common\models\user\Role;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\Operator */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/user/user', 'Operators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operator-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend/actions', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a(Yii::t('backend/actions', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend/actions', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])
        ?>
        <?php if (Role::SUPER_ADMIN === Yii::$app->user->role) : ?>
            <?=
            Html::a(Yii::t('backend/user/user', 'Change password'), ['/user/user/change-password', 'id' => $model->id], [
                'class' => 'btn btn-info pull-right',
            ])
            ?>
        <?php endif; ?>
    </p>

    <?php
    $loc = $model->location;
    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id'
            , 'slug'
            , 'username'
            , 'statusName'
            , [
                'attribute' => 'role',
                'value' => Role::getName($model->role)
            ]
            , 'name'
            , 'last_name'
            , 'genderName'
            , 'email:email'
            , 'phone'
            , [
                'label' => Yii::t('common/geo/country', 'Country'),
                'value' => $loc !== null && $loc->country !== null ? $loc->country->name : null
            ]
            , [
                'label' => Yii::t('common/geo/zone', 'Zone'),
                'value' => $loc !== null && $loc->zone !== null ? $loc->zone->name : null
            ]
            , [
                'label' => Yii::t('common/geo/region', 'Region'),
                'value' => $loc !== null && $loc->region !== null ? $loc->region->name : null
            ]
            , [
                'label' => Yii::t('common/geo/place', 'Place'),
                'value' => $loc !== null && $loc->place !== null ? $loc->place->name : null
            ]
            , 'created_at:datetime'
            , 'created_by'
            , 'updated_at:datetime'
            , 'updated_by'
        ],
    ])
    ?>

</div>
