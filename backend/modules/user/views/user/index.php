<?php

use custom\helpers\Html;
use yii\grid\GridView;
use common\models\user\Role,
    common\models\user\User;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\user\models\SearchUser */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common/user/user', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?=
        Html::a(Yii::t('backend/actions', 'Create {modelClass}', [
                    'modelClass' => Yii::t('common/user/user', 'User')
                ]), ['create'], ['class' => 'btn btn-success'])
        ?>
    </p>

    <?php
    $passwordChangeLambda = function ($url, $model) {
        $options = array_merge([
            'title' => Yii::t('backend/user/user', 'Change password')
            , 'aria-label' => Yii::t('backend/user/user', 'Change password')
            , 'data-pjax' => '0'
        ]);
        return Html::a('<span class="glyphicon glyphicon-edit"></span>', ['/user/user/change-password', 'id' => $model->id], $options);
    };
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id'
            , [
                'attribute' => 'status'
                , 'filter' => Html::activeDropDownList($searchModel, 'status', User::getStatusList(), [
                    'class' => 'form-control'
                    , 'prompt' => Yii::t('backend/actions', 'Select')
                ])
                , 'value' => 'statusName'
            ]
            , 'username'
            , [
                'attribute' => 'role',
                'value' => function($model) {
                    return Role::getName($model->role);
                }
                , 'filter' => Html::activeDropDownList($searchModel, 'role', [
                    Role::INSTRUCTOR => Yii::t('common/user/user', 'Instructor'),
                    Role::STUDENT => Yii::t('common/user/user', 'Student')
                        ], [
                    'class' => 'form-control',
                    'prompt' => Yii::t('backend/actions', 'Select')
                ])
            ]
            , 'name'
            , 'last_name'
            , [
                'attribute' => 'gender'
                , 'filter' => Html::activeDropDownList($searchModel, 'gender', User::getGenderList(), [
                    'class' => 'form-control'
                    , 'prompt' => Yii::t('backend/actions', 'Select')
                ])
                , 'value' => 'genderName'
            ]
            , 'updated_at:datetime'
            , [
                'class' => 'yii\grid\ActionColumn'
                , 'template' => '{view} {update} {delete} {password}'
                , 'buttons' => [
                    'password' => $passwordChangeLambda
                ]
            ],
        ],
    ]);
    ?>

</div>
