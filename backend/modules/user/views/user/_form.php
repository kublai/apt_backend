<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\geo\widgets\LocationFormWidget;
use common\models\user\User,
    common\models\user\Role,
    common\models\user\Operator;
use yii \helpers\ArrayHelper;

/* @var $this yii\web\View */

/* @var $model backend\modules\user\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $options = [
        'prompt' => Yii::t('backend/user/user', 'Select an operator')
    ];
    if (Role::check(Role::OPERATOR, Yii::$app->user->role)) {
        $options['disabled'] = 'disabled';
    }
    echo $form->field($model, 'operator_id')->dropDownList(ArrayHelper::map(Operator::find()->orderBy([ 'name' => SORT_ASC])->all(), 'id', 'fullName'), $options);
    ?>
    <?= $form->field($model, 'username')->textInput() ?>
    <?=
    $form->field($model, 'role')->dropDownList([
        Role::STUDENT => Yii::t('common/user/user', 'Student'),
        Role::INSTRUCTOR => Yii::t('common/user/user', 'Instructor')
    ])
    ?>
    <?= $form->field($model, 'status')->dropDownList(User::getStatusList()) ?>
    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'last_name')->textInput() ?>
    <?= $form->field($model, 'gender')->dropDownList(User::getGenderList()) ?>
    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'phone')->textInput() ?>
    <?= LocationFormWidget::widget([ 'form' => $form, 'model' => $model]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend/actions', 'Create') : Yii::t('backend/actions', 'Update'), [ 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>  

</div>
