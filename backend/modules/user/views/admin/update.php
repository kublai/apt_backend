<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\Admin */

$this->title = Yii::t('backend/actions', 'Update {modelClass}: ', [
            'modelClass' => Yii::t('common/user/user', 'Admin'),
        ]) . ' ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/user/user', 'Admins'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend/actions', 'Update');
?>
<div class="admin-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
