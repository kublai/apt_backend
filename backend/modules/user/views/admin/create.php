<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\Admin */

$this->title = Yii::t('backend/actions', 'Create {modelClass}', [
            'modelClass' => Yii::t('common/user/user', 'Admin')
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common/user/user', 'Admins'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
