<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\user\Role;
use common\models\user\User;
use backend\modules\geo\widgets\LocationFormWidget;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\Admin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput() ?>
    <?=
    $form->field($model, 'role')->dropDownList([
        Role::ADMIN => Yii::t('common/user/user', 'Admin'),
        Role::SUPER_ADMIN => Yii::t('common/user/user', 'Superadmin')
    ]);
    ?>
    <?= $form->field($model, 'status')->dropDownList(User::getStatusList()) ?>
    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'last_name')->textInput() ?>
    <?= $form->field($model, 'gender')->dropDownList(User::getGenderList()) ?>
    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'phone')->textInput() ?>

    <?= LocationFormWidget::widget(['form' => $form, 'model' => $model]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend/actions', 'Create') : Yii::t('backend/actions', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
