<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\user\models;

use common\models\user\User as BaseUser;
use Yii;

/**
 * 

 */
class Admin extends BaseUser {

    public function rules() {
        return array_merge(parent::rules(), [
            ['email', 'required']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        $this->on(self::EVENT_BEFORE_VALIDATE, function($event) {
            if ($this->isNewRecord) {
                $this->setPassword(Yii::$app->security->generateRandomString(10));
                $this->generateAuthKey();
            }
        });
    }

}
