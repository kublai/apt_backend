<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\user\models;

use common\models\user\Operator as BaseOperator;
use Yii;

/**
 * 

 */
class Operator extends BaseOperator {

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        $this->on(self::EVENT_BEFORE_VALIDATE, function($event) {
            if ($this->isNewRecord) {
                $this->setPassword(Yii::$app->security->generateRandomString(10));
                $this->generateAuthKey();
            }
        });
    }

}
