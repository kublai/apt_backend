<?php

namespace backend\modules\user\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\user\models\Admin;
use common\models\user\Role;

/**
 * SearchAdmin represents the model behind the search form about `backend\modules\user\models\Admin`.
 */
class SearchAdmin extends Admin {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'status', 'gender', 'role', 'location_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['slug', 'username', 'auth_key', 'password_hash', 'password_reset_token', 'name', 'last_name', 'email', 'phone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Admin::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id
            , 'status' => $this->status
            , 'gender' => $this->gender
        ]);

        $query->andFilterWhere(['like', 'slug', $this->slug])
                ->andFilterWhere(['like', 'username', $this->username, false])
                ->andFilterWhere(['like', 'name', $this->name, false])
                ->andFilterWhere(['like', 'last_name', $this->last_name, false])
                ->andFilterWhere(['like', 'email', $this->email])
                ->andWhere(['<=', 'role', Role::ADMIN]);

        return $dataProvider;
    }

    public function behaviors() {
        return [];
    }

}
