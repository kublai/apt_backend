<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\modules\user\models;

use common\models\user\User as BaseUser,
    common\models\user\Role;
use Yii;

/**
 * 

 */
class User extends BaseUser {

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        $this->on(self::EVENT_BEFORE_VALIDATE, function($event) {
            if ($this->isNewRecord) {
                $this->setPassword(Yii::$app->security->generateRandomString(10));
                $this->generateAuthKey();
            }
        });
        $this->presetOperator();
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null) {
        if (parent::load($data, $formName)) {
            $this->presetOperator();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Presets the operator if the current web user is an operator.
     */
    protected function presetOperator() {
        if (Role::check(Role::OPERATOR, Yii::$app->user->role)) {
            $this->operator_id = Yii::$app->user->id;
        }
    }
}
