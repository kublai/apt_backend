<?php

namespace backend\modules\user\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\user\models\User;
use common\models\user\Role;

/**
 * SearchUser represents the model behind the search form about `backend\modules\user\models\User`.
 */
class SearchUser extends User {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'status', 'gender', 'role', 'location_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['slug', 'username', 'auth_key', 'password_hash', 'password_reset_token', 'name', 'last_name', 'email', 'phone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user.id' => $this->id,
            'user.status' => $this->status,
            'user.gender' => $this->gender,
            'user.role' => $this->role
        ]);

        $query->andFilterWhere(['like', 'user.slug', $this->slug])
                ->andFilterWhere(['like', 'user.username', $this->username . '%', false])
                ->andFilterWhere(['like', 'user.name', '%' . $this->name . '%', false])
                ->andFilterWhere(['like', 'user.last_name', '%' . $this->last_name . '%', false])
                ->andFilterWhere(['like', 'user.email', $this->email])
                ->andWhere(['or', ['user.role' => Role::INSTRUCTOR], ['user.role' => Role::STUDENT]]);

        if (Role::check(Role::OPERATOR, Yii::$app->user->role)) {
            $query->joinWith('operator');
            $query->andWhere(['operator.id' => Yii::$app->user->id]);
        }
        return $dataProvider;
    }

    public function behaviors() {
        return [];
    }

}
