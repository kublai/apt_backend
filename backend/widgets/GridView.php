<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace backend\widgets;

use Yii;
use yii\grid\GridView as BaseGridView;
use backend\assets\GridSelectAllAsset;
use yii\helpers\Html;

/**
 * 

 */
class GridView extends BaseGridView {

    /**
     * @var string the layout that determines how different sections of the list view should be organized.
     * The following tokens will be replaced with the corresponding section contents:
     *
     * - `{selector}`: the selector section. See [[renderSelector()]].
     * - `{summary}`: the summary section. See [[renderSummary()]].
     * - `{errors}`: the filter model error summary. See [[renderErrors()]].
     * - `{items}`: the list items. See [[renderItems()]].
     * - `{sorter}`: the sorter. See [[renderSorter()]].
     * - `{pager}`: the pager. See [[renderPager()]].
     */
    public $layout = "{selector}\n{summary}\n{items}\n{pager}";

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        GridSelectAllAsset::register($this->getView());
    }

    /**
     * @inheritdoc
     */
    public function renderSection($name) {
        switch ($name) {
            case '{selector}':
                return $this->renderSelector();
            default:
                return parent::renderSection($name);
        }
    }

    /**
     * Renders selector.
     * 
     * @return string the rendering result.
     */
    public function renderSelector() {
        return Html::tag('p', Html::a(Yii::t('backend/actions', 'Add selected'), ['#'], [
                            'class' => 'btn btn-success pull-right btn-add-selected disabled'
                        ]), ['class' => 'clearfix'])
                . Html::tag('p', Yii::t('backend/actions', 'The {selected} results on this page have been selected. {link}', [
                            'selected' => $this->dataProvider->getCount(),
                            'link' => Html::a(Yii::t('backend/actions', 'Select the {total} results on the list', [
                                        'total' => $this->dataProvider->getTotalCount()
                                    ]), '#', [
                                'class' => 'select-all'
                            ])
                        ]), [
                    'class' => 'clearfix selection-resume hidden',
                    'data-count' => $this->dataProvider->getTotalCount()
                ])
                . Html::tag('p', Yii::t('backend/actions', 'The {total} results have been selected. {link}', [
                            'total' => $this->dataProvider->getTotalCount(),
                            'link' => Html::a(Yii::t('backend/actions', 'Cancel selection'), '#', [
                                'class' => 'unselect-all'
                            ])
                        ]), [
                    'class' => 'clearfix selection-all hidden'
        ]);
    }

}
