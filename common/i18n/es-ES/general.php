<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
return [
    'All' => 'Todos',
    'Yes' => 'Si',
    'No' => 'No',
    'Created At' => 'Fecha creación',
    'Created By' => 'Creado por',
    'Updated At' => 'Ult. actulización',
    'Updated By' => 'Actualizado por'
];
