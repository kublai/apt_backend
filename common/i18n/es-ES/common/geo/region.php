<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

return [
    'ID' => 'Id',
    'Zone ID' => 'Id Zona',
    'Name' => 'Nombre',
    'Region' => 'Región',
    'Regions' => 'Regiones',
    'Select region' => 'Selecciona una región'
];