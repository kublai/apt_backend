<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

return [
    'ID' => "Id",
    'Name' => "País",
    'Country' => 'País',
    'Countries' => 'Paises',
    'Select country' => 'Selecciona un país'
];