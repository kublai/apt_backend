<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

return [
    'ID' => 'Id',
    'Country ID' => 'ID País',
    'Name' => 'Nombre',
    'Zone' => 'Zona',
    'Zones' => 'Zonas',
    'Select zone' => 'Selecciona una zona'
];