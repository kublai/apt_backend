<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

return [
    'ID' => 'Id',
    'Region ID' => 'Id Region',
    'Name' => 'Nombre',
    'Place' => 'Lugar',
    'Places' => 'Lugares',
    'Select place' => 'Selecciona un lugar'
];