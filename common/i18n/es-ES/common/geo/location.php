<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
return [
    'ID' => 'Id',
    'Country ID' => 'País Id',
    'Zone ID' => 'Zona Id',
    'Region ID' => 'Región Id',
    'Place ID' => 'Lugar Id',
    'Address' => 'Dirección',
    'Latitude' => 'Latitud',
    'Longitude' => 'Longitud'
];