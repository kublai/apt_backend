<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
return [
    'Campaign' => 'Campaña',
    'Campaigns' => 'Campañas',
    'Campaign {name}' => 'Campaña {name}',
    'Select campaign' => 'Selecciona una campaña',
    'Campaign modules' => 'Módulos de la campaña',
    'Available modules' => 'Módulos disponibles',
    'Sessions' => 'Sesiones',
    'Campaign has begun and therefore cannot be modified.' => 'La campaña se encuentra en curso y por lo tanto no puede ser modificada.',
    'An error has ocurred when trying to exclude the module from the campaign.' => 'Ha ocurrido un error al intentar eliminar un módulo de la campaña.',
    'An error has ocurred when trying to move the module.' => 'Ha ocurrido un error al intentar mover un módulo de campaña.',
    'Completed' => 'Completado',
    'Session date' => 'Fecha de la sesión',
    'Experienced Time' => 'Tiempo',
    'Students with sessions' => 'Estudiantes con sesiones',
    'Students without sessions' => 'Estudiantes sin sesiones',
    'Minimum number of syncs per week' => 'Número mínimo de sincronizaciones por semana',
    'Minimum number of syncs per month' => 'Número mínimo de sincronizaciones por mes',
    'Max. Km between sessions' => 'Número máximo de Km entre sesiones',
    'Max. absence instructor days' => 'Número máximo de dias de ausencia entre sesiones',
    'Sessions number' => 'Número de sesiones',
    'Analize from' => 'Fecha inicio análisis',
    'Analize to' => 'Fecha fin análisis'
    
];
