<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
return [
    'Integration file doesn\'t exist' => 'El fichero de integración no existe'
    , 'Country with name [{name}] does not exist' => 'El país con nombre [{name}] no existe'
    , 'Zone with name [{name}] does not exist in country [{containerName}]' => 'La zona [{name}] no existe en el país [{containerName}]'
    , 'Region with name [{name}] does not exist in zone [{containerName}]' => 'La región [{name}] no existe en la zona [{containerName}]'
    , 'Place with name [{name}] does not exist in region [{containerName}]' => 'El lugar [{name}] no existe en la región [{containerName}]'
    , 'Error in line [{n}]:' => 'Error en la línea [{n}]:'
    , 'The file has been integrated successfully' => 'El archivo se ha integrado satisfactoriamente'
    , 'Instructions to integrate a CSV' => 'Instrucciones para integrar un CSV'
    , 'The CSV should have the default format in which most spreadsheets export, that is, fields delimited by \',\' (comma), text fields optionally encapsulated in \'"\' (double quote) and the escape character \'\\\' (backslash).' => 'El CSV debe llevar el formato por defecto en el que exportan la mayoría de hojas de cálculo, es decir, los campos delimitados por \',\' (comas), los campos de texto opcionalmente encapsulados en \'"\' (comillas dobles) y el caracter de escape \'\\\' (barra invertida).'
    , 'CSV columns must have the following information, except optional columns that should be left blank if they have any data.' => 'Las columnas del CSV a integrar deben contener la siguiente información, exceptuando las opcionales que deberán dejarse en blanco si no se quieren registrar los datos.'
    , 'Name of the user' => 'Nombre del usuario'
    , 'Last name of the user' => 'Apellidos del usuario'
    , 'Identity number' => 'Número de cédula'
    , 'Phone (optional)' => 'Teléfono (opcional)'
    , 'Country name (Exactly as it appears in the management area)' => 'Nombre del país (Exactamente igual que aparece en el área de gestión)'
    , 'Zone name (Exactly as it appears in the management area)' => 'Nombre de la zona (Exactamente igual que aparece en el área de gestión)'
    , 'Region name (Exactly as it appears in the management area)' => 'Nombre de la región (Exactamente igual que aparece en el área de gestión)'
    , 'Place name (Exactly as it appears in the management area)' => 'Nombre del lugar (Exactamente igual que aparece en el área de gestión)'
    , 'Address (optional)' => 'Dirección (opcional)'
    , 'Latitude (optional)' => 'Latitud (opcional)'
    , 'Longitude (optional)' => 'Longitud (opcional)'
    , 'The integration is done atomically, so if an error occurs during it, no records will be processed. In this case, you must correct any errors and re-integrate the CSV.' => 'La integración se realiza de manera atómica, por lo que si ocurre algún error durante la misma, no se procesará ningún registro. En este caso, deberás subsanar los posibles errores y volver a integrar el CSV completo.'
    , 'Gender [1 for Male and 2 for Female]' => 'Género [1 para Masculino y 2 para Femenino]'
];
