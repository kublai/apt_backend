<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
return [
    'Instructor' => 'Facilitador'
    , 'Instructors' => 'Facilitadores'
    , 'Student' => 'Participante'
    , 'Students' => 'Participantes'
    , 'Begin At' => 'Inicio'
    , 'End At' => 'Fin'
    , 'Created At' => 'Creación'
    , 'Updated At' => 'Actualización'
    , 'Created By' => 'Creado por'
    , 'Updated By' => 'Actualizado por'
    , 'Completed' => 'Completado'
    , 'Utility' => 'Utilidad'
    , 'Easiness' => 'Facilidad'
    , 'Begins After' => 'Comienza después de'
    , 'Begins Before' => 'Comienza antes de'
    , 'Ends After' => 'Finaliza antes de'
    , 'Ends Before' => 'Finaliza después de'
    , 'The operator you are trying to delete has students with session data and therefore can not be deleted' => 'El operador que estás intentado eliminar tiene estudiantes con datos de sesiones y debido a esto no puede ser eliminado'
    , 'The instructor you are trying to delete has students with session data and therefore can not be deleted' => 'El facilitador que estás intentado eliminar tiene estudiantes con datos de sesiones y debido a esto no puede ser eliminado'
    , 'The student you are trying to delete has session data and therefore can not be deleted' => 'El estudiante que estás intentado eliminar tiene datos de sesiones y debido a esto no puede ser eliminado'
    , 'The sessions data of the student has been cleared' => 'Los datos de sesiones del estudiante han sido eliminados'
];
