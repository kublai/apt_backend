<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
return [
    'This feature allows you to change the instructor of a student and move all of his session data already synchronized, so before using it you have to consider that...' => 'Esta funcionalidad permite cambiar de facilitador a un estudiante y mover con él todos sus datos de sesiones ya sincronizados, por lo que antes de utilizarla debes considerar que...'
    , 'A student could only be assigned to instructors of his same operator.' => 'Un estudiante sólo puede ser asignado a los facilitadores de su mismo operador.'
    , 'The statistics of the two instructors will be affected.' => 'Las estadísticas de los dos facilitadores se verán afectadas.'
    , 'All of the session data of the student not yet synchronized from the tablet of the original instructor will be lost.' => 'Se perderán todos los datos de sesiones del estudiante de la tableta del facilitador de origen aún no sincronizados.'
];
