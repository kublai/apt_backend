<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
return [
    'Code' => 'Código'
    , 'Module' => 'Módulo'
    , 'Modules' => 'Módulos'
    , 'Select module' => 'Selecciona un modulo'
    , 'Module Name' => 'Módulo'
    , 'Module Code' => 'Cód. módulo'
    , 'Date' => 'Fecha'
];
