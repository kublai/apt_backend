<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

return [
    'Created At' => 'Fecha'
    , 'Status' => 'Estado'
    , 'Info' => 'Información'
    , 'Started' => 'Iniciado'
    , 'Success' => 'OK'
    , 'Failed' => 'Fallido'
    , 'Warning' => 'Alerta'
];