<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

return [
    'Hello {username}' => 'Hola {username}'
];