<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
return [
    'ID' => 'ID',
    'Username' => 'Nombre de usuario',
    'Role' => 'Rol',
    'Operator' => 'Operador',
    'Name' => 'Nombre',
    'Last Name' => 'Apellido',
    'Email' => 'Email',
    'Phone' => 'Teléfono',
    'Location ID' => 'ID de Ubicación',
    'Created At' => 'Creación',
    'Created By' => 'Creado por',
    'Updated At' => 'Actualización',
    'Updated By' => 'Actualizado por',
    'Operator' => 'Operador',
    'Operators' => 'Operadores',
    'Admin' => 'Administrador',
    'Superadmin' => 'Superadministrador',
    'Students' => 'Estudiantes',
    'Student' => 'Estudiante',
    'Instructors' => 'Facilitadores',
    'Instructor' => 'Facilitador',
    'Active' => 'Activo',
    'Inactive' => 'Inactivo',
    'Admins' => 'Administradores',
    'Superadmins' => 'Superadministradores',
    'User' => 'Usuario',
    'Users' => 'Usuarios',
    'Operator' => 'Operador',
    'Operators' => 'Operadores'
    , 'Status' => 'Estado'
    , 'Gender' => 'Género'
    , 'Male' => 'Masculino'
    , 'Female' => 'Femenino'
    , 'Instructors Activity' => 'Actividad de Facilitadores'
];
