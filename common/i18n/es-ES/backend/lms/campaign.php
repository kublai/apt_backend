<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
return [
    'Drag the modules you want to include or exlude from one side to the other. You can also establish the order you want that the modules appear in the campaign.' => 'Arrastra los módulos que quieras incluir o excluir de la campaña de un lado al otro. Tambíen puedes establecer el orden en el que quieras que los módulos aparezcan en la campaña.'
    , 'Add {modelClass} to {username}' => 'Añadir {modelClass} a {username}'
    , 'Add {modelClass} to {username} of {operator}' => 'Añadir {modelClass} a {username} del operador {operator}'
    , 'Active campaign' => 'Campaña activa'
    , 'You wouldn\'t be allowed to exclude module from the campaign.' => 'No podrás excluir módulos de la campaña.'
    , 'Location ID' => 'ID Ubicación'
    , 'Name' => 'Nombre'
    , 'Begin At' => 'Inicio'
    , 'End At' => 'Final'
    , 'Country Id' => 'País Id'
    , 'Created At' => 'Creación'
    , 'Updated At' => 'Actualización'
    , 'Created By' => 'Creado por'
    , 'Updated By' => 'Actualizado por'
    , 'The campaign contains student progress and therefore cannot be deleted' => 'La campaña contiene progreso de los estudiantes y por lo tanto no puede ser eliminada'
    , 'Export Report' => 'Exportar Informe'
    , 'Report' => 'Informe'
    , 'Integrate {modelClass} to {username}' => 'Integrar {modelClass} a {username}'
    , 'Integrate {modelClass} to {username} of {operator}' => 'Integrar {modelClass} a {username} del operador {operator}'
    , 'Delete all sessions' => 'Borrar todas las sesiones'
    , 'Are you sure you want to delete all the sessions of this student?' => '¿Estás seguro de querer borrar todas las sesiones del estudiante?'
    , 'Stats' => 'Estadísticas'
    , 'Stats by Module' => 'Estadísticas por Módulo'
    , 'Stats by Student' => 'Estadísticas por Estudiante'
    , '{username} Stats' => 'Estadísticas de {username}'
    , 'Modules completed by students' => 'Módulos completados por estudiantes'
    , 'Invalid instructor selected' => 'El facilitador seleccionado no es válido'
    , 'An error ocurred on changing the student instructor' => 'Ha ocurrido un error al cambiar de facilitador al estudiante'
    , 'The instructor of {username} has been changed' => 'El facilitador de {username} ha sido cambiado'
    , 'Change {username} instructor' => 'Cambio de facilitador de {username}'
    , 'Synchronization Log' => 'Log de Sincronización'
    , '{username} Synchronization Log' => 'Log de Sincronización de {username}'
    , 'Location of the sessions' => 'Localización de las sesiones'
    , 'This operator has {n} instructors associated therefore cannot be erased' => 'El operador tiene {n} instructor(es) asociados por lo tanto no puede ser eliminado'
];
