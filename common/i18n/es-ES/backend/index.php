<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

return [
    'Graduation Project' => 'Proyecto Graduación',
    'Management area' => 'Área de gestión',
    'Campaign' => 'Campaña',
    'From' => 'Desde',
    'To' => 'Hasta',
    'Apply' => 'Aplicar',
    'KPI\'s' => 'KPI\'s',
    'Reports' => 'Reportes',
    'Alarms' => 'Alarmas',
    'Filter' => 'Filtro',
    'ACTIVE' => 'ACTIVA',
    'CLOSED' => 'CERRADA',
    'Module' => 'Módulo'
];