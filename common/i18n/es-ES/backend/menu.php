<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

return [
    'Users' => 'Usuarios',
    'Admins' => 'Administradores',
    'Geolocation' => 'Geolocalización',
    'Countries' => 'Países',
    'Zones' => 'Zonas',
    'Regions' => 'Regiones',
    'Places' => 'Lugares',
    'Campaigns' => 'Campañas',
    'Modules' => 'Módulos',
    'Operators' => 'Operadores'
];