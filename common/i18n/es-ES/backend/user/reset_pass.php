<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
return [
    'If you forgot your password you can reset it {link}' => 'Si has olvidado tu contraseña puedes restaurarla {link}',
    'Follow the link below to reset your password:' => 'Haz click en el siguiente enlace para restaurar tu contraseña:',
    'New password was saved.' => 'La nueva contraseña ha sido guardada.',
    'Check your email for further instructions. Don\'t forget to check the spam folder if the email doesn\'t appear in your inbox.' => 'Revisa tu correo para continuar con el proceso de restauración. No olvides revisar el correo no deseado si no el mail no aparece en tu bandeja de entrada.',
    'Sorry, we are unable to reset password for email provided.' => 'Lo sentimos, pero no podemos restaurar la contraseña para el correo indicado.',
    'Request password reset' => 'Restaura tu contraseña',
    'Please fill out your email. A link to reset password will be sent there.' => 'Indica tu email. El enlace para restaurar tu contraseña será enviado a esa dirección.',
    'Reset password' => 'Restaura tu contraseña',
    'Please choose your new password:' => 'Por favor elige una nueva contraseña:',
    'Password reset for {platformName}' => 'Restaurar contraseña de {platformName}'
];
