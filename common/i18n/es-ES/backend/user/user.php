<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
return [
    'Change password' => 'Modificar contraseña',
    'Select an operator' => 'Selecciona un operador'
];