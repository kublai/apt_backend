<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
return [
    'Alarms' => 'Alarmas' ,
    'Apply' => 'Aplicar' ,
    'Campaign parameters' => 'Parámetros de configuración',
    'Min syncs per week' => 'Nº mínimo de sincronizaciones por semana',
    'Min syncs per month' => 'Nº mínimo de sincronizaciones por mes',
    'Max Km between 2 sessions' => 'Nº máximo de Km entre dos sesiones consecutivas',
    'Max absence instructor days between 2 sessions' => 'Nº máximo de días entre dos sesiones consecutivas',
    'Analize from' => 'Analizar datos desde',
    'Analize to' => 'Hasta',
    'Sessions per week and instructor' => 'Nº de sesiones por semana',
    'This report show  the number of sessions per week, each column correspond to one week.'=>'Este reporte muestra el número de sesiones por semana, cada columna corresponde a una semana. Las semanas de contabilizan de lunes a domingo. Tanto la primera como la última semana podrían no tener los 7 días completos dependiendo de las fechas que se hayan elegido para el análisis',   
    'Syncs per week and instructor' => 'Sincronizaciones por SEMANA para cada instructor',
    'Number of synchronizations per week, if the number calculated is less than the minimum then the cell has a red background' => 'Este reporte muestra el número de sincronizaciones que ha realizado cada instructor semana a semana. Las columnas muestran las semanas numeradas de S1 en adelante. Si el número calculado es inferior al número previsto en los parámetros de análisis la celda se muestra en rojo. Si no hay datos para una semana la celda se muestra en blanco',
    'Syncs per month and instructor' =>  'Sincronizaciones por MES para cada instructor',
    'Number of syncs per month. the first and last month could not be completed, is depends of the  analized period' => 'Cada columna muestra los datos correspondientes a un mes, los meses se numeran a partir de M1 en adelante, es posible que tanto el primer como el último mes no esten completos dependiendo del periodo analizado.',
    'Distance between sessions' => 'Distancia entre sesiones',
    'This report shows the distance between to consecutive sessions' => ' Se muestra la distancia entre dos sesiones consecutivas. Si la distancia es superior al criterio establecido en los parámetros de la cabecera entonces la celda se muestra en rojo',
    'Days between sessions' => 'Días entre sesiones',
    'number of days between 2 consecutive sessions' => 'Número de días transcurridos entre dos sesiones consecutivas, si el periodo transcurrido el mayor al establecido en los parámetros de cabecera entonces la celda que muestro los días transcurridos se muestra en rojo'
];