<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
return[
    'Training Modules' => 'Módulos de Formación',
    'Instructors by Operator' => 'Facilitadores por Operador',
    'User times by module'=> 'Tiempos promedio de usuario por módulo ',
    'Campaign general information' => 'Información general de la campaña',
    'Name' => 'Nombre',
    'From' => 'Desde',
    'To' => 'Hasta',
    'Global progress' => 'Avance global',
    'Global progress by instructor' => 'Avance global por facilitador',
    'Total Instructors' => 'Total Facilitadores',
    'Total Students' => 'Total Estudiantes'
    , 'Number of Women' => 'Total de Mujeres'
    , 'Number of Men' => 'Total de Hombres'
    , 'X axis: ' => 'Eje X: '
    , 'Y axis: ' => 'Eje Y: '
    , 'Note: ' => 'Nota: '
    , 'Radar: ' => 'Radar: '
    , 'Code modules that are in the project' => 'Códigos de los módulos que forman parte del proyecto. El orden mostrado se corresponde con el orden en el que aparecen en la App.'  
    , 'Percentage of users that completed a module' => 'Porcentaje de usuarios que han completado el módulo'
    , 'Average time (in minutes) that users take in each module' => 'Tiempo promedio (en minutos) que tardan los usuarios del programa en terminar un módulo.'
    , 'Number of users asigned to each manager.' => 'Número de usuarios asignados que han completado satisfactoriamente el módulo'
    , 'Select module' => 'Seleccionar módulo'
    , 'This graph show th actual number of asigned users to this instructor. Users without sessions are not showed here. If a user has completed a module more than once counts as one time only' => 'Este reporte se calcula teniendo en cuenta solo el número vigente de usuarios asignados. Los usuarios sin sesiones no se tienen en cuenta. Si un usuario ha completado un módulo más de una vez solo se cuenta la primera para este reporte'
        ];