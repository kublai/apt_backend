<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
return [
    'Your user doesn\'t have a valid role to enter the management area' => 'No dispones de los permisos necesarios para acceder al área de gestión'
];