<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */
return [
    'Update' => 'Modificar',
    'Update {modelClass}: ' => 'Modificar {modelClass}: ',
    'Create' => 'Crear',
    'Create {modelClass}' => 'Crear {modelClass}',
    'Add' => 'Añadir',
    'Add {modelClass}' => 'Añadir {modelClass}',
    'Add selected' => 'Añadir seleccionados',
    'Edit' => 'Editar',
    'Edit {modelClass}' => 'Editar {modelClass}',
    'Search' => 'Buscar',
    'Reset' => 'Resetear',
    'Delete' => 'Eliminar',
    'Delete all' => 'Borrar todo',
    'Are you sure you want to delete this item?' => '¿Estás seguro de querer eliminar este elemento?',
    'Advanced search' => 'Búsqueda avanzada',
    'here' => 'aquí',
    'Send' => 'Enviar',
    'Return' => 'Volver',
    'Process' => 'Procesar',
    'Integrate CSV' => 'Integrar CSV',
    'Current' => 'Actual',
    'Save' => 'Guardar',
    'Change {modelClass}' => 'Cambiar {modelClass}',
    'Select' => 'Selecciona',
    '{modelClass} List' => 'Listado de {modelClass}',
    'Export Data' => 'Exportar Datos',
    'Module' => 'Módulo',
    'Active / Inactive' => 'Activo / Inactivo'
];
