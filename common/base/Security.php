<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\base;

use yii\base\Security as BaseSecurity;
use yii\base\InvalidParamException;

/**
 * Custom implementation of the hash algorithm for the grad project.
 * 

 */
class Security extends BaseSecurity {

    /**
     * @inheritdoc
     */
    public function generatePasswordHash($password, $cost = 13) {
        return sha1($password);
    }

    /**
     * @inheritdoc
     */
    public function validatePassword($password, $hash) {
        if (!is_string($password) || $password === '') {
            throw new InvalidParamException('Password must be a string and cannot be empty.');
        }

        if (preg_match('/^[0-9a-z]{40}$/i', $hash) === false) {
            throw new InvalidParamException('Hash is invalid.');
        }

        return $this->compareString(sha1($password), $hash);
    }

}
