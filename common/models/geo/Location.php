<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\geo;

use custom\validators\AddressValidator;
use yii;

/**
 * Class Location represents a concrete location object.
 * 
 * @property Place $place The place in which this location is located or null if not defined.
 * @property Region $region The region in which this location is located or null if not defined.
 * @property Zone $zone The zone in which this region is located or null if not defined.
 * @property Country $county The country in which this zone is located or null if not defined.
 * 

 */
class Location extends base\Location {

    /**
     * @inheritdoc
     */
    public function rules() {
        return array_merge(parent::rules(), [
            ['address', 'default', 'value' => null],
            ['address', AddressValidator::className()]
        ]);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getPlace() {
        return $this->hasOne(Place::className(), ['id' => 'place_id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getRegion() {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getZone() {
        return $this->hasOne(Zone::className(), ['id' => 'zone_id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getCountry() {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common/geo/location', 'ID'),
            'country_id' => Yii::t('common/geo/location', 'Country ID'),
            'zone_id' => Yii::t('common/geo/location', 'Zone ID'),
            'region_id' => Yii::t('common/geo/location', 'Region ID'),
            'place_id' => Yii::t('common/geo/location', 'Place ID'),
            'address' => Yii::t('common/geo/location', 'Address'),
            'latitude' => Yii::t('common/geo/location', 'Latitude'),
            'longitude' => Yii::t('common/geo/location', 'Longitude'),
        ];
    }
}
