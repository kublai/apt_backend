<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\geo;

use Yii;
use custom\validators\NameValidator;

/**
 * 
 * @property Region[] $regions A collection of Region objects.
 * @property Location[] $locations A collection of Location objects.
 * @property Country $county The country in which this zone is located.
 * 

 */
class Zone extends base\Zone {

    /**
     * @inheritdoc
     */
    public function rules() {
        return array_merge(parent::rules(), [
        ]);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getRegions() {
        return $this->hasMany(Region::className(), ['zone_id' => 'id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getLocations() {
        return $this->hasMany(Location::className(), ['zone_id' => 'id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getCountry() {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common/geo/zone', 'ID'),
            'country_id' => Yii::t('common/geo/zone', 'Country ID'),
            'name' => Yii::t('common/geo/zone', 'Name'),
            'created_at' => Yii::t('common/general', 'Created At'),
            'created_by' => Yii::t('common/general', 'Created By'),
            'updated_at' => Yii::t('common/general', 'Updated At'),
            'updated_by' => Yii::t('common/general', 'Updated By'),
        ];
    }
}
