<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\geo;

use Yii;
use custom\validators\NameValidator;

/**
 * 
 * @property Zone[] $zones A collection of Zone objects.
 * @property Location[] $locations A collection of Location objects.
 * 

 */
class Country extends base\Country {

    /**
     * @inheritdoc
     */
    public function rules() {
        return array_merge(parent::rules(), [
            ['name', NameValidator::className()],
            ['timezone', 'validateTimeZone']
        ]);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getZones() {
        return $this->hasMany(Zone::className(), ['country_id' => 'id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getLocations() {
        return $this->hasMany(Location::className(), ['country_id' => 'id']);
    }

    /**
     * Validates the timezone field.
     * 
     * @return boolean
     */
    public function validateTimeZone() {
        if (timezone_open($this->timezone) === false) {
            $this->addError('timezone', Yii::t('country', 'Timezone does not exist'));
            return false;
        } else {
            return true;
        }
    }

    /**
     * Returns a nested array of timezones indexed by its codes and classified by 
     * its continents or main regions.
     * 
     * @return array
     */
    public static function getTimezoneList() {
        $zones = \DateTimeZone::listIdentifiers();
        $locations = [];
        foreach ($zones as $zone) {
            $zone = explode('/', $zone);
            if ($zone[0] == 'Africa' || $zone[0] == 'America' || $zone[0] == 'Antarctica' || $zone[0] == 'Arctic' || $zone[0] == 'Asia' || $zone[0] == 'Atlantic' || $zone[0] == 'Australia' || $zone[0] == 'Europe' || $zone[0] == 'Indian' || $zone[0] == 'Pacific') {
                if (isset($zone[1]) != '') {
                    $locations[$zone[0]][$zone[0] . '/' . $zone[1]] = str_replace('_', ' ', $zone[1]);
                }
            }
        }
        return $locations;
    }

     /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common/geo/country', 'ID'),
            'name' => Yii::t('common/geo/country', 'Name'),
            'created_at' => Yii::t('common/general', 'Created At'),
            'created_by' => Yii::t('common/general', 'Created By'),
            'updated_at' => Yii::t('common/general', 'Updated At'),
            'updated_by' => Yii::t('common/general', 'Updated By'),
        ];
    }

}
