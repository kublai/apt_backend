<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\geo;

/**
 * 

 */
interface LocationInterface {

    /**
     * @return array
     */
    public function locationRules();

    /**
     * @return bool
     */
    public function validateZone();

    /**
     * @return bool
     */
    public function validateRegion();

    /**
     * @return bool
     */
    public function validatePlace();

    /**
     * @return bool
     */
    public function validateCountry();

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getLocation();

    /**
     * @return string
     */
    public function getCountryPropertyName();

    /**
     * @return string
     */
    public function getZonePropertyName();

    /**
     * @return string
     */
    public function getRegionPropertyName();

    /**
     * @return string
     */
    public function getPlacePropertyName();
}
