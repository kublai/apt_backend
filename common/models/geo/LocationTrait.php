<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\geo;

use common\models\geo\Country,
    common\models\geo\Zone,
    common\models\geo\Region,
    common\models\geo\Place;
use custom\validators\AddressValidator;

/**
 * 
 * @property Location $location The location of the user or null if not defined.
 * 

 */
trait LocationTrait {

    /**
     *
     * @var int
     */
    public $country_id;

    /**
     *
     * @var int
     */
    public $zone_id;

    /**
     *
     * @var int
     */
    public $region_id;

    /**
     *
     * @var int
     */
    public $place_id;

    /**
     *
     * @var string
     */
    public $address;

    /**
     *
     * @var double
     */
    public $latitude;

    /**
     *
     * @var double
     */
    public $longitude;

    /**
     * @inheritdoc
     */
    public function locationRules() {
        return [
            [['country_id', 'zone_id', 'region_id', 'place_id'], 'integer'],
            ['address', 'string'],
            ['address', AddressValidator::className()],
            [['latitude', 'longitude'], 'double'],
            ['place_id', 'validatePlace'],
            ['region_id', 'validateRegion'],
            ['zone_id', 'validateZone'],
            ['country_id', 'validateCountry'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function locationAttributeLabels() {
        return (new Location())->attributeLabels();
    }

    /**
     * @inheritdoc
     */
    public function validatePlace() {
        if ($this->place_id !== null) {
            $place = Place::findOne($this->place_id);
            if ($place === null) {
                $this->addError('place_id', Yii::t('location', 'Place with id {[id]} doesn\'t exist', [
                            'id' => $this->place_id
                ]));
                return false;
            }
            $this->region_id = $place->region_id;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function validateRegion() {
        if ($this->region_id !== null) {
            $region = Region::findOne($this->region_id);
            if ($region === null) {
                $this->addError('zone_id', Yii::t('location', 'Region with id {[id]} doesn\'t exist', [
                            'id' => $this->region_id
                ]));
                return false;
            }
            $this->zone_id = $region->zone_id;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function validateZone() {
        if ($this->zone_id !== null) {
            $zone = Zone::findOne($this->zone_id);
            if ($zone === null) {
                $this->addError('zone_id', Yii::t('location', 'Zone with id {[id]} doesn\'t exist', [
                            'id' => $this->zone_id
                ]));
                return false;
            }
            $this->country_id = $zone->country_id;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function validateCountry() {
        if ($this->country_id !== null) {
            $country = Country::findOne($this->country_id);
            if ($country === null) {
                $this->addError('country_id', Yii::t('location', 'Country with id {[id]} doesn\'t exist', [
                            'id' => $this->country_id
                ]));
                return false;
            }
        }
        return true;
    }

    /**
     * Saves the related location object.
     * 
     * @param bool $runValidation
     * @param array $attributeNames
     * @return boolean
     * @see \yii\db\ActiveRecord::save()
     */
    public function saveLocation($runValidation = true, $attributeNames = null) {
        if (!$this->country_id && !$this->zone_id && !$this->region_id && !$this->place_id && !$this->address && !$this->latitude && !$this->longitude) {
            return true;
        }

        $location = $this->location;
        if ($location === null) {
            $location = new Location();
        }

        $location->country_id = $this->country_id;
        $location->zone_id = $this->zone_id;
        $location->region_id = $this->region_id;
        $location->place_id = $this->place_id;
        $location->address = $this->address;
        $location->latitude = $this->latitude;
        $location->longitude = $this->longitude;

        if (is_array($attributeNames)) {
            $attributesNames = array_intersect(
                    ['country_id', 'zone_id', 'region_id', 'place_id', 'address', 'latitude', 'longitude'], $attributesNames
            );
        }
        if (empty($attributeNames)) {
            $attributeNames = null;
        }
        if ($location->save($runValidation, $attributeNames) === false) {
            $this->addErrors($location->getErrors());
            return false;
        }
        $this->location_id = $location->id;
        return true;
    }

    public function populateLocationOwner() {
        $location = $this->location;
        if ($location !== null) {
            $this->country_id = $location->country_id;
            $this->zone_id = $location->zone_id;
            $this->region_id = $location->region_id;
            $this->place_id = $location->place_id;
            $this->address = $location->address;
            $this->latitude = $location->latitude;
            $this->longitude = $location->longitude;
        }
    }

    /**
     * @inheritdoc
     */
    public function getLocation() {
        return $this->hasOne(Location::className(), ['id' => 'location_id']);
    }

    /**
     * @inheritdoc
     */
    public function getCountryPropertyName() {
        return 'country_id';
    }

    /**
     * @inheritdoc
     */
    public function getZonePropertyName() {
        return 'zone_id';
    }

    /**
     * @inheritdoc
     */
    public function getRegionPropertyName() {
        return 'region_id';
    }

    /**
     * @inheritdoc
     */
    public function getPlacePropertyName() {
        return 'place_id';
    }

}
