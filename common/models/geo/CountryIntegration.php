<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\geo;

use Yii;
use yii\base\Model,
    yii\base\InvalidConfigException;
use \common\models\geo\Country;
use \common\models\geo\Zone;
use \common\models\geo\Region;
use \common\models\geo\Place;
/**
 * CSV Country integration 
 *
 * @author Kublai Gómez
 */
class CountryIntegration extends Model {
    /**
     * file with geo information of the country 'country,zone,region,place' 
     * @var string
     */
    public $filePath;
    
    /**
     * current line in file
     * @var integer 
     */
    protected $line;




    public function init(){
        parent::init();
    }
    
    public function rules() {
        return [
            ['filePath','fileExist']
        ];
        
    }


    public function process(){
        $this->line = 0;
        if ($this->validate() === false) {
            return false;    
        }
        $handle = fopen($this->filePath,"r");
        $trans = Yii::$app->db->beginTransaction();        
        while ($dataline = fgetcsv($handle,1000,";")){
            list ($country,$zone,$region,$place) = $dataline;
            $this->line++;
            $this->processLine($country,$zone,$region,$place);
        }
        if (sizeof($this->getErrors()) == 0){
            $trans->commit();
            return true;
        }else{
            $trans->rollBack();
            return false;
        }
        
    }
    
    /**
     * 
     * @param string $country
     * @param string $zone
     * @param string $region
     * @param string $place
     */
    protected function processLine($country,$zone,$region,$place){  
        $countryId= $this->processLineCountry($country);
        $zoneId = $this->processLineZone($countryId,$zone);
        $regionId = $this->processLineRegion($countryId,$zoneId,$region);
        $this->processLinePlace($regionId,$place);
    }
    
    /**
     * 
     * @param string $country
     * @return boolean
     */
    protected function processLineCountry($country){
        $countryId=0;
        $countryCheck =new Country();
        $countryControl= $countryCheck->find()->where(['name'=>$country])->one();
        if ($countryControl == null){
            $this->addError('control',"Line " . $this->line .": Country " . $country . " does not exist");
            return false;
        }else{
            $countryId=$countryControl->id;
        }  
        return $countryId;
    }
    
    /**
     * 
     * @param integer $countryId
     * @param string $zone
     * @return integer
     */
    protected function processLineZone($countryId,$zone){
         
        $zoneCheck =new Zone();
        $zoneControl= $zoneCheck->find()->where(['name'=>$zone, 'country_id'=>$countryId])->one();
        $zoneId = 0;
        if ($zoneControl == null && $countryId > 0){
            $zoneObj = new Zone();
            $zoneObj->country_id = $countryId;
            $zoneObj->name = $zone;
            if ($zoneObj->save()){
                $zoneId = $zoneObj->id;
            }else{
                $this->addError('control',"Line ". $this->line . ": Error saving zone '" . $zone . "'"); 
            }
        }else{
            if ($countryId>0){
                $zoneId=$zoneControl->id; 
            }
        }
        return $zoneId;
    }
    
    
    /**
     * 
     * @param integer $countryId
     * @param integer $zoneId
     * @param string $region
     * @return integer
     */
    protected function processLineRegion($countryId,$zoneId,$region){
        $regionCheck =new Region();
        $regionControl = $regionCheck->find()->where(['name'=>$region, 'zone_id'=>$zoneId])->one();           
        $regionId=0;
        if ($regionControl == null && $zoneId>0){
            $regionObj= new Region();
            $regionObj->name=$region;
            $regionObj->zone_id = $zoneId;
            if ($regionObj->save()){
                $regionId = $regionObj->id;
            }else{
                $this->addError('control',"Line ". $this->line . ": Error saving region '" . $region . "'"); 
            }
        }else{
            if ($countryId>0 && $zoneId>0 ){
                $regionId=$regionControl->id;
            }
        }
        return $regionId;
    }
    
    /**
     * 
     * @param integer $regionId
     * @param string $place
     */
    protected function processLinePlace($regionId,$place){
        $placeCheck =new Place();
        $placeControl = $placeCheck->find()->where(['name'=>$place, 'region_id'=>$regionId])->one();
        if($placeControl == null && $regionId>0){
            $placeObj = new Place();
            $placeObj->name = $place;
            $placeObj->region_id = $regionId;
            if (!$placeObj->save()){
                $this->addError('control',"Line ". $this->line . ": Error saving place '" . $place . "'");
            }
        }
    }


    /**
     * 
     * @return boolean
     */
    public function fileExist($attribute, $params) {
        if (file_exists($this->$attribute) === false) {
            $this->addError($attribute, Yii::t('common/lms/integration', 'Use: ./yii lms/integrate-country-csv {datafile.csv}'));
        }
    }
    
}
