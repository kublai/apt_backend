<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\geo;

use Yii;
use custom\validators\NameValidator;

/**
 * 
 * @property Place[] $places A collection of Place objects.
 * @property Location[] $locations A collection of Location objects.
 * @property Zone $zone The zone in which this region is located.
 * 

 */
class Region extends base\Region {

    /**
     * @inheritdoc
     */
    public function rules() {
        return array_merge(parent::rules(), [
        ]);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getPlaces() {
        return $this->hasMany(Place::className(), ['region_id' => 'id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getLocations() {
        return $this->hasMany(Location::className(), ['region_id' => 'id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getZone() {
        return $this->hasOne(Zone::className(), ['id' => 'zone_id']);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common/geo/region', 'ID'),
            'zone_id' => Yii::t('common/geo/region', 'Zone ID'),
            'name' => Yii::t('common/geo/region', 'Name'),
            'created_at' => Yii::t('common/general', 'Created At'),
            'created_by' => Yii::t('common/general', 'Created By'),
            'updated_at' => Yii::t('common/general', 'Updated At'),
            'updated_by' => Yii::t('common/general', 'Updated By'),
        ];
    }
}
