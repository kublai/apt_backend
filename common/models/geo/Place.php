<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\geo;

use Yii;
use custom\validators\NameValidator;

/**
 * 
 * @property Location[] $locations A collection of Location objects.
 * @property Region $region The region in which this place is located.
 * 

 */
class Place extends base\Place {

    /**
     * @inheritdoc
     */
    public function rules() {
        return array_merge(parent::rules(), [
        ]);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getLocations() {
        return $this->hasMany(Location::className(), ['place_id' => 'id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getRegion() {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common/geo/place', 'ID'),
            'region_id' => Yii::t('common/geo/place', 'Region ID'),
            'name' => Yii::t('common/geo/place', 'Name'),
            'created_at' => Yii::t('common/general', 'Created At'),
            'created_by' => Yii::t('common/general', 'Created By'),
            'updated_at' => Yii::t('common/general', 'Updated At'),
            'updated_by' => Yii::t('common/general', 'Updated By'),
        ];
    }

}
