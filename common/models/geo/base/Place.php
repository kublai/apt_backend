<?php

namespace common\models\geo\base;

use Yii;

/**
 * This is the model class for table "geo_place".
 *
 * @property integer $id
 * @property integer $region_id
 * @property string $name
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class Place extends \custom\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'geo_place';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['region_id', 'name'], 'required'],
            [['region_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }


}
