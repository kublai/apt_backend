<?php

namespace common\models\geo\base;

use Yii;

/**
 * This is the model class for table "geo_zone".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $name
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class Zone extends \custom\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'geo_zone';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['country_id', 'name'], 'required'],
            [['country_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }



}
