<?php

namespace common\models\geo\base;

use Yii;
use custom\base\traits\ClassNameTrait;

/**
 * This is the model class for table "geo_location".
 *
 * @property integer $id
 * @property integer $country_id
 * @property integer $zone_id
 * @property integer $region_id
 * @property integer $place_id
 * @property string $address
 * @property double $latitude
 * @property double $longitude
 */
class Location extends \yii\db\ActiveRecord {

    use ClassNameTrait;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'geo_location';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['country_id', 'zone_id', 'region_id', 'place_id'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['address'], 'string', 'max' => 255]
        ];
    }



}
