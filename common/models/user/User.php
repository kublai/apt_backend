<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\user;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\web\IdentityInterface;
use common\models\geo\Location,
    common\models\geo\LocationInterface,
    common\models\geo\LocationTrait;
use custom\validators\UsernameValidator,
    custom\validators\NameValidator,
    custom\validators\AddressValidator;
use common\models\user\Role;
use common\models\lms\Campaign,
    common\models\lms\Student;

/**
 * 
 * @property Location $location The location of the user or null if not defined.
 * @property static[] $instructors The instructors of the user.
 * @property static[] $students The students that this user instrucs.
 * @property Campaign[] $instructedCampaigns The campaigns where the user is instructor
 * @property Campaign[] $studedCampaigns The campaigns where the user is student
 * 
 
 */
class User extends base\User implements IdentityInterface, LocationInterface {

    use LocationTrait;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 10;
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    /**
     * @inheritdoc
     */
    public function rules() {
        return array_merge(parent::rules(), [
            ['username', UsernameValidator::className()],
            ['username', 'string', 'min' => 4],
            [['name', 'last_name', 'address'], 'default', 'value' => null],
            [['name', 'last_name'], NameValidator::className()],
            ['address', AddressValidator::className()],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],
            ['gender', 'in', 'range' => [self::GENDER_MALE, self::GENDER_FEMALE]],
            ['role', 'in', 'range' => [Role::SUPER_ADMIN, Role::ADMIN, Role::OPERATOR, Role::INSTRUCTOR, Role::STUDENT]],
                ], $this->locationRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), [
            'statusName' => Yii::t('common/user/user', 'Status')
            , 'genderName' => Yii::t('common/user/user', 'Gender')
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
                    'password_reset_token' => $token,
                    'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Gets the user hashed password.
     * 
     * @return string
     */
    public function getPassword() {
        return $this->password_hash;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
            [
                'class' => SluggableBehavior::className(),
                'ensureUnique' => true,
                'immutable' => true,
                'value' => function($event) {
                    return md5($this->username);
                }
            ]
        ]);
    }

    /**
     * Checks if this user object has the given rol.
     * 
     * @param int $rol
     * @return bool
     */
    public function hasRol($rol) {
        return (new Role($rol))->checkUser($this);
    }

    /**
     * 
     * @return array
     */
    public static function getStatusList() {
        return [
            self::STATUS_ACTIVE => Yii::t('common/user/user', 'Active'),
            self::STATUS_INACTIVE => Yii::t('common/user/user', 'Inactive')
        ];
    }

    /**
     * 
     * @return array
     */
    public static function getGenderList() {
        return [
            self::GENDER_MALE => Yii::t('common/user/user', 'Male'),
            self::GENDER_FEMALE => Yii::t('common/user/user', 'Female')
        ];
    }

    /**
     * Gets the status of the user.
     * 
     * @return string
     */
    public function getStatusName() {
        return static::getStatusList()[$this->status];
    }

    /**
     * Gets the gender of the user.
     * 
     * @return string
     */
    public function getGenderName() {
        return static::getGenderList()[$this->gender];
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null) {
        $trans = Yii::$app->db->beginTransaction();
        if ($this->saveLocation($runValidation, $attributeNames) && parent::save($runValidation, $attributeNames)) {
            $trans->commit();
            return true;
        } else {
            $trans->rollBack();
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public static function populateRecord($record, $row) {
        parent::populateRecord($record, $row);
        $record->populateLocationOwner();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstructor() {
        $this->hasOne(\common\models\lms\Student::className(), ['user_id' => 'id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getInstructors() {
        $q = static::find()
                ->innerJoin('lms_instructor', 'lms_instructor.user_id = user.id')
                ->innerJoin('lms_student', 'lms_student.instructor_id = lms_instructor.user_id')
                ->where(['lms_student.user_id' => $this->id]);
        $q->multiple = true;
        return $q;
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getStudents() {
        return $this->hasMany(Student::className(), ['instructor_id' => 'id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getCampaignStudents(Campaign $campaign) {
        return $this->getStudents()->where(['campaign_id' => $campaign->id]);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getOperatedCampaigns() {
        return $this->hasMany(Campaign::className(), ['id' => 'campaign_id'])
                        ->viaTable('lms_operator', ['operator_id' => 'id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getInstructedCampaigns() {
        return $this->hasMany(Campaign::className(), ['id' => 'campaign_id'])
                        ->viaTable('lms_instructor', ['user_id' => 'id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getStudedCampaigns() {
        return $this->hasMany(Campaign::className(), ['id' => 'campaign_id'])
                        ->viaTable('lms_student', ['user_id' => 'id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getOperator() {
        return $this->hasOne(Operator::className(), ['id' => 'operator_id']);
    }

    /**
     * 
     * @return string
     */
    public function getFullName() {
        return trim($this->name) . ' ' . trim($this->last_name);
    }

}