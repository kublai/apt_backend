<?php

namespace common\models\user;

use common\models\lms\base\Instructor;
use common\models\user\User;
use Yii;
use yii\base\Model,
    yii\base\InvalidConfigException;
use yii\helpers\Console;

/**
 *
 * Process batch changes for activation/deactivation of users
 *
 */
class UserStatus extends Model
{
    /**
     * @var string
     */
    public $path;

    /**
     * @var campaignId
     */
    public $campaignId;

    /**
     * @var boolean
     */
    public $state;

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['path', 'required'],
            ['path', 'fileExist'],
            ['instructorUsername', 'required'],
            ['campaignId', 'required'],
            ['state', 'required']
        ];
    }

    /**
     *
     * @return boolean
     */
    public function fileExist() {
        if (file_exists($this->path) === false) {
            $this->addError('path', Yii::t('common/user/status', 'Status list file doesn\'t exist'));
            return false;
        } else {
            return true;
        }
    }

    public function process(){
        $handler = fopen($this->path,'r+');
        $trans = Yii::$app->db->beginTransaction();
        try {
            $i=0;
            while ($data = fgetcsv($handler)) {
                $i++;
                list($instructorUsername, $username) = $data;
                $user = User::findOne(['username' => $username]);
                $instructor = User::findOne(['username' =>$instructorUsername]);
                if (!$user){
                    $this->addLineError($i,"User with username $username does not exist");
                }
                If (!$instructor){
                    $this->addLineError($i,"Instructor with username $instructorUsername does not exist in the system");
                }
                if ($instructor && $user){
                    //check if the instructor belongs to the campaign that we want to work with
                    $instructorValidated = \common\models\lms\Instructor::findOne(['campaign_id' => $this->campaignId, 'user_id' => $instructor->id]);
                    if ($instructorValidated == null) {
                        $this->addLineError($i,"The instructor $instructorUsername does not exist for campaign with Id number $this->campaignId");
                    }

                    if (!$user) {
                        $this->addLineError($i,"User $username doesn\'t exist or does not belongs to Instructor $instructorUsername");
                    }else{
                        $user->status = $this->state? User::STATUS_ACTIVE:User::STATUS_INACTIVE;
                        if ($user->save() === false) {
                            $this->addModelErrors($i, $user);
                            return false;
                        }
                    }
                }

            }
            if($this->errors){
                $trans->rollBack();
                fclose($handler);
                return false;
            }else{
                $trans->commit();
                fclose($handler);
                return true;
            }
        }catch(\Exception $e){
            $trans->rollBack();
            fclose($handler);
            return false;
        }
    }

    /**
     * Wraps the errors of the provided model to this model.
     *
     * @param int $i
     * @param Model $model
     */
    protected function addModelErrors($i, Model $model) {
        foreach ($model->getErrors() as $errors) {
            foreach ($errors as $error) {
                $this->addLineError($i, $error);
            }
        }
    }

    /**
     * Adds an error including the line where it is generated.
     *
     * @param int $i
     * @param string $message
     */
    protected function addLineError($i, $message) {
        $this->addError('process', Yii::t('common/user/status', "Error in line [{n}]:", [
                'n' => $i
            ]) . " $message");
    }
}