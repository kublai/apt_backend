<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\user;

use custom\base\Model;
use Yii;

/**
 * Model used to change a concrete user password
 * 

 */
class PasswordChange extends Model {

    /**
     *
     * @var string
     */
    public $password;

    /**
     *
     * @var string
     */
    public $password_check;

    /**
     *
     * @var User
     */
    public $user;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['password_check', 'required'],
            ['password_check', 'string', 'min' => 6],
            ['password_check', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('signup', "Passwords don't match")],
        ];
    }

    /**
     * 
     * @param integer $id
     * @return boolean
     */
    public function loadUser($id) {
        $user = User::findOne($id);
        if ($user === null) {
            return false;
        }
        $this->user = $user;
        return true;
    }

    /**
     * 
     * @return boolean
     */
    public function save() {
        if ($this->validate()) {
            $this->user->setPassword($this->password);
            return $this->user->save();
        }
        return false;
    }

    /**
     * 
     * @return int The number of users updated.
     */
    public function saveMassive() {
        $this->user = User::find()->one();
        $this->save();

        return User::updateAll(['password_hash' => $this->user->password]);
    }

}
