<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\user;

use yii\db\ActiveQuery;

/**
 * 
 * @property User[] $users
 * 

 */
class Operator extends User {

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        $this->role = Role::OPERATOR;
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return array_merge(parent::rules(), [
            ['email', 'required']
        ]);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['operator_id' => 'id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getInstructors() {
        $q = static::find()->innerJoin('lms_instructor', 'lms_instructor.user_id = operator.id')
                ->where(['lms_instructor.operator_id' => $this->id]);
        $q->multiple = true;
        return $q;
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getStudents() {
        $q = static::find()
                ->innerJoin('lms_student', 'lms_student.user_id = user.id')
                ->innerJoin('lms_instructor', 'lms_instructor.user_id = lms_student.instructor_id AND lms_instructor.campaign_id = lms_student.campaign_id')
                ->where(['lms_instructor.operator_id' => $this->id]);
        $q->multiple = true;
        return $q;
    }

    /**
     * 
     * @return OperatorQuery
     */
    public static function find() {
        return new OperatorQuery(get_called_class());
    }

}

/**
 * 

 */
class OperatorQuery extends ActiveQuery {

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        $this->from('user operator');
        $this->andWhere(['operator.role' => Role::OPERATOR]);
    }

}
