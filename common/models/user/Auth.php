<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\user;

use Yii;
use custom\db\exceptions\SaveException;
use common\models\user\User;

/**
 * 
 * 

 */
class Auth extends base\Auth {

    /**
     * Generates a new access token.
     */
    public function generateAccessToken() {
        $this->access_token = unpack('H*', Yii::$app->security->generateRandomKey(32))[1];
        $this->expires_at = time() + Yii::$app->params['access_token_maxlifetime'];
    }

    /**
     * 
     * @return yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }

    /**
     * Authorizes a user to enter the api.
     * 
     * @param User $user
     * @return string The authorized access token.
     * @throws SaveException
     */
    public static function authorize(User $user) {
        $auth = static::findOne(['id' => $user->id]);
        if ($auth === null) {
            $auth = new static(['id' => $user->id]);
        }
        $auth->generateAccessToken();
        if ($auth->save() === false) {
            throw new SaveException($auth);
        }
        return $auth;
    }

}
