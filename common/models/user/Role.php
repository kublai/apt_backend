<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\user;

use InvalidArgumentException;
use custom\base\traits\ClassNameTrait;
use Yii;
use custom\web\User as WebUser;

/**
 * Role class to create concrete platform rol objects and contain common static 
 * methods to interact with roles.
 * 

 */
class Role {

    use ClassNameTrait;

    /**
     * Admin rol value
     */
    const SUPER_ADMIN = 1;

    /**
     * Customer rol value
     */
    const ADMIN = 2;

    /**
     * Operator rol value
     */
    const OPERATOR = 4;

    /**
     * Instructor signup rol value
     */
    const INSTRUCTOR = 8;

    /**
     * Student rol value
     */
    const STUDENT = 16;

    /**
     * Rol value
     * @var int
     */
    protected $val;

    /**
     * Constructor.
     * 
     * @param int $role
     * @throws InvalidArgumentException
     */
    public function __construct($role) {
        $r = (int) $role;
        $t = array_sum(static::table());
        if (0 >= $r || $r > $t) {
            throw new InvalidArgumentException('$role must be one of the valid platform roles');
        }
        $this->val = (int) $role;
    }

    /**
     * 
     * @return type
     */
    public function getValue() {
        return $this->val;
    }

    /**
     * Checks if the given User object has the rol.
     * 
     * @param User $user
     * @return bool
     */
    public function checkUser(User $user) {
        $ur = (int) $user->role;
        return ($this->val & $ur) === $this->val;
    }

    /**
     * Checks if the userRole contains the given role.
     * 
     * @param int $role
     * @param int $userRole
     * @return bool
     */
    public static function check($role, $userRole) {
        $ur = (int) $userRole;
        $objRole = new static($role);
        return ($objRole->getValue() & $ur) === $objRole->getValue();
    }

    /**
     * Returns a list with all the platform roles.
     * 
     * @return int[]
     */
    public static function table() {
        return [self::SUPER_ADMIN, self::ADMIN, self::OPERATOR, self::INSTRUCTOR, self::STUDENT];
    }

    /**
     * Returns a list with all the platform roles.
     * 
     * @return int[]
     */
    public static function tableName() {
        return [
            self::SUPER_ADMIN => Yii::t('common/user/user', 'Super Admin'),
            self::ADMIN => Yii::t('common/user/user', 'Admin'),
            self::OPERATOR => Yii::t('common/user/user', 'Operator'),
            self::INSTRUCTOR => Yii::t('common/user/user', 'Instructor'),
            self::STUDENT => Yii::t('common/user/user', 'Student')
        ];
    }

    /**
     * 
     * @param int $role
     */
    public static function getName($role) {
        $roles = static::tableName();
        return $roles[$role];
    }
    
    /**
     * Returns a map with the plaform roles as keys.
     * 
     * @return array map<int, int>
     */
    public static function map() {
        return array_flip(static::table());
    }

    /**
     * 
     * @return string
     */
    public static function className() {
        return get_called_class();
    }
    
    /**
     * 
     * @param \custom\web\User $user
     * @return boolean
     */
    public static function isOperator(WebUser $user) {
        return Role::check(Role::OPERATOR, $user->getRole());
    }
}
