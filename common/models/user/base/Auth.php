<?php

namespace common\models\user\base;

use Yii;

/**
 * This is the model class for table "usr_auth".
 *
 * @property integer $id
 * @property string $access_token
 * @property integer $expires_at
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class Auth extends \custom\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usr_auth';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'access_token'], 'required'],
            [['id', 'expires_at', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['access_token'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('XXXXXX::underscoredName()', 'ID'),
            'access_token' => Yii::t('XXXXXX::underscoredName()', 'Access Token'),
            'expires_at' => Yii::t('XXXXXX::underscoredName()', 'Expires At'),
            'created_at' => Yii::t('XXXXXX::underscoredName()', 'Created At'),
            'created_by' => Yii::t('XXXXXX::underscoredName()', 'Created By'),
            'updated_at' => Yii::t('XXXXXX::underscoredName()', 'Updated At'),
            'updated_by' => Yii::t('XXXXXX::underscoredName()', 'Updated By'),
        ];
    }
}
