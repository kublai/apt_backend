<?php

/**

 * @copyright  jl
 * @version     1.0
 */

namespace common\models\user\base;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $slug
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $status
 * @property integer $role
 * @property integer $operator_id
 * @property string $name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property integer $gender
 * @property integer $location_id
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class User extends \custom\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['slug', 'auth_key', 'password_hash'], 'required'],
            [['status', 'role', 'operator_id', 'gender', 'location_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['slug', 'auth_key'], 'string', 'max' => 32],
            [['username', 'password_reset_token', 'name', 'last_name', 'email', 'phone'], 'string', 'max' => 255],
            [['password_hash'], 'string', 'max' => 40],
            [['slug'], 'unique'],
            [['username'], 'unique'],
            [['operator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['operator_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common/user/user', 'ID'),
            'slug' => Yii::t('common/user/user', 'Slug'),
            'username' => Yii::t('common/user/user', 'Username'),
            'auth_key' => Yii::t('common/user/user', 'Auth Key'),
            'password_hash' => Yii::t('common/user/user', 'Password Hash'),
            'password_reset_token' => Yii::t('common/user/user', 'Password Reset Token'),
            'status' => Yii::t('common/user/user', 'Status'),
            'role' => Yii::t('common/user/user', 'Role'),
            'operator_id' => Yii::t('common/user/user', 'Operator ID'),
            'name' => Yii::t('common/user/user', 'Name'),
            'last_name' => Yii::t('common/user/user', 'Last Name'),
            'email' => Yii::t('common/user/user', 'Email'),
            'phone' => Yii::t('common/user/user', 'Phone'),
            'gender' => Yii::t('common/user/user', 'Gender'),
            'location_id' => Yii::t('common/user/user', 'Location ID'),
            'created_at' => Yii::t('common/user/user', 'Created At'),
            'created_by' => Yii::t('common/user/user', 'Created By'),
            'updated_at' => Yii::t('common/user/user', 'Updated At'),
            'updated_by' => Yii::t('common/user/user', 'Updated By'),
        ];
    }

}
