<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\lms;

trait traitFileCampaignUtils {

    /**
     *
     * @var string 
     */
    public $filePath;

    /**
     *
     * @var int 
     */
    public $campaignId;
    
    /**
     *
     * @var object 
     */
    private $campaign;

    public function init() {
        parent::init();
        if (intval($this->campaignId) == 0) {
            throw new \yii\base\InvalidConfigException('Campaign Id must be provided');
        }
    }

    /**
     * 
     * @return array
     */
    public function rules() {
        return [
            ['filePath', 'required'],
            ['filePath', 'fileExist']
        ];
    }

    /**
     * 
     * @return boolean
     */
    protected function fileExist() {
        if (file_exists($filename) === FALSE) {
            $this->addError('filePath', 'File does not exist');
            return false;
        } else {
            return true;
        }
    }

    /**
     * 
     * @param type $c_id
     */
    public function setCampaignId($c_id) {
        $this->campaignId = $c_id;
    }

    /**
     * 
     * @return int
     */
    public function getCampaignId() {
        return $this->campaignId;
    }
    
    /**
     * 
     * @param string $message
     * @param int $counter
     */    
    private function addErrorLine($message,$counter=0){
        if ($counter >0){
            $this->addError('deleteuser', "line ". $counter .": " . $message);
        }else{
            $this->addError('deleteuser',  $message);
        }
    }

}
