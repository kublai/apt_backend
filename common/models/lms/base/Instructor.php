<?php

namespace common\models\lms\base;

use Yii;

/**
 * This is the model class for table "lms_instructor".
 *
 * @property integer $user_id
 * @property integer $campaign_id
 * @property integer $operator_id
 *
 * @property LmsCampaign $campaign
 * @property LmsOperator $operator
 * @property User $user
 * @property LmsStudent[] $lmsStudents
 */
class Instructor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lms_instructor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'campaign_id', 'operator_id'], 'required'],
            [['user_id', 'campaign_id', 'operator_id'], 'integer'],
            [['campaign_id'], 'exist', 'skipOnError' => true, 'targetClass' => LmsCampaign::className(), 'targetAttribute' => ['campaign_id' => 'id']],
            [['operator_id'], 'exist', 'skipOnError' => true, 'targetClass' => LmsOperator::className(), 'targetAttribute' => ['operator_id' => 'operator_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'campaign_id' => Yii::t('app', 'Campaign ID'),
            'operator_id' => Yii::t('app', 'Operator ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampaign()
    {
        return $this->hasOne(LmsCampaign::className(), ['id' => 'campaign_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperator()
    {
        return $this->hasOne(LmsOperator::className(), ['operator_id' => 'operator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLmsStudents()
    {
        return $this->hasMany(LmsStudent::className(), ['instructor_id' => 'user_id']);
    }
}
