<?php

namespace common\models\lms\base;

use Yii;

/**
 * This is the model class for table "lms_student_module".
 *
 * @property integer $student_id
 * @property integer $campaign_id
 * @property integer $module_id
 * @property integer $completed
 * @property string $best_session_id
 */
class StudentModule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lms_student_module';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_id', 'campaign_id', 'module_id'], 'required'],
            [['student_id', 'campaign_id', 'module_id', 'completed'], 'integer'],
            [['best_session_id'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'student_id' => Yii::t('XXXXXX::underscoredName()', 'Student ID'),
            'campaign_id' => Yii::t('XXXXXX::underscoredName()', 'Campaign ID'),
            'module_id' => Yii::t('XXXXXX::underscoredName()', 'Module ID'),
            'completed' => Yii::t('XXXXXX::underscoredName()', 'Completed'),
            'best_session_id' => Yii::t('XXXXXX::underscoredName()', 'Best Session ID'),
        ];
    }
}
