<?php

namespace common\models\lms\base;

use Yii;
use custom\base\Setup;

/**
 * This is the model class for table "lms_campaign".
 *
 * @property integer $id
 * @property integer $location_id
 * @property string $name
 * @property integer $begin_at
 * @property integer $end_at
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class Campaign extends \custom\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'lms_campaign';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['location_id', 'name', 'begin_at', 'end_at'], 'required'],
            [['min_sync_week','min_sync_month','max_km_between_classes','max_absence_instructor_days','location_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['alarm_begin_at','alarm_end_at'],'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common/lms/campaign', 'ID'),
            'location_id' => Yii::t('common/lms/campaign', 'Location ID'),
            'name' => Yii::t('common/lms/campaign', 'Name'),
            'begin_at' => Yii::t('common/lms/campaign', 'Begin At'),
            'end_at' => Yii::t('common/lms/campaign', 'End At'),
            'min_sync_week' => Yii::t('common/lms/campaign', 'Minimum number of syncs per week'),
            'min_sync_month' => Yii::t('common/lms/campaign', 'Minimum number of syncs per month'),
            'max_km_between_classes' => Yii::t('common/lms/campaign', 'Max. Km between sessions'),
            'max_absence_instructor_days' => Yii::t('common/lms/campaign', 'Max. absence instructor days'),
            'alarm_begin_at' => Yii::t('common/lms/campaign', 'Analize from'),
            'alarm_begin_at' => Yii::t('common/lms/campaign', 'Analize to'),
            'created_at' => Yii::t('common/lms/campaign', 'Created At'),
            'created_by' => Yii::t('common/lms/campaign', 'Created By'),
            'updated_at' => Yii::t('common/lms/campaign', 'Updated At'),
            'updated_by' => Yii::t('common/lms/campaign', 'Updated By'),
        ];
    }

}
