<?php

namespace common\models\lms\base;

use Yii;

/**
 * This is the model class for table "lms_campaign_module".
 *
 * @property integer $module_id
 * @property integer $campaign_id
 * @property integer $order
 */
class CampaignModule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lms_campaign_module';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module_id', 'campaign_id'], 'required'],
            [['module_id', 'campaign_id', 'order','active'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'module_id' => Yii::t('XXXXXX::underscoredName()', 'Module ID'),
            'campaign_id' => Yii::t('XXXXXX::underscoredName()', 'Campaign ID'),
            'order' => Yii::t('XXXXXX::underscoredName()', 'Order'),
            'active' => Yii::t('XXXXXX::underscoredName()', 'Active'),
        ];
    }
}
