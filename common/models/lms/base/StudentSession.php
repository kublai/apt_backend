<?php

namespace common\models\lms\base;

use Yii;

/**
 * This is the model class for table "lms_student_session".
 *
 * @property string $id
 * @property integer $student_id
 * @property integer $campaign_id
 * @property integer $module_id
 * @property integer $location_id
 * @property integer $begin_at
 * @property integer $end_at
 * @property integer $completed
 * @property integer $utility
 * @property integer $difficulty
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class StudentSession extends \custom\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lms_student_session';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'student_id', 'campaign_id', 'module_id', 'begin_at'], 'required'],
            [['student_id', 'campaign_id', 'module_id', 'location_id', 'begin_at', 'end_at', 'completed', 'utility', 'difficulty', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['id'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/lms/user', 'ID'),
            'student_id' => Yii::t('common/lms/user', 'Student ID'),
            'campaign_id' => Yii::t('common/lms/user', 'Campaign ID'),
            'module_id' => Yii::t('common/lms/user', 'Module ID'),
            'location_id' => Yii::t('common/lms/user', 'Location ID'),
            'begin_at' => Yii::t('common/lms/user', 'Begin At'),
            'end_at' => Yii::t('common/lms/user', 'End At'),
            'completed' => Yii::t('common/lms/user', 'Completed'),
            'utility' => Yii::t('common/lms/user', 'Utility'),
            'difficulty' => Yii::t('common/lms/user', 'Difficulty'),
            'created_at' => Yii::t('common/lms/user', 'Created At'),
            'created_by' => Yii::t('common/lms/user', 'Created By'),
            'updated_at' => Yii::t('common/lms/user', 'Updated At'),
            'updated_by' => Yii::t('common/lms/user', 'Updated By'),
        ];
    }
}
