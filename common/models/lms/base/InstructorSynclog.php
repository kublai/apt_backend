<?php

namespace common\models\lms\base;

use Yii;

/**
 * This is the model class for table "lms_instructor_synclog".
 *
 * @property integer $id
 * @property integer $instructor_id
 * @property integer $campaign_id
 * @property integer $created_at
 * @property integer $status
 * @property resource $info
 * @property resource $log
 *
 */
class InstructorSynclog extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'lms_instructor_synclog';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['instructor_id', 'campaign_id', 'created_at'], 'required'],
            [['instructor_id', 'campaign_id', 'created_at', 'status'], 'integer'],
            [['info', 'log'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common/lms/instructor-synclog', 'ID'),
            'instructor_id' => Yii::t('common/lms/instructor-synclog', 'Instructor ID'),
            'campaign_id' => Yii::t('common/lms/instructor-synclog', 'Campaign ID'),
            'created_at' => Yii::t('common/lms/instructor-synclog', 'Created At'),
            'status' => Yii::t('common/lms/instructor-synclog', 'Status'),
            'info' => Yii::t('common/lms/instructor-synclog', 'Info'),
            'log' => Yii::t('common/lms/instructor-synclog', 'Log'),
        ];
    }

}
