<?php

namespace common\models\lms\base;

use Yii;

/**
 * This is the model class for table "lms_student".
 *
 * @property integer $campaign_id
 * @property integer $user_id
 * @property integer $instructor_id
 */
class Student extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lms_student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['campaign_id', 'user_id', 'instructor_id'], 'required'],
            [['campaign_id', 'user_id', 'instructor_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'campaign_id' => Yii::t('XXXXXX::underscoredName()', 'Campaign ID'),
            'user_id' => Yii::t('XXXXXX::underscoredName()', 'User ID'),
            'instructor_id' => Yii::t('XXXXXX::underscoredName()', 'Instructor ID'),
        ];
    }
}
