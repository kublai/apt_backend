<?php

namespace common\models\lms\base;

use Yii;

/**
 * This is the model class for table "lms_module".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $description
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class Module extends \custom\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'lms_module';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'code'], 'required'],
            [['description'], 'string'],
            [['created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name', 'code'], 'string', 'max' => 255],
            [['code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('common/lms/module', 'ID'),
            'code' => Yii::t('common/lms/module', 'Code'),
            'name' => Yii::t('common/lms/module', 'Name'),
            'description' => Yii::t('common/lms/module', 'Description'),
            'created_at' => Yii::t('common/lms/module', 'Created At'),
            'created_by' => Yii::t('common/lms/module', 'Created By'),
            'updated_at' => Yii::t('common/lms/module', 'Updated At'),
            'updated_by' => Yii::t('common/lms/module', 'Updated By'),
        ];
    }

}
