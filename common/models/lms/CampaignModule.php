<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\lms;

use custom\behaviors\ActiveOrderBehavior;

/**
 * 
 * @property Module $module The related module object.
 * @property Campaign $campaign The related campaign object.
 * 

 */
class CampaignModule extends base\CampaignModule {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
            [
                'class' => ActiveOrderBehavior::className(),
                'referenceColumns' => 'campaign_id'
            ]
        ]);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getCampaign() {
        return $this->hasOne(Campaign::className(), ['id' => 'campaign_id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getModule() {
        return $this->hasOne(Module::className(), ['id' => 'module_id']);
    }
    
}
