<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\lms;

/**
 * 
 * @property Campaign[] $campaigns The campaings wheres this module is included.
 * 

 */
class Module extends base\Module {

    /**
     * 
     * @return yii\db\ActiveQuery
     */
    public function getCampaigns() {
        return $this->hasMany(Campaign::className(), ['id' => 'campaign_id'])
                        ->viaTable('lms_campaign_module', ['module_id' => 'id']);
    }


}
