<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\lms;

use common\models\geo\Location,
    common\models\geo\LocationInterface,
    common\models\geo\LocationTrait;
use Yii;
use custom\db\exceptions\SaveException;
use yii\base\Exception as BaseException;
use Exception;
use yii\db\ActiveQuery;

/**
 * 
 * @property Campaign $campaign The campaign to which this session belongs.
 * @property Module $module The module to which this session belongs.
 * @property Student $student The student who has done the session.
 * @property Location $location The location object attached to the session.
 * 

 */
class StudentSession extends base\StudentSession implements LocationInterface {

    use LocationTrait;

    /**
     * @inheritdoc
     */
    public function rules() {
        return array_merge(parent::rules(), [
                ], $this->locationRules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), [
            'difficulty' => Yii::t('common/lms/user', 'Easiness')
                ], $this->locationAttributeLabels());
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null) {
        if ($runValidation && $this->validate($attributeNames) === false) {
            return false;
        }
        $trans = $this->getDb()->beginTransaction();
        try {
            StudentModule::processSession($this);
            if ($this->saveLocation($runValidation, $attributeNames) === false || parent::save(false, $attributeNames) === false) {
                throw new SaveException($this);
            }
            $trans->commit();
            return true;
        } catch (BaseException $ex) {
            $trans->rollBack();
            throw $ex;
        } catch (Exception $ex) {
            $trans->rollBack();
            throw $ex;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete() {
        $params = [
            'student_id' => $this->student_id
            , 'module_id' => $this->module_id
            , 'campaign_id' => $this->campaign_id
        ];
        if (static::find()->andWhere($params)->count() <= 0) {
            StudentModule::deleteAll($params);
        }
    }

    /**
     * @inheritdoc
     */
    public static function populateRecord($record, $row) {
        parent::populateRecord($record, $row);
        $record->populateLocationOwner();
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getStudent() {
        return $this->hasOne(Student::className(), [
                    'user_id' => 'student_id',
                    'campaign_id' => 'campaign_id'
        ]);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getModule() {
        return $this->hasOne(Module::className(), ['id' => 'module_id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getCampaign() {
        return $this->hasOne(Campaign::className(), ['id' => 'campaign_id']);
    }

    /**
     * 
     * @return StudentSessionQuery
     */
    public static function find() {
        return Yii::createObject(StudentSessionQuery::className(), [get_called_class()]);
    }

}

class StudentSessionQuery extends ActiveQuery {

    /**
     * 
     * @return $this
     */
    public function onlyLocalized() {
        return $this
                        ->innerJoin('geo_location gl', 'gl.id = location_id')
                        ->andWhere(['not', ['gl.latitude' => null, 'gl.longitude' => null]]);
    }

}
