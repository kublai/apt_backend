<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\lms;

use Yii;
use yii\base\Model;
use \common\models\user\User;


/**
 * Description of StudentMaintenanceUtils
 *
 * @author kublai
 */
class DeleteStudentsCampaign extends Model{     
   use traitFileCampaignUtils;
   // public $filePath;
   // public $campaignId;
   // private $campaign;
   
    /**
     * 
     * @return boolean
     * @throws exception
     */
    public function process(){
        $file = fopen($this->filePath, "r+");
        $this->campaign = Campaign::find()->where(['id' => $this->campaignId])->one();
        if (!$this->campaign->isActive()){
            addErrorLine("This campaign it's not active. data cannot be modified");
            return false;
        }
        $trans = Yii::$app->db->beginTransaction();
        try{
            $c = 0;
            while ($dataline = fgetcsv($file, 1000)){
                if (count($dataline)>1){ //checking for empty lines in file
                    list ($userName, $name, $lastname) = $dataline;
                    $this->processLine($userName,++$c);
                }
            }
            if ($this->hasErrors()){
                $trans->rollBack();
                return 0;
            }
            $trans->commit();
            return $c;
        } catch (\Exception $ex) {
            $trans->rollBack();
            throw $ex;
        }

    }
    
    private function processLine($userName,$counter){
        $user = User::find()->where(['username' => $userName])->one();
        if ($user === NULL){
            $this->addErrorLine("User with username = " . $userName . " does not exist.", $counter);
            return;
        }      
        $student = Student::find()->where(['user_id' => $user->id, 'campaign_id' => $this->campaign->id])->one();
        if ($student === NULL){ //se ha encontrado el estudiante y pertenece a la campaña, además no es facilitador
             $this->addErrorLine("User with username = " . $userName . 
                     " does not belongs to campaing '" . $this->campaign->name .
                     "' (Campaign Id = ". $this->campaignId . ")", $counter);
             return;
        }
        $student->unlinkAll('sessions',true);
        $student->unlinkAll('modules',true);
        $this->campaign->unlink('students', $student, true);         
    }

    
}
