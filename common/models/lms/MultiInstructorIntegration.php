<?php

/**
 * @author      Kublai Gómez <kublai@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @date        2017-01-31
 * @version     1.0
 */

namespace common\models\lms;

use Yii,
    yii\base\Model,
    yii\base\InvalidConfigException;
use common\models\user\User,
    common\models\user\Role,
    common\models\user\Operator,
    common\models\geo\Country,
    common\models\geo\Zone,
    common\models\geo\Region,
    common\models\geo\Place,
    common\models\lms\Campaign;

/**
 * Class to handle the process of integrate a file for multiple instrutors and theirs students.
 * 
 * @author <kublai@bytebox.es>
 */
class MultiInstructorIntegration extends Model {

    /**
     *
     * @var string
     */
    public $filePath;
    
    /**
     *
     * @var int 
     */
    public $idCampaign;
    
    /**
     *
     * @var int 
     */
    public $idOperator;
    

    /**
     *
     * @var Instructor
     */
    protected $instructor;

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        if ($this->idOperator === null) {
            throw new InvalidConfigException('Id Operator must be provided on initialization');
        }
        if ($this->idCampaign === null) {
            throw new InvalidConfigException('Id Campaign must be provided on initialization');
        }
        $operatorModel = Operator::findOne(['id' => $this->idOperator ]);
        if ($operatorModel === null){
            throw new InvalidConfigException(Yii::t('common/lms/integration','Operator with Id [{idOperator}] is not defined',[
               'idOperator' =>  $this->idOperator
            ]));
        }
        $campaignModel = Campaign::findOne(['id' => $this->idCampaign ]);
        $operatorExistInCampaign = $campaignModel->getOperators()->where(['id'=>$this->idOperator])->all();
        if ( empty($operatorExistInCampaign)){
            throw new InvalidConfigException(Yii::t('common/lms/integration','Operator [{idOperator}] does not exists in Campaign [{idCampaign}]',[
                'idOperator' => $this->idOperator,
                'idCampaign' => $this->idCampaign
            ]));
        }
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['filePath', 'required'] ,
            ['filePath', 'fileExist']
        ];
    }

    /**
     * 
     * @return boolean
     */
    public function fileExist() {
        if (file_exists($this->filePath) === false) {
            $this->addError('filePath', Yii::t('common/lms/integration', 'Integration file doesn\'t exist'));
            return false;
        } else {
            return true;
        }
    }


    /**
     * Runs the integration process.
     * 
     * @return boolean
     * @throws \Exception
     */
    public function process() {
        if ($this->validate() === false) {
            return false;
        }
        $handle = fopen($this->filePath, 'r');
        $trans = Yii::$app->db->beginTransaction();
        try {
            $i = 0;
            while ($data = fgetcsv($handle)) {                
                $this->processUser($data, ++$i);
            }
            if ($this->hasErrors()) {
                $trans->rollBack();
                return false;
            }
            $trans->commit();
            return true;
        } catch (\Exception $e) {
            $trans->rollBack();
            throw $e;
        }
    }

    /**
     * Processes a single data entry by creating a user with it.
     * 
     * @param array $data
     * @param int $i
     * @return boolean
     */
    public function processUser(array $data, $i = null) {
        //idInstructor,name,lastname,gender,userid,phone,country,zone,region,place,address,long,lat
        $idInstructor = !empty($data[0]) ? trim($data[0]) : null;
        $name = !empty($data[1]) ? trim($data[1]) : null;
        $lastName = !empty($data[2]) ? trim($data[2]) : null;
        $gender = !empty($data[3]) ? trim($data[3]) : null;
        $cardId = !empty($data[4]) ? trim($data[4]) : null;
        $phone = !empty($data[5]) ? trim($data[5]) : null;
        $countryName = !empty($data[6]) ? trim($data[6]) : null;
        $zoneName = !empty($data[7]) ? trim($data[7]) : null;
        $regionName = !empty($data[8]) ? trim($data[8]) : null;
        $placeName = !empty($data[9]) ? trim($data[9]) : null;
        $address = !empty($data[10]) ? trim($data[10]) : null;
        $longitude = !empty($data[11]) ? trim($data[11]) : null;
        $latitude = !empty($data[12]) ? trim($data[12]) : null;
          
        /* Validating Country information */
        $country = Country::find()->where(['name' => $countryName])->one();
        if ($country === null) {
            $this->addLineError($i, Yii::t('common/lms/integration', 'Country with name [{name}] does not exist', [
                        'name' => $countryName
            ]));
            return false;
        }
        $zone = Zone::find()->where(['name' => $zoneName])->one();
         if ($zone === null) {
            $this->addLineError($i, Yii::t('common/lms/integration', 'Zone with name [{name}] does not exist in country [{containerName}]', [
                        'name' => $zoneName
                        , 'containerName' => $countryName
            ]));
            return false;
        }
        $region = Region::find()->where(['name' => $regionName, 'zone_id' => $zone->id])->one();
        if ($region === null) {
            $this->addLineError($i, Yii::t('common/lms/integration', 'Region with name [{name}] does not exist in zone [{containerName}]', [
                        'name' => $regionName
                        , 'containerName' => $zoneName
            ]));
            return false;
        }
        // if place is not founded, null will be used
        $place = Place::find()->where(['name' => $placeName, 'region_id' => $region->id])->one();
        
        
        $userIdInstructor = User::findOne(['username' => $idInstructor]);
        if ($userIdInstructor === null){
            $this->addLineError($i, "instructor Id does not exist");
            return false;
        }else{
            $this->instructor = Instructor::findOne(['campaign_id' => $this->idCampaign, 'user_id' => $userIdInstructor->id]);
            if ($this->instructor === null){
                $this->addLineError($i, "instructor Id not defined in this campaign");
                return false;
            }
        }
        $user = new User();
        $user->username = $cardId;
        $user->status = User::STATUS_ACTIVE;
        $user->role = Role::STUDENT;
        $user->operator_id = $this->instructor->operator_id;
        $user->name = mb_convert_case($name, MB_CASE_TITLE, "UTF-8");
        $user->last_name = mb_convert_case($lastName, MB_CASE_TITLE, "UTF-8");
        $user->gender = (int) $gender;
        $user->email = null;
        $user->phone = $phone;
        $user->generateAuthKey();
        $user->setPassword(uniqid($cardId));
        $user->country_id = $country->id;
        $user->zone_id = ($zone!=NULL)?$zone->id:NULL;
        $user->region_id = ($region!=NULL)?$region->id:NULL;
        $user->place_id = ($place!=NULL)?$place->id:NULL;
        $user->address = $address;
        $user->longitude = $longitude;
        $user->latitude = $latitude;
        if ($user->save() === false) {
            $this->addModelErrors($i, $user);
            return false;
        }
        $student = new Student();
        $student->campaign_id = $this->instructor->campaign_id;
        $student->user_id = $user->id;
        $student->instructor_id = $this->instructor->user_id;
        if ($student->save() === false) {
            $this->addModelErrors($i, $student);
            return false;
        }
        return true;
    }

    /**
     * Adds an error including the line where it is generated.
     * 
     * @param int $i
     * @param string $message
     */
    protected function addLineError($i, $message) {
        $this->addError('process', Yii::t('common/lms/integration', "Error in line [{n}]:", [
                    'n' => $i
                ]) . " $message");
    }

    /**
     * Wraps the errors of the provided model to this model.
     * 
     * @param int $i
     * @param Model $model
     */
    protected function addModelErrors($i, Model $model) {
        foreach ($model->getErrors() as $errors) {
            foreach ($errors as $error) {
                $this->addLineError($i, $error);
            }
        }
    }

}

