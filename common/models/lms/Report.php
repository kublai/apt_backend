<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\lms;

use custom\base\Model;
use Yii;
use yii\db\Query;
use common\models\user\User;
use common\models\geo\Country;
use yii\helpers\ArrayHelper;

class Report extends Model {

    /**
     *
     * @var User
     */
    protected $operator;

    /**
     * 
     * @param Operator $o
     */
    public function setOperator(User $o = null) {
        $this->operator = $o;
    }

    /**
     * 
     * @return Operator
     */
    public function getOperator() {
        return $this->operator;
    }

    /**
     * Generator of the full Lms report.
     * 
     * @yield array with report data
     * @return \Generator
     */
    public function fullLms($withHeader = false) {
        $countries = ArrayHelper::map(Country::find()->all(), 'id', 'timezone');
        if ($withHeader) {
            yield [
                Yii::t('common/lms/campaign', 'ID')
                , Yii::t('common/lms/campaign', 'Campaign')
                , Yii::t('common/user/user', 'Operator')
                , Yii::t('common/user/user', 'Instructor')
                , Yii::t('common/user/user', 'Student')
                , Yii::t('common/user/user', 'Username')
                , Yii::t('common/lms/module', 'Module Code')
                , Yii::t('common/lms/module', 'Module Name')
                , Yii::t('common/lms/user', 'Begin At')
                , Yii::t('common/lms/user', 'End At')
                , Yii::t('common/lms/user', 'Completed')
                , Yii::t('common/lms/user', 'Utility')
                , Yii::t('common/lms/user', 'Easiness')
                , Yii::t('common/geo/location', 'Latitude')
                , Yii::t('common/geo/location', 'Longitude')
            ];
        }
        $query = new Query();
        if ($this->operator !== null) {
            $query->andWhere(['lo.operator_id' => $this->operator->id]);
        }
        foreach ($query
                ->from('lms_campaign lc')
                ->innerJoin('geo_location lcloc', 'lcloc.id = lc.location_id')
                ->innerJoin('lms_operator lo', 'lo.campaign_id = lc.id')
                ->innerJoin('user opr', 'opr.id = lo.operator_id')
                ->innerJoin('lms_instructor li', 'li.operator_id = lo.operator_id AND li.campaign_id = lc.id')
                ->innerJoin('user ins', 'ins.id = li.user_id')
                ->innerJoin('lms_student ls', 'ls.instructor_id = li.user_id AND li.campaign_id = lc.id')
                ->innerJoin('user std', 'std.id = ls.user_id')
                ->innerJoin('lms_student_session lss', 'lss.student_id = ls.user_id AND lss.campaign_id = lc.id')
                ->innerJoin('lms_module lm', 'lm.id = lss.module_id')
                ->leftJoin('geo_location lssloc', 'lssloc.id = lss.location_id')
                ->select([
                    'lc.id'
                    , 'lcloc.country_id'
                    , 'lc.name campaign_name'
                    , 'CONCAT(opr.name, " ", opr.last_name) operator_name'
                    , 'CONCAT(ins.name, " ", ins.last_name) instructor_name'
                    , 'CONCAT(std.name, " ", std.last_name) student_name'
                    , 'std.username'
                    , 'lm.code'
                    , 'lm.name module_name'
                    , 'lss.begin_at'
                    , 'lss.end_at'
                    , 'lss.completed'
                    , 'lss.utility'
                    , 'lss.difficulty'
                    , 'lssloc.latitude'
                    , 'lssloc.longitude'
                ])
                ->orderBy([
                    'lc.name' => SORT_ASC
                ])
                ->each() as $row) {
            yield [
                $row['id']
                , $row['campaign_name']
                , $row['operator_name']
                , $row['instructor_name']
                , $row['student_name']
                , $row['username']
                , $row['code']
                , $row['module_name']
                , (new \DateTime('@' . $row['begin_at']))->setTimezone(new \DateTimeZone($countries[$row['country_id']]))->format('Y-m-d H:i:s')
                , (new \DateTime('@' . $row['end_at']))->setTimezone(new \DateTimeZone($countries[$row['country_id']]))->format('Y-m-d H:i:s')
                , $row['completed']
                , $row['utility']
                , $row['difficulty']
                , $row['latitude']
                , $row['longitude']
            ];
        }
    }

}
