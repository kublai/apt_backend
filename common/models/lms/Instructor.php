<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\lms;

use common\models\user\User,
    common\models\user\Operator;


/**
 * 
 * @property Operator $operator The operator of the instructor.
 * @property User $user The user model of the instructor.
 * @property Campaign $campaign The campaign model where this instructor is in.
 * @property StudentSession[] $studentSessions The sessions of the students of this instructor.
 * 

 */
class Instructor extends base\Instructor {

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getOperator() {
        return Operator::find()
                        ->innerJoin('lms_operator lo', 'lo.operator_id = operator.id')
                        ->andWhere([
                            'operator.id' => $this->operator_id,
                            'lo.campaign_id' => $this->campaign_id]
        );
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getCampaign() {
        return $this->hasOne(Campaign::className(), ['id' => 'campaign_id']);
    }

    /**
     * 
     * @return StudentSessionQuery
     */
    public function getStudentSessions() {
        $q = StudentSession::find()
                ->innerJoin('lms_student ls', 'ls.user_id = lms_student_session.student_id AND ls.campaign_id = lms_student_session.campaign_id')
                ->andWhere([
            'ls.instructor_id' => $this->user_id
            , 'ls.campaign_id' => $this->campaign_id
        ]);
        $q->multiple = true;
        return $q;
    }

}
