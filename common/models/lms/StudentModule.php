<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\lms;

use custom\db\exceptions\SaveException;

/**
 * 

 */
class StudentModule extends base\StudentModule {

    /**
     * Processes a student session in order to check its data and recover 
     * information of the module globals.
     * 
     * @param StudentSession $session
     * @throws SaveException
     */
    public static function processSession(StudentSession $session) {
        $sModule = static::findOne([
                    'student_id' => $session->student_id,
                    'campaign_id' => $session->campaign_id,
                    'module_id' => $session->module_id
        ]);
        if ($sModule === null) {
            $sModule = new static();
            $sModule->student_id = $session->student_id;
            $sModule->module_id = $session->module_id;
            $sModule->campaign_id = $session->campaign_id;
        }
        if ($session->completed) {
            $sModule->completed = $session->completed;
        }
        if ($sModule->save() === false) {
            throw new SaveException($sModule);
        }
    }

}
