<?php

/**
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\lms;

use Yii;
use yii\db\ActiveQuery;
use common\models\user\User;
use common\models\user\Operator;
use common\models\geo\LocationInterface;
use common\models\geo\LocationTrait;
use custom\validators\NameValidator;

/**
 * 
 * @property Module[] $modules The modules included in this campaign.
 * @property User[] $instructors The instructors of this campaign.
 * @property User[] $students The students of the campaign.
 * @property Operator[] $operators The operators of the campaign.
 * 

 */
class Campaign extends base\Campaign implements LocationInterface {

    use LocationTrait;

    /**
     * @inheritdoc
     */
    public function rules() {
        return array_merge(parent::rules(), [
            ['name', NameValidator::className()]
                ], $this->locationRules());
    }

    /**
     * return all active modules (active status is defined in lms_campaign_module
     * Kublai Gómez | 20-08-2020
     * 
     * @return \yii\db\ActiveQuery
     */
    /* public function getActiveModules(){
        $query = "SELECT
                        lm.id, lm.code, lm.name, lm.description
                    FROM
                        `lms_module` `lm`
                        INNER JOIN `lms_campaign_module` `lcm` ON lcm.module_id = lm.id
                    WHERE
                        lcm.campaign_id = " . $this->id . " AND lcm.active = 1 " .
                    " ORDER BY lcm.order";
        $q = Module::findBySql($query);
        $q->multiple = true;
        return $q;
    } */

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getModules() {
        $q = Module::find()
                ->innerJoin('lms_campaign_module', 'lms_module.id = lms_campaign_module.module_id')
                ->innerJoin('lms_campaign', 'lms_campaign.id = lms_campaign_module.campaign_id')
                ->orderBy(['lms_campaign_module.order' => SORT_ASC])
                ->andWhere(['lms_campaign.id' => $this->id]);
        $q->multiple = true;
        return $q;
    }

    /**
     * 
     * return all active modules (active status is defined in lms_campaign_module
     * Kublai Gómez | 20-08-2020
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getActiveModules(){
        $q = Module::find()
                ->innerJoin('lms_campaign_module', 'lms_module.id = lms_campaign_module.module_id')
                ->innerJoin('lms_campaign', 'lms_campaign.id = lms_campaign_module.campaign_id')
                ->orderBy(['lms_campaign_module.order' => SORT_ASC])
                ->andWhere(['lms_campaign.id' => $this->id])
                ->andWhere(['lms_campaign_module.active' => 1]);
        $q->multiple = true;
        return $q;
    }
    
    
    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getOperators() {
        return $this->hasMany(Operator::className(), ['id' => 'operator_id'])
                        ->viaTable('lms_operator', ['campaign_id' => 'id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getInstructors() {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
                        ->viaTable('lms_instructor', ['campaign_id' => 'id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getStudents() {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
                        ->viaTable('lms_student', ['campaign_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null) {
        $trans = Yii::$app->db->beginTransaction();
        if ($this->saveLocation($runValidation, $attributeNames) && parent::save($runValidation, $attributeNames)) {
            $trans->commit();
            return true;
        } else {
            $trans->rollBack();
            return false;
        }
    }

    /**
     * Checks if the campaign is active or not.
     * 
     * @return bool
     */
    public function isActive() {
        $now = time();
        return $this->begin_at <= $now && ($this->end_at === null || $this->end_at > $now);
    }

    /**
     * Clears all the student progress from the campaign.
     * 
     */
    public function clearProgress() {
        StudentModule::deleteAll(['campaign_id' => $this->id]);
    }

    /**
     * @inheritdoc
     */
    public static function populateRecord($record, $row) {
        parent::populateRecord($record, $row);
        $record->populateLocationOwner();
    }

    /**
     * 
     * @return Campaign
     */
    public static function findCurrent() {
        $now = time();
        return static::find()
                        ->andWhere(['and',
                            ['not', ['lms_campaign.begin_at' => null]],
                            ['<=', 'lms_campaign.begin_at', $now]
                        ])
                        ->andWhere(['or',
                            ['lms_campaign.end_at' => null],
                            ['>=', 'lms_campaign.end_at', $now]
        ]);
    }

    /**
     * @inhertidoc
     */
    public static function find() {
        return new CampaignQuery(get_called_class());
    }

    /**
     * 
     * @param User $user
     * @return boolean
     */
    public static function isOperator(User $user) {
        return static::find()
                        ->joinWith('operators')
                        ->andWhere(['operator.id' => $user->id])
                        ->exists();
    }
    
    
    public function getTimeZone(){
        $row=  \common\models\geo\Country::find()
                ->innerJoin('geo_location','geo_country.id = geo_location.country_id')
                ->innerJoin('lms_campaign','geo_location.id = lms_campaign.location_id')
                ->andWhere(['lms_campaign.id' => $this->id])->one();
        return $row->timezone;
    }
    
    /**
     * 
     * @param int $startDate
     * @param int $endDate
     */
    public function getInstructorsTotalSessionsBetweenDates($startDate, $endDate){
        $query="SELECT `lms_student`.instructor_id, count(`lms_student_session`.completed) as total_sessions
            FROM `lms_student_session` LEFT JOIN `lms_student` ON lms_student_session.student_id = lms_student.user_id 
            WHERE ( `lms_student_session`.`campaign_id` = " . $this->id .") "
                . "AND  (`lms_student_session`.`begin_at` >= ". $startDate .") "
                . "AND (`lms_student_session`.`end_at` <= ". $endDate .")     
            GROUP BY `lms_student`.`instructor_id`";
        return $this->executeQuery($query) ;    
    }
    
    public function getInstructorsTotalSyncsBetweenDates($startDate, $endDate){
        $query = "SELECT instructor_id, count(*) as total FROM  lms_instructor_synclog "
                . " WHERE ( created_at >= " . $startDate .") "
                . " AND  ( created_at <= " . $endDate .") "
                . " AND  ( campaign_id = " . $this->id .") "
                . "GROUP BY instructor_id";
              //  . "AND ( status = " . InstructorSynclog::STATUS_SUCCESS .") ";
        return $this->executeQuery($query) ;     
    }
    
    public function  getInstructorSessionsLocationHistory($startDate, $endDate, $instructor_id){
        $query = "SELECT 
                        lms_student.instructor_id, 
                        lms_student.user_id,
                        user.name,
                        user.last_name,
                        lms_student_session.location_id, 
                        lms_student_session.begin_at, 
                        geo_location.latitude, 
                        geo_location.longitude
                        FROM lms_student_session
                        INNER JOIN geo_location ON lms_student_session.location_id = geo_location.id
                        INNER JOIN lms_student ON lms_student_session.student_id = lms_student.user_id
                        INNER JOIN user ON lms_student.user_id = user.id
                        AND lms_student_session.campaign_id = lms_student.campaign_id
                        WHERE (  lms_student.instructor_id = " . $instructor_id .") 
                        AND (  lms_student_session.created_at >= " . $startDate .") 
                        AND ( lms_student_session.created_at <= " . $endDate .") 
                        AND (    lms_student_session.campaign_id = " . $this->id .") 
                        ORDER BY  lms_student.instructor_id, user.name, user.last_name, lms_student_session.begin_at";          
        return $this->executeQuery($query) ; 
    }
    
    public function getInstructorDaysHistory($startDate, $endDate, $instructor_id){
        $query = "SELECT 
                        lms_student.instructor_id, 
                        user.name,
                        user.last_name,
                        lms_module.code as module_code,
                        lms_student_session.begin_at,
                        lms_student_session.id                        
                        FROM lms_student_session
                        INNER JOIN lms_student ON lms_student_session.student_id = lms_student.user_id
                        INNER JOIN user ON  lms_student_session.student_id = user.id
                        INNER JOIN lms_module ON lms_student_session.module_id = lms_module.id
                        AND lms_student_session.campaign_id = lms_student.campaign_id
                        WHERE (  lms_student.instructor_id = " . $instructor_id .") 
                        AND (  lms_student_session.created_at >= " . $startDate .") 
                        AND ( lms_student_session.created_at <= " . $endDate .") 
                        AND (    lms_student_session.campaign_id = " . $this->id .") 
                        ORDER BY  lms_student.instructor_id, user.name, user.last_name, lms_student_session.begin_at";          
        return $this->executeQuery($query) ; 
    }
    
    private function executeQuery($query){
         $connection = Yii::$app->getDb();
        $comand= $connection->createCommand($query);
        $result = $comand->queryAll();
        return $result;  
    }
}

class CampaignQuery extends ActiveQuery {
    
}
