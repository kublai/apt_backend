<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\lms;

use Yii;
use common\models\user\User;
use yii\base\Model;

/**
 * @property Campaign $campaign
 * @property Instructor $instructor
 * @property User $user
 * 
 * @authorjl <jl@bytebox.es>
 */
class InstructorSynclog extends base\InstructorSynclog {

    const STATUS_STARTED = 1;
    const STATUS_SUCCESS = 2;
    const STATUS_FAILED = 3;
    const STATUS_WARNING = 4;

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        $this->info = '';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return array_merge(parent::rules(), [
            ['status', 'in', 'range' => array_keys(static::getStatusList())]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), [
            'statusName' => Yii::t('common/lms/instructor-synclog', 'Status')
        ]);
    }

    /**
     * @return CampaignQuery
     */
    public function getCampaign() {
        return $this->hasOne(Campaign::className(), ['id' => 'campaign_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstructor() {
        return $this->hasOne(Instructor::className(), ['user_id' => 'instructor_id', 'campaign_id' => 'campaign_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'instructor_id']);
    }

    /**
     * Returns the status text of the object.
     * 
     * @return string
     */
    public function getStatusName() {
        return static::getStatusList()[$this->status];
    }

    /**
     * 
     * @param string $message
     */
    public function addLine($message) {
        $this->info .= $message . '<br>';
    }

    /**
     * 
     * @param Model $model
     */
    public function logModelErrors(Model $model) {
        foreach ($model->getErrors() as $errors) {
            foreach ($errors as $error) {
                $this->addLine($error);
            }
        }
    }

    /**
     * Gets the status map.
     * 
     * @return array
     */
    public static function getStatusList() {
        return [
            self::STATUS_STARTED => Yii::t('common/lms/instructor-synclog', 'Started')
            , self::STATUS_SUCCESS => Yii::t('common/lms/instructor-synclog', 'Success')
            , self::STATUS_FAILED => Yii::t('common/lms/instructor-synclog', 'Failed')
            , self::STATUS_WARNING => Yii::t('common/lms/instructor-synclog', 'Warning')
        ];
    }

}
