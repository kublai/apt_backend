<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\lms;

use Yii;
use yii\base\Model;
use \common\models\user\User;


/**
 * Change Facilitators for students file: student_username, current_facilitator_id, new_facilitator_id
 *
 * @author kublai
 */
class ChangeStudentFacilitator extends Model{
   use traitFileCampaignUtils;
   // public $filePath;
   // public $campaignId;
   // private $campaign;
   
    /**
     * 
     * @return boolean
     * @throws exception
     */
    public function process(){
        $file = fopen($this->filePath, "r");
        $this->campaign = Campaign::find()->where(['id' => $this->campaignId])->one();
        if (!$this->campaign->isActive()){
            addErrorLine("This campaign it's not active. data cannot be modified");
            return false;
        }
        $trans = Yii::$app->db->beginTransaction();
        try{
            $c = 0;
            while ($dataline = fgetcsv($file, 1000)){
                if (count($dataline)>1){ //checking for empty lines in file
                    $this->processLine($dataline,++$c);
                }
            }
            if ($this->hasErrors()){
                $trans->rollBack();
                return 0;
            }
            $trans->commit();
            return $c;
        } catch (\Exception $ex) {
            $trans->rollBack();
            throw $ex;
        }

    }
    
    private function processLine($dataline,$counter){
        list ($userName, $currentFacilitadorId, $newfacilitatorId) = $dataline;
        $user = User::find()->where(['username' => $userName])->one();
        $currentInstructor = User::find()->where(['id' => $currentFacilitadorId])->one();
        $newInstructor = User::find()->where(['id' => $newfacilitatorId])->one();
        $errorFlag = FALSE;
        if ($user === NULL){
           $this->addErrorLine("User with username = " . $userName .
                   " does not exist.", $counter);
           $errorFlag = TRUE;
        }  
        
        if ($currentInstructor === NULL){
           $this->addErrorLine("Currect facilitador with id = " .
                   $currentFacilitadorId . " does not exist.", $counter);
           $errorFlag = TRUE;
        } 
        
        if ($newInstructor === NULL){
           $this->addErrorLine("New facilitador with id = " .
                   $newfacilitatorId . " does not exist.", $counter);
           $errorFlag = TRUE;
        } 
        
        if ($errorFlag){
            return;
        }
        
        $student = Student::find()->where(['user_id' => $user->id, 'campaign_id' => $this->campaign->id])->one();
        if ($student === NULL){ //se ha encontrado el estudiante y pertenece a la campaña, además no es facilitador
             $this->addErrorLine("User with username = " . $userName . 
                     " does not belongs to campaing '" . $this->campaign->name .
                     "' (Campaign Id = ". $this->campaignId . ")", $counter);
             return;
        }
        //verify that current facilitator is OK
        if ($student->instructor_id !== $currentInstructor->id){
            $this->addErrorLine("Current Instructor (id=" .$currentFacilitadorId.") it's  not valid.", $counter);
            return;
        }
        
        //verify that new facilitator is valid for this campaign
        $newFac = $this->campaign->getInstructors()->where(['id' => $newfacilitatorId])->one();
        if ($newFac === NULL){
            $this->addErrorLine("NEW Instructor (id=" .$newfacilitatorId.") it's  not valid in this campaign", $counter);
            return;
        }
                
        //change facilitator
        $student->instructor_id = $newfacilitatorId;
        $student->save();         
    }
    

    
}
