<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\lms;

use Yii;
use yii\base\Model,
    yii\base\InvalidConfigException;
use common\models\user\User,
    common\models\user\Role;
use common\models\geo\Country,
    common\models\geo\Zone,
    common\models\geo\Region,
    common\models\geo\Place;

/**
 * Class to handle the process of integrate a file with students.
 * 
 * @author <jl@bytebox.es>
 */
class InstructorIntegration extends Model {

    /**
     *
     * @var string
     */
    public $path;

    /**
     *
     * @var Instructor
     */
    protected $instructor;

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        if ($this->instructor === null) {
            throw new InvalidConfigException('Instructor must be provided on initialization');
        }
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['path', 'required']
            , ['path', 'fileExist']
        ];
    }

    /**
     * 
     * @return boolean
     */
    public function fileExist() {
        if (file_exists($this->path) === false) {
            $this->addError('path', Yii::t('common/lms/integration', 'Integration file doesn\'t exist'));
            return false;
        } else {
            return true;
        }
    }

    /**
     * 
     * @param Instructor $i
     */
    public function setInstructor(Instructor $i) {
        $this->instructor = $i;
    }

    /**
     * 
     * @return Instructor
     */
    public function getInstructor() {
        return $this->instructor;
    }

    /**
     * Runs the integration process.
     * 
     * @return boolean
     * @throws \Exception
     */
    public function process() {
        if ($this->validate() === false) {
            return false;
        }
        $handle = fopen($this->path, 'r');
        $trans = Yii::$app->db->beginTransaction();
        try {
            $i = 0;
            while ($data = fgetcsv($handle)) {
                $this->processUser($data, ++$i);
            }
            if ($this->hasErrors()) {
                $trans->rollBack();
                return false;
            }
            $trans->commit();
            return true;
        } catch (\Exception $e) {
            $trans->rollBack();
            throw $e;
        }
    }

    /**
     * Processes a single data entry by creating a user with it.
     * 
     * @param array $data
     * @param int $i
     * @return boolean
     */
    public function processUser(array $data, $i = null) {
        $name = !empty($data[0]) ? trim($data[0]) : null;
        $lastName = !empty($data[1]) ? trim($data[1]) : null;
        $gender = !empty($data[2]) ? trim($data[2]) : null;
        $cardId = !empty($data[3]) ? trim($data[3]) : null;
        $phone = !empty($data[4]) ? trim($data[4]) : null;
        $countryName = !empty($data[5]) ? trim($data[5]) : null;
        $zoneName = !empty($data[6]) ? trim($data[6]) : null;
        $regionName = !empty($data[7]) ? trim($data[7]) : null;
        $placeName = !empty($data[8]) ? trim($data[8]) : null;
        $address = !empty($data[9]) ? trim($data[9]) : null;
        $longitude = !empty($data[10]) ? trim($data[10]) : null;
        $latitude = !empty($data[11]) ? trim($data[11]) : null;

        $country = Country::find()->where(['name' => $countryName])->one();
        if ($country === null) {
            $this->addLineError($i, Yii::t('common/lms/integration', 'Country with name [{name}] does not exist', [
                        'name' => $countryName
            ]));
            return false;
        }
        $zone = Zone::find()->where(['name' => $zoneName])->one();
         if ($zone === null) {
            $this->addLineError($i, Yii::t('common/lms/integration', 'Zone with name [{name}] does not exist in country [{containerName}]', [
                        'name' => $zoneName
                        , 'containerName' => $countryName
            ]));
            return false;
        }
        $region = Region::find()->where(['name' => $regionName, 'zone_id' => $zone->id])->one();
        if ($region === null) {
            $this->addLineError($i, Yii::t('common/lms/integration', 'Region with name [{name}] does not exist in zone [{containerName}]', [
                        'name' => $regionName
                        , 'containerName' => $zoneName
            ]));
            return false;
        }
	if ($placeName != NULL){
        	$place = Place::find()->where(['name' => $placeName, 'region_id' => $region->id])->one();
        	if ($place === null) {
            		$this->addLineError($i, Yii::t('common/lms/integration', 'Place with name [{name}] does not exist in region [{containerName}]', [
                        	'name' => $placeName
                        	, 'containerName' => $regionName
            		]));
            	return false;
       		}	
	}else{
		$place= NULL;
	}
        $user = new User();
        $user->username = $cardId;
        $user->status = User::STATUS_ACTIVE;
        $user->role = Role::STUDENT;
        $user->operator_id = $this->instructor->operator_id;
        $user->name = mb_convert_case($name, MB_CASE_TITLE, "UTF-8");
        $user->last_name = mb_convert_case($lastName, MB_CASE_TITLE, "UTF-8");
        $user->gender = (int) $gender;
        $user->email = null;
        $user->phone = $phone;
        $user->generateAuthKey();
        $user->setPassword(uniqid($cardId));
        $user->country_id = $country->id;
        $user->zone_id = ($zone!=NULL)?$zone->id:NULL;
        $user->region_id = ($region!=NULL)?$region->id:NULL;
        $user->place_id = ($place!=NULL)?$place->id:NULL;
        $user->address = $address;
        $user->longitude = $longitude;
        $user->latitude = $latitude;
        if ($user->save() === false) {
            $this->addModelErrors($i, $user);
            return false;
        }
        $student = new Student();
        $student->campaign_id = $this->instructor->campaign_id;
        $student->user_id = $user->id;
        $student->instructor_id = $this->instructor->user_id;
        if ($student->save() === false) {
            $this->addModelErrors($i, $student);
            return false;
        }
        return true;
    }

    /**
     * Adds an error including the line where it is generated.
     * 
     * @param int $i
     * @param string $message
     */
    protected function addLineError($i, $message) {
        $this->addError('process', Yii::t('common/lms/integration', "Error in line [{n}]:", [
                    'n' => $i
                ]) . " $message");
    }

    /**
     * Wraps the errors of the provided model to this model.
     * 
     * @param int $i
     * @param Model $model
     */
    protected function addModelErrors($i, Model $model) {
        foreach ($model->getErrors() as $errors) {
            foreach ($errors as $error) {
                $this->addLineError($i, $error);
            }
        }
    }

}
