<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace common\models\lms;

use common\models\user\User;

/**
 * 
 * @property User $user The user of this student.
 * @property User $instructor The instructor of this student in this campaign.
 * @property User $operator The operator of this student in this campaign.
 * @property StudentSession[] $sessions The sessions done by the student.
 * 

 */
class Student extends base\Student {

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getSessions() {
        return $this->hasMany(StudentSession::className(), [
                    'student_id' => 'user_id',
                    'campaign_id' => 'campaign_id'
        ]);
    }
    
    public function getModules(){
        return $this->hasMany(StudentModule::className(), [
                    'student_id' => 'user_id',
                    'campaign_id' => 'campaign_id'
        ]);
    }

    /**
     * Wrapper for user_id.
     * 
     * @return integer
     */
    public function getId() {
        return $this->user_id;
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getOperator() {
        return User::find()
                        ->innerJoin('lms_operator lo', 'lo.operator_id = user.id')
                        ->innerJoin('lms_instructor li', 'li.operator_id = lo.operator_id AND li.campaign_id = lo.campaign_id')
                        ->innerJoin('lms_student ls', 'ls.instructor_id = li.user_id AND li.campaign_id = lo.campaign_id')
                        ->andWhere(['and'
                            , ['lo.campaign_id' => $this->campaign_id]
                            , ['ls.user_id' => $this->user_id]
        ]);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getInstructor() {
        return $this->hasOne(User::className(), ['id' => 'instructor_id']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getCampaign() {
        return $this->hasOne(Campaign::className(), ['id' => 'campaign_id']);
    }

}
