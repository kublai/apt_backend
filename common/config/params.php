<?php

return [
    'platformName' => 'Backend Apptitude'
    , 'adminEmail' => 'info@bytebox.es'
    , 'supportEmail' => 'kublai@outlook.com'
    , 'user.passwordResetTokenExpire' => 3600
    , 'MAPBOX_ACCESS_TOKEN' => 'pk.eyJ1IjoiYnl0ZWJveCIsImEiOiJjaWt4cjFpankwMHh3dXlrc3hqejZqN3A2In0.iC8-5oF1J6sgPBnuJ2kMpw'
];
