<?php

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'es-ES',
    'charset' => 'utf-8',
    'aliases' => [
        '@custom' => dirname(dirname(__DIR__)) . '/custom',
        '@api' => dirname(dirname(__DIR__)) . '/api'
    ],
    'name' => 'Servidor Backend Apptitude',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/i18n',
                    'sourceLanguage' => 'en-US',
                ],
            ],
        ],
        'security' => [
            'class' => 'common\base\Security'
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.zoho.com',
                'username' => 'info@bytebox.es',
                'password' => 'Tk6NvayQc6Yph',
                'port' => ' 587',
                'encryption' => 'tls',
            ],
        ],
    ],
];
