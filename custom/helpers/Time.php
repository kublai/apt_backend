<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace custom\helpers;

class Time {

    const FORMAT_HOUR = 1;
    const FORMAT_MINUTE = 2;
    const FORMAT_SECOND = 3;

    public static function getTimestring($seconds, $format = self::FORMAT_HOUR) {
        $s = '';
        switch ($format) {
            case self::FORMAT_HOUR:
                $aux = $seconds % 3600;
                $s .= str_pad(($seconds - $aux) / 3600, 2, '0', STR_PAD_LEFT) . ':';
                $seconds = $aux;
            case self::FORMAT_MINUTE:
                $aux = $seconds % 60;
                $s .= str_pad(($seconds - $aux) / 60, 2, '0', STR_PAD_LEFT) . ':';
                $seconds = $aux;
            default:
                $s .= str_pad($seconds, 2, '0', STR_PAD_LEFT);
        }
        return $s;
    }

}
