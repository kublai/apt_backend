<?php

/**

 * @copyright  jl
 * @version     1.0
 */

namespace custom\helpers;

use yii\helpers\Html as BaseHtml;

class Html extends BaseHtml {

    public static function redactor($text) {
        return static::tag('div', $text, ['class' => 'redactor-content']);
    }

}
