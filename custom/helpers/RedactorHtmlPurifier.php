<?php

/**

 * @copyright  jl
 * @version     1.0
 */

namespace custom\helpers;

class RedactorHtmlPurifier extends \yii\helpers\HtmlPurifier {

    public static function configure($config) {
        $config->set('HTML.AllowedAttributes', ['img.src', '*.style']);
    }

}
