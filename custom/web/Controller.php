<?php

/**

 * @copyright  jl
 * @version     1.0
 */

namespace custom\web;

use yii\web\Controller as YiiController;

/**
 * Custom controller class to extend yii web controller with custom behaviors.
 * 
 * @author Jose Lorente <j>
 */
class Controller extends YiiController {
    
}
