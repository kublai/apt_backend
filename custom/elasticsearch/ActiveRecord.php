<?php

/**

 * @copyright  jl
 * @version     1.0
 */

namespace custom\elasticsearch;

use yii\elasticsearch\ActiveRecord as BaseActiveRecord;
use custom\i18n\FileNameTrait;
use Yii;

/**
 * Custom implementation of elasticsearch ActiveRecord
 *

 */
class ActiveRecord extends BaseActiveRecord {

    use FileNameTrait;

    public function getId() {
        return $this->getPrimaryKey();
    }

    public function _array($attribute, $params) {
        $r = true;
        if (!is_array($this->$attribute)) {
            if (empty($this->$attribute)) {
                $this->$attribute = [];
            } else {
                $this->addError(
                        $this, $attribute, isset($params['message']) ? $params['message'] : Yii::t(
                                        'validator', '{attribute} must be an array', [
                                    'attribute' => $attribute
                                ])
                );
                $r = false;
            }
        } elseif (isset($params['class'])) {
            foreach ($this->$attribute as $el) {
                if (is_a($el, $params['class'])) {
                    $this->addError(
                            $this, $attribute, isset($params['message']) ? $params['message'] : Yii::t(
                                            'validator', 'Elements in {attribute} must be of class {class}.', [
                                        'attribute' => $attribute,
                                        'class' => $params['class']
                                    ])
                    );
                    $r = false;
                }
            }
        }
        return $r;
    }

    public function _stringArray($attribute, $params) {
        $r = $this->_array($attribute, $params);
        if ($r) {
            foreach ($this->$attribute as $param) {
                if (is_numeric($param) === true || is_string($param) === false) {
                    $this->addError(
                            $this, $attribute, isset($params['message']) ? $params['message'] : Yii::t('validator', '{element} must be a string')
                    );
                    $r = false;
                }
            }
        }
        return $r;
    }
}
