<?php

/**

 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace custom\db\exceptions;

use yii\base\Model;
use yii\helpers\Json;

/**
 * Exception to be thrown on failed saving operations.
 * 

 */
class SaveException extends \yii\base\Exception {
    
    public function __construct(Model $model, \Exception $previous = null) {
        $message = 'Unable to save ' . get_class($model) . '. Errors: ['.Json::encode($model->getErrors()).'].';
        parent::__construct($message, 1100, $previous);
    }
    
    /**
     * @inheritdoc
     */
    public function getName() {
        return 'SaveException';
    }
}