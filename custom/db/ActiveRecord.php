<?php

/**

 * @copyright  jl
 * @version     1.0
 */

namespace custom\db;

use yii\db\ActiveRecord as BaseActiveRecord;
use yii\behaviors\TimestampBehavior;
use custom\base\traits\ClassNameTrait;
use yii\db\Query;
use yii\behaviors\BlameableBehavior;

/**
 * Custom implementation of ActiveRecord
 *

 */
class ActiveRecord extends BaseActiveRecord {

    use ClassNameTrait;

    public function behaviors() {
        return array_merge(parent::behaviors(), [
            TimestampBehavior::className(),
            BlameableBehavior::className()
        ]);
    }

    public function isLinked($name, $model, $extraColumns = []) {
        $relation = $this->getRelation($name);

        if ($relation->via !== null) {
            if ($this->getIsNewRecord() || $model->getIsNewRecord()) {
                throw new InvalidCallException('Unable to link models: both models must NOT be newly created.');
            }
            if (is_array($relation->via)) {
                /* @var $viaRelation ActiveQuery */
                list($viaName, $viaRelation) = $relation->via;
                $viaClass = $viaRelation->modelClass;
                // unset $viaName so that it can be reloaded to reflect the change
                unset($this->_related[$viaName]);
            } else {
                $viaRelation = $relation->via;
                $viaTable = reset($relation->via->from);
            }
            $columns = [];
            foreach ($viaRelation->link as $a => $b) {
                $columns[$a] = $this->$b;
            }
            foreach ($relation->link as $a => $b) {
                $columns[$b] = $model->$a;
            }
            foreach ($extraColumns as $k => $v) {
                $columns[$k] = $v;
            }
            if (is_array($relation->via)) {
                /* @var $viaClass ActiveRecordInterface */
                /* @var $record ActiveRecordInterface */
                return $viaClass::find()->where($columns)->exists();
            } else {
                /* @var $viaTable string */

                return (new Query([
                    'from' => [$viaTable],
                    'where' => $columns
                        ]))->exists();
            }
        }
    }

}
