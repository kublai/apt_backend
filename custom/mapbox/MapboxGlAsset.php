<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace custom\mapbox;

use yii\web\AssetBundle;

/**
 * 
 * @authorjl <jl@bytebox.es>
 */
class MapboxGlAsset extends AssetBundle {

    /**
     *
     * @var string 
     */
    public $version = 'v0.14.2';

    /**
     * @inheritdoc 
     */
    public function init() {
        $this->js[] = "//api.tiles.mapbox.com/mapbox-gl-js/{$this->version}/mapbox-gl.js";
        $this->css[] = "//api.tiles.mapbox.com/mapbox-gl-js/{$this->version}/mapbox-gl.css";
        parent::init();
    }

}
