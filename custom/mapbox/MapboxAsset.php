<?php

/**
 * @author     jl <jl@bytebox.es>
 * @copyright   Proyecto graduación <http://fundacioncapital.org/>
 * @version     1.0
 */

namespace custom\mapbox;

use yii\web\AssetBundle;

/**
 * 
 * @authorjl <jl@bytebox.es>
 */
class MapboxAsset extends AssetBundle {

    /**
     *
     * @var string 
     */
    public $version = 'v2.3.0';

    /**
     * @inheritdoc 
     */
    public function init() {
        $this->js[] = "//api.mapbox.com/mapbox.js/{$this->version}/mapbox.js";
        $this->css[] = "//api.mapbox.com/mapbox.js/{$this->version}/mapbox.css";
        parent::init();
    }

}
