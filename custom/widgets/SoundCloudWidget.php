<?php

/**

 * @copyright   Gr11 en 11 <http://gr11en11.org>
 * @version     1.0
 */

namespace custom\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class SoundCloudWidget extends Widget {

    /**
     * @var array options that will be passed to [[Html::tag()]]
     */
    public $iframeOptions;
    
    /**
     *
     * @var string Url obtained from soundcloud 
     */
    public $src;

    /**
     * @inheritdoc
     */
    public function init() {
        if ($this->src === null) {
            throw new InvalidConfigException('SoundCloudWidget::src must be set');
        }
        $this->iframeOptions = array_merge([
            'width' => '100%',
            'height' => '166',
            'scrolling' => 'no',
            'frameborder' => 'no'
                ], $this->iframeOptions);
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run() {
        $options = $this->iframeOptions;
        $options['src'] = $this->src;
        echo Html::tag('iframe', '', $options);
    }

}