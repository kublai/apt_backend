<?php

/**

 * @copyright  jl
 * @version     1.0
 */

namespace custom\console;

use yii\console\Controller as BaseController;
use yii\helpers\Console;

class Controller extends BaseController {

    public function output($string) {
        if ($this->isColorEnabled()) {
            $args = func_get_args();
            array_shift($args);
            $string = Console::ansiFormat($string, $args);
        }
        $b = Console::stdout($string);
        echo PHP_EOL;
        return $b;
    }

}
