<?php

/**

 * @copyright  jl
 * @version     1.0
 */

namespace custom\base;

use yii\base\Model as YiiModel;
use custom\base\traits\ClassNameTrait;

class Model extends YiiModel {

    use ClassNameTrait;


}
