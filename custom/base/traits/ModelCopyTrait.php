<?php

/**

 * @copyright  jl
 * @version     1.0
 */

namespace custom\base\traits;

use yii\base\UnknownMethodException;

trait ModelCopyTrait {

    private $_original;

    /**
     * @param string $name
     * @param array $params
     */
    public function __call($name, $params) {
        try {
            return parent::__call($name, $params);
        } catch (UnknownMethodException $e) {
            return call_user_func_array([$this->_original(), $name], $params);
        }
    }

    private function _original() {
        if ($this->_original === null || ($this->_original->id === null && empty($this->id) === false)) {
            $class = self::original();
            $original = isset($this->id) && $this->id !== null ? $class::findOne($this->id) : new $class();
            $this->_original = $original;
        }
        return $this->_original;
    }

    public function attributes() {
        return array_merge(parent::attributes(), $this->_original()->attributes());
    }

    public function rules() {
        return array_merge(parent::rules(), $this->_original()->rules());
    }

    /**
     * @return string Full qualified class name from which the current class is copied.
     */
    public static abstract function original();
}
