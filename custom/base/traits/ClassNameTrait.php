<?php

/**

 * @copyright  jl
 * @version     1.0
 */
namespace custom\base\traits;

trait ClassNameTrait {

    public static function underscoredName() {
        return strtolower(str_replace('\\', '_', self::className()));
    }

    public static function i18nFileName() {
        return 'models/' . static::underscoredName();
    }
}
