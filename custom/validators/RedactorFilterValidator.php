<?php

/**

 * @copyright  jl
 * @version     1.0
 */

namespace custom\validators;

use yii\validators\FilterValidator;
use custom\helpers\RedactorHtmlPurifier;

/**
 * Filters the content coming from the redactor widget.
 *

 */
class RedactorFilterValidator extends FilterValidator {

    public $allowedTags = '<code><span><div><label><a><br><p><b><i><del><strike><u><img><video><audio><iframe><object><embed><param><blockquote><mark><cite><small><ul><ol><li><hr><dl><dt><dd><sup><sub><big><pre><code><figure><figcaption><strong><em><table><tr><td><th><tbody><thead><tfoot><h1><h2><h3><h4><h5><h6>';
    
    /**
     * @inheritdoc
     */
    public function init() {
        $this->filter = function($value) {
            $value = strip_tags($value, $this->allowedTags);
            return RedactorHtmlPurifier::process($value);
        };

        parent::init();
    }

}
