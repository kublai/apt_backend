<?php

/**

 * @copyright   Gr11 en 11 <http://gr11en11.org>
 * @version     1.0
 */

namespace custom\validators;

use yii\validators\RegularExpressionValidator;

/**
 * ColorValidator validates that the attribute value matches a valid hexadecimal color.
 * You may invert the validation logic with help of the {@link not} property (available since 1.1.5).
 *

 */
class ColorValidator extends RegularExpressionValidator {

    /**
     * Pattern for color validation
     * 
     * @var string 
     */
    public $pattern = '/^#[a-f0-9]{6}$/';

}
