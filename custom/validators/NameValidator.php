<?php

/**

 * @copyright  jl
 * @version     1.0
 */

namespace custom\validators;

use yii\validators\RegularExpressionValidator;
use Yii;

/**
 * NameValidator validates that the attribute value matches to the specified 
 * pattern regular expression.
 *

 */
class NameValidator extends RegularExpressionValidator {

    /**
     * @inheritdoc
     */
    public function init() {
        //$this->pattern = '/^[a-zA-ZÀ-ÖØ-öø-ÿ]+(([.\'ªº]{1}[\s]?|[\s\-]{1})[a-zA-ZÀ-ÖØ-öø-ÿ]+)*[.ªº]?$/u';
        $this->pattern = '/^[a-zA-ZÀ-ÖØ-öø-ÿ.\'ªº ]*/u';

        if ($this->message === null) {
            $this->message = Yii::t('validator', "A name can consist of Latin alphabetic characters. It can contain points, apostrophes ['] and ordinals [ºª] as terminators of words, and blank spaces [ ].");
        }
        parent::init();
    }
}