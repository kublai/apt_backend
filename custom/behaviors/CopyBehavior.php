<?php

/**

 * @copyright  jl
 * @version     1.0
 */

namespace custom\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\base\Model;
use yii\base\InvalidConfigException;
use yii\db\Exception;
use yii\helpers\Json;

class CopyBehavior extends Behavior {

    public $copyTo;
    public $with;
    public $options;

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        if (empty($this->copyTo)) {
            throw new InvalidConfigException('attribute copyTo must be provided');
        }
    }

    public function events() {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'insertCopy',
            ActiveRecord::EVENT_AFTER_UPDATE => 'updateCopy',
            ActiveRecord::EVENT_AFTER_DELETE => 'deleteCopy'
        ];
    }

    public function insertCopy($event) {
        $class = $this->copyTo;
        $model = new $class();
        $this->populate($model);
        if (empty($this->with) === false) {
            $this->processWith($model);
        }
        if ($model->save() === false) {
            throw new Exception('Unable to insert the copy of ' . $this->ownerClass() . '. Errors: [' . Json::encode($model->getErrors()) . ']');
        }
        return true;
    }

    public function updateCopy($event) {
        $class = $this->copyTo;
        $model = $class::get($this->owner->primaryKey, $this->getOptionsParam());
        if ($model === null) {
            $model = new $class();
        }
        $this->populate($model);
        if (empty($this->with) === false) {
            $this->processWith($model);
        }
        if ($model->save() === false) {
            throw new Exception('Unable to update the copy of ' . $this->ownerClass() . '. Errors: [' . Json::encode($model->getErrors()) . ']');
        }
        return true;
    }

    public function deleteCopy($event) {
        $class = $this->copyTo;
        $model = $class::get($this->owner->primaryKey, $this->getOptionsParam());
        if ($model->delete() === false) {
            throw new Exception('Unable to delete the copy of ' . $this->ownerClass() . '. Errors: [' . Json::encode($model->getErrors()) . ']');
        }
    }

    protected function populate(Model $model) {
        $model->attributes = $this->owner->attributes;
    }

    protected function processWith(Model $model) {
        if (is_array($this->with)) {
            foreach ($this->with as $with) {
                $this->_processWith($model, $with);
            }
        } else {
            $this->_processWith($model, $this->with);
        }
    }

    protected function _processWith(Model $model, array $with) {
        list($attribute, $ownerAttribute, $ownerProperty) = $with;

        $collection = [];
        unset($this->owner->$ownerAttribute);
        foreach ($this->owner->$ownerAttribute as $object) {
            $collection[] = $object->$ownerProperty;
        }
        $model->$attribute = $collection;
    }

    public function ownerClass() {
        return get_class($this->owner);
    }

    public function getOptionsParam() {
        if ($this->options !== null && is_callable($this->options)) {
            return call_user_func($this->options);
        } elseif (is_array($this->options)) {
            return $this->options;
        } else {
            return [];
        }
    }
}
